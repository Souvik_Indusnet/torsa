import mongoose from 'mongoose';

const userSchema = mongoose.Schema(
  {
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "User",
    },
    
  },
  { timestamps: true }
);

const UserSignList = mongoose.model('Usersignlist', userSchema);

export default UserSignList;
