import mongoose from "mongoose";

const machineSchema = mongoose.Schema(
  {
    equipment: {
      type: String,
      required: true,
    },
    model: [
      {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Machine",
      },
    ],

    active: {
      type: Boolean,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

const MachineName = mongoose.model("MachineName", machineSchema);

export default MachineName;
