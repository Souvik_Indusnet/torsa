import mongoose from 'mongoose';

const notificatioSchema = new mongoose.Schema(
  {
    order_id: {
      type: mongoose.Schema.ObjectId,
      unique: false,
      ref: 'Payment',
    },
    generateCount: {
      type: Number,
      default: 1,
    },

    user: {
      type: mongoose.Schema.ObjectId,
      unique: false,
      ref: 'User',
    },
    data: {
      type: Object,
      required: true,
    },
    isRead: {
      type: Boolean,
      default: false,
    },
    type: {
      type: String,
      required: true,
    },
    url: {
      type: String,
    },
    isCreatedByAdmin: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

const Notification = mongoose.model(
  'notificationCollection',
  notificatioSchema
);
export default Notification;
