import mogoose from 'mongoose';
const { Schema } = mogoose;

const PaymentSchema = mogoose.Schema(
  {
    // order_id: {
    //   type: mogoose.Schema.Types.ObjectId,
    //   ref: 'Order',
    // },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    part: {
      type: Schema.Types.ObjectId,
      ref: 'Part',
    },
    qty: {
      type: Number,
      default: 1,
    },
    Mode_Of_Payment: {
      type: String,
      enum: ['Cash', 'Online'],
      default: 'Cash',
    },
    Payment_Status: {
      type: String,
      enum: ['Due', 'Paid'],
      default: 'Due',
    },
    Installment: {
      type: String,
      enum: ['1', '2'],
      default: '1',
    },
    Type_Of_Payment: {
      type: String,
      enum: ['Partial', 'Full'],
      default: 'Full',
    },
    Partial_Amount_Paid_in_percentage: {
      type: String,
      default: '0',
    },
    Partial_Remaining_Amount_in_percentage: {
      type: String,
      default: '100',
    },
    Partial_Amount_Paid: {
      type: String,
      default: '0',
    },
    Partial_Remaining_Amount: {
      type: String,
      default: '0',
    },
    Partial_Due: {
      type: String,
      default: '0',
    },
    Partial_Date: {
      type: String,
    },
    isApproved: {
      type: String,
      enum: ['pending', 'approved', 'rejected'],
      default: 'pending',
    },
    reason:{
      type: String,
    },
    orderStatus: {
      type: String,
      enum: ['Placed', 'Ready_to_ship', 'Shipped', 'Delivered','Cancelled'],
      default: 'Placed',
    },
    isShippedDate:{
    type:Date,
    default:null
    },
    isDeliveredDate:{
      type:Date,
      default:null
    },
    isCancelledDate:{
      type:Date,
      default:null
    },
    for_Enquery: {
      type: Boolean,
      default: true,
    },
    Delivered: {
      type: Boolean,
      default: false,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

const Payment = mogoose.model('Payment', PaymentSchema);

export default Payment;
