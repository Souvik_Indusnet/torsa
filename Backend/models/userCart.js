import mongoose from 'mongoose';
const userSchema = mongoose.Schema(
  {
    userID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    qty: {
      type: Number,
      required: true,
      default: 1,
    },
    part: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      unique: false,
      ref: 'Part',
    },
  },
  { timestamps: true }
);
// userSchema.createIndex({ userID: 1 });

const Usercart = mongoose.model('usercart', userSchema);
export default Usercart;
