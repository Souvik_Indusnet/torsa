import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';

const userSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    image: {
      type: String,
      required: false,
      default: '1f3b6da5b8045892871111887f84efa3.jpeg',
    },
    contactNo: {
      type: String,
      required: false,
    },
    buisnessManager: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Admin',
    },

    password: {
      type: String,
      required: true,
    },
    active: {
      type: Boolean,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
    otp: {
      type: String,
    },
    password_change_req: {
      type: Boolean,
      default: false,
    },
    address: {
      Name: String,
      Locality: String,
      Town: String,
      Address: String,
      City: String,
      District: String,
      State: String,
      postalCode: String,
    },
    BillingAdress: {
      Name: String,
      Locality: String,
      Town: String,
      Address: String,
      City: String,
      District: String,
      State: String,
      postalCode: String,
    },
    token: {
      type: String,
    },
  },
  { timestamps: true }
);

userSchema.methods.matchPassword = async function (enterPassword) {
  return await bcrypt.compare(enterPassword, this.password);
};

userSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    next();
  }

  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

const User = mongoose.model('User', userSchema);

export default User;
