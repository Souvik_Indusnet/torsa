import mongoose from 'mongoose';
// import SubCategory from "./subcategoryModel";

const partSchema = mongoose.Schema(
  {
    partName: {
      type: String,
      required: true,
    },
    image: [{ type: String }],
    category: {
      type: mongoose.Schema.Types.ObjectId,

      ref: 'Category',
    },
    subcategory: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'SubCategory',
    },
    description: {
      type: String,
    },

    active: {
      type: Boolean,
      default: true,
    },
    amount: {
      type: Number,
    },
    inventoryStock: { type: Number },

    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

const Part = mongoose.model('Part', partSchema);

export default Part;
