import mongoose from 'mongoose';

// const pageNameSchema = new mongoose.Schema({
//   pageName: {
//     type: String,
//     required: true,
//   },
// });
// const PageName = mongoose.model('PageName', pageNameSchema);

const pageSchema = new mongoose.Schema(
  {
    forPageName: {
      type: String,
    },
    content: { type: String },
    status: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: true,
  }
);

// pageSchema.index({ forPageName: 1 }, { unique: true });

const Page = mongoose.model('Page', pageSchema);

export { 
//  PageName,
   Page };
