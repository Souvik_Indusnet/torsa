import mongoose from 'mongoose';
const userSchema = mongoose.Schema(
  {
    userID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    address: {
      Locality: String,
      Town: String,
      Address: String,
      City: String,
      District: String,
      State: String,
      postalCode: String,
    },
    BillingAdress: {
      Locality: String,
      Town: String,
      Address: String,
      City: String,
      District: String,
      State: String,
      postalCode: String,
    },
  },
  { timestamps: true }
);

const UserAddress = mongoose.model('userAddress', userSchema);
export default UserAddress;
