import mogoose from 'mongoose';
const { Schema } = mogoose;

const TransactionDetailSchema = mogoose.Schema(
  {
    OrderId: {
      type: Schema.Types.ObjectId,
      ref: 'Payment',
    },
    TransactionDetails: {
      type: Object,
    },
  },
  { timestamps: true }
);

const TransactionDetail = mogoose.model('TransactionDetail', TransactionDetailSchema);

export default TransactionDetail;
