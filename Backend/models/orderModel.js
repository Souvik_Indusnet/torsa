import mongoose from "mongoose";

const orderSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
    orderItems: [
      {
        qty: { type: Number, required: true },
        // price: { type: Number, required: fals },
        product: {
          type: mongoose.Schema.Types.ObjectId,
          required: true,
          ref: "Part",
        },
        shippingAddress: {
          Locality: String,
          Town: String,
          Address: String,
          City: String,
          District: String,
          State: String,
          postalCode: String,
        },
        paymentMethod: {
          type: String,
          required: true,
        },
        paymentResult: {
          id: { type: String },
          status: { type: String },
          update_time: { type: String },
          email_address: { type: String },
        },
        taxPrice: {
          type: Number,
          required: true,
          default: 0.0,
        },
        shippingPrice: {
          type: Number,
          required: true,
          default: 0.0,
        },
        totalPrice: {
          type: Number,
          required: true,
          default: 0.0,
        },
        isPaid: {
          type: Boolean,
          required: true,
          default: false,
        },
        paidAt: {
          type: Date,
        },
        isDelivered: {
          type: Boolean,
          required: true,
          default: false,
        },
        deliveredAt: {
          type: Date,
        },
        createdDate: {
          type: Date,
          default: Date.now,
        },
        updatedDate: {
          type: Date,
          default: Date.now,
        },
        orderNumber: {
          type: String,
          required: true,
        },
        isShipped: {
          type: Boolean,
          default: false,
        },
        isTransit: { type: Boolean, default: false },
        isCancelled: { type: Boolean, default: false },
        isPlaced: { type: Boolean, default: false },
      },
    ],
  },
  {
    timestamps: { createdAt: "createdDate", updatedAt: "updatedDate" },
  }
);

const Order = mongoose.model("Order", orderSchema);

export default Order;
