import mongoose from 'mongoose';

const subCategorySchema = mongoose.Schema({
  category: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Category',
  },
  name: {
    type: String,
    required: false,
  },
  active: {
    type: Boolean,
    default: true,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: String,
    default: new Date().toDateString(),
  },
});

const SubCategory = mongoose.model('SubCategory', subCategorySchema);

export default SubCategory;
