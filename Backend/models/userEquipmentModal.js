import mongoose from 'mongoose';
const userSchema = mongoose.Schema(
  {
    userID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    equipmentModal: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'SubCategory',
    },
  },
  { timestamps: true }
);
// userSchema.createIndex({ userID: 1 });

const UserEquipmentModal = mongoose.model('userequipmentmodal', userSchema);
export default UserEquipmentModal;
