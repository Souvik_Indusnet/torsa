import mongoose from "mongoose";

const machineSchema = mongoose.Schema(
  {
    machineName: {
      type: String,
      required: true,
    },
    active: {
      type: Boolean,
      default: true,
    },
    equipment: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "MachineName",
    },

    parts: [
      {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Part",
      },
    ],
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

const Machine = mongoose.model("Machine", machineSchema);

export default Machine;
