import mongoose from 'mongoose';
const userSchema = mongoose.Schema(
  {
    userID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    equipment: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Category',
    },
  },
  { timestamps: true }
);

// userSchema.createIndex({ userID: 1 });
const UserEquipment = mongoose.model('userEquipment', userSchema);
export default UserEquipment;
