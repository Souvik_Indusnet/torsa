import mongoose from 'mongoose'
import dotenv from 'dotenv'
import color from 'colors'
import connectDB from './config/db.js'

//data
import user from './data/user.js'
import machine from './data/machine.js'
import admin from './data/admin.js'
import category from './data/category.js'
import part from './data/part.js'

//Model
import Admin from './models/Admin.js'
import Category from './models/categoryModel.js'
import User from './models/userModel.js'
import Machine from './models/machineModel.js'
import Part from './models/partModel.js'


dotenv.config()
connectDB()

const importData = async () => {
    try {
        await Admin.deleteMany()
        await Machine.deleteMany()
        await User.deleteMany()
        await Category.deleteMany()
        await Part.deleteMany()
        await Category.insertMany(category)
        const createMachine = await Machine.insertMany(machine)
        await Part.insertMany(part)
        const createdAdmin = await Admin.insertMany(admin)
        const sampleUser = user.map(u => {
            return { ...u, machines: createMachine[0]._id, buisnessManager: createdAdmin[1]._id }
        })
        await User.insertMany(sampleUser)
        console.log(`Data Imported!`.green.inverse)
        process.exit()
    } catch (error) {
        console.error(`${error}`.red.inverse);
        process.exit(1)
    }
}

const destroyData = async () => {
    try {
        await Admin.deleteMany()
        await Machine.deleteMany()
        await User.deleteMany()
        await Category.deleteMany()


        console.log(`Data Destroyed!`.red.inverse)
        process.exit()
    } catch (error) {
        console.error(`${error}`.red.inverse);
        process.exit(1)
    }
}

importData()