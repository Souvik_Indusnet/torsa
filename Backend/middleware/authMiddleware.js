import jwt, { decode } from 'jsonwebtoken';
import { asyncHandler } from '../middleware/errorMiddleware.js';
import User from '../models/userModel.js';
import Admin from '../models/Admin.js';

const protect = asyncHandler(async (req, res, next) => {
  let token;
  // console.log(req.headers.authorization)
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
  }
  { 
    token = req.headers.authorization.split(' ')[1];
    const decoded = jwt.verify(token,process.env.JWT_SECRET);
    //  console.log('19',decoded);
    req.user = await User.findById(decoded.id).select('-password');
    next();
  }

  if (!token) {
    res.status(401);
    throw new Error(' Un-authorized');
  }
});

const adminProtect = asyncHandler(async (req, res, next) => {
  //  console.log('29',req.headers.authorization);
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
    ) {
    }
    {
      token = req.headers.authorization.split(' ')[1];
      const decoded = jwt.verify(token,''+process.env.JWT_SECRET);
    req.admin = await Admin.findById(decoded.id).select('-password');
    next();
  }

  if (!token) {
    res.status(401);
    throw new Error('Un-authorized');
  }
});

const sysAdmin = (req, res, next) => {
  if (req.admin && req.admin.isSystemAdmin) {
    next();
  } else {
    res.status(401);
    throw new Error('Un-authorized  admin');
  }
};

const busAdmin = (req, res, next) => {
  if (req.admin && req.admin.isSystemAdmin === false) {
    next();
  } else {
    res.status(401);
    throw new Error('Un-authorized  admin');
  }
};

export { protect, adminProtect, sysAdmin, busAdmin };
