import path from 'path';
import { asyncHandler } from './errorMiddleware.js';

const addImage = asyncHandler(async (req, res, next) => {
  console.log(req.files);
  if (req?.files?.image == undefined) {
    return next();
  }
  // else if (req?.files?.image.length == 'undefined') {
  const image = req?.files?.image || req?.files?.file;

  const filename = image.md5 + path.extname(image.name);
  const uplaodPath = 'uploads/' + filename;
  console.log('14',req.imageLink,uplaodPath);
  image.mv(uplaodPath, function (err) {
    if (err) {
      return res.status(500).json({ message: 'Error during uploading file' });
    } else {
      req.imageLink = uplaodPath.replace('uploads/', '');
      console.log('19',req.imageLink)
      next();
    }
  });
});
export { addImage };
