import express from 'express';
import AWS from 'aws-sdk';
import fileUpload from 'express-fileupload';

const router = express.Router();
import dotenv from 'dotenv';
dotenv.config();
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  accessSecretKey: process.env.AWS_SECRET_ACCESS_KEY,
});
const s3 = new AWS.S3({});
// router.post("/", function (req, res) {
//   if (!req.files) {
//     return res.status(400).send("No files were uploaded.");
//   }
//   if (req.files.images.name == undefined) {
//     var url = [];
//     for (var i = 0; i < req.files.images.length; i++) {
//       let sampleFile = req.files.images[i];
//       var file_key = "";
//       if (req.body != undefined) {
//         let extension = sampleFile.mimetype.split("/")[1];
//         file_key =
//           "images/" + Date.now() + "_" + new Date().getTime() + "." + extension;
//       } else {
//         file_key =
//           "images/" +
//           Date.now() +
//           "_" +
//           new Date().getTime() +
//           "." +
//           sampleFile.mimetype.split("/")[1];
//       }
//       const params = {
//         Bucket: "bridge-media-2021",
//         ACL: "public-read",
//         Body: sampleFile.data,
//         Key: file_key,
//       };
//       var ref = this;
//       s3.upload(params, function (err, data) {
//         if (err) {
//           console.log("Error uploading image: ", err);
//         } else {
//           url.push(data.Location);
//         }
//       });
//     }
//     req.file_url=url;
//   } else {
//     let sampleFile = req.files;
//     console.log(sampleFile.images.mimetype);
//     var file_key = "";
//     if (req.body != undefined) {
//       let extension = sampleFile.images.mimetype.split("/")[1];
//       file_key =
//         "images/" + Date.now() + "_" + new Date().getTime() + "." + extension;
//     } else {
//       file_key =
//         "images/" +
//         Date.now() +
//         "_" +
//         new Date().getTime() +
//         "." +
//         sampleFile.images.mimetype.split("/")[1];
//     }
//     const params = {
//       Bucket: "bridge-media-2021",
//       ACL: "public-read",
//       Body: sampleFile.images.data,
//       Key: file_key,
//     };
//     var ref = this;
//     s3.upload(params, function (err, data) {
//       if (err) {
//         console.log("Error uploading image: ", err);
//       } else {
//         var obj = {
//           s3_url: params.Key,
//           bucket: params.Bucket,
//         };
//         req.file_url=[data.Location]
//         // console.log(req.file_url);
//         // return res.status(200).send(data.Location);
//       }
//     });
//   }
// });
const Upload_AWS = (req, res, next) => {
  console.log('files', req.files, !req.files);
  // console.log('already uploaded',req.body.alreadyUploaded);
  // console.log('length',req.body?.alreadyUploaded.length!=0)
  // console.log(req.files,req.body.alreadyUploaded,(!req.files)&&req.body.alreadyUploaded.length!=0)

  if (!req.files && req?.body?.alreadyUploaded?.length != 0) {
    console.log('97.................');
    next();
  } else {
    if (!req.files) {
      next();
      // return res.status(400).send('No files were uploaded.');
    }
    if (req.files.images.name == undefined) {
      var url = [];
      console.log('105......................', req.files.images.length);
      for (var i = 0; i < req.files.images.length; i++) {
        let sampleFile = req.files.images[i];
        var file_key = '';
        if (req.body != undefined) {
          let extension = sampleFile.mimetype.split('/')[1];
          file_key =
            'images/' +
            Date.now() +
            '_' +
            new Date().getTime() +
            '.' +
            extension;
        } else {
          file_key =
            'images/' +
            Date.now() +
            '_' +
            new Date().getTime() +
            '.' +
            sampleFile.mimetype.split('/')[1];
        }
        const params = {
          Bucket: 'bridge-media-2021',
          ACL: 'public-read',
          Body: sampleFile.data,
          Key: file_key,
        };
        var ref = this;
        s3.upload(params, function (err, data) {
          if (err) {
            console.log('Error uploading image: ', err);
          } else {
            url.push(data.Location);
            console.log('134......................', url);
            req.file_url = url;
            console.log('136......................', req.file_url);
          }
        });
      }
      setTimeout(() => {
        console.log('130.............', req.file_url);
        next();
      }, [2000]);
    } else {
      // console.log('133',req.files.images.name)
      let sampleFile = req.files;
      var file_key = '';
      if (req.body != undefined) {
        let extension = sampleFile.images.mimetype.split('/')[1];
        file_key =
          'images/' + Date.now() + '_' + new Date().getTime() + '.' + extension;
      } else {
        file_key =
          'images/' +
          Date.now() +
          '_' +
          new Date().getTime() +
          '.' +
          sampleFile.images.mimetype.split('/')[1];
      }
      const params = {
        Bucket: 'bridge-media-2021',
        ACL: 'public-read',
        Body: sampleFile.images.data,
        Key: file_key,
      };
      var ref = this;
      s3.upload(params, function (err, data) {
        if (err) {
          console.log('Error uploading image: ', err);
        } else {
          var obj = {
            s3_url: params.Key,
            bucket: params.Bucket,
          };
          req.file_url = [data.Location];
          setTimeout(() => {
            console.log('130.............', req.file_url);
            next();
          }, [2000]);
          //  console.log('162',req.file_url);
          // return res.status(200).send(data.Location);
        }
      });
    }
  }
};
export default Upload_AWS;
