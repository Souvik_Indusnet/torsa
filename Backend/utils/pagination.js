import { asyncHandler } from '../middleware/errorMiddleware.js';

const paginatedResults = (model, modalString) => {
  return asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page) || 1;
    const limit = 10;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    var count;
    if (modalString === 'Payment') {
      count = await model.aggregate([
        {
          $match: {
            $expr: {
              $eq:
                req.admin?.isSystemAdmin == false
                  ? ['$buisnessManager._id', req.admin?._id]
                  : ['', ''],
            },
          },
        },
        {
          $lookup: {
            from: 'users',
            localField: 'user',
            foreignField: '_id',
            as: 'user',
          },
        },
        {
          $unwind: '$user',
        },
        {
          $lookup: {
            from: 'parts',
            localField: 'part',
            foreignField: '_id',
            as: 'part',
          },
        },
        {
          $unwind: '$part',
        },
        {
          $match: {
            $or:
              req.query.keyword != 'undefined'
                ? [
                    {
                      Mode_Of_Payment: {
                        $regex: req.query.keyword,
                        $options: 'i',
                      },
                    },
                    {
                      Type_Of_Payment: {
                        $regex: req.query.keyword,
                        $options: 'i',
                      },
                    },
                    {
                      Payment_Status: {
                        $regex: req.query.keyword,
                        $options: 'i',
                      },
                    },
                    {
                      orderStatus: {
                        $regex: req.query.keyword,
                        $options: 'i',
                      },
                    },
                  ]
                : [{}],
          },
        },
        {
          $match: {
            for_Enquery: false,
          },
        },
        {
          $count: 'totalDocuments',
        },
      ]);
      count = count[0].totalDocuments ? count[0].totalDocuments : 0;
      // .countDocuments({});
    } else if (modalString === 'Admin') {
      count = await model
        .countDocuments({
          $and: [
            { isSystemAdmin: false },
            {
              $or:
                req.query.keyword != 'undefined'
                  ? [
                      {
                        name: {
                          $regex: req.query.keyword,
                          $options: 'i',
                        },
                      },
                      {
                        email: {
                          $regex: req.query.keyword,
                          $options: 'i',
                        },
                      },
                      {
                        partName: {
                          $regex: req.query.keyword,
                          $options: 'i',
                        },
                      },
                      {
                        'category.name': {
                          $regex: req.query.keyword,
                          $options: 'i',
                        },
                      },
                      {
                        'subcategory.name': {
                          $regex: req.query.keyword,
                          $options: 'i',
                        },
                      },
                      {
                        Type_Of_Payment: {
                          $regex: req.query.keyword,
                          $options: 'i',
                        },
                      },
                      {
                        Mode_Of_Payment: {
                          $regex: req.query.keyword,
                          $options: 'i',
                        },
                      },
                      {
                        Payment_Status: {
                          $regex: req.query.keyword,
                          $options: 'i',
                        },
                      },
                      {
                        orderStatus: {
                          $regex: req.query.keyword,
                          $options: 'i',
                        },
                      },
                    ]
                  : [{}],
            },
          ],
        })
        .exec();
    } else {
      count = await model
        .countDocuments({
          $or:
            req.query.keyword != 'undefined'
              ? [
                  {
                    name: {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                  {
                    email: {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                  {
                    partName: {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                  {
                    'category.name': {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                  {
                    'subcategory.name': {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                  {
                    Type_Of_Payment: {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                  {
                    Mode_Of_Payment: {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                  {
                    Payment_Status: {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                  {
                    orderStatus: {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                ]
              : [{}],
        })
        .exec();
      // temp=query;
    }
    // console.log('229.......',count)
    const results = {};
    results.current = {
      page: page,
      limit: limit,
      totalPage: Math.ceil(count / limit),
    };

    if (endIndex < count) {
      results.next = {
        page: page + 1,
      };
    }

    if (startIndex > 0) {
      results.previous = {
        page: page - 1,
      };
    }

    const populationFunc = (modals) => {
      switch (modals) {
        case 'SubCategory':
          return [{ path: 'category', select: 'name active ' }];
        // case 'Page':
        //   return [{ path: 'forPageName' }, { path: 'pageName' }];
        case 'Part':
          return [{ path: 'category' }, { path: 'subcategory' }];
        // case 'Payment':
        //   return [
        //     { path: 'user' },
        //     { path: 'part' },
        // { path: 'part', populate: { path: 'category' } },
        // { path: 'part', populate: { path: 'subcategory' } },
        // ];
        default:
          return { path: '_id' };
      }
    };

    const FilteFunc = (modals) => {
      switch (modals) {
        case 'Admin':
          return [
            {
              name: {
                $regex: req.query.keyword,
                $options: 'i',
              },
            },
            {
              email: {
                $regex: req.query.keyword,
                $options: 'i',
              },
            },
          ];
        case 'Part':
          return [
            {
              partName: {
                $regex: req.query.keyword,
                $options: 'i',
              },
            },
            {
              'category.name': {
                $regex: req.query.keyword,
                $options: 'i',
              },
            },
            {
              'subcategory.name': {
                $regex: req.query.keyword,
                $options: 'i',
              },
            },
          ];
        default:
          return [{}];
      }
    };

    results.results =
      modalString == 'Payment'
        ? await model
            .aggregate([
              {
                $match: {
                  $expr: {
                    $eq:
                      req.admin?.isSystemAdmin == false
                        ? ['$buisnessManager._id', req.admin?._id]
                        : ['', ''],
                  },
                },
              },
              {
                $lookup: {
                  from: 'users',
                  localField: 'user',
                  foreignField: '_id',
                  as: 'user',
                },
              },
              {
                $unwind: '$user',
              },
              {
                $lookup: {
                  from: 'parts',
                  localField: 'part',
                  foreignField: '_id',
                  as: 'part',
                },
              },
              {
                $unwind: '$part',
              },
              {
                $match: {
                  $or:
                    req.query.keyword != 'undefined'
                      ? [
                          {
                            Mode_Of_Payment: {
                              $regex: req.query.keyword,
                              $options: 'i',
                            },
                          },
                          {
                            Type_Of_Payment: {
                              $regex: req.query.keyword,
                              $options: 'i',
                            },
                          },
                          {
                            Payment_Status: {
                              $regex: req.query.keyword,
                              $options: 'i',
                            },
                          },
                          {
                            orderStatus: {
                              $regex: req.query.keyword,
                              $options: 'i',
                            },
                          },
                        ]
                      : [{}],
                },
              },
              {
                $match: {
                  for_Enquery: false,
                },
              },
            ])
            .sort({ _id: -1 })
            .skip(startIndex)
            .limit(limit)
            .exec()
        : modalString == 'User'
        ? await model
            .aggregate([
              {
                $lookup: {
                  from: 'admins',
                  localField: 'buisnessManager',
                  foreignField: '_id',
                  as: 'buisnessManager',
                },
              },
              {
                $unwind: '$buisnessManager',
              },
              {
                $lookup: {
                  from: 'userequipments',
                  localField: '_id',
                  foreignField: 'userID',
                  as: 'equipments',
                },
              },
              {
                $lookup: {
                  from: 'categories',
                  localField: 'equipments.equipment',
                  foreignField: '_id',
                  as: 'equipments.equipment',
                },
              },
              {
                $lookup: {
                  from: 'userequipmentmodals',
                  localField: '_id',
                  foreignField: 'userID',
                  as: 'EquipmentModal',
                },
              },
              {
                $lookup: {
                  from: 'subcategories',
                  localField: 'EquipmentModal.equipmentModal',
                  foreignField: '_id',
                  as: 'EquipmentModal.equipmentModal',
                },
              },
              {
                $match: {
                  $or:
                    req.query.keyword != 'undefined'
                      ? [
                          {
                            name: { $regex: req.query.keyword, $options: 'i' },
                          },
                          {
                            email: { $regex: req.query.keyword, $options: 'i' },
                          },
                          {
                            'buisnessManager.name': {
                              $regex: req.query.keyword,
                              $options: 'i',
                            },
                          },
                        ]
                      : [{}],
                },
              },
              {
                $match: {
                  $expr: {
                    $eq:
                      req.admin?.isSystemAdmin == false
                        ? ['$buisnessManager._id', req.admin?._id]
                        : ['', ''],
                  },
                },
              },
            ])
            .sort({ _id: -1 })
            .limit(limit)
            .skip(startIndex)
            .exec()
        : modalString == 'Category' ||
          modalString == 'Part' ||
          modalString == 'Page'
        ? await model
            .find({
              $or:
                req.query.keyword != 'undefined'
                  ? FilteFunc(modalString)
                  : [{}],
            })
            .populate(populationFunc(modalString))
            .sort({ _id: -1 })
            .limit(limit)
            .skip(startIndex)
            .exec()
        : modalString == 'SubCategory'
        ? await model
            .find({
              $or:
                req.query.keyword != 'undefined'
                  ? FilteFunc(modalString)
                  : [{}],
            })
            .populate(populationFunc(modalString))
            .sort({ _id: -1 })
            .limit(limit)
            .skip(startIndex)
            .exec()
        : await model
            .find({
              $or:
                req.query.keyword != 'undefined'
                  ? FilteFunc(modalString)
                  : [{}],
              isSystemAdmin: false,
            })
            .populate(populationFunc(modalString))
            .sort({ _id: -1 })
            .skip(startIndex)
            .limit(limit)
            .exec();
    res.paginatedResults = results;
    next();
  });
};

export { paginatedResults };
