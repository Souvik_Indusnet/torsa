import mailjet from 'node-mailjet';
import dotenv from 'dotenv';
dotenv.config();

//update
const MailJet = async ({
  email,
  name,
  Subject,
  textpart,
  htmlpart,

  msg,
}) => {
  // CompositionEvent;
  const request = await mailjet

    .connect(`${process.env.mailjetSTR1}`, `${process.env.mailjetSTR2}`)
    .post('send', { version: 'v3.1' })
    .request({
      Messages: [
        {
          From: {
            Email: `${process.env.mailjetEmail}`,
            Name: `${process.env.mailjetName}`,
          },
          To: [
            {
              Email: `${email}`,
              Name: `${name}`,
            },
          ],
          Subject: `${Subject}`,
          TextPart: `${textpart}`,
          HTMLPart: `${htmlpart}` || `${msg}`,
        },
      ],
    });
  (email = ''), (name = '');
  return request;
};

export { MailJet };
