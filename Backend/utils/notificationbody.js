import { notificationType } from './notificationname.js';
import {
  Enquiry_Updated,
  Enquiry_from_User,
  Payment_by_User,
  Last_Payment_Oneshot_Payment,
  Payment_Due_Admin,
  Enquiry_Approved,
  Enquiry_Rejected,
  Payment_by_User1,
  Payment_Due_User,
  Order_Shipped,
  Order_Delivered,
  Order_Cancelled,
  For_User,
  for_BA,
} from '../constant/notificationconstant.js';
export function fetchNotificationBody(i) {
  if (i.type == Enquiry_Approved) {
    return notificationType({
      type: i.type,
      sparepart_name: i.data.sparepart_name,
    });
  }
  if (i.type == Enquiry_Rejected) {
    return notificationType({
      type: i.type,
      sparepart_name: i.data.sparepart_name,
      reason: i.data.reason,
    });
  }
  if (i.type == Enquiry_from_User && i.data.resend) {
    return notificationType({
      type: i.type,
      user: i.data.user,
      resend: i.data.resend,
      partName: i.data.partName,
    });
  }
  if (i.type == Enquiry_from_User) {
    return notificationType({ type: i.type, user: i.data.user });
  }
  if (i.type == Order_Shipped) {
    return notificationType({
      type: i.type,
      user: i.data.user,
      order: i.data.order,
      order_id: i.data.order_id,
    });
  }
  if (i.type == Order_Cancelled) {
    return notificationType({
      type: i.type,
      user: i.data.user,
      part: i.data.part,
      order_id: i.data.order_id,
      date: i.data.date,
    });
  }
  if (i.type == Payment_Due_User) {
    return notificationType({
      type: i.type,
      user: i.data.user,
      order: i.data.order,
      order_id: i.data.order_id,
    });
  }
  if (i.type == Payment_by_User) {
    return notificationType({
      type: i.type,
      user: i.data.user,
      payment: i.data.payment,
      Due_payment: i.data.Due_payment,
      Due_date: i.data.Due_payment,
    });
  }
  if (i.type == Last_Payment_Oneshot_Payment) {
    return notificationType({ type: i.type, user: i.data.user });
  }
  if (i.type == Payment_Due_Admin) {
    return notificationType({
      type: i.type,
      user: i.data.user,
      Due_date: i.data.Due_date,
    });
  }
  if (i.type == Payment_by_User1) {
    return notificationType({
      type: i.type,
      sparepart_name: i.data.sparepart_name,
      installment: i.data.installment,
    });
  }
  if (i.type == Order_Delivered) {
    return notificationType({
      type: i.type,
      user: i.data.user,
      product: i.data.product,
      orderId: i.data.orderId,
    });
  }
  if (i.type == Enquiry_Updated) {
    return notificationType({
      type: i.type,
      amount: i.data.amount,
      date: i.data.date,
    });
  }
  if (i.type == For_User) {
    return notificationType({
      type: i.type,
      user: i.data.user,
      url: i.data.url,
      text: i.data.msg,
    });
  }
  if (i.type == for_BA) {
    return notificationType({
      type: i.type,
      user: i.data.user,
      url: i.data.url,
      text: i.data.msg,
    });
  }
}
