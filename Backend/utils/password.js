import bcrypt from "bcryptjs";
export const returnpassword = async (password) => {
  let passwordjan;
  const salt = await bcrypt.genSalt(10);
  passwordjan = await bcrypt.hash(password, salt);
  // console.log(passwordjan);
  return passwordjan;
};
