import UserSignList from "../models/SignInList.js";
import Payment from "../models/payment.js";
export async function fetchAdminDashboardData(filter) {
  const totalSalesCount = await Payment.aggregate([
    {
      $match: filter
        ? {
            createdAt: {
              $gt: new Date(Date.now() - filter * 24 * 60 * 60 * 1000),
            },
          }
        : {
            createdAt: {
              $gt: new Date(Date.now() - 1 * 24 * 60 * 60 * 1000),
            },
          },
    },
    {
      $match: {
        $expr: {
          $and: [{ $eq: ["$isDeleted", false] }],
        },
      },
    },
    {
      $count: "totalSalesCount",
    },
  ]);
  const signInList = await UserSignList.aggregate([
    {
      $match: filter
        ? {
            createdAt: {
              $gt: new Date(Date.now() - filter * 24 * 60 * 60 * 1000),
            },
          }
        : {
            createdAt: {
              $gt: new Date(Date.now() - 1 * 24 * 60 * 60 * 1000),
            },
          },
    },
    {
      $group: {
        _id: "$user",
      },
    },
    {
      $count: "signInList",
    },
  ]);
  return { totalSalesCount: totalSalesCount, signInList: signInList };
}
export async function fetchBusinessAdminDashboardData(filter,req) {
  const signInList = await UserSignList.aggregate([
    {
      $lookup: {
        from: "users",
        localField: "user",
        foreignField: "_id",
        as: "user_details",
      },
    },
    {
      $unwind: {
        path: "$user_details",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $match: {
        $expr: {
          $and: [{ $eq: ["$user_details.buisnessManager", req.admin._id] }],
        },
      },
    },
    {
      $match: filter
        ? {
            createdAt: {
              $gt: new Date(Date.now() - filter * 24 * 60 * 60 * 1000),
            },
          }
        : {
            createdAt: {
              $gt: new Date(Date.now() - 1 * 24 * 60 * 60 * 1000),
            },
          },
    },
    {
      $group: {
        _id: "$user",
      },
    },
    {
      $count: "signInList",
    },
  ]);
  return { signInList: signInList };
}
