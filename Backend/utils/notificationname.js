import {
  Enquiry_Updated,
  Enquiry_from_User,
  Payment_by_User,
  Last_Payment_Oneshot_Payment,
  Payment_Due_Admin,
  Enquiry_Approved,
  Enquiry_Rejected,
  Payment_by_User1,
  Payment_Due_User,
  Order_Shipped,
  Order_Delivered,
  Order_Cancelled,
  For_User,
  for_BA,
} from '../constant/notificationconstant.js';
const notificationType = (data) => {
  // console.log('2',data.resend);
  switch (data.type) {
    case Enquiry_Updated:
      return {
        title: `Your Payment of Amount ${data.amount} is due`,
        text: `Your Payment of Amount Rs ${data.amount} is due. The last date for paying the amount is ${data.date}`,
        path: '/admin/notification',
      };
    case Enquiry_from_User:
      if (data.resend == undefined)
        return {
          title: `Enquiry from ${data.user}`,
          text: `You've received an Enquiry from User ${data.user}. Click here to see the enquiry Details.`,
          path: '/admin/notification',
        };
      else {
        return {
          title: `Enquiry from ${data.user}`,
          text: ` User ${data.user} has resend requested more information about modal part ${data.partName}.`,
          path: '/admin/notification',
        };
      }
    case Payment_by_User:
      return {
        title: `User ${data.user} has made a payment of  Rs${data.payment}`,
        text: `User ${data.user} has made a payment of  Rs${data.payment}. The next due date for payment of Ammount is ${data.Due_payment} is on ${data.Due_date}.`,
        path: '/admin/notification',
      };
    case Last_Payment_Oneshot_Payment:
      return {
        title: `User ${data.user} has paid all the installments successfully`,
        text: `User ${data.user} has paid all the installments successfully.`,
        path: '/admin/notification',
      };
    case Payment_Due_Admin:
      return {
        title: `Reminder -Due date of User ${data.user}`,
        text: `Reminder -Due date of User ${data.user} is on ${data.Due_date}.`,
        path: '/admin/notification',
      };
    case Enquiry_Approved:
      return {
        title: `Your enquiry has been approved by the Admin`,
        text: `Your enquiry for spare part ${data.sparepart_name} has been approved by the Admin.Please click here to proceed further.`,
        path: '/notification',
      };
    case Enquiry_Rejected:
      return {
        title: `Your enquiry has been rejected by the Admin`,
        text: `Your enquiry for spare part ${data.sparepart_name} has been rejected by the Admin.Reason is ${data?.reason}.`,
        path: '/notification',
      };
    case Payment_by_User1:
      return {
        title: `You've Successfully paid your installment for spare part${data.sparepart_name}.`,
        text: `You've Successfully paid your installment of Rs${data.installment}. for spare part ${data.sparepart_name}, Thank you for purchasing from TORSA.`,
        path: '/notification',
      };
    case Payment_Due_User:
      return {
        title: 'Reminder',
        text: `Reminder -Due date of User ${data.user} is on ${data.Due_date}.`,
        path: '/notification',
      };
    case Order_Shipped:
      return {
        title: 'Order Shipped',
        text: `Hi ${data.user} your order ${data.order} with Order Id :${data.order_id}  has been shipped.`,
        path: '/notification',
      };
    case Order_Delivered:
      return {
        title: 'Order Delivered',
        text: `Hi ${data.user} your order ${data.product} with Order Id :${data.orderId}  has been delivered at your Address.Thank you for shopping from TORSA.`,
        path: '/notification',
      };
    case Order_Cancelled:
      return {
        title: 'Order Cancelled',
        text: `Hi ${data.user} your order ${data.part} with Order Id :${data.order_id}  has been cancelled on ${data.date}.`,
        path: '/notification',
      };
    case For_User:
      return {
        title: 'New notification',
        text: data.text,
        path: '/notification',
        url: data.url,
        user: data.user,
      };
    case for_BA:
      return {
        title: 'New notification',
        text: data.text,
        path: '/notification',
        url: data.url,
      };
  }
};
export { notificationType };
