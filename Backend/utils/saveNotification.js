import Notification from '../models/notificationlist.js';

export const SaveNotification = async (
  userID,
  type,
  data,
  order_id,
  isCreatedByAdmin
) => {
  // console.log('4.........Save Notification',userID, type,data,order_id);
  const NotificationData = await Notification.create({
    user: userID,
    type: type,
    data: data,
    generateCount: 1,
    order_id: order_id,
    isCreatedByAdmin: isCreatedByAdmin,
  });
  // await NotificationData.save();
  // console.log('12', NotificationData);
};
