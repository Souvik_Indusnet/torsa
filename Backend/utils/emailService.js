import nodemailer from "nodemailer";
export const transport = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 587,
  // secure: true,
  auth: {
    user: process.env.mailjetEmail,
    pass: process.env.mailjetPassword,
  },
});
export const mailOption = (email, securePassword, user) => {
  console.log(user);
  return {
    to: email,
    subject: user
      ? "User credentials||Torsa machinary"
      : " Business Admin account ||Torsa Business Admin",
    html: user
      ? `<p>Hii ,</p>
          <p>This is Your New credetials</p>
          <p>UserName: ${email}</p>
          <p>Password: ${securePassword} </p>

          `
      : `<p>Hii Admin,</p>
                <p>here is your Login credential https://torsa.herokuapp.com go to and login</p>
                <p>username: ${email}</p>
                <p>password: ${securePassword}</p>
          `,
  };
};
