import express from 'express';
import * as socketIO from 'socket.io';
import dotenv from 'dotenv';
import { errorHandler, notFound } from './middleware/errorMiddleware.js';
import connectDB from './config/db.js';
import cors from 'cors';
import path from 'path';
import bodyParser from 'body-parser';
import { createServer } from 'http';
import upload from 'express-fileupload';
//Routes
import machineRoutes from './routes/machineRoutes.js';
import userRoutes from './routes/userRoutes.js';
import adminRoutes from './routes/adminRoutes.js';
import uploadRoutes from './routes/uploadRoutes.js';
import morgan from 'morgan';
import CMSRouter from './routes/cmsRoutes.js';
import orderRoutes from './routes/orderRoutes.js';
import Order from './models/orderModel.js';
import Aws_Routes from './utils/aws_configuration.js';
import Upload_AWS from './utils/aws_configuration.js';
// import upload from './middleware/uploadMiddleware.js'
dotenv.config();

connectDB();

const app = express();
app.use(upload());
const server = createServer(app);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/upload_aws',Upload_AWS);

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

app.use(express.json());
// app.use(fileUpload());
const __dirname = path.resolve();
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));

app.use('/product', machineRoutes);
app.use('/user', userRoutes);
app.use('/admin', adminRoutes);
// app.use('/upload', uploadRoutes);
app.use('/cms', CMSRouter);
app.use('/order', orderRoutes);

const io = new socketIO.Server(server, {
  transports: ['websocket', 'polling'],
});

//listen socket

// io.on("connection", (client) => {
//   setInterval(() => {
//     const changeStream = Order.watch();
//     changeStream.on("change", (change) => {
//       const task = change.fullDocument;
//       client.emit("orderNotification", task);
//     });
//   }, 1000);
// });

if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, '/Frontend/build')));

  app.get('*', (req, res) =>
    res.sendFile(path.resolve(__dirname, 'Frontend', 'build', 'index.html'))
  );
} else {
  app.get('/', (req, res) => {
    res.send('API is running....');
  });
}

//Error Middlewares
app.use(notFound);
app.use(errorHandler);
const PORT = process.env.PORT || 5000;

app.listen(
  PORT,
  console.log(
    `Server Running in ${process.env.NODE_ENV} on port ${PORT}`.yellow.bold
  )
);
