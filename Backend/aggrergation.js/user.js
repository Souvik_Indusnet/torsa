import mongoose from 'mongoose';

const userEquipmentAndModal = ({ id }) => [
  {
    $match: {
      _id: { $eq: mongoose.Types.ObjectId(id) },
    },
  },
  {
    $lookup: {
      from: 'userequipments',
      localField: '_id',
      foreignField: 'userID',
      as: 'equipment',
    },
  },
  {
    $lookup: {
      from: 'categories',
      localField: 'equipment.equipment',
      foreignField: '_id',
      as: 'equipment',
    },
  },
  {
    $lookup: {
      from: 'userequipmentmodals',
      localField: '_id',
      foreignField: 'userID',
      as: 'equipmentmodal',
    },
  },
  {
    $lookup: {
      from: 'subcategories',
      localField: 'equipmentmodal.equipmentModal',
      foreignField: '_id',
      as: 'equipmentmodal',
    },
  },

  {
    $project: {
      _id: 0,
      'equipment.createdAt': 0,
      'equipment.isDeleted': 0,
      'equipment.active': 0,
      'equipment.__v': 0,

      'equipmentmodal.createdAt': 0,
      'equipmentmodal.isDeleted': 0,
      'equipmentmodal.active': 0,
      'equipmentmodal.__v': 0,
      'equipmentmodal.category': 0,

      image: 0,
      machines: 0,
      active: 0,
      isDeleted: 0,
      buisnessManager: 0,
      name: 0,
      email: 0,
      password: 0,
      contactNo: 0,
      cart: 0,
      createdAt: 0,
      updatedAt: 0,
      __v: 0,
    },
  },
];

export { userEquipmentAndModal };
