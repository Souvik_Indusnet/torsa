import mogoose from "mongoose";
import color from "colors";

const connectDB = async () => {
  try {
    // const DB = 'mongodb+srv://mondal:vQHrpD9LEmdfsyrx@cluster0.dowei.mongodb.net/cms?retryWrites=true&w=majority'
    const DB =
    process.env.MONGO_URI;
      // "mongodb+srv://ashish:9781ashi@cluster0.4b504.mongodb.net/torsa";
    const conn = await mogoose.connect(DB, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    console.log(`MongoDB connected: ${conn.connection.host}`.cyan.underline);
  } catch (error) {
    console.error(`Error: ${error.message}`.red.underline.bold);
    process.exit(1);
  }
};

export default connectDB;
