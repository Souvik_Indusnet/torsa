import express from 'express';
import {
  authUser,
  authUserSignout,
  getUserProfile,
  // getUserMachine,
  forgetPassword,
  userParts,
  newOrder,
  getpartsByid,
  updateUsrProfile,
  updateUserProfile,
  getOrderInfo,
  AddCartItems,
  getCartsItems,
  getEnqueries,
  enqueryForm,
  paymentGateWay,
  paymentSignatureVerification,
  payment,
  getUserEquipments,
  postUpdateUserAddress,
  postChangeUserPassword,
  // getModelsByMachineId,
  deleteTocartItems,
  getOrdersByUser,
  updateOrderbyId,
  GetEquipmentModalByEquipment,
  getAllPartsByEq_Modals,
  getProductDetailsByID,
  AddProductTomyCart,
  CartProductActions,
  getMycart,
  removeProductFromMyCart,
  SendAskForPrice,
  getMyEnquiry,
  getMyOrder,
  matchOtp,
  change_password,
  // getUserDetailsById,
} from '../controllers/userController.js';

import { protect } from '../middleware/authMiddleware.js';
import { addImage } from '../middleware/Uploader.js';

const router = express.Router();

//auth api
router.route('/signin').post(authUser);
router.route('/signout').get(protect,authUserSignout);
// @user part flow
//user Equipment flow
router.route('/equipment').get(protect, getUserEquipments);
router.route('/updateAddress').post(protect, postUpdateUserAddress);
router.route('/updatePassword').post(protect, postChangeUserPassword);
router
  .route('/equipmentmodalbyequipment')
  .get(protect, GetEquipmentModalByEquipment);
router.route('/productbyEquipmentModal').post(protect, getAllPartsByEq_Modals);
router.route('/productDetails/:product').get(protect, getProductDetailsByID);

//user cart actions
router
  .route('/usercart')
  .post(protect, AddProductTomyCart)
  .put(protect, CartProductActions)
  .delete(protect, removeProductFromMyCart)
  .get(protect, getMycart);

//enquiry route

router
  .route('/user_enquiry')
  .post(protect, SendAskForPrice)
  .get(protect, getMyEnquiry);

//order Routes
router.route('/user_order').get(protect, getMyOrder);

router
  .route('/profile')
  .get(protect, getUserProfile)
  .put(protect, updateUsrProfile)
  .post(protect,updateUserProfile);
// router.route('/userDetailsById', getUserDetailsById);

router.route('/forget').post(forgetPassword);
router.route('/otpmatch').patch(matchOtp);
router.route('/otp_based_changepassword').put(change_password);
// router.route("/machine").get(protect, getUserMachine)

// router.route('/equipmentModel').post(protect, getModelsByMachineId);
router.route('/parts').get(protect, userParts).post(protect, getpartsByid);
router
  .route('/cart')
  .post(protect, AddCartItems)
  .get(protect, getCartsItems)
  .delete(protect, deleteTocartItems);
router.route('/order').post(protect, newOrder).get(protect, getOrdersByUser).put(protect,updateOrderbyId);
router.route('/getOrderNotification').get(getOrderInfo);
router.route('/enquery').get(protect, getEnqueries).post(protect, enqueryForm);
router.route('/payment').post(protect, payment);
router.route('/transactionUpdate').post(protect, paymentGateWay);
router.route('/verifyPaymentSignature').post(protect, paymentSignatureVerification);

export default router;