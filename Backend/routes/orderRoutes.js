import express from "express";
import {
  getorderById,
  getOrderByUser,
  getOrders,
  newOrder,
  updateOrderStatus,
} from "../controllers/userController.js";
import { adminProtect, protect } from "../middleware/authMiddleware.js";
const router = express.Router();

router.post("/", protect, newOrder);
router.get("/", adminProtect, getOrders);
router.patch("/", adminProtect, updateOrderStatus);
router.get("/getbyid/:id", getorderById);
router.post("/orderUser", getOrderByUser);
export default router;
