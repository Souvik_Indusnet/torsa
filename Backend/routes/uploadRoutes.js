import path from 'path';
import express from 'express';
import multer from 'multer';
import _ from 'lodash';
import Admin from '../models/Admin.js';
import { adminProtect, protect } from '../middleware/authMiddleware.js';
const router = express.Router();

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'uploads/product');
  },
  filename(req, file, cb) {
    cb(
      null,
      `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`
    );
  },
});

function checkFileType(file, cb) {
  const filetypes = /jpg|jpeg|png/;
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);

  if (extname && mimetype) {
    return cb(null, true);
  } else {
    cb('Images only!');
  }
}

const upload = multer({
  storage,
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  },
});

router.post('/', upload.array('files'), async (req, res) => {
  let { files } = req;

  return res.json(files);
});

router.post(
  '/admin',
  adminProtect,
  upload.single('admin'),
  async (req, res) => {
    // console.log(req.admin);
    const { _id } = req.admin;
    // console.log(req.file);
    const admin = await Admin.findByIdAndUpdate(_id, {
      image: req.file && req.file.filename,
    });
    res.status(201).json(admin);
  }
);

export default router;
