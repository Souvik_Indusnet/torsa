import express from 'express';
import { paginateCategory } from '../controllers/adminController.js';
import {
  createPageName,
  getAllPageName,
  createPage,
  getAllPage,
  editPage,
  deletePage,
  getOnePage,
  statusUpdate,
  getCmsbyString,
} from '../controllers/cmsController.js';
import { Page } from '../models/cmsModel.js';
import { paginatedResults } from '../utils/pagination.js';
const CMSRouter = express.Router();

CMSRouter.post('/create-page-name', createPageName);
CMSRouter.get('/all-page-name', getAllPageName);
CMSRouter.post('/create-page', createPage);
CMSRouter.get('/all-page', getAllPage);
CMSRouter.route('/cmspages').get(
  paginatedResults(Page, 'Page'),
  paginateCategory
);

CMSRouter.get('/get-one-page', getOnePage);
CMSRouter.put('/edit-page', editPage);
CMSRouter.delete('/delete-page', deletePage);
CMSRouter.put('/update-status', statusUpdate);

CMSRouter.get('/getcmsdetailsbystring', getCmsbyString);
export default CMSRouter;
