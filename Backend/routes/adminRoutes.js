import express from 'express';
import path from 'path';
import {
  authAdmin,
  createBuisnessAdmin,
  createUser,
  deleteAdmin,
  deleteUser,
  getAdminById,
  getAllAdmin,
  getAllUser,
  getUserById,
  updateAdmin,
  updateAdminStatus,
  updateUser,
  updateUserStatus,
  getAdminByEachId,
  updateAdminByEachId,
  getadminByuserAssosiated,
  adminDashbooardGraph,
  AdminProfileLists,
  BusinessAdminGraphApi,
  EnquiryList,
  EnquiryListUpdate,
  PaymentList,
  paginateCategory,
  PaymentListBusinessAdminId,
  GetDetalsByids,
  GetReports,
  GetReportsGraphs,
  Order_Del_Status,
  getOrderDetailsById,
  UserCSV,
  ProductCSV,
  BaCSV,
  EnquiryCSV,
  PaymentCSV,
  OrderCSV,
  CmsCSV,
  EquipemntCSV,
  EquipmentModal,
  getalluserPaginate,
  adminDashboard,
  BusinessOrderCsv,
  businessAdminDashboard,
  createNotification,
  getBusAdminNotification,
  unreadNotification,
  getAllCreateNotification,
  setMarkReadNotificatin,
  setMarkReadAllNotificatin,
} from '../controllers/adminController.js';
import {
  getNotification,
  fireNotification,
  getNotificationbyBusinessAdmin,
  getReadNotification,
} from '../controllers/notification.js';
import { getUserDetailsById } from '../controllers/userController.js';
// import { newNotification } from "../controllers/notification.js";
import {
  adminProtect,
  busAdmin,
  protect,
  sysAdmin,
} from '../middleware/authMiddleware.js';
import { asyncHandler } from '../middleware/errorMiddleware.js';
import { addImage } from '../middleware/Uploader.js';
import Admin from '../models/Admin.js';
import Category from '../models/categoryModel.js';
import Payment from '../models/payment.js';
import SubCategory from '../models/subcategoryModel.js';
import User from '../models/userModel.js';
import { paginatedResults } from '../utils/pagination.js';
const router = express.Router();

//Auth
router.route('/signin').post(authAdmin);
//master setting
router
  .route('/equipment')
  .get(paginatedResults(Category, 'Category'), paginateCategory);
router
  .route('/equipmentmodal')
  .get(paginatedResults(SubCategory, 'SubCategory'), paginateCategory);

// //order management
router
  .route('/orderTable')
  .get(paginatedResults(Payment, 'Payment'), paginateCategory);
router.route('/order_del_status/:id').put(Order_Del_Status);
router.route('/orderDetailsByid').get(getOrderDetailsById);
router
  .route('/orderTable/orderlist')
  .get(adminProtect, PaymentListBusinessAdminId);
//User Management
router.route('/getallusers').get(
  adminProtect,
  getalluserPaginate
  // paginatedResults(User, 'User'), paginateCategory
);
router.route('/getAllUser').get(adminProtect, sysAdmin, getAllUser);
router.route('/getUser/:id').get(getUserById);
router.route('/addUser').post(adminProtect, sysAdmin, createUser);
router.route('/deleteUser/:id').delete(adminProtect, sysAdmin, deleteUser);
router.route('/updateUser/:id').put(adminProtect, sysAdmin, updateUser);
router
  .route('/updateUserStatus/:id')
  .put(adminProtect, sysAdmin, updateUserStatus);

//Admin Management
router
  .route('/getallbusinessadmin')
  .get(paginatedResults(Admin, 'Admin'), paginateCategory);
router.route('/getAllAdmin').get(adminProtect, sysAdmin, getAllAdmin);
router.route('/dashboard').get(adminProtect, sysAdmin, adminDashboard);
router
  .route('/businessadmindashboard')
  .get(adminProtect, businessAdminDashboard);
router
  .route('/dashboardgraph')
  .get(adminProtect, sysAdmin, adminDashbooardGraph);
router.route('/getAdmin/:id').get(adminProtect, sysAdmin, getAdminById);
router.route('/addBuisnessAdmin').post(
  // adminProtect, sysAdmin,
  createBuisnessAdmin
);
router.route('/updateAdmin/:id').put(protect, addImage, updateAdmin);
router
  .route('/updateAdminStatus/:id')
  .put(adminProtect, sysAdmin, updateAdminStatus);

//business user Routes
router.route('/userAdmin/:id').post(getAdminByEachId);
router.route('/updateUserAdmin').put(updateAdminByEachId);
router.route('/userDetailsById').get(getUserDetailsById);
// router.route('/notification').post(newNotification);
// router.route('/getNotification').get(getNotification).post(updateOrder);
router.route('/getUserByadminId').post(getadminByuserAssosiated);
router.route('/getAdminProfile').post(AdminProfileLists);
// router.route('/getbusinessAdminDashboard').post(BusinessAdminGraphApi);
//
// router.route('/dashboard').get(adminProtect,);
//notificatrions
router.route('/allnotification').get(protect, adminProtect, getNotification);
router.route('/firenotification').get(fireNotification);
router
  .route('/allnotificationbyBusinessAdmin')
  .get(getNotificationbyBusinessAdmin);
router.route('/updateNotifications').post(protect, getReadNotification);
//enquiery
router
  .route('/enquiry')
  .get(protect, adminProtect, EnquiryList)
  .put(protect, adminProtect, EnquiryListUpdate);

//payment
router.route('/payment').get(protect, adminProtect, PaymentList);

//parts routers
router.route('/partDetailsByid').get(protect, adminProtect, GetDetalsByids);
//Reports Routes
router.route('/Reports').get(protect, adminProtect, GetReports);
router.route('/ReportGraphs').get(protect, adminProtect, GetReportsGraphs);
//csv data file api
router.get('/customer-csv', protect, adminProtect, UserCSV);
router.get('/product-csv', protect, adminProtect, ProductCSV);
router.get('/ba-csv', protect, adminProtect, BaCSV);
router.get('/enquiry-csv', protect, adminProtect, EnquiryCSV);
router.get('/payment-csv', protect, adminProtect, PaymentCSV);
router.get('/order-csv', protect, adminProtect, OrderCSV);
router.get('/cms-csv', protect, adminProtect, CmsCSV);
router.get('/equipment-csv', protect, adminProtect, EquipemntCSV);
router.get('/equpment_modal-csv', protect, adminProtect, EquipmentModal);
router.get('/businsess-order-csv', protect, adminProtect, BusinessOrderCsv);
router.post('/create-notification', protect, adminProtect, createNotification);

router
  .get(
    '/notification',
    protect,
    adminProtect,
    busAdmin,
    getBusAdminNotification
  )
  .put('/notification', protect, adminProtect, busAdmin, setMarkReadNotificatin)
  .post(
    '/notification',
    protect,
    adminProtect,
    busAdmin,
    setMarkReadAllNotificatin
  );
router.get('/created-notification', getAllCreateNotification);

router.get(
  '/unread-count-notification',
  protect,
  adminProtect,
  busAdmin,
  unreadNotification
);

router.post(
  '/imageupload2',
  asyncHandler((req, res) => {
    // console.log(req.files?.images);
    const image = req.files?.images;
    let data = [];
    image?.map((i) => {
      const filename = i.md5 + path.extname(i.name);
      const uplaodPath = 'uploads/product/' + filename;
      i.mv(uplaodPath, function (err) {
        if (err) {
          return res
            .status(500)
            .json({ message: 'Error during uploading file' });
        } else {
          // console.log('181');
          // console.log('183', uplaodPath);
          let imageLink = uplaodPath.replace('uploads/product/', '');
          // return res.status(200).json({ imageLink });
          data.push(imageLink);
        }
      });
    });
    setTimeout(() => {
      return res.status(200).json({ data });
    }, [2000]);
  })
);
export default router;
