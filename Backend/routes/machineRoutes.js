import express from 'express';
import { paginateCategory } from '../controllers/adminController.js';
const router = express.Router();
import {
  getMachineById,
  getMachines,
  updateMachineStatus,
  deleteMachine,
  getCategory,
  getPartById,
  getParts,
  addMachine,
  addCategory,
  deleteCategory,
  addPart,
  updatePartStatus,
  deletePart,
  updateInventory,
  getSubCategory,
  addSubCategory,
  deleteSubCategory,
  statusUpdate,
  editCategory,
  editSubCategory,
  getMachinAndPartByarrayId,
  addMachineName,
  getEquipments,
  updateMachineName,
  deleteEquipment,
  updatestatusMachineName,
  getModelByArrayId,
  UpdatePartsByID,
} from '../controllers/machineController.js';
import { adminProtect, sysAdmin } from '../middleware/authMiddleware.js';
import { addImage } from '../middleware/Uploader.js';
import Part from '../models/partModel.js';
import { paginatedResults } from '../utils/pagination.js';
import { Uploader } from '../utils/uploader.js';
import Upload_AWS from '../utils/aws_configuration.js';
//Machines
router
  .route('/machine')
  .post(addMachine)
  .get(getMachines)
  .put(updateMachineStatus);

router.route('/getmachinesInfo').post(getMachinAndPartByarrayId);
router
  .route('/machineName')
  .post(addMachineName)
  .get(getEquipments)
  .put(updateMachineName)
  .patch(updatestatusMachineName);
router.post('/machineModel', adminProtect, sysAdmin, getModelByArrayId);
router.route('/machineName/:id').delete(deleteEquipment);
// .delete(deleteMachine);
router.route('/machine/:id').delete(deleteMachine);
router.route('/machineById/:id').get(adminProtect, sysAdmin, getMachineById);

//Category
router
  .route('/category')
  .get(getCategory)
  .post(Upload_AWS, addCategory)
  .put(statusUpdate);
router.delete('/category/:id', deleteCategory);
router.put('/edit-category', Upload_AWS, editCategory);
router.route('/categoryById/:id').get(adminProtect, sysAdmin, getCategory);

//SubCategory
router
  .route('/subcategory')
  .get(getSubCategory)
  .post(addSubCategory)
  .put(editSubCategory);
router.route('/subcategory/:id').delete(deleteSubCategory);
//Parts

router
  .route('/parts')
  .post(adminProtect, sysAdmin, Upload_AWS, addPart)
  .get(paginatedResults(Part, 'Part'), paginateCategory)
  .patch(adminProtect, sysAdmin, Upload_AWS, updatePartStatus)
  .put(adminProtect, sysAdmin, Uploader, UpdatePartsByID);
// .patch(deletePart);
router
  .route('/partById/:id')
  .get(adminProtect, sysAdmin, getPartById)
  .post(adminProtect, sysAdmin, updateInventory);

export default router;
