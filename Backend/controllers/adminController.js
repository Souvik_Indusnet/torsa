import { asyncHandler } from '../middleware/errorMiddleware.js';
import Admin from '../models/Admin.js';
import Machine from '../models/machineModel.js';
import Part from '../models/partModel.js';
import User from '../models/userModel.js';
import generateToken from '../utils/generateToken.js';
import { generatePassword } from '../utils/passwordGenerate.js';
import MachineName from '../models/machineName.js';
import Order from '../models/orderModel.js';
import equiryDetails from '../models/payment.js';
import { SaveNotification } from '../utils/saveNotification.js';
import {
  Enquiry_Approved,
  Enquiry_Rejected,
  Enquiry_Updated,
  for_BA,
  For_User,
} from '../constant/notificationconstant.js';
import { MailJet } from '../utils/mailjet.js';
import UserEquipment from '../models/userEquipment.js';
import UserEquipmentModal from '../models/userEquipmentModal.js';
import mongoose from 'mongoose';
import { userEquipmentAndModal } from '../aggrergation.js/user.js';
import Payment from '../models/payment.js';
import { Page } from '../models/cmsModel.js';
import Category from '../models/categoryModel.js';
import SubCategory from '../models/subcategoryModel.js';
import { notificationType } from '../utils/notificationname.js';
import { send_notification } from '../utils/sendAndroidNotification.js';
import {
  fetchAdminDashboardData,
  fetchBusinessAdminDashboardData,
} from '../utils/dashboardcount.js';
import Notification from '../models/notificationlist.js';
//@desc Admin Dashboard Data
//route   get /user/adminDashboard
//@access Private /Admin
const adminDashboard = asyncHandler(async (req, res) => {
  const admin1 = await Admin.countDocuments({
    isSystemAdmin: false,
    isDeleted: false,
  });
  const userCount = await User.countDocuments({ isDeleted: false });
  const equipmentsCount = await Category.countDocuments({ isDeleted: false });
  const equipmentsModelsCount = await SubCategory.countDocuments({
    isDeleted: false,
  });
  var time = [1, 7, 30, 365];
  var day = await fetchAdminDashboardData(time[0]);
  var week = await fetchAdminDashboardData(time[1]);
  var month = await fetchAdminDashboardData(time[2]);
  var year = await fetchAdminDashboardData(time[3]);
  const lastUser = await User.aggregate([
    {
      $match: {
        $expr: [{ $eq: ['$active', true] }, { $eq: ['$isDeleted', false] }],
      },
    },
    {
      $sort: {
        _id: -1,
      },
    },
    {
      $project: {
        name: 1,
        email: 1,
        active: 1,
        createdAt: 1,
      },
    },
    {
      $limit: 5,
    },
  ]);
  const lastBA = await Admin.aggregate([
    {
      $match: {
        $expr: [
          { $eq: ['$active', true] },
          { $eq: ['$isDeleted', false] },
          { $eq: ['isSystemAdmin', false] },
        ],
      },
    },
    {
      $project: {
        name: 1,
        email: 1,
        active: 1,
        createdAt: 1,
      },
    },
    {
      $sort: {
        _id: -1,
      },
    },
    {
      $limit: 5,
    },
  ]);
  const lastProduct = await Part.aggregate([
    {
      $match: {
        $expr: [{ $eq: ['$active', true] }, { $eq: ['$isDeleted', false] }],
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'category',
        foreignField: '_id',
        as: 'category',
      },
    },

    {
      $unwind: '$category',
    },

    {
      $sort: {
        _id: -1,
      },
    },
    {
      $project: {
        partName: 1,
        category: 1,
        active: 1,
        createdAt: 1,
        amount: 1,
      },
    },
    {
      $limit: 5,
    },
  ]);
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    admin: admin1,
    user: userCount,
    machines: equipmentsCount,
    machineName: equipmentsModelsCount,
    lastUser: lastUser,
    lastBA: lastBA,
    lastProduct: lastProduct,
    totalSalesCount: [
      day.totalSalesCount[0]?.totalSalesCount
        ? day.totalSalesCount[0]?.totalSalesCount
        : 0,
      week.totalSalesCount[0]?.totalSalesCount
        ? week.totalSalesCount[0]?.totalSalesCount
        : 0,
      month.totalSalesCount[0]?.totalSalesCount
        ? month.totalSalesCount[0]?.totalSalesCount
        : 0,
      year.totalSalesCount[0]?.totalSalesCount
        ? year.totalSalesCount[0]?.totalSalesCount
        : 0,
    ],
    signInList: [
      day.signInList[0]?.signInList ? day.signInList[0]?.signInList : 0,
      week.signInList[0]?.signInList ? week.signInList[0]?.signInList : 0,
      month.signInList[0]?.signInList ? month.signInList[0]?.signInList : 0,
      year.signInList[0]?.signInList ? year.signInList[0]?.signInList : 0,
    ],
  });
});
const businessAdminDashboard = asyncHandler(async (req, res) => {
  const userCount = await User.countDocuments({
    buisnessManager: req.admin._id,
    isDeleted: false,
  });
  const equipmentsCount1 = await UserEquipment.aggregate([
    {
      $lookup: {
        from: 'users',
        localField: 'userID',
        foreignField: '_id',
        as: 'users',
      },
    },
    {
      $unwind: '$users',
    },
    {
      $match: {
        $expr: {
          $eq: ['$users.buisnessManager', req.admin._id],
        },
      },
    },
    {
      $group: {
        _id: '$equipment',
        uniqueIds: { $addToSet: '$equipment' },
      },
    },
    {
      $count: 'totalCount',
    },
  ]);
  const equipmentsCount = equipmentsCount1[0]?.totalCount
    ? equipmentsCount1[0]?.totalCount
    : 0;
  const equipmentsModelsCount1 = await UserEquipmentModal.aggregate([
    {
      $lookup: {
        from: 'users',
        localField: 'userID',
        foreignField: '_id',
        as: 'users',
      },
    },
    {
      $unwind: '$users',
    },
    {
      $match: {
        $expr: {
          $eq: ['$users.buisnessManager', req.admin._id],
        },
      },
    },
    {
      $group: {
        _id: '$equipmentModal',
        uniqueIds: { $addToSet: '$equipmentModal' },
      },
    },
    {
      $count: 'totalCount',
    },
  ]);
  const equipmentsModelsCount = equipmentsModelsCount1[0]?.totalCount
    ? equipmentsModelsCount1[0]?.totalCount
    : 0;
  var time = [1, 7, 30, 365];
  var day = await fetchBusinessAdminDashboardData(time[0], req);
  var Week = await fetchBusinessAdminDashboardData(time[1], req);
  var Month = await fetchBusinessAdminDashboardData(time[2], req);
  var Year = await fetchBusinessAdminDashboardData(time[3], req);
  const totalSales = await Payment.aggregate([
    {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'user_details',
      },
    },
    {
      $unwind: {
        path: '$user_details',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $match: {
        $expr: {
          $and: [{ $eq: ['$user_details.buisnessManager', req.admin._id] }],
        },
      },
    },
    {
      $match: {
        $expr: {
          $and: [{ $eq: ['$isDeleted', false] }],
        },
      },
    },
    {
      $addFields: {
        datetoyear: { $year: '$createdAt' },
      },
    },
    {
      $match: {
        $expr: {
          $and: [{ $eq: ['$datetoyear', parseInt(req.query.year)] }],
        },
      },
    },
    {
      $addFields: {
        datetomonth: { $month: '$createdAt' },
      },
    },
    {
      $group: {
        _id: '$datetomonth',
        count: { $sum: 1 },
      },
    },
  ]);
  var not_found = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  var totalSales1 = totalSales.map((p) => {
    if (not_found.indexOf(p._id)) {
      not_found.splice(not_found.indexOf(p._id), 1);
    }
    return p._id == 1
      ? { ...p, name: 'January' }
      : p._id == 2
      ? { ...p, name: 'February' }
      : p._id == 3
      ? { ...p, name: 'March' }
      : p._id == 4
      ? { ...p, name: 'April' }
      : p._id == 5
      ? { ...p, name: 'May' }
      : p._id == 6
      ? { ...p, name: 'June' }
      : p._id == 7
      ? { ...p, name: 'July' }
      : p._id == 8
      ? { ...p, name: 'August' }
      : p._id == 9
      ? { ...p, name: 'September' }
      : p._id == 10
      ? { ...p, name: 'October' }
      : p._id == 11
      ? { ...p, name: 'November' }
      : p._id == 12
      ? { ...p, name: 'December' }
      : p;
  });
  not_found.forEach((i) => {
    i == 1
      ? totalSales1.push({ _id: i, count: 0, name: 'January' })
      : i == 2
      ? totalSales1.push({ _id: i, count: 0, name: 'February' })
      : i == 3
      ? totalSales1.push({ _id: i, count: 0, name: 'March' })
      : i == 4
      ? totalSales1.push({ _id: i, count: 0, name: 'April' })
      : i == 5
      ? totalSales1.push({ _id: i, count: 0, name: 'May' })
      : i == 6
      ? totalSales1.push({ _id: i, count: 0, name: 'June' })
      : i == 7
      ? totalSales1.push({ _id: i, count: 0, name: 'July' })
      : i == 8
      ? totalSales1.push({ _id: i, count: 0, name: 'August' })
      : i == 9
      ? totalSales1.push({ _id: i, count: 0, name: 'September' })
      : i == 10
      ? totalSales1.push({ _id: i, count: 0, name: 'October' })
      : i == 11
      ? totalSales1.push({ _id: i, count: 0, name: 'November' })
      : i == 12
      ? totalSales1.push({ _id: i, count: 0, name: 'December' })
      : {};
  });
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    year: req.query.year,
    user: userCount,
    machines: equipmentsCount,
    machineName: equipmentsModelsCount,
    totalSalesCount: totalSales1.sort((a, b) => a._id - b._id),
    signInList: [
      day.signInList[0]?.signInList ? day.signInList[0]?.signInList : 0,
      Week.signInList[0]?.signInList ? Week.signInList[0]?.signInList : 0,
      Month.signInList[0]?.signInList ? Month.signInList[0]?.signInList : 0,
      Year.signInList[0]?.signInList ? Year.signInList[0]?.signInList : 0,
    ],
  });
});
// @desc Auth Admin & get token
// @route GET /admin/signin
// @access Public
const authAdmin = asyncHandler(async (req, res) => {
  const { email, password } = req.body;

  const admin = await Admin.findOne({ email });

  if (admin && (await admin.matchPassword(password))) {
    res.json({
      name: admin.name,
      _id: admin._id,
      email: admin.email,
      active: admin.active,
      image: admin.image,
      contactNo: admin.contactNo,
      isSystemAdmin: admin.isSystemAdmin,
      token: generateToken(admin._id),
    });
  } else {
    res.status(401).json({
      errorCode: 401,
      errorMessage: 'Invalid email or Password',
    });
    // throw new Error('Invalid email or Password');
  }
});
// @desc Get Reports of All Business Admins
// @route GET /admin/Reports
// @access Private
const GetReports = asyncHandler(async (req, res) => {
  const allAdmin = await Payment.aggregate([
    {
      $match: {
        $expr: {
          $and: [{ $eq: ['$isDeleted', false] }],
        },
      },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
      $unwind: '$user',
    },
    {
      $lookup: {
        from: 'admins',
        localField: 'user.buisnessManager',
        foreignField: '_id',
        as: 'user.buisnessManager',
      },
    },
    {
      $unwind: '$user.buisnessManager',
    },
    {
      $group: {
        _id: '$user.buisnessManager._id',
        name: { $first: '$user.buisnessManager.name' },
        value: { $sum: '$qty' },
      },
    },
  ]);
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    admin: allAdmin,
  });
});
const GetReportsGraphs = asyncHandler(async (req, res) => {
  const totalSales = await Payment.aggregate([
    {
      $match: {
        $expr: {
          $and: [{ $eq: ['$isDeleted', false] }],
        },
      },
    },
    {
      $addFields: {
        datetoyear: { $year: '$createdAt' },
      },
    },
    {
      $match: {
        $expr: {
          $and: [{ $eq: ['$datetoyear', parseInt(req.query.year)] }],
        },
      },
    },
    {
      $addFields: {
        datetomonth: { $month: '$createdAt' },
      },
    },
    {
      $group: {
        _id: '$datetomonth',
        count: { $sum: 1 },
      },
    },
  ]);
  var not_found = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  var totalSales1 = totalSales.map((p) => {
    if (not_found.indexOf(p._id) != -1) {
      not_found.splice(not_found.indexOf(p._id), 1);
    }
    return p._id == 1
      ? { ...p, name: 'January' }
      : p._id == 2
      ? { ...p, name: 'February' }
      : p._id == 3
      ? { ...p, name: 'March' }
      : p._id == 4
      ? { ...p, name: 'April' }
      : p._id == 5
      ? { ...p, name: 'May' }
      : p._id == 6
      ? { ...p, name: 'June' }
      : p._id == 7
      ? { ...p, name: 'July' }
      : p._id == 8
      ? { ...p, name: 'August' }
      : p._id == 9
      ? { ...p, name: 'September' }
      : p._id == 10
      ? { ...p, name: 'October' }
      : p._id == 11
      ? { ...p, name: 'November' }
      : p._id == 12
      ? { ...p, name: 'December' }
      : p;
  });
  not_found.forEach((i) => {
    i == 1
      ? totalSales1.push({ _id: i, count: 0, name: 'January' })
      : i == 2
      ? totalSales1.push({ _id: i, count: 0, name: 'February' })
      : i == 3
      ? totalSales1.push({ _id: i, count: 0, name: 'March' })
      : i == 4
      ? totalSales1.push({ _id: i, count: 0, name: 'April' })
      : i == 5
      ? totalSales1.push({ _id: i, count: 0, name: 'May' })
      : i == 6
      ? totalSales1.push({ _id: i, count: 0, name: 'June' })
      : i == 7
      ? totalSales1.push({ _id: i, count: 0, name: 'July' })
      : i == 8
      ? totalSales1.push({ _id: i, count: 0, name: 'August' })
      : i == 9
      ? totalSales1.push({ _id: i, count: 0, name: 'September' })
      : i == 10
      ? totalSales1.push({ _id: i, count: 0, name: 'October' })
      : i == 11
      ? totalSales1.push({ _id: i, count: 0, name: 'November' })
      : i == 12
      ? totalSales1.push({ _id: i, count: 0, name: 'December' })
      : {};
  });
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    admin: totalSales1.sort((a, b) => a._id - b._id),
  });
});
// @desc Get all User
// @route GET /admin/getAllUser
// @access Private
const getAllUser = asyncHandler(async (req, res) => {
  const keyword = req.query.keyword
    ? {
        name: {
          $regex: req.query.keyword,
          $options: 'i',
        },
      }
    : {};
  const user = await User.find({ ...keyword })
    .sort({ _id: -1 })
    .select('-password')
    .populate('equipment')
    .populate('machines', 'machineName')
    .populate('buisnessManager', 'name');
  // console.log(user);
  if (user) {
    res.send(user);
  } else {
    res.status(400);
    throw new Error('Invalid User Data');
  }
});

// @desc Get User By Id
// @route GET /admin/getUser/:id
// @access Private
const getUserById = asyncHandler(async (req, res) => {
  const getUser = await User.aggregate(
    userEquipmentAndModal({ id: req.params.id })
  );

  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    user: getUser[0],
  });
});

// @desc Create New User
// @route GET /admin/addUser
// @access Private
const createUser = asyncHandler(async (req, res) => {
  const { name, email, contactNo, buisnessManager, equipment, equipmentModal } =
    req.body;
  console.log('365...................', email);
  const userExist = await User.findOne({ email });

  if (userExist) {
    res.status(400);
    throw new Error('User Already Exists');
  }

  const password = await generatePassword(12, name, contactNo, email);

  const user = await User.create({
    name,
    email,
    password,
    contactNo,
    buisnessManager,
  });
  equipment?.map(async (el) => {
    const userEquipment = await UserEquipment.create({
      userID: user._id,
      equipment: el,
    });
  });

  equipmentModal?.map(async (el) => {
    const userEquipmentModal = await UserEquipmentModal.create({
      userID: user._id,
      equipmentModal: el,
    });
  });

  await MailJet({
    email: email,
    name: name,
    Subject: 'User Credentials',
    textpart: 'User Credentials',
    htmlpart: `
        <p>username:${email} </p><br/>
        <p>password:${password}</p><br />
        `,
  });
  res.status(201).json({
    _id: user._id,
    name: user.name,
    email: user.email,
    buisnessManager: user.buisnessManager,
    machines: user.machines,
    token: generateToken(user._id),
  });
});

// @desc    Update a User
// @route   DELETE /user/updateUser/:id
// @access  Private/Admin
const updateUser = asyncHandler(async (req, res) => {
  const { name, email, contactNo, buisnessManager, equipment, equipmentModal } =
    req.body;
  const user = await User.findById(req.params.id);
  const find_Old_User_Eq = await UserEquipment.find({ userID: user._id });
  const find_Old_userEqModal = await UserEquipmentModal.find({
    userID: user._id,
  });

  //find and add EQ
  let find_this_Equipment_and_add_UserEQ = equipment.filter((x) => {
    const user_EQ_Ids = find_Old_User_Eq?.map((i) => i.equipment.toString());
    return !user_EQ_Ids?.includes(x.toString());
  });
  find_this_Equipment_and_add_UserEQ?.map(async (el) => {
    const userEquipment = await UserEquipment.create({
      userID: user._id,
      equipment: el,
    });
    console.log(
      'check1',
      await UserEquipment.find({ userID: user._id, equipment: el })
    );
  });
  console.log(
    'Add user Equipment',
    find_this_Equipment_and_add_UserEQ,
    equipment
  );
  // find And Add EQ_Modal
  let find_this_Equipment_and_add_UserEQ_modal = equipmentModal.filter((x) => {
    const user_EQ_Ids = find_Old_userEqModal?.map((i) =>
      i.equipmentModal.toString()
    );
    return !user_EQ_Ids?.includes(x.toString());
  });
  find_this_Equipment_and_add_UserEQ_modal?.map(async (el) => {
    const userEquipment = await UserEquipmentModal.create({
      userID: user._id,
      equipmentModal: el,
    });
    console.log(
      'check2',
      await UserEquipmentModal.find({
        userID: user._id,
        equipmentModal: el,
      })
    );
  });
  console.log(
    'Add user Equipment Model',
    find_this_Equipment_and_add_UserEQ_modal,
    equipmentModal
  );
  // find and remove EQ_Modal
  const user_EQ_Ids_modal = find_Old_userEqModal?.map((i) =>
    i.equipmentModal.toString()
  );
  let find_this_EQ_and_remove_userEQ_modal = user_EQ_Ids_modal.filter((x) => {
    return !equipmentModal.includes(x.toString());
  });
  console.log(
    'Remove user Equipment Model',
    find_this_EQ_and_remove_userEQ_modal,
    equipmentModal
  );
  find_this_EQ_and_remove_userEQ_modal?.map(async (el) => {
    await UserEquipmentModal.findOneAndDelete({
      userID: user._id,
      equipmentModal: el,
    });
    console.log(
      'check3',
      await UserEquipmentModal.find({
        userID: user._id,
        equipmentModal: el,
      })
    );
  });

  // find and remove EQ
  const user_EQ_Ids = find_Old_User_Eq?.map((i) => i.equipment.toString());
  let find_this_EQ_and_remove_userEQ = user_EQ_Ids.filter((x) => {
    return !equipment.includes(x.toString());
  });
  console.log(
    'Remove user Equipment',
    find_this_EQ_and_remove_userEQ,
    equipment
  );
  find_this_EQ_and_remove_userEQ?.map(async (el) => {
    await UserEquipment.findOneAndDelete({ userID: user._id, equipment: el });
    console.log(
      'check4',
      await UserEquipment.find({ userID: user._id, equipment: el })
    );
  });
  console.log(
    'updated',
    await UserEquipment.find({ userID: user._id }),
    await UserEquipmentModal.find({
      userID: user._id,
    })
  );
  setTimeout(async () => {
    user.name = name || user.name;
    user.email = email || user.email;
    user.machines = req.body.machines || user.machines;
    user.contactNo = contactNo || user.contactNo;
    user.buisnessManager = buisnessManager || user.buisnessManager;
    const updatedUser = await user.save();
    res.json({
      _id: updatedUser._id,
      name: updatedUser.name,
      email: updatedUser.email,
      buisnessManager: updatedUser.buisnessManager,
      machines: updatedUser.machines,
      token: generateToken(updatedUser._id),
    });
  }, [200]);
});

// @desc    Update a UserStatus
// @route   DELETE /user/updateUserStatus/:id
// @access  Private/Admin
const updateUserStatus = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id);
  // console.log(req.body);

  if (user) {
    user.active = req.body.active;
    const updatedUser = await user.save();

    res.json({
      _id: updatedUser._id,
      name: updatedUser.name,
      active: updatedUser.active,
      buisnessManager: updatedUser.buisnessManager,
      machines: updatedUser.machines,
      token: generateToken(updatedUser._id),
    });
  } else {
    res.status(404);
    throw new Error('User not found');
  }
});

// @desc    Delete a User
// @route   DELETE /user/deleteUser
// @access  Private/Admin
const deleteUser = asyncHandler(async (req, res) => {
  const user = await User.findByIdAndUpdate(req.params.id, { isDeleted: true });

  if (user) {
    // await user.remove();
    res.json({ message: 'User removed' });
  } else {
    res.status(404);
    throw new Error('User not found');
  }
});

// @desc Get all Buissness Admin
// @route GET /admin/getAllAdmin
// @access Private
const getAllAdmin = asyncHandler(async (req, res) => {
  const keyword = req.query.keyword
    ? {
        name: {
          $regex: req.query.keyword,
          $options: 'i',
        },
      }
    : {};
  const admin = await Admin.find({ ...keyword }).select('-password');

  if (admin) {
    res.send(admin);
  } else {
    res.status(400);
    throw new Error('Invalid User Data');
  }
});

// @desc Get User By Id
// @route GET /admin/getUser/:id
// @access Private
const getAdminById = asyncHandler(async (req, res) => {
  const admin = await Admin.findById(req.params.id);
  // console.log(req.params.id);
  if (admin) {
    res.send(admin);
  } else {
    res.status(400);
    throw new Error('Admin Not Found');
  }
});

// @desc Create New Buissness Admin
// @route GET /admin/addBuisnessAdmin
// @access Private
const createBuisnessAdmin = asyncHandler(async (req, res) => {
  // console.log(req.body);
  const { name, email, image, contactNo } = req.body;

  const adminExist = await Admin.findOne({ email });
  if (adminExist) {
    // console.log(adminExist);
    res.status(400);
    throw new Error('Admin Already Exists');
  }

  const securePassword = await generatePassword(
    12,
    name,
    contactNo,
    email
  ).trim();
  // console.log(securePassword);
  if (!adminExist) {
    const newAdmin = await Admin.create({
      name,
      email,
      image,
      contactNo,
      password: securePassword,
    });

    await MailJet({
      email: email,
      name: name,
      Subject: 'Admin Credentials',
      textpart: 'Admin Credentials',
      htmlpart: `
          <p>username:${email} </p><br/>
          <p>password:${securePassword}</p><br />
          `,
    });

    res.status(201).json({
      data: newAdmin,
    });
  }
});

// @desc    Update a Admin
// @route   UPDATE /admin/updateAdmin/:id
// @access  Private/Admin
const updateAdmin = asyncHandler(async (req, res) => {
  const admin = await Admin.findById(req.params.id);

  if (req.body.email != admin.email) {
    const getExistEmail = await Admin.findOne({ email: req.body.email });

    if (getExistEmail) {
      return res.status(422).json({
        errorCode: 422,
        errorMessage: 'Email already exists',
      });
    }
  } else if (req.body.contactNo != admin.contactNo) {
    const getExistContact = await Admin.findOne({
      contactNo: req.body.contactNo,
    });
    if (getExistContact) {
      return res.status(422).json({
        errorCode: 422,
        errorMessage: 'Contact already exists',
      });
    }
  }

  admin.name = req.body.name || admin.name;
  admin.email = req.body.email || admin.email;
  admin.contactNo = req.body.contactNo || admin.contactNo;
  admin.image = req?.imageLink || admin.image;
  req.body.password && (admin.password = req.body.password);
  const updatedAdmin = await admin.save();

  res.json({
    _id: updatedAdmin._id,
    email: updatedAdmin.email,
    name: updatedAdmin.name,
    active: updatedAdmin.active,
    image: updatedAdmin.image,
    contactNo: updatedAdmin.contactNo,
    isSystemAdmin: updatedAdmin.isSystemAdmin,
    token: generateToken(updatedAdmin._id),
  });
});

// @desc    Update a AdminStatus
// @route   put /user/updateAdminStatus/:id
// @access  Private/Admin
const updateAdminStatus = asyncHandler(async (req, res) => {
  const admin = await Admin.findById(req.params.id);
  //console.log(req.body);

  if (admin) {
    admin.active = req.body.active;
    const updatedUser = await admin.save();

    res.json({
      _id: updatedUser._id,
      active: updatedUser.active,
      token: generateToken(updatedUser._id),
    });
  } else {
    res.status(404);
    throw new Error('Admin not found');
  }
});

// @desc    Delete a Admin
// @route   DELETE /user/deleteAdmin
// @access  Private/Admin
const deleteAdmin = asyncHandler(async (req, res) => {
  const admin = await Admin.findByIdAndUpdate(req.params.id, {
    isDeleted: true,
  });
  if (admin) {
    // await admin.remove();
    res.json({ message: 'Admin removed' });
  } else {
    res.status(404);
    throw new Error('Admin not found');
  }
});

const adminDashbooardGraph = asyncHandler(async (req, res) => {
  const getAllOrder = await Order.find();
  const arrayIds = getAllOrder.map((i) => i.orderItems).flat();
  const allIds = arrayIds.map((i) => i.product);

  let count = {};

  allIds.forEach(function (i) {
    count[i] = (count[i] || 0) + 1;
  });

  const finalArray = Object.keys(count);
  const product = await Part.find({ _id: { $in: finalArray } });

  let newProduct = [];
  newProduct = product;

  for (let j = 0; j < newProduct.length; j++) {
    // if (newProduct[j]._id === finalArray[j])
    newProduct[j]._doc['orderQty'] = Object.values(count)[j];
  }

  res.json(newProduct);
});

const BusinessAdminGraphApi = asyncHandler(async (req, res) => {
  const { id } = req.body;
  const users = await User.find({
    buisnessManager: id,
    isDeleted: false,
    active: true,
  });
  let userID = users.map((i) => i._id);
  const orders = await Order.find({ user: { $in: userID } });
  let arrayIds = orders.map((i) => i.orderItems).flat();
  let allIds = arrayIds.map((i) => i.product);
  let count = {};

  allIds.forEach(function (i) {
    count[i] = (count[i] || 0) + 1;
  });

  const finalArray = Object.keys(count);
  const product = await Part.find({ _id: { $in: finalArray } });
  let newProduct = [];
  newProduct = product;

  for (let j = 0; j < newProduct.length; j++) {
    // if (newProduct[j]._id === FinalArray[j])
    newProduct[j]._doc['orderQty'] = Object.values(count)[j];
  }

  res.json(newProduct);
});

//```````````````Business admin routes``````````

const getAdminByEachId = asyncHandler(async (req, res) => {
  // console.log(req.params);
  const { id } = req.params;
  const isadminExist = await Admin.findById(id);
  // console.log(isadminExist);
  if (isadminExist) {
    res.json(isadminExist);
  } else {
    res.status(404);
    throw new Error('Admin not found');
  }
});

const updateAdminByEachId = asyncHandler(async (req, res) => {
  const { _id, name, email, contact } = req.body;
  const query = { _id: _id };
  const update = { $set: { name: name, email: email, contactNo: contact } };
  const options = { returnNewDocument: true };
  const userAdmin = await Admin.findOneAndUpdate(query, update, options);
  if (userAdmin) {
    res.json(userAdmin);
  } else {
    res.status(404);
    throw new Error('Admin not found');
  }
});

//get user by admin assosiated

const getadminByuserAssosiated = asyncHandler(async (req, res) => {
  const { id } = req.body;
  const userList = await User.find({ buisnessManager: id });
  res.status(202).json(userList);
});

const AdminProfileLists = asyncHandler(async (req, res) => {
  const { id } = req.body;
  const userList = await User.find({ buisnessManager: id });
  let userIds = userList.map((i) => i._id);
  console.log(userList.map((i) => i._id));
  const OrderList = await Order.find({
    user: { $in: userIds },
  });
  if (OrderList) {
    res.json({ userList, OrderList });
  }
});

//get all enquiry list
const EnquiryList = asyncHandler(async (req, res) => {
  const page = parseInt(req.query.page) || 1;
  const limit = 10;
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;
  const count = await equiryDetails
    .find({ for_Enquery: true })
    .sort({ _id: -1 })
    .populate({ path: 'user ', select: 'name' })
    .populate({
      path: 'part',
      select: 'partName inventoryStock',
    })
    .lean()
    .countDocuments();

  const results = {};

  results.current = {
    page: page,
    limit: limit,
    totalPage: Math.ceil(count / limit),
  };
  if (endIndex < count) {
    results.next = {
      page: page + 1,
    };
  }

  if (startIndex > 0) {
    results.previous = {
      page: page - 1,
    };
  }
  results.getAll = await equiryDetails
    .find({ for_Enquery: true })
    .sort({ _id: -1 })
    .populate({ path: 'user ', select: 'name' })
    .populate({
      path: 'part',
      select: 'partName inventoryStock',
    })
    .lean()
    .skip((results.current.page - 1) * limit)
    .limit(limit);
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'succss',
    list: results,
  });
});

//get all payment list
const PaymentList = asyncHandler(async (req, res) => {
  const page = parseInt(req.query.page) || 1;
  const limit = 10;
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;
  const count = await equiryDetails.countDocuments({
    $and: [
      { isApproved: 'approved' },
      {
        $or:
          req.query.keyword != 'undefined'
            ? [
                {
                  Type_Of_Payment: {
                    $regex: req.query.keyword,
                    $options: 'i',
                  },
                },
                {
                  Mode_Of_Payment: {
                    $regex: req.query.keyword,
                    $options: 'i',
                  },
                },
                {
                  Payment_Status: {
                    $regex: req.query.keyword,
                    $options: 'i',
                  },
                },
                {
                  orderStatus: {
                    $regex: req.query.keyword,
                    $options: 'i',
                  },
                },
                {
                  isApproved: {
                    $regex: req.query.keyword,
                    $options: 'i',
                  },
                },
              ]
            : [{}],
      },
    ],
  });

  const results = {};
  results.current = {
    page: page,
    limit: limit,
    totalPage: Math.ceil(count / limit),
  };

  if (endIndex < count) {
    results.next = {
      page: page + 1,
    };
  }

  if (startIndex > 0) {
    results.previous = {
      page: page - 1,
    };
  }
  results.results = await equiryDetails
    .find({
      $and: [
        { isApproved: 'approved' },
        {
          $or:
            req.query.keyword != 'undefined'
              ? [
                  {
                    Type_Of_Payment: {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                  {
                    Mode_Of_Payment: {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                  {
                    Payment_Status: {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                  {
                    orderStatus: {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                  {
                    isApproved: {
                      $regex: req.query.keyword,
                      $options: 'i',
                    },
                  },
                ]
              : [{}],
        },
      ],
    })
    .sort({ _id: -1 })
    .populate({ path: 'user ', select: 'name' })
    .populate({
      path: 'part',
      select: 'partName inventoryStock amount',
    })
    .sort({ _id: -1 })
    .skip(startIndex)
    .limit(limit)
    .exec();
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'succss',
    list: results,
  });
});

//send enquiry result to user
const EnquiryListUpdate = asyncHandler(async (req, res) => {
  const {
    user,
    part,
    Mode_Of_Payment,
    Payment_Status,
    Type_Of_Payment,
    Partial_Amount_Paid_in_percentage,
    Partial_Remaining_Amount_in_percentage,
    Partial_Amount_Paid,
    Partial_Remaining_Amount,
    Partial_Due,
    Partial_Date,
    isApproved,
    reason,
    enquiry_update,
  } = req.body;
  const getEnquiry = await equiryDetails
    .findOne({ _id: req.query._id })
    .populate('part user');

  getEnquiry.Mode_Of_Payment = Mode_Of_Payment || getEnquiry.Mode_Of_Payment;

  getEnquiry.Payment_Status = Payment_Status || getEnquiry.Payment_Status;

  getEnquiry.Partial_Amount_Paid_in_percentage =
    Partial_Amount_Paid_in_percentage ||
    getEnquiry.Partial_Amount_Paid_in_percentage;

  getEnquiry.Type_Of_Payment = Type_Of_Payment || getEnquiry.Type_Of_Payment;

  getEnquiry.Partial_Remaining_Amount_in_percentage =
    Partial_Remaining_Amount_in_percentage ||
    getEnquiry.Partial_Remaining_Amount_in_percentage;

  getEnquiry.Partial_Amount_Paid =
    Partial_Amount_Paid || getEnquiry.Partial_Amount_Paid;

  getEnquiry.Partial_Remaining_Amount =
    Partial_Remaining_Amount || getEnquiry.Partial_Remaining_Amount;

  getEnquiry.Partial_Due = Partial_Due || getEnquiry.Partial_Due;

  getEnquiry.Partial_Date = Partial_Date || getEnquiry.Partial_Date;

  getEnquiry.isApproved = isApproved || getEnquiry.isApproved;
  if (isApproved == 'rejected') {
    getEnquiry.reason = reason || getEnquiry.reason;
  }
  await getEnquiry.save();

  if (getEnquiry.isApproved == 'approved') {
    var product = getEnquiry.part;
    // await Part.findById(getEnquiry.part._id);
    var data = {
      sparepart_name: product.partName,
    };
    SaveNotification(req.admin._id, Enquiry_Approved, data, getEnquiry._id);
    var body = notificationType({
      type: Enquiry_Approved,
      sparepart_name: data.sparepart_name,
    });

    var user1 = getEnquiry.user;

    send_notification(
      user1.token,
      body.title,
      body.text,
      'Enquiry_Listing',
      getEnquiry
    );
  }

  if (getEnquiry.isApproved == 'rejected') {
    var product = getEnquiry.part;
    // await Part.findById(getEnquiry.part._id);
    var data = {
      sparepart_name: product.partName,
      reason: getEnquiry?.reason,
    };
    var user1 = getEnquiry.user;
    console.log(
      'dher',
      user1._id
      // Enquiry_Rejected,
      // data,
      // getEnquiry._id,
      // user1.token,
      // body.title,
      // body.text,
      // 'Enquiry_Listing',
      // getEnquiry
    );
    SaveNotification(user1._id, Enquiry_Rejected, data, getEnquiry._id);
    var body = notificationType({
      type: Enquiry_Rejected,
      sparepart_name: data.sparepart_name,
      reason: data?.reason,
    });

    var user1 = getEnquiry.user;

    send_notification(
      user1.token,
      body.title,
      body.text,
      'Enquiry_Listing',
      getEnquiry
    );
  }

  // console.log('new update', enquiry_update != 'undefined', enquiry_update);
  if (enquiry_update != undefined) {
    var data = {
      amount:
        parseInt(getEnquiry.Partial_Remaining_Amount) *
        parseInt(getEnquiry.qty),
      date: getEnquiry.Partial_Date,
    };
    var user1 = getEnquiry.user;
    SaveNotification(user1._id, Enquiry_Updated, data, getEnquiry._id);
    var body = notificationType({
      type: Enquiry_Updated,
      amount: data.amount,
      date: data.date,
    });

    // await User.findById(getEnquiry.user);
    send_notification(
      user1.token,
      body.title,
      body.text,
      'Enquiry_Listing',
      getEnquiry
    );
  }
  // console.log('1052.......................',getEnquiry);
  return res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
  });
});

//master setting  pagination equipment
//@get Req

const paginateCategory = asyncHandler(async (req, res) => {
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: res.paginatedResults,
  });
});
//PaymentListBusinessAdminId
const PaymentListBusinessAdminId = asyncHandler(async (req, res) => {
  let { page, size, keyword } = req.query;
  if (!page || page == '0') {
    page = 1;
  }

  if (!size) {
    size = 10;
  }
  const limit = parseInt(size);
  const skip = (page - 1) * size;
  const getDetails = await Payment.aggregate([
    {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
      $lookup: {
        from: 'parts',
        localField: 'part',
        foreignField: '_id',
        as: 'part',
      },
    },
    {
      $unwind: {
        path: '$user',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unwind: {
        path: '$part',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'part.category',
        foreignField: '_id',
        as: 'category',
      },
    },
    {
      $unwind: {
        path: '$category',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: 'subcategories',
        localField: 'part.subcategory',
        foreignField: '_id',
        as: 'subcategory',
      },
    },
    {
      $unwind: {
        path: '$subcategory',
        preserveNullAndEmptyArrays: true,
      },
    },
    // {
    //   $match:{
    //      $expr:{
    //        $eq:['$user.buisnessManager',mongoose.Types.ObjectId(req.admin._id)]
    //      }
    //   }
    // },
    {
      $match: {
        $expr: {
          $regexMatch: {
            input: { $toString: '$_id' },
            regex: keyword,
            options: 'i',
          },
        },
      },
    },
  ])
    .sort({ _id: -1 })
    .skip(skip)
    .limit(limit);
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getDetails,
  });
});
//get part details by ids
const GetDetalsByids = asyncHandler(async (req, res) => {
  const getDetails = await Part.aggregate([
    {
      $match: {
        $expr: {
          $eq: ['$_id', mongoose.Types.ObjectId(req.query.id)],
        },
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'category',
        foreignField: '_id',
        as: 'category',
      },
    },
    {
      $unwind: '$category',
    },
    {
      $lookup: {
        from: 'subcategories',
        localField: 'subcategory',
        foreignField: '_id',
        as: 'subcategory',
      },
    },
    {
      $unwind: '$subcategory',
    },
    {
      $project: {
        category: 1,
        subcategory: 1,
        _id: 1,
        amount: 1,
        description: 1,
        inventoryStock: 1,
        partName: 1,
      },
    },
  ]);
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getDetails,
  });
});

const Order_Del_Status = asyncHandler(async (req, res) => {
  const getOrders = await Payment.findById(req.params.id);
  getOrders.orderStatus = req.body.active || getOrders.orderStatus;
  if (req.body.active == 'Placed') {
    getOrders.isShippedDate = null;
    getOrders.isDeliveredDate = null;
    getOrders.isCancelledDate = null;
  } else if (req.body.active == 'Shipped') {
    getOrders.isShippedDate = new Date();
  } else if (req.body.active == 'Delivered') {
    getOrders.isDeliveredDate = new Date();
  } else if (req.body.active == 'Cancelled') {
    getOrders.isCancelledDate = new Date();
  }
  await getOrders.save();
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'Success',
  });
});

//get orderDetails by id
const getOrderDetailsById = asyncHandler(async (req, res) => {
  const getDetails = await Payment.aggregate([
    {
      $match: {
        $expr: {
          $eq: ['$_id', mongoose.Types.ObjectId(req.query.id)],
        },
      },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
      $unwind: '$user',
    },
    {
      $lookup: {
        from: 'parts',
        localField: 'part',
        foreignField: '_id',
        as: 'part',
      },
    },
    {
      $unwind: '$part',
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'part.category',
        foreignField: '_id',
        as: 'equipment',
      },
    },
    {
      $lookup: {
        from: 'subcategories',
        localField: 'part.subcategory',
        foreignField: '_id',
        as: 'equipmentModal',
      },
    },
  ]);

  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getDetails[0],
  });
});

//csvReport
const UserCSV = asyncHandler(async (req, res) => {
  const getUser = await User.aggregate([
    {
      $lookup: {
        from: 'admins',
        localField: 'buisnessManager',
        foreignField: '_id',
        as: 'buisnessManager',
      },
    },
    {
      $unwind: '$buisnessManager',
    },
    {
      $lookup: {
        from: 'userequipments',
        localField: '_id',
        foreignField: 'userID',
        as: 'equipments',
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'equipments.equipment',
        foreignField: '_id',
        as: 'equipments.equipment',
      },
    },
    {
      $lookup: {
        from: 'userequipmentmodals',
        localField: '_id',
        foreignField: 'userID',
        as: 'EquipmentModal',
      },
    },
    {
      $lookup: {
        from: 'subcategories',
        localField: 'EquipmentModal.equipmentModal',
        foreignField: '_id',
        as: 'EquipmentModal.equipmentModal',
      },
    },
    {
      $match: {
        $expr: {
          $eq:
            req.admin?.isSystemAdmin == false
              ? ['$buisnessManager._id', req.admin?._id]
              : ['', ''],
        },
      },
    },
  ]).sort({ _id: -1 });
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getUser,
  });
});

const ProductCSV = asyncHandler(async (req, res) => {
  const products = await Part.aggregate([
    {
      $lookup: {
        from: 'categories',
        localField: 'category',
        foreignField: '_id',
        as: 'equipment',
      },
    },
    {
      $unwind: '$equipment',
    },
    {
      $lookup: {
        from: 'subcategories',
        localField: 'subcategory',
        foreignField: '_id',
        as: 'equipmentmodal',
      },
    },
    {
      $unwind: '$equipmentmodal',
    },
  ]);

  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: products,
  });
});

const BaCSV = asyncHandler(async (req, res) => {
  const getBA = await Admin.find({
    $and: [{ isSystemAdmin: false }, { isDeleted: false }],
  });
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getBA,
  });
});

const EnquiryCSV = asyncHandler(async (req, res) => {
  const getEquiries = await Payment.aggregate([
    {
      $match: {
        $expr: {
          $eq: ['$for_Enquery', true],
        },
      },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
      $unwind: '$user',
    },
    {
      $lookup: {
        from: 'parts',
        localField: 'part',
        foreignField: '_id',
        as: 'part',
      },
    },
    {
      $unwind: '$part',
    },
  ]);
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getEquiries,
  });
});

const PaymentCSV = asyncHandler(async (req, res) => {
  const getPaymenCsv = await Payment.aggregate([
    {
      $match: {
        $expr: {
          $eq: ['$Payment_Status', 'Paid'],
        },
      },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
      $unwind: '$user',
    },
    {
      $lookup: {
        from: 'parts',
        localField: 'part',
        foreignField: '_id',
        as: 'part',
      },
    },
    {
      $unwind: '$part',
    },
  ]);
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getPaymenCsv,
  });
});

const OrderCSV = asyncHandler(async (req, res) => {
  const getOrders = await Payment.aggregate([
    {
      $match: {
        $expr: {
          $eq: ['$for_Enquery', false],
        },
      },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
      $unwind: '$user',
    },
    {
      $lookup: {
        from: 'parts',
        localField: 'part',
        foreignField: '_id',
        as: 'part',
      },
    },
    {
      $unwind: '$part',
    },
  ]);
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getOrders,
  });
});
const BusinessOrderCsv = asyncHandler(async (req, res) => {
  const getDetails = await Payment.aggregate([
    {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
      $lookup: {
        from: 'parts',
        localField: 'part',
        foreignField: '_id',
        as: 'part',
      },
    },
    {
      $unwind: {
        path: '$user',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $unwind: {
        path: '$part',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'part.category',
        foreignField: '_id',
        as: 'category',
      },
    },
    {
      $unwind: {
        path: '$category',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: 'subcategories',
        localField: 'part.subcategory',
        foreignField: '_id',
        as: 'subcategory',
      },
    },
    {
      $unwind: {
        path: '$subcategory',
        preserveNullAndEmptyArrays: true,
      },
    },
    // {
    //   $match:{
    //      $expr:{
    //        $eq:['$user.buisnessManager',mongoose.Types.ObjectId(req.admin._id)]
    //      }
    //   }
    // },
  ]);
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getDetails,
  });
});
const CmsCSV = asyncHandler(async (req, res) => {
  const getCms = await Page.find().populate('forPageName');
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getCms,
  });
});

const EquipemntCSV = asyncHandler(async (req, res) => {
  const getEquipment = await Category.find({ isDeleted: false });
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getEquipment,
  });
});

const getalluserPaginate = asyncHandler(async (req, res) => {
  const page = parseInt(req.query.page) || 1;
  const limit = 10;
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;
  // console.log('1391', req.query);
  const count1 = await User.aggregate([
    {
      $lookup: {
        from: 'admins',
        localField: 'buisnessManager',
        foreignField: '_id',
        as: 'buisnessManager',
      },
    },
    {
      $unwind: '$buisnessManager',
    },
    {
      $lookup: {
        from: 'userequipments',
        localField: '_id',
        foreignField: 'userID',
        as: 'equipments',
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'equipments.equipment',
        foreignField: '_id',
        as: 'equipments.equipment',
      },
    },
    {
      $lookup: {
        from: 'userequipmentmodals',
        localField: '_id',
        foreignField: 'userID',
        as: 'EquipmentModal',
      },
    },
    {
      $lookup: {
        from: 'subcategories',
        localField: 'EquipmentModal.equipmentModal',
        foreignField: '_id',
        as: 'EquipmentModal.equipmentModal',
      },
    },
    {
      $match: {
        $or:
          req.query.keyword != 'undefined' || req.query.keyword != ''
            ? [
                {
                  name: { $regex: req.query.keyword, $options: 'i' },
                },
                {
                  email: { $regex: req.query.keyword, $options: 'i' },
                },
                {
                  'buisnessManager.name': {
                    $regex: req.query.keyword,
                    $options: 'i',
                  },
                },
              ]
            : [{}],
      },
    },
    {
      $match: {
        $expr: {
          $eq:
            req.admin?.isSystemAdmin == false
              ? ['$buisnessManager._id', req.admin?._id]
              : ['', ''],
        },
      },
    },
    {
      $count: 'totalPage',
    },
  ]);
  var count = count1[0]?.totalPage !== undefined ? count1[0]?.totalPage : 0;
  const results = {};

  results.current = {
    page: page,
    limit: limit,
    totalPage: Math.ceil(count / limit),
  };

  if (endIndex < count) {
    results.next = {
      page: page + 1,
    };
  }

  if (startIndex > 0) {
    results.previous = {
      page: page - 1,
    };
  }

  results.results = await User.aggregate([
    {
      $lookup: {
        from: 'admins',
        localField: 'buisnessManager',
        foreignField: '_id',
        as: 'buisnessManager',
      },
    },
    {
      $unwind: '$buisnessManager',
    },
    {
      $lookup: {
        from: 'userequipments',
        localField: '_id',
        foreignField: 'userID',
        as: 'equipments',
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'equipments.equipment',
        foreignField: '_id',
        as: 'equipments.equipment',
      },
    },
    {
      $lookup: {
        from: 'userequipmentmodals',
        localField: '_id',
        foreignField: 'userID',
        as: 'EquipmentModal',
      },
    },
    {
      $lookup: {
        from: 'subcategories',
        localField: 'EquipmentModal.equipmentModal',
        foreignField: '_id',
        as: 'EquipmentModal.equipmentModal',
      },
    },
    {
      $match: {
        $or:
          req.query.keyword != 'undefined'
            ? [
                {
                  name: { $regex: req.query.keyword, $options: 'i' },
                },
                {
                  email: { $regex: req.query.keyword, $options: 'i' },
                },
                {
                  'buisnessManager.name': {
                    $regex: req.query.keyword,
                    $options: 'i',
                  },
                },
              ]
            : [{}],
      },
    },
    {
      $match: {
        $expr: {
          $eq:
            req.admin?.isSystemAdmin == false
              ? ['$buisnessManager._id', req.admin?._id]
              : ['', ''],
        },
      },
    },
  ])
    .sort({ _id: -1 })
    .skip(startIndex)
    .limit(limit);
  res.status(200).json({
    list: results,
  });
});

const EquipmentModal = asyncHandler(async (req, res) => {
  const getModal = await SubCategory.aggregate([
    {
      $match: {
        $expr: {
          $eq: ['$isDeleted', false],
        },
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'category',
        foreignField: '_id',
        as: 'category',
      },
    },
  ]);
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getModal,
  });
});

//create notification
const createNotification = asyncHandler(async (req, res) => {
  const { category, notification_text, notification_url } = req.body;
  // console.log(notification_url)
  const get_all =
    category == 'user'
      ? await User.find({ active: true }).select('_id name token')
      : await Admin.find({
          $and: [{ active: true }, { isSystemAdmin: false }],
        }).select('_id name token');

  get_all.map((el) => {
    SaveNotification(
      el._id,
      category == 'user' ? For_User : for_BA,
      {
        user: el?.name,
        msg: notification_text,
        url: notification_url,
      },
      null,
      true
    );

    if (category == 'user') {
      send_notification(
        el.token,
        'New notification',
        notification_url
          ? `${notification_text} ${notification_url}`
          : `${notification_text} `,
        'notification'
        // enquiry
      );
    }
  });
  res.status(201).json({
    errorCode: 201,
    errorMessage: 'success',
  });
});

//get all notification
const getAllCreateNotification = asyncHandler(async (req, res) => {
  let { page, size, query } = req.query;
  if (!page || page == '0') {
    page = 1;
  }

  if (!size) {
    size = 10;
  }
  const limit = parseInt(size);
  const skip = (page - 1) * size;

  const getAllCreated = await Notification.aggregate([
    {
      $match: {
        $expr: {
          $eq: ['$isCreatedByAdmin', true],
        },
      },
    },

    {
      $group: {
        _id: '$data.msg',
        type: { $first: '$type' },
        createdAt: { $first: '$createdAt' },
      },
    },
    {
      $sort: {
        _id: -1,
      },
    },
    {
      $facet: {
        total: [
          {
            $count: 'createdAt',
          },
        ],
        data: [
          {
            $addFields: {
              _id: '$_id',
            },
          },
        ],
      },
    },
    {
      $unwind: '$total',
    },
    {
      $project: {
        data: {
          $slice: [
            '$data',
            skip,
            {
              $ifNull: [limit, '$total.createdAt'],
            },
          ],
        },
        meta: {
          total: '$total.createdAt',
          limit: {
            $literal: limit,
          },
          page: {
            $literal: skip / limit + 1,
          },
          pages: {
            $ceil: {
              $divide: ['$total.createdAt', limit],
            },
          },
        },
      },
    },
  ]);

  res.status(200).json({
    successCode: 200,
    successMessage: 'admin created notification',
    list: getAllCreated,
  });
});

//function for notification read count done
const getList = (arr) => {
  arr?.map(async (el) => {
    el.generateCount = 0;
    await el.save();
  });
};

//get business admin notification
const getBusAdminNotification = asyncHandler(async (req, res) => {
  let { page, size, query } = req.query;
  if (!page || page == '0') {
    page = 1;
  }

  if (!size) {
    size = 100;
  }
  const limit = parseInt(size);
  const skip = (page - 1) * size;
  const totalRecord = await Notification.countDocuments({ user: req.admin.id });
  const totalPage = Math.ceil(totalRecord / limit);

  const getNotification = await Notification.find({ user: req.admin.id })
    .sort({ _id: -1 })
    .skip(skip)
    .limit(limit);
  // await getList(getNotification);
  res.status(200).json({
    successMessage: 'Business Admin notification',
    successCode: 200,
    list: getNotification,
    page,
    size,
    totalRecord,
    totalPage,
  });
});

const setMarkReadNotificatin = asyncHandler(async (req, res) => {
  const { id } = req.body;
  const getNotifications = await Notification.findByIdAndUpdate(id, {
    generateCount: 0,
  });

  res.status(200).json({
    successCode: 200,
    successMessage: 'Notification mark read update',
    list: getNotifications,
  });
});

//read allnotification
const setMarkReadAllNotificatin = asyncHandler(async (req, res) => {
  const getNotification = await Notification.find({
    $and: [{ user: req.admin.id }, { generateCount: 1 }],
  });
  await getList(getNotification);
  res.status(200).json({
    successCode: 200,
    successMessage: 'Notification mark read update',
    list: getNotification,
  });
});

//unread notification for business admin
const unreadNotification = asyncHandler(async (req, res) => {
  const getNotification = await Notification.countDocuments({
    $and: [{ user: req.admin.id }, { generateCount: 1 }],
  });
  res.status(200).json({
    successMessage: 'Business Admin notification',
    successCode: 200,
    list: getNotification,
  });
});

export {
  getOrderDetailsById,
  Order_Del_Status,
  authAdmin,
  GetReports,
  GetReportsGraphs,
  getAllUser,
  createUser,
  updateUser,
  getUserById,
  updateUserStatus,
  deleteUser,
  getAllAdmin,
  getAdminById,
  createBuisnessAdmin,
  updateAdmin,
  updateAdminStatus,
  deleteAdmin,
  adminDashbooardGraph,
  getAdminByEachId,
  updateAdminByEachId,
  getadminByuserAssosiated,
  AdminProfileLists,
  BusinessAdminGraphApi,
  EnquiryList,
  EnquiryListUpdate,
  PaymentList,
  paginateCategory,
  PaymentListBusinessAdminId,
  GetDetalsByids,
  //csvdata
  UserCSV,
  BaCSV,
  ProductCSV,
  EnquiryCSV,
  PaymentCSV,
  OrderCSV,
  BusinessOrderCsv,
  CmsCSV,
  EquipemntCSV,
  EquipmentModal,
  getalluserPaginate,
  adminDashboard,
  businessAdminDashboard,
  createNotification,
  getBusAdminNotification,
  unreadNotification,
  getAllCreateNotification,
  setMarkReadNotificatin,
  setMarkReadAllNotificatin,
};
