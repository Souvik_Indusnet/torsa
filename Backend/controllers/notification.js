import Notification from '../models/notificationlist.js';
import mongoose from 'mongoose';

import { fetchNotificationBody } from '../utils/notificationbody.js';
import {
  Enquiry_from_User,
  Payment_by_User,
  Last_Payment_Oneshot_Payment,
  Payment_Due_Admin,
  Enquiry_Approved,
  Payment_by_User1,
  Payment_Due_User,
  Order_Shipped,
  Order_Delivered,
  Order_Cancelled,
  Enquiry_Updated,
  for_BA,
  For_User,
} from '../constant/notificationconstant.js';
import Payment from '../models/payment.js';
const getNotification = async (req, res) => {
  try {
    var getAllNotification = [];
    var notification_count = 0;
    var getAllNotification1 = [];
    var UserType =
      req.admin != null && req.admin.isSystemAdmin == true
        ? 'Admin'
        : req.admin != null && req.admin.isSystemAdmin == false
        ? 'SubAdmin'
        : req.user != null
        ? 'User'
        : undefined;
    if (UserType == 'Admin') {
      var temp = [
        Enquiry_from_User,
        Payment_by_User,
        Last_Payment_Oneshot_Payment,
        Payment_Due_Admin,
      ];
      getAllNotification = await Notification.find({
        type: { $in: temp },
      }).sort({ _id: -1 });
      for (var i = 0; i < getAllNotification.length; i++) {
        var data = await Payment.findById(
          getAllNotification[i].order_id
        ).populate('part user');
        getAllNotification1.push(fetchNotificationBody(getAllNotification[i]));
      }
    } else if (UserType == 'SubAdmin') {
      var temp = [
        Enquiry_from_User,
        Payment_by_User,
        Last_Payment_Oneshot_Payment,
        Payment_Due_Admin,
        for_BA,
      ];
      getAllNotification = await Notification.find({ type: { $in: temp } });
      for (var i = 0; i < getAllNotification.length; i++) {
        var data = await Payment.findById(
          getAllNotification[i].order_id
        ).populate('part user');
        getAllNotification1.push(fetchNotificationBody(getAllNotification[i]));
      }
    } else if (UserType == 'User') {
      var temp = [
        Last_Payment_Oneshot_Payment,
        Enquiry_Updated,
        Enquiry_Approved,
        Payment_by_User1,
        Payment_Due_User,
        Order_Shipped,
        Order_Delivered,
        Order_Cancelled,
        For_User,
      ];
      getAllNotification = await Notification.aggregate([
        // {
        //   $match: { type: { $in: temp } },
        // },
        {
          $match: {
            $expr: {
              $eq: [mongoose.Types.ObjectId(req.user._id), '$user'],
            },
          },
        },
        {
          $lookup: {
            from: 'payments',
            localField: 'order_id',
            foreignField: '_id',
            as: 'order',
          },
        },
        {
          $unwind: { path: '$order', preserveNullAndEmptyArrays: true },
        },
        // {
        //   $match: {
        //     $expr: {
        //       $eq: ['$order.user', req.user._id],
        //     },
        //   },
        // },
        {
          $lookup: {
            from: 'users',
            localField: 'order.user',
            foreignField: '_id',
            as: 'order.user',
          },
        },
        {
          $lookup: {
            from: 'parts',
            localField: 'order.part',
            foreignField: '_id',
            as: 'order.part',
          },
        },
        {
          $unwind: { path: '$order.part', preserveNullAndEmptyArrays: true },
          // preserveNullAndEmptyArrays: true,
        },
        {
          $unwind: { path: '$order.user', preserveNullAndEmptyArrays: true },
          // preserveNullAndEmptyArrays: true,
        },
        {
          $sort: {
            _id: -1,
          },
        },
      ]);
      var count = await Notification.aggregate([
        // {
        //   $match: { type: { $in: temp } },
        // },
        {
          $match: {
            $expr: {
              $eq: [mongoose.Types.ObjectId(req.user._id), '$user'],
            },
          },
        },
        {
          $lookup: {
            from: 'payments',
            localField: 'order_id',
            foreignField: '_id',
            as: 'order',
          },
        },
        {
          $unwind: { path: '$order', preserveNullAndEmptyArrays: true },
          // preserveNullAndEmptyArrays: true,
        },
        // {
        //   $match: {
        //     $expr: {
        //       $eq: ['$order.user', req.user._id],
        //     },
        //   },
        // },
        {
          $match: {
            $expr: {
              $eq: ['$isRead', false],
            },
          },
        },
        {
          $count: 'total',
        },
      ]);
      notification_count = count[0] != undefined ? count[0].total : 0;
      for (var i = 0; i < getAllNotification.length; i++) {
        getAllNotification1.push({
          body: fetchNotificationBody(getAllNotification[i]),
          data: getAllNotification[i].order || getAllNotification[i].data,
          isRead: getAllNotification[i].isRead,
          _id: getAllNotification[i]._id,
          full: getAllNotification[i],
        });
      }
    }
    getAllNotification = getAllNotification1;
    if (getAllNotification) {
      res.status(200).json({
        errorcode: 1,
        errormessage:
          getAllNotification.length > 0 ? 'Records found' : 'Empty Record',
        list: getAllNotification,
        unread_notification_count: notification_count,
      });
    } else {
      res.status(500).json({
        errorcode: 0,
        errormessage: 'Server Error',
      });
    }
  } catch (error) {
    res.status(500).json({
      errorCode: 0,
      errorMessage: error.message,
    });
  }
};

const fireNotification = async (req, res) => {
  try {
    const GetAllNotification = await Notification.find({
      userID: req.query?.id || req.user._id,
    }).select('-_id -userID');
    const getCOunt = GetAllNotification.map((i) => i.generateCount);
    const FireNot = getCOunt && getCOunt.reduce((a, c) => a + c, 0);
    if (FireNot) {
      res.status(200).json({
        errorcode: 0,
        message: 'new Notification',
        newNotification: FireNot,
      });
    } else {
      res.status(200).json({
        errorcode: 1,
        message: 'No notification',
        newNotification: FireNot,
      });
    }
  } catch (error) {
    res.status(500).json({
      errorCode: 0,
      errorMessage: error.message,
    });
  }
};
const getNotificationbyBusinessAdmin = async (req, res) => {
  try {
    // console.log('fgdfgdfg', req.query.id);
    const getAllNote = await Notification.find({
      userID: req.query?.id || req.user.id,
    }).sort({ _id: -1 });
    getAllNote.filter((i) => {
      i.generateCount = 0;
      return i;
    });

    getAllNote.map(async (i, index) => {
      await getAllNote[index].save();
      return true;
    });

    if (getAllNote) {
      // await getAllNote.save();
      res.status(200).json({
        errorcode: 1,
        errormessage: getAllNote.length > 0 ? 'Records found' : 'Empty Record',
        list: getAllNote,
      });
    } else {
      res.status(500).json({
        errorcode: 0,
        errormessage: 'Server Error',
      });
    }
  } catch (error) {
    res.status(500).json({
      errorCode: 0,
      errorMessage: error.message,
    });
  }
};
const getReadNotification = async (req, res) => {
  var { type, id } = req.body;
  const notification = await Notification.findById(id);
  notification.isRead = true;
  await notification.save();
  // var temp = [
  //   Enquiry_Approved,
  //   Payment_by_User1,
  //   Payment_Due_User,
  //   Order_Shipped,
  //   Order_Delivered,
  //   Order_Cancelled,
  // ];
  // if (type) {
  //   var notifiation = await Notification.find({
  //     type: { $in: temp },
  //   })
  //     .populate('order_id')
  //     .sort({ _id: -1 });
  //   notifiation = notifiation
  //     .filter((p) => {
  //       return p.order_id.user.toString() == req.user._id.toString();
  //     })
  //     .forEach(async (p) => {
  //       await Notification.updateMany({ _id: p._id }, { isRead: true });
  //     });
  // } else {
  //   await Notification.updateMany({ _id: id }, { isRead: true });
  // }
  res.status(200).json({
    errorcode: 1,
    errormessage: 'Success',
  });
};
export {
  getNotification,
  fireNotification,
  getNotificationbyBusinessAdmin,
  getReadNotification,
};
