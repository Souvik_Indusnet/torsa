import { asyncHandler } from '../middleware/errorMiddleware.js';
import { Page,
  // PageName 
  } from '../models/cmsModel.js';

//********************Create PageNames......................
const createPageName = async (req, res) => {
  const { pageName } = req.body;
  if (!pageName) return res.status(404).json({ message: 'Name Not Found' });
  try {
    const result = await PageName.create(req.body);
    res.status(200).json({ result });
  } catch (error) {
    if (error) res.status(400).json(error);
  }
};
const getAllPageName = async (req, res) => {
  try {
    const resultData = await PageName.find().select('-__v');
    if (resultData) res.status(200).json({ resultData });
  } catch (error) {
    if (error) res.status(400).json({ message: 'Something went Wrong!!!' });
  }
};

//********************Create Pages......................
const createPage = asyncHandler(async (req, res) => {
  const { forPageName, content } = req.body;
  const getexist = await Page.findOne({ forPageName: forPageName });
  if (!getexist) {
    const newpage = await Page.create({
      forPageName,
      content,
    });
    return res.status(200).json({
      successCode: 200,
      successMessage: 'created',
      list: newpage,
    });
  } else {
    return res.status(404).json({ message: 'Page already exists' });
  }

  // if (!forPageName)
  //   return res.status(404).json({ message: 'PageName Not Found' });
  // try {
  //   const result = await Page.create(req.body);
  //   if (result) {
  //     res.status(200).json({ result });
  //   }
  // } catch (error) {
  //   if (error.code === 11000)
  //     return res.status(400).json({ message: 'PageName already defined!!' });
  //   if (error) res.status(400).json(error);
  // }
});

const getAllPage = async (req, res) => {
  console.log('cmspag all');
  try {
    const resultData = await Page.find().select('-__v');
    if (resultData) res.status(200).json({ resultData });
  } catch (error) {
    if (error) res.status(400).json({ message: 'Something went Wrong!!!' });
  }
};

const getOnePage = async (req, res) => {
  try {
    const { id } = req.query;
    if (!id) return res.status(404).json({ message: 'ID Not Found' });
    const resultData = await Page.findById(id)
      .populate({
        path: 'forPageName',
        select: 'pageName',
      })
      .select('-__v');
    if (resultData) res.status(200).json({ resultData });
  } catch (error) {
    if (error) res.status(400).json({ message: 'Something went Wrong!!!' });
  }
};

const editPage = asyncHandler(async (req, res) => {
  const { editID, forPageName, content } = req.body;
  const existspage = await Page.findOne({ forPageName: forPageName });
  // console.log(existspage.forPageName !== forPageName);
  if (existspage && existspage?.forPageName !== forPageName) {
    return res.status(404).json({ message: 'Page already exists' });
  } else {
    const resultData = await Page.findById(editID);
    resultData.forPageName = forPageName || resultData.forPageName;
    resultData.content = content || resultData.content;
    await resultData.save();
    res.status(200).json({ status: 'Updated!!!' });
  }
  // if (!editID) return res.status(404).json({ message: 'ID NOT FOUND!!!' });
  // const getexist = await Page.findOne({ forPageName: forPageName });
  // if (getexist) return res.status(404).json({ message: 'Page already exists' });

  // const resultData = await Page.findByIdAndUpdate(
  //   editID,
  //   forPageName,
  //   content,
  //   {
  //     new: true,
  //   }
  // );
  // res.status(200).json({ status: 'Updated!!!' });
});

const deletePage = async (req, res) => {
  const { id } = req.body;
  if (!id) return res.status(404).json({ message: 'ID NOT FOUND!!!' });
  try {
    const resultData = await Page.findByIdAndDelete(id);
    if (resultData) res.status(200).json({ status: 'Deleted!!!' });
  } catch (error) {
    if (error) res.status(400).json({ message: 'Something went Wrong!!!' });
  }
};

const statusUpdate = async (req, res) => {
  const { status, id } = req.body;

  try {
    const resultData = await Page.findById(id);

    resultData.status = status == false ? false : status;
    await resultData.save();

    if (resultData) res.status(200).json({ resultData });
  } catch (error) {
    if (error) res.status(400).json({ message: 'Something went Wrong!!!' });
  }
};

const getCmsbyString = asyncHandler(async (req, res) => {
  const { cmspage } = req.query;

  const detailspage = await Page.aggregate([
    {
      $match: {
        forPageName: { $regex: cmspage, $options: 'i' },
      },
    },
  ]);
  res.status(200).json({
    successCode: 200,
    successMessage: 'success',
    detail: detailspage,
  });
});

export {
  createPageName,
  getAllPageName,
  createPage,
  getAllPage,
  getOnePage,
  editPage,
  deletePage,
  statusUpdate,
  getCmsbyString,
};
