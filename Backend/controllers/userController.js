import { asyncHandler } from '../middleware/errorMiddleware.js';
import generateToken from '../utils/generateToken.js';
import User from '../models/userModel.js';
import Machine from '../models/machineModel.js';
import generateOtp from '../utils/generateOtp.js';
import Order from '../models/orderModel.js';
import { orderNumber } from '../utils/orderNumber.js';
import Part from '../models/partModel.js';
import bcrypt from 'bcryptjs';
import PartNotification from '../models/notificationlist.js';
import { generatePassword } from '../utils/passwordGenerate.js';
import { transport, mailOption } from '../utils/emailService.js';
import equiryDetails from '../models/payment.js';
// import askforprice from '../models/ask.notification.js';
import Payment from '../models/payment.js';
import { SaveNotification } from '../utils/saveNotification.js';
import {
  Payment_by_User,
  Payment_by_User1,
  Payment_Due_Admin,
  Payment_Due_User,
  Enquiry_from_User,
  Order_Shipped,
  Order_Delivered,
  Order_Cancelled,
  Last_Payment_Oneshot_Payment,
} from '../constant/notificationconstant.js';
import Admin from '../models/Admin.js';
import mongoose from 'mongoose';
import UserEquipment from '../models/userEquipment.js';
import UserEquipmentModal from '../models/userEquipmentModal.js';
import TransactionDetailModal from '../models/TransactionDetails.js';
import Usercart from '../models/userCart.js';
import { MailJet } from '../utils/mailjet.js';
import UserSignList from '../models/SignInList.js';
import { notificationType } from '../utils/notificationname.js';
import { send_notification } from '../utils/sendAndroidNotification.js';
import Razorpay from 'razorpay';
import crypto from 'crypto';
var instance = new Razorpay({
  key_id: 'rzp_test_k93iFQaQFiSZOG',
  key_secret: 'XU3rP2gWBytUTW7vrTXkRlbY',
});

// @desc Auth User & get token
// @route GET /user/login
// @access Public
const payment = asyncHandler(async (req, res) => {
  var data = req.body;
  console.log(req.body.amount);
  var options = {
    amount: parseInt(data.amount) * 100, // amount in the smallest currency unit
    currency: 'INR',
    receipt: 'rcpt',
  };
  instance.orders.create(options, function (err, order) {
    console.log(order);
    return res.send({ orderId: order.id });
  });
});
const paymentSignatureVerification = asyncHandler(async (req, res) => {
  let body =
    req.body.response.razorpay_order_id +
    '|' +
    req.body.response.razorpay_payment_id;
  var expectedSignature = crypto
    .createHmac('sha256', 'XU3rP2gWBytUTW7vrTXkRlbY')
    .update(body.toString())
    .digest('hex');
  console.log('sig received ', req.body.response.razorpay_signature);
  console.log('sig generated ', expectedSignature);
  var response = { signatureIsValid: 'false' };
  if (expectedSignature === req.body.response.razorpay_signature)
    response = { signatureIsValid: 'true' };
  res.send(response);
});
const authUser = asyncHandler(async (req, res) => {
  const { email, password, token } = req.body;
  console.log('79................email', email);
  const user = await User.findOne({ $and: [{ email }, { isDeleted: false }] });
  console.log('80...............', user);
  if (user) {
    if (!user.active) {
      return res.status(401).json({
        errorCode: 401,
        invalidtype: 'inactive',
        errorMessage: 'user status inactive from admin',
      });
    }
    if (await user.matchPassword(password)) {
      user.token = token;
      await user.save();
      await UserSignList.create({ user: user._id });
      return user.BillingAdress.Name != undefined &&
        user.address.Name != undefined
        ? res.status(200).json({
            _id: user._id,
            name: user.name,
            email: user.email,
            image: user.image,
            contactNo: user.contactNo,
            address: user.address,
            BillingAdress: user.BillingAdress,
            token: generateToken(user._id),
          })
        : res.status(200).json({
            _id: user._id,
            name: user.name,
            email: user.email,
            image: user.image,
            contactNo: user.contactNo,
            token: generateToken(user._id),
          });

      // return res.status(200).json({
      //   _id: user._id,
      //   name: user.name,
      //   email: user.email,
      //   image: user.image,
      //   contactNo: user.contactNo,
      //   token: generateToken(user._id),
      // });
    } else {
      return res.status(401).json({
        errorCode: 401,
        invalidtype: 'password',
        errorMessage: 'Invalid password',
      });
    }
  } else {
    return res.status(401).json({
      errorCode: 401,
      invalidtype: 'email',
      errorMessage: 'Invalid email',
    });
  }
});
const authUserSignout = asyncHandler(async (req, res) => {
  await User.updateOne({ _id: req.user._id }, { $unset: { token: '' } });
  return res.status(401).json({
    errorCode: 200,
    errorMessage: 'Success',
  });
});
const postChangeUserPassword = asyncHandler(async (req, res) => {
  var { userID, Oldpassword, NewPassword } = req.body;
  const user = await User.findById(userID);
  if (user) {
    if (!user.active) {
      return res.status(401).json({
        errorCode: 401,
        invalidtype: 'inactive',
        errorMessage: 'user status inactive from admin',
      });
    }
    if (await user.matchPassword(Oldpassword)) {
      user.password = NewPassword;
      await user.save();
      return res.status(200).json({
        errorCode: 200,
        errorMessage: 'Password Changed',
      });
    } else {
      return res.status(401).json({
        errorCode: 200,
        errorMessage: 'Invalid User',
      });
    }
  } else {
    return res.status(401).json({
      errorCode: 401,
      errorMessage: 'Unauthorised Access',
    });
  }
});
const postUpdateUserAddress = asyncHandler(async (req, res) => {
  var { userID, BillingAdress, ShippingAddress } = req.body;
  var updateAddress = await User.findById(userID);
  if (updateAddress) {
    updateAddress.address = ShippingAddress;
    updateAddress.BillingAdress = BillingAdress;
    await updateAddress.save();
    return res.status(200).json(updateAddress);
  } else {
    return res.status(401).json({
      errorCode: 401,
      errorMessage: 'Unauthorised Access',
    });
  }
});
// get machine
//@secure by user
// route /user/machine
//use dashboard after the login
const getUserEquipments = asyncHandler(async (req, res) => {
  console.log(req.user.id);
  const getEquipmentList = await UserEquipment.aggregate([
    {
      $match: {
        $expr: {
          $eq: [mongoose.Types.ObjectId(req.user.id), '$userID'],
        },
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'equipment',
        foreignField: '_id',
        as: 'equipment',
      },
    },

    {
      $unwind: '$equipment',
    },
    {
      $match: {
        $expr: {
          $eq: ['$equipment.active', true],
        },
      },
    },
  ]);
  res.status(200).json({
    successCode: 200,
    successMessage: 'success',
    list: getEquipmentList,
  });
});

const GetEquipmentModalByEquipment = asyncHandler(async (req, res) => {
  const { equipment } = req.query;
  const getModals = await UserEquipmentModal.aggregate([
    {
      $match: {
        $expr: {
          $eq: [mongoose.Types.ObjectId(req.user.id), '$userID'],
        },
      },
    },
    {
      $lookup: {
        from: 'subcategories',
        localField: 'equipmentModal',
        foreignField: '_id',
        as: 'equipmentModal',
      },
    },
    {
      $unwind: '$equipmentModal',
    },
    {
      $match: {
        $expr: {
          $eq: ['$equipmentModal.active', true],
          $eq: ['$equipmentModal.category', mongoose.Types.ObjectId(equipment)],
        },
      },
    },
  ]);

  res.status(200).json({
    successCode: 200,
    successMessage: 'success',
    list: getModals,
  });
});

const getAllPartsByEq_Modals = asyncHandler(async (req, res) => {
  const { equipment, equipmentmodal, name } = req.body;
  // const { equipmentmodal } = req.body;
  if (equipment && equipmentmodal.length !== 0) {
    var equipmentmodal1 = equipmentmodal?.map((p) =>
      mongoose.Types.ObjectId(p)
    );
  }
  const getParts = await Part.aggregate([
    {
      $match:
        equipment && equipmentmodal.length !== 0
          ? {
              $expr: {
                $and: [
                  { $eq: ['$category', mongoose.Types.ObjectId(equipment)] },
                  { $in: ['$subcategory', equipmentmodal1] },
                  { $eq: ['$active', true] },
                  { $gte: ['$inventoryStock', 1] },
                  { $eq: ['$isDeleted', false] },
                  { $gt: ['$amount', 0] },
                ],
              },
            }
          : equipment && equipmentmodal.length == 0
          ? {
              $expr: {
                $and: [
                  { $eq: ['$category', mongoose.Types.ObjectId(equipment)] },

                  { $eq: ['$active', true] },
                  { $gte: ['$inventoryStock', 1] },
                  { $eq: ['$isDeleted', false] },
                  { $gt: ['$amount', 0] },
                ],
              },
            }
          : {
              $expr: {
                $and: [
                  { $eq: ['$active', true] },
                  { $gte: ['$inventoryStock', 1] },
                  { $eq: ['$isDeleted', false] },
                  { $gt: ['$amount', 0] },
                ],
              },
            },
    },
    {
      $lookup: {
        from: 'userequipmentmodals',
        localField: 'subcategory',
        foreignField: 'equipmentModal',
        as: 'user_equipment',
      },
    },
    {
      $unwind: {
        path: '$user_equipment',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: 'usercarts',
        let: {
          partId: '$_id',
          userID: '$user_equipment.userID',
        },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$userID', '$$userID'] },
                  { $eq: ['$part', '$$partId'] },
                ],
              },
            },
          },
        ],
        as: 'cart',
      },
    },
    {
      $lookup: {
        from: 'payments',
        let: {
          partId: '$_id',
          userID: '$user_equipment.userID',
        },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$user', '$$userID'] },
                  { $eq: ['$part', '$$partId'] },
                ],
              },
            },
          },
        ],
        as: 'enquiry',
      },
    },
    {
      $lookup: {
        from: 'payments',
        let: {
          partId: '$_id',
          userID: '$user_equipment.userID',
        },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$user', '$$userID'] },
                  { $eq: ['$part', '$$partId'] },
                  { $eq: ['$isApproved', 'pending'] },
                ],
              },
            },
          },
        ],
        as: 'enquiry1',
      },
    },

    // {
    //   $unwind:
    //   {
    //     path:'$cart',
    //     preserveNullAndEmptyArrays:true,
    //   }
    // },
    // {
    //   $unwind: {
    //     path: "$enquiry",
    //     preserveNullAndEmptyArrays: true,
    //   },
    // },
    {
      $addFields: {
        inCart: {
          $cond: {
            if: { $and: [{ $eq: ['$cart', []] }] },
            then: false,
            else: true,
          },
        },
        inEnquiry: {
          $cond: {
            if: {
              $or: [
                { $eq: ['$enquiry1', []] },
                // { $eq: ["$enquiry", null] },
                // {
                //   $and: [
                //     { $eq: ["$enquiry.isApproved", "pending"] },
                //     // { $eq: ["$enquiry.for_Enquery", true] },
                //   ],
                // },
              ],
            },
            then: false,
            else: true,
          },
        },
      },
    },
    {
      $match: {
        $expr: {
          $and: [
            {
              $eq: [
                '$user_equipment.userID',
                mongoose.Types.ObjectId(req.user.id),
              ],
            },
            // // { $eq: ['$cart.userID', mongoose.Types.ObjectId(req.user.id)]}
          ],
        },
      },
    },
    {
      $match: {
        $expr: {
          $regexMatch: {
            input: { $toString: '$partName' },
            regex: name,
            options: 'i',
          },
        },
      },
    },
    // {
    //   $project:{
    //     _id: 1,
    //     image: 1,
    //     partName:1,
    //     category:1,
    //     subcategory:1,
    //     description:1,
    //     amount:1,
    //     inventoryStock:1,
    //     createdAt:1,
    //     user_equipment:1,
    //     inCart:1,
    //     inEnquiry:1,
    //   }
    // }
  ]);
  // console.log("209", getParts.length);
  res.status(200).json({
    successCode: 200,
    successMessage: 'success',
    list: getParts,
  });
});

const getProductDetailsByID = asyncHandler(async (req, res) => {
  const { product } = req.params;
  const getDetails = await Part.aggregate([
    {
      $match: {
        $expr: {
          $eq: [mongoose.Types.ObjectId(product), '$_id'],
        },
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'category',
        foreignField: '_id',
        as: 'Equipment',
      },
    },
    { $unwind: '$Equipment' },
    {
      $lookup: {
        from: 'subcategories',
        localField: 'subcategory',
        foreignField: '_id',
        as: 'Equipmentmodal',
      },
    },
    { $unwind: '$Equipmentmodal' },
  ]);
  res.status(200).json({
    successCode: 200,
    successMessage: 'success',
    list: getDetails[0],
  });
});

const AddProductTomyCart = asyncHandler(async (req, res) => {
  const { qty, part } = req.body;
  const findSameData = await Usercart.findOne({
    $and: [{ userID: req.user.id }, { part: part }],
  });
  if (!findSameData) {
    const addmycart = await Usercart.create({ userID: req.user.id, qty, part });
    res.status(200).json({
      successCode: 200,
      successMessage: 'success',
      list: addmycart,
    });
  } else {
    res.status(404).json({
      errorCode: 404,
      errorMessage: 'Already in your cart',
    });
  }
});

const CartProductActions = asyncHandler(async (req, res) => {
  const { qty, part } = req.body;
  const findSameData = await Usercart.findOne({
    $and: [{ userID: req.user.id }, { part: part }],
  });
  findSameData.qty = qty || findSameData.qty;
  await findSameData.save();
  res.status(200).json({
    successCode: 200,
    successMessage: 'success',
    list: findSameData,
  });
});

const removeProductFromMyCart = asyncHandler(async (req, res) => {
  const { part } = req.body;
  await Usercart.findOneAndDelete({
    $and: [{ userID: req.user.id }, { part: part }],
  });
  res.status(200).json({
    successCode: 200,
    successMessage: 'delete success',
  });
});

const getMycart = asyncHandler(async (req, res) => {
  const mycarts = await Usercart.aggregate([
    {
      $match: {
        $expr: {
          $eq: ['$userID', mongoose.Types.ObjectId(req.user.id)],
        },
      },
    },
    {
      $lookup: {
        from: 'parts',
        localField: 'part',
        foreignField: '_id',
        as: 'part',
      },
    },
    {
      $unwind: '$part',
    },
  ]);
  const mycartscount = await Usercart.aggregate([
    {
      $match: {
        $expr: {
          $eq: ['$userID', mongoose.Types.ObjectId(req.user.id)],
        },
      },
    },
    {
      $lookup: {
        from: 'parts',
        localField: 'part',
        foreignField: '_id',
        as: 'part',
      },
    },
    {
      $unwind: '$part',
    },
    {
      $count: 'cartscount',
    },
  ]);
  res.status(200).json({
    successCode: 200,
    successMessage: ' success',
    list: mycarts,
    count: mycartscount[0] != undefined ? mycartscount[0].cartscount : 0,
  });
});

const SendAskForPrice = asyncHandler(async (req, res) => {
  const { list } = req.body;
  console.log('275', list);

  if (list.length != 0) {
    list.forEach(async (p, i) => {
      const { part, qty, type_of_payment } = p;
      var payment_date =
        new Date().getFullYear() +
        '-' +
        new Date().getMonth() +
        '-' +
        new Date().getDate();
      const partById = await Part.findById(part);
      var enquiry;
      if (type_of_payment == undefined) {
        enquiry = await Payment.create({
          user: req.user.id,
          part: part,
          qty: qty,
          Partial_Remaining_Amount: partById.amount,
        });
        await Usercart.deleteOne({ part: part, userID: req.user._id });
      } else {
        enquiry = await Payment.create({
          user: req.user.id,
          part: part,
          qty: qty,
          // Type_Of_Payment: type_of_payment,
          Partial_Date: payment_date,
          Partial_Remaining_Amount: partById.amount,
        });
        await Usercart.deleteOne({ part: part, userID: req.user._id });
        var schedule = require('node-schedule');
        var date = payment_date.split('/');
        var date = new Date(date[0], date[1], date[2], 12, 0, 0);
        var j = schedule.scheduleJob(date, async function () {
          var data = {
            user: req.user.name,
            Due_date: payment_date,
          };
          SaveNotification(req.user.id, Payment_Due_Admin, data, enquiry._id);
          SaveNotification(req.user.id, Payment_Due_User, data, enquiry._id);
          var body = notificationType({
            type: Payment_Due_User,
            user: data.user,
            Due_date: data.Due_date,
          });
          var user = await User.findById(req.user.id);
          send_notification(
            user.token,
            body.title,
            body.text,
            'Payment',
            enquiry
          );
        });
      }
      var data = {
        user: req.user.name,
      };
      SaveNotification(req.user.id, Enquiry_from_User, data, enquiry._id);
      // var body = notificationType({type: Payment_Due_User,user: data.user,Due_date: payment_date,});
      // var user = await User.findById(req.user.id);
      // send_notification(user.token,body.title,body.text,"Payment",enquiry);
    });
    res.status(201).json({
      successCode: 201,
      successMessage: 'enquiry send to admin',
    });
  } else {
    res.status(404).json({
      errorCode: 204,
      errorMessage: 'Please add Parts in the Cart',
    });
  }
});

const getMyEnquiry = asyncHandler(async (req, res) => {
  var getEnquiries = await Payment.find({
    $and: [
      { user: req.user.id },
      { Delivered: false },
      { isDeleted: false },
      { for_Enquery: true },
    ],
  })
    .populate({ path: 'user', select: 'name email contactNo' })
    .populate({ path: 'part', select: 'partName image amount' })
    .select(
      'qty Installment createdAt isApproved Partial_Amount_Paid Partial_Remaining_Amount Partial_Remaining_Amount_in_percentage Partial_Amount_Paid_in_percentage Type_Of_Payment'
    )
    .sort({ _id: -1 });
  res.status(200).json({
    successCode: 200,
    successMessage: 'enquiry  list ',
    list: getEnquiries,
  });
});

//user order controller

const getMyOrder = asyncHandler(async (req, res) => {
  const { Orderquery } = req.query;
  const FilterBody = ['Placed', 'Ready_to_ship', 'Shipped', 'Delivered'];
  // console.log(Orderquery);
  var temp = false;
  if (Orderquery != undefined) {
    temp = true;
  }
  const getrders = await Payment.aggregate([
    {
      $match: {
        $expr: {
          $and: [
            { $eq: ['$user', mongoose.Types.ObjectId(req.user.id)] },
            { $eq: ['$for_Enquery', false] },
            { $eq: ['$isApproved', 'approved'] },
            // { $eq: ['$orderStatus', temp ? FilterBody[Orderquery] : null],},
          ],
        },
      },
    },
    {
      $lookup: {
        from: 'parts',
        localField: 'part',
        foreignField: '_id',
        as: 'part',
      },
    },
    { $unwind: '$part' },
    {
      $project: {
        'part.partName': 1,
        'part.image': 1,
        'part.amount': 1,
        orderStatus: 1,
        isShippedDate: 1,
        isDeliveredDate: 1,
        isCancelledDate: 1,
        qty: 1,
        partName: 1,
        image: 1,
        amount: 1,
        createdAt: 1,
        updatedAt: 1,
        Type_Of_Payment: 1,
      },
    },
  ]).sort({ _id: -1 });
  console.log('375');
  res.status(200).json({
    successCode: 200,
    successMessage: 'order  list ',
    list: getrders,
  });
});

// @desc Get user Profile
// @route GET /user/profile
// @access Private
const getUserProfile = asyncHandler(async (req, res) => {
  if (req.user) {
    const userDetails = await User.findById(req.user._id);
    // .select('image name email');
    // .populate("equipment")
    // .populate("machines")
    // .populate("buisnessManager");
    if (userDetails) {
      res.json(userDetails);
    } else {
      res.status(401);
      throw new Error('User Not Found');
    }
  } else {
    res.status(401);
    throw new Error('server Error');
  }
});

//get userdetails by id
const getUserDetailsById = asyncHandler(async (req, res) => {
  const UserDetails = await User.aggregate([
    {
      $match: {
        $expr: {
          $eq: ['$_id', mongoose.Types.ObjectId(req.query.id)],
        },
      },
    },
    {
      $lookup: {
        from: 'userequipments',
        localField: '_id',
        foreignField: 'userID',
        as: 'equipments',
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'equipments.equipment',
        foreignField: '_id',
        as: 'equipments.equipment',
      },
    },
    {
      $lookup: {
        from: 'admins',
        localField: 'buisnessManager',
        foreignField: '_id',
        as: 'businessadmin',
      },
    },
    {
      $unwind: '$businessadmin',
    },
    {
      $lookup: {
        from: 'userequipmentmodals',
        localField: '_id',
        foreignField: 'userID',
        as: 'EquipmentModal',
      },
    },
    {
      $lookup: {
        from: 'subcategories',
        localField: 'EquipmentModal.equipmentModal',
        foreignField: '_id',
        as: 'EquipmentModal.equipmentModal',
      },
    },
    {
      $lookup: {
        from: 'payments',
        localField: '_id',
        foreignField: 'user',
        as: 'Order',
      },
    },
    {
      $group: {
        _id: '$_id',
        name: { $first: '$name' },
        email: { $first: '$email' },
        phone: { $first: '$contactNo' },
        added: { $first: '$createdAt' },
        equipments: { $first: '$equipments' },
        businessadmin: { $first: '$businessadmin' },
        EquipmentModal: { $first: '$EquipmentModal' },
        Order: { $first: '$Order' },
      },
    },
  ]);
  res.status(200).json({
    errorCode: 200,
    errorMessage: 'Success',
    list: UserDetails[0],
  });
});

// @desc update user Profile
// @route update /user/profileupdate
// @access Private
const updateUsrProfile = asyncHandler(async (req, res) => {
  if (req.user) {
    const userDetails = await User.findById(req.user._id);

    if (userDetails && req.body.Oldpassword) {
      const data = await bcrypt.compare(
        req.body.Oldpassword,
        userDetails.password
      );
      if (data) {
        userDetails.password = req.body.password;
        const success = await userDetails.save();
        if (success) {
          return res.status(200).json('password update Success');
        }
      } else {
        return res.status(401).json('Old Password was wrong');
      }
    }
    if (userDetails) {
      userDetails.name = req.body.name || userDetails.name;
      userDetails.email = req.body.email || userDetails.email;
      userDetails.image = req.body.image || userDetails.image;
      userDetails.contactNo = req.body.contactNo || userDetails.contactNo;
      userDetails.address = req.body.address || userDetails.address;
      userDetails.BillingAdress =
        req.body.BillingAdress || userDetails.BillingAdress;
    }
    const result = await userDetails.save();
    if (result) {
      res.json(result);
    } else {
      res.status(401).json({ message: 'Server Error' });
    }
  } else {
    res.status(401);
    throw new Error('server Error');
  }
});
const updateUserProfile = asyncHandler(async (req, res) => {
  const { email, name, userId, imageLink } = req.body;
  console.log('630', req.body, req?.imageLink);
  const userDetails = await User.findById(userId);
  console.log('632', userDetails);
  if (userDetails) {
    userDetails.name = name || userDetails.name;
    userDetails.email = email || userDetails.email;
    userDetails.image = imageLink || userDetails.image;
    console.log('637', userDetails);
    const result = await userDetails.save();
    console.log('639', userDetails, result);
    res.json(result);
  } else {
    res.status(401).json({ message: 'Server Error' });
  }
});
// @desc Forget Passwird
// @route GET /user/forget
// @access Private
const forgetPassword = asyncHandler(async (req, res) => {
  const val = Math.floor(1000 + Math.random() * 9000);

  const userinfo = await User.findOne({ email: req.body.email });
  // userinfo.otp = val;
  const newpass = generatePassword(
    8,
    userinfo.name,
    userinfo.contactNo,
    userinfo.email
  );
  userinfo.password = newpass;
  await userinfo.save();

  await MailJet({
    email: userinfo.email,
    name: userinfo.name,
    Subject: 'Password change',
    textpart: 'Password change',
    htmlpart: `
        <p> new  password :   ${newpass} </p><br/>
       
        `,
  });
  res.status(200).json({
    successCode: 200,
    successMessage: 'Password change',
  });
});

const matchOtp = await asyncHandler(async (req, res) => {
  const userinfo = await User.findOne({ email: req.body.email });
  if (userinfo.otp.toString() == req.body.otp.toString()) {
    userinfo.password_change_req = true;
    await userinfo.save();

    res.status(200).json({
      successCode: 200,
      successMessage: 'Otp match success',
    });
  } else {
    res.status(404).json({
      errorCode: 404,
      errorMessage: 'Otp  match failed',
    });
  }
});

const change_password = asyncHandler(async (req, res) => {
  const userinfo = await User.findOne({ email: req.body.email });
  if (userinfo.password_change_req) {
    userinfo.password = req.body.password;
    userinfo.password_change_req = false;
    await userinfo.save();
    res.status(200).json({
      successCode: 200,
      successMessage: 'password change ',
    });
  } else {
    res.status(404).json({
      errorCode: 401,
      errorMessage: 'already  password change',
    });
  }
});

const getOrderInfo = asyncHandler(async (req, res) => {
  const order = await PartNotification.find()
    .populate('product')
    .populate('user');
  const askForPrice = await Payment.find().populate('product').populate('user');
  if (order && askForPrice) {
    res.json({ order, askForPrice });
  } else {
    res.status(404);
    throw new Error('part Not Found');
  }
});

// @desc Get User Machines
// @route GET /user/machine
// @access Private

// @desc Get User parts
// @route GET /user/parts
// @access Private

const userParts = asyncHandler(async (req, res) => {
  let { machines } = req.user;

  if (req.user) {
    const parts = await Part.find({
      $and: [
        {
          machineName: {
            $in: machines,
          },
        },
      ],
    });
    res.json(
      parts
        .filter((i) => i.active === true)
        .filter((i) => i.isDeleted === 'false')
    );
  } else {
    res.status(404);
    throw new Error('User Not Found');
  }
});

// @desc Get User parts by id
// @route GET /user/parts
// @access Private
const getpartsByid = asyncHandler(async (req, res) => {
  const partsDetails = await Part.find({ machineName: req.body.id })
    .populate('machineName')
    .populate('category')
    .populate('subcategory');
  if (partsDetails) {
    res.json(partsDetails);
  } else {
    res.status(404);
    throw new Error('Parts Not Found');
  }
});

// @desc Get User Machines
// @route GET /user/machine
// @access Private
const newOrder = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  if (!_id) {
    res.status(404).json('No data Avaialble');
  }
  if (_id) {
    const newOrder = await equiryDetails.insertMany(req.body);
    newOrder.forEach(async (p) => {
      var order = await equiryDetails.findById(p._id);
      order.for_Enquery = false;
      var data = {
        user: req.user.name,
        order: order.partName,
        order_id: order._id,
      };
      SaveNotification(req.admin._id, Order_Shipped, data, order._id);
      var body = notificationType({
        type: Order_Shipped,
        user: data.user,
        order: data.order,
        order_id: data.order_id,
      });
      var user = await User.findById(req.user.id);
      send_notification(user.token, body.title, body.text, order);
    });
    return res.status(201).json(newOrder);
  } else {
    res.status(404);
    throw new Error('User Not Found');
  }
});

//by admin orders needs
const getOrders = asyncHandler(async (req, res) => {
  const getAllOrder = await equiryDetails.find({ user: req.user._id });
  //.populate('user')
  //.populate({ path: 'orderItems', populate: { path: 'product' } });

  res.json(getAllOrder);
});

//get order by user

//update order status
const updateOrderStatus = asyncHandler(async (req, res) => {
  const { id, status } = req.body;
  const getOrder = await Order.findOneAndUpdate(
    { _id: id },
    { isDelivered: status ? 'true' : 'false' },
    function (err, info) {
      if (info) {
        res.send(info);
      } else {
        res.status(404);
      }
    }
  );
  var order = await Part.findById(getOrder.orderItems[0].product);
  var data = {
    user: req.user.name,
    order: order.partName,
    order_id: getOrder._id,
  };
  SaveNotification(req.admin._id, Order_Shipped, data, getOrder._id);
  var body = notificationType({
    type: Order_Shipped,
    user: data.user,
    order: data.order,
    order_id: data.order_id,
  });
  var user = await User.findById(req.user.id);
  send_notification(user.token, body.title, body.text, order);
});

//get order by id

const getorderById = asyncHandler(async (req, res) => {
  const { id } = req.params;
  const getorders = await Order.findOne({ _id: id })
    .populate({ path: 'orderItems', populate: { path: 'product' } })
    .populate('user');

  if (getorders) {
    return res.status(200).json(getorders);
  } else {
    res.status(404).json('Order Not Found');
  }
});

//ge order by cusomer id
const getOrderByUser = asyncHandler(async (req, res) => {
  // console.log(req.body);

  const orders = await Order.find({ user: req.body.id })
    .populate({ path: 'orderItems', populate: { path: 'product' } })
    .populate('user');
  if (orders) {
    res.json(orders);
  } else {
    res.status(404);
  }
});

//for the usesecure
//get roure

const getOrdersByUser = asyncHandler(async (req, res) => {
  const orders = await equiryDetails.find({
    user: req.user._id,
    for_Enquery: false,
  });
  if (orders) {
    res.json(orders);
  } else {
    res.status(404);
  }
});
const updateOrderbyId = asyncHandler(async (req, res) => {
  const { _id } = req.body;
  console.log(req.body);
  const orders = await equiryDetails.findById(_id);
  if (orders) {
    req.body.orderStatus == 'Shipped'
      ? (req.body.isShippedDate = new Date.now())
      : req.body.orderStatus == 'Delivered'
      ? (req.body.isDeliveredDate = new Date.now())
      : req.body.orderStatus == 'Cancelled'
      ? (req.body.isCancelledDate = new Date.now())
      : null;
    await equiryDetails.findByIdAndUpdate(_id, req.body);
    const updatedOrder = await equiryDetails.findById(_id);
    if (updatedOrder.orderStatus == 'Shipped') {
      var data = {
        user: await User.findById(updateOrder.user).name,
        order_id: _id,
        order: await Part.findById(updateOrder.part).partName,
      };
      SaveNotification(req.admin._id, Order_Shipped, data, _id);
      var body = notificationType({
        type: Order_Shipped,
        user: data.user,
        order: data.order,
        order_id: data.order_id,
      });
      var user1 = await User.findById(req.user.id);
      send_notification(
        user1.token,
        body.title,
        body.text,
        'Order_Listing',
        updatedOrder
      );
    } else if (updatedOrder.orderStatus == 'Delivered') {
      var data = {
        user: await User.findById(updateOrder.user).name,
        orderId: _id,
        product: await Part.findById(updateOrder.part).partName,
      };
      SaveNotification(req.admin._id, Order_Delivered, data, _id);
      var body = notificationType({
        type: Order_Delivered,
        user: data.user,
        product: data.product,
        orderId: data.orderId,
      });
      var user1 = await User.findById(req.user.id);
      send_notification(
        user1.token,
        body.title,
        body.text,
        'Order_Listing',
        updatedOrder
      );
    } else if (updatedOrder.orderStatus == 'Cancelled') {
      var data = {
        user: await User.findById(updateOrder.user).name,
        order_id: _id,
        part: await Part.findById(updateOrder.part).partName,
        date: updatedOrder.updatedAt,
      };
      SaveNotification(req.admin._id, Order_Cancelled, data, _id);
      var body = notificationType({
        type: Order_Cancelled,
        user: data.user,
        order: data.order,
        order_id: data.order_id,
      });
      var user1 = await User.findById(req.user.id);
      send_notification(
        user1.token,
        body.title,
        body.text,
        'Order_Listing',
        updatedOrder
      );
    }
    res.json(updatedOrder);
  } else {
    res.status(404);
  }
});
//add to cart functionalit

const AddCartItems = asyncHandler(async (req, res) => {
  const userDetails = await User.findById(req.user._id);

  if (userDetails) {
    let carts = userDetails.cart;

    let itemIndex = carts.findIndex((i) => i.part._id == req.body.part);

    if (itemIndex > -1) {
      let productItem = carts[itemIndex];
      productItem.qty = req.body.qty;
      carts[itemIndex] = productItem;
    } else {
      carts.push({ qty: req.body.qty, part: req.body.part });
    }
    carts = await userDetails.save();
    return res.status(201).send(carts);
  } else {
    res.status(404).json({ message: 'user Not found' });
  }
});

const getCartsItems = asyncHandler(async (req, res) => {
  const userDetails = await User.findById(req.user._id).populate({
    path: 'cart',
    populate: { path: 'part' },
  });
  if (userDetails) {
    res.status(200).json(userDetails.cart);
  } else {
    res.status(404).json({ message: 'user Not found' });
  }
});

//delete to cart
//secure by user
//post request
const deleteTocartItems = asyncHandler(async (req, res) => {
  const { partId } = req.body;
  const userDetails = await User.findById(req.user._id);
  if (userDetails && partId) {
    const carts = userDetails.cart;
    carts.splice(
      carts.findIndex((i) => i.part._id == partId),
      1
    );
    await userDetails.save();
    res.status(200).json({ message: 'cart item remove success' });
  } else {
    res.status(404).json({ message: 'user Not found' });
  }
});

//enquery detais
//@ user protected routes
//post request

const enqueryForm = asyncHandler(async (req, res) => {
  const existUser = await User.findById(req.user._id);
  const existEnquiry = await equiryDetails.findOne({ user: req.user._id });
  const { partName, amount } = await Part.findById(req.body.part);
  const AdminData = await Admin.find();

  // existEnquiry.partDetails.forEach(async (element, index) => {
  if (
    existEnquiry !== null &&
    existEnquiry.part == req.body.part &&
    existEnquiry.isApproved == 'pending'
  ) {
    return res.status(200).json({
      errorCode: 400,
      errorMessage: 'Already There and status are pending ',
    });
  } else if (existEnquiry !== null && existEnquiry.part == req.body.part) {
    existEnquiry.qty = req.body.qty;
    existEnquiry.isApproved = 'pending';
    await existEnquiry.save();
    AdminData.forEach(async (e) => {
      var data = {
        user: req.user.name,
        resend: true,
        partName: partName,
      };
      SaveNotification(e._id, Enquiry_from_User, data, existEnquiry._id);
    });
    var body = notificationType({
      type: Enquiry_from_User,
      user: data.user,
      resend: data.resend,
      partName: data.partName,
    });
    var user = await User.findById(req.user.id);
    send_notification(user.token, body.title, body.text, existEnquiry);
    return res.status(200).json({
      errorCode: 200,
      errorMessage: 'Enquery update success and sent  to admin',
    });
  }
  const newEnquery = await equiryDetails.create({
    ...req.body,
    user: req.user._id,
    Partial_Remaining_Amount: amount,
    Partial_Due: amount,
  });
  let carts = existUser.cart;
  carts.splice(
    carts.findIndex((j) => j.part._id == req.body.part),
    1
  );
  await existUser.save();

  AdminData.forEach(async (e) => {
    var data = {
      user: req.user?.name,
    };
    SaveNotification(e._id, Enquiry_from_User, data, newEnquery._id);
    var body = notificationType({ type: Enquiry_from_User, user: data.user });
    var user = await User.findById(req.user.id);
    send_notification(user.token, body.title, body.text, newEnquery);
  });
  res.status(200).json({
    errorCode: 201,
    errorMessage: 'New Enquiry Added',
  });
});
const paymentGateWay = asyncHandler(async (req, res) => {
  const { details, TransactionDetails, OrderId } = req.body;
  // details.transaction_amount=parseInt(details.transaction_amount)/10;
  console.log(
    'transaction_amount................................',
    details.transaction_amount
  );

  try {
    await TransactionDetailModal.create({
      OrderId: OrderId,
      TransactionDetails: TransactionDetails,
    });
    const order = await Payment.findById(OrderId).populate('part user');
    var data = {
      user: req.user.name,
      payment: details.transaction_amount / order.qty,
      Due_payment: 0,
      Due_date: 0,
    };
    if (order.Partial_Amount_Paid <= 0) {
      order.Partial_Remaining_Amount =
        order.Partial_Remaining_Amount > 0
          ? parseInt(order.Partial_Remaining_Amount) - parseInt(data.payment)
          : 0;
      order.Partial_Amount_Paid =
        parseInt(order.Partial_Amount_Paid) + parseInt(data.payment);
      const total =
        parseInt(order.Partial_Amount_Paid) +
        parseInt(order.Partial_Remaining_Amount);
      order.Partial_Amount_Paid_in_percentage = Math.round(
        (parseInt(order.Partial_Amount_Paid) / total) * 100
      );
      // order.Partial_Remaining_Amount_in_percentage=Math.round(parseInt(order.Partial_Remaining_Amount)/total*100)
      order.Partial_Remaining_Amount_in_percentage =
        100 - parseInt(order.Partial_Amount_Paid_in_percentage);
      order.Installment = '2';
    } else {
      order.Partial_Amount_Paid =
        parseInt(order.Partial_Amount_Paid) +
        parseInt(order.Partial_Remaining_Amount);
      order.Partial_Remaining_Amount = 0;
      order.Partial_Amount_Paid_in_percentage = 100;
      order.Partial_Remaining_Amount_in_percentage = 0;
    }
    var part = order.part;
    // await Part.findById(order.part);
    order.Mode_Of_Payment = 'Online';
    data.sparepart_name = part.partName;
    data.installment = details.transaction_amount;
    if (order.Partial_Remaining_Amount > 0) {
      data.Due_date = order.Partial_Date;
      data.Due_payment = order.Partial_Remaining_Amount;
      SaveNotification(req.user.id, Payment_by_User, data, OrderId);
      console.log('part0 .......................', part);
      console.log('part name .......................', part.partName);
      SaveNotification(req.user.id, Payment_by_User1, data, OrderId);
      var body = notificationType({
        type: Payment_by_User1,
        sparepart_name: data.sparepart_name,
        installment: data.installment,
      });
      var user1 = order.user;
      // await User.findById(req.user.id);
      console.log(
        'payment..............................',
        order.Type_Of_Payment,
        order.Type_Of_Payment == 'Full'
      );
      if (order.Type_Of_Payment == 'Full') {
        order.for_Enquery = false;
        order.Payment_Status = 'Paid';
      }
      console.log(
        'payment1..............................',
        order.for_Enquery,
        order.for_Enquery === false
      );
      send_notification(
        user1.token,
        body.title,
        body.text,
        'Order_Listing',
        order
      );
    } else if (order.Partial_Remaining_Amount <= 0) {
      order.for_Enquery = false;
      order.Payment_Status = 'Paid';
      console.log('last payment............................', data);
      SaveNotification(
        req.user.id,
        Last_Payment_Oneshot_Payment,
        data,
        order._id
      );
      var body = notificationType({
        type: Payment_by_User1,
        sparepart_name: data.sparepart_name,
        installment: data.installment,
      });
      var user1 = order.user;
      //await User.findById(req.user.id);

      send_notification(
        user1.token,
        body.title,
        body.text,
        'Order_Listing',
        order
      );
    }
    console.log('1378.......................', order);
    await order.save();
    res.status(200).json({
      errorCode: 201,
      errorMessage: 'Payment Success',
      data: data,
    });
  } catch (error) {
    console.log(error);
    return res.status(200).json({
      errorCode: 400,
      errorMessage: 'Payment Failed,Please Again Later',
    });
  }
});

const getEnqueries = asyncHandler(async (req, res) => {
  const userDetails = await User.findById(req.user._id);
  if (userDetails) {
    let Enquery = await equiryDetails
      .find({ user: req.user._id })
      .populate({ path: 'part' })
      .select('-user');
    res.status(200).json(Enquery);
  } else {
    res.status(404).json({ message: 'user not login' });
  }
});

export {
  authUser,
  authUserSignout,
  payment,
  paymentSignatureVerification,
  getUserEquipments,
  postUpdateUserAddress,
  postChangeUserPassword,
  GetEquipmentModalByEquipment,
  getAllPartsByEq_Modals,
  getProductDetailsByID,
  AddProductTomyCart,
  CartProductActions,
  removeProductFromMyCart,
  getMycart,
  SendAskForPrice,
  getMyEnquiry,
  getMyOrder,
  forgetPassword,
  matchOtp,
  change_password,
  // getUserMachine,
  getUserProfile,
  newOrder,
  getOrders,
  updateOrderStatus,
  getorderById,
  getOrderByUser,
  getOrdersByUser,
  updateOrderbyId,
  userParts,
  getpartsByid,
  updateUsrProfile,
  updateUserProfile,
  getOrderInfo,
  AddCartItems,
  getCartsItems,
  deleteTocartItems,
  enqueryForm,
  paymentGateWay,
  getEnqueries,
  // getModelsByMachineId,
  getUserDetailsById,
};
