import { asyncHandler } from '../middleware/errorMiddleware.js';
import Category from '../models/categoryModel.js';
import Machine from '../models/machineModel.js';
import MachineName from '../models/machineName.js';
import Part from '../models/partModel.js';
import path from 'path';
import SubCategory from '../models/subcategoryModel.js';
// import { Model } from "mongoose";
// @desc fetch all machine
// @route GET machine/all
// @access Private
const getMachines = asyncHandler(async (req, res) => {
  const keyword = req.query.keyword
    ? {
        name: {
          $regex: req.query.keyword,
          $options: 'i',
        },
      }
    : {};
  const machines = await Machine.find({ ...keyword }).populate('equipment');
  res.json(machines);
});

//get equipments

const getEquipments = asyncHandler(async (req, res) => {
  const keyword = req.query.keyword
    ? {
        name: {
          $regex: req.query.keyword,
          $options: 'i',
        },
      }
    : {};
  // console.log(
  //   req.query.keyword,
  //   await MachineName.find({}).populate('model', 'machineName')
  // );
  const equipments = await MachineName.find({ ...keyword }).populate(
    'model',
    'machineName'
  );
  // console.log(equipments);
  res.json(equipments);
});

// @desc fetch single machine
// @route GET machine/:id
// @access Private
const getMachineById = asyncHandler(async (req, res) => {
  const machine = await Machine.findById(req.params.id)
    .populate('parts')
    .populate('equipment');
  if (machine) {
    res.send(machine);
  } else {
    res.status(404);
    throw new Error('Machine not Found');
  }
});

// @desc    Add a Machine model
// @route   post /product/machine
// @access  Private/Admin
const addMachine = asyncHandler(async (req, res) => {
  const { machinemodel, state } = req.body;
  // console.log(req.body);
  const machineExist = await Machine.findOne({ machinemodel });

  const machineNameExist = await MachineName.findById(state);
  if (machineExist) {
    return res.status(400).json({ message: 'Machine model Already Exist' });
  }

  const machine = await Machine.create({
    machineName: machinemodel,
    equipment: state,
  });
  if (machineNameExist && machine) {
    machineNameExist.model.push(machine);
    const regas = await machineNameExist.save();
    // console.log(machineNameExist);

    res.status(201).json({
      machine,
    });
  } else {
    res.status(400);
    throw new Error('Invalid Machine model Data');
  }
});

//get equipment mdel by id
//secure by admin
//post route
const getModelByArrayId = asyncHandler(async (req, res) => {
  const { ids } = req.body;
  // console.log('99')
  const getmodels = await SubCategory.find({
    $and: [{ category: { $in: ids } }, { active: true }],
  });
  // console.log('103')
  if (getmodels) {
    res.json(getmodels);
  } else {
    res.status(400);
    throw new Error('Invalid Machine model Data');
  }
});

// @desc    Add a Machine name
// @route   post /product/machine
// @access  Private/Admin
const addMachineName = asyncHandler(async (req, res) => {
  const { machinename } = req.body;

  const machineExist = await MachineName.findOne({ equipment: machinename });

  // console.log("work", machineExist);
  if (machineExist) {
    return res.status(400).json({ message: 'Machine name Already Exists' });
  }

  const machine = await MachineName.create({
    equipment: machinename,
  });

  if (machine) {
    res.status(201).json({
      _id: machine._id,
      equipment: machine.machineName,
    });
  } else {
    res.status(400);
    throw new Error('Invalid Machine model Data');
  }
});

//update machine name

const updateMachineName = asyncHandler(async (req, res) => {
  // console.log(req.body);
  const { machinename, _id } = req.body;
  const machineExist = await MachineName.findByIdAndUpdate(
    _id,
    {
      equipment: machinename,
    },
    {
      new: true,
    }
  );
  // console.log(machinename);
  if (machineExist) {
    return res.status(201).json(machineExist);
  } else {
    req.json(404);
  }
});

const updatestatusMachineName = asyncHandler(async (req, res) => {
  const { id, newStatus } = req.body;
  const machineExist = await MachineName.findByIdAndUpdate(
    id,
    {
      active: newStatus,
    },
    {
      new: true,
    }
  );
  if (machineExist) {
    return res.status(201).json(machineExist);
  } else {
    req.json(404);
  }
});

// @desc    Update Machine Status
// @route   PUT /product/machine
// @access  Private/Admin
const updateMachineStatus = asyncHandler(async (req, res) => {
  // console.log(req.body);
  const { machineName, id, active, equipment } = req.body;
  var varsome = active === 'InActive' ? 'false' : 'true';
  const MachineExist = await Machine.findById(id);
  const result = await Machine.findByIdAndUpdate(
    id,
    {
      machineName: machineName || MachineExist.machineName,
      active: varsome,
      equipment: equipment || MachineExist.equipment,
    },
    {
      new: true,
    }
  );
  if (equipment) {
    const equipmmentName = await MachineName.findById(equipment);
    equipmmentName.model.push(result._id);
    await equipmmentName.save();
    // console.log(equipmmentName);
    if (result) {
      return res.status(201).json({
        message: 'Updated!!!',
      });
    } else {
      res.status(400);
      throw new Error('Invalid Machine Data');
    }
  } else {
    return res.json({
      message: 'Updated!!!',
    });
  }
});

// @desc    Delete a Machine
// @route   DELETE /api/products/:id
// @access  Private/Admin
const deleteMachine = asyncHandler(async (req, res) => {
  const { id } = req.params;
  const product = await Machine.findByIdAndUpdate(id, { isDeleted: true });

  if (product) {
    await product.remove();
    res.json({ message: 'Machine removed' });
  } else {
    res.status(404);
    throw new Error('Machine not found');
  }
});
const deleteEquipment = asyncHandler(async (req, res) => {
  const { id } = req.params;
  const equipment = await MachineName.findByIdAndUpdate(id, {
    isDeleted: true,
  });
  if (equipment) {
    res.json({ message: 'Equipment has removed success' });
  } else {
    res.status(404);
    throw new Error('Equipment not here');
  }
});

// @desc fetch all category
// @route GET machine/category
// @access Private
const getCategory = asyncHandler(async (req, res) => {
  const keyword = req.query.keyword
    ? {
        name: {
          $regex: req.query.keyword,
          $options: 'i',
        },
      }
    : {};
  const category = await Category.find({ ...keyword });
  // console.log('266',category)
  res.send(category);
});

// @desc    Add a Machine
// @route   post /product/machine
// @access  Private/Admin
const addCategory = asyncHandler(async (req, res) => {
  const { category } = req.body;
  // console.log(req.file_url[0], category);
  const categoryExist = await Category.findOne({ name: category });
  if (categoryExist) {
    res.status(400);
    throw new Error('Category Already Exists');
  }

  const newcategory = await Category.create({
    name: category,
    image: req.file_url[0],
  });

  if (newcategory) {
    res.status(201).json({
      _id: newcategory._id,
      name: newcategory.name,
    });
  } else {
    res.status(400);
    throw new Error('Invalid Category Data');
  }
});

const editCategory = async (req, res) => {
  console.log(req.body, req?.file_url?.[0]);
  const { id, category } = req.body;
  if (!id) return res.status(404).json({ message: 'ID NOT FOUND!!!' });
  try {
    // const categoryExist = await Category.findOne({
    //   name: category,
    // });
    // if (categoryExist) {
    //   return res.status(500).json({ message: 'Category Already Exists' });
    // }
    const getcategory = await Category.findById(id);
    getcategory.name = category || getcategory.name;
    getcategory.image = req?.file_url?.[0] || getcategory.image;
    await getcategory.save();
    // const resultData = await Category.findByIdAndUpdate(
    //   id,
    //   { name: category ,image:},
    //   {
    //     new: true,
    //   }
    // );
    if (resultData) res.json({ status: 'Updated!!!' });
  } catch (error) {
    if (error) res.status(400).json({ message: 'Something went Wrong!!!' });
  }
};

const statusUpdate = async (req, res) => {
  const { active, id } = req.body;
  // var varsome = active === "InActive" ? false : true;
  if (!active || !id)
    return res.status(404).json({ message: 'Status and ID not Found!!!' });
  try {
    const resultData = await Category.findByIdAndUpdate(
      id,
      { active: active === 'false' ? false : active },
      {
        new: true,
        upsert: true,
      }
    );
    if (resultData) res.status(200).json({ resultData });
  } catch (error) {
    if (error) res.status(400).json({ message: 'Something went Wrong!!!' });
  }
};

// @desc    Delete a Category
// @route   DELETE /products/category
// @access  Private/Admin
const deleteCategory = asyncHandler(async (req, res) => {
  const { id } = req.params;
  const product = await Category.findByIdAndUpdate(id, { isDeleted: 'true' });
  if (product) {
    res.json({ message: 'Category removed' });
  } else {
    res.status(404);
    throw new Error('Category not found');
  }
});

// @desc fetch all part
// @route GET machine/parts
// @access Private
const getParts = asyncHandler(async (req, res) => {
  const keyword = req.query.keyword
    ? {
        name: {
          $regex: req.query.keyword,
          $options: 'i',
        },
      }
    : {};
  const part = await Part.find({ ...keyword })
    .populate('machineName')
    .populate('category')
    .populate('subcategory')
    .populate('equipment');
  // .populate("partName");
  res.send(part);
});

// @desc    Add a Part
// @route   post /product/part
// @access  Private/Admin
const addPart = asyncHandler(async (req, res) => {
  const { partName, category, subcategory, description, amount, stock, image } =
    req.body;
  const findPart = await Part.findOne({ partName: partName });
  if (!findPart) {
    const newPart = await Part.create({
      partName,
      category,
      subcategory,
      description,
      amount,
      inventoryStock: stock,
      // image: req.body.imageUrlarray.split(','),
      image: req.file_url,
    });
    res.status(201).json({
      errorCode: 201,
      errorMessage: 'success',
      list: newPart,
    });
  } else {
    res.status(500).json({
      errorCode: 500,
      errorMessage: 'product name already there',
    });
  }
});

// @desc    Update Part Status
// @route   PUT /product/part
// @access  Private/Admin

const updatePartStatus = asyncHandler(async (req, res) => {
  if (req.file_url == undefined || req.file_url == null) {
    req.file_url = [];
  }
  var imagesArray = [];
  if (Array.isArray(req.body.alreadyUploaded)) {
    imagesArray = [...req.body.alreadyUploaded, ...req.file_url];
  } else if (req.body.alreadyUploaded != undefined) {
    imagesArray = [req.body.alreadyUploaded, ...req.file_url];
  } else {
    imagesArray = [...req.file_url];
  }
  const {
    id,
    partName,
    active,
    category,
    subcategory,
    description,
    amount,
    stock,
    status,
    imagearray,
  } = req.body;
  console.log('..............imagesArray', imagesArray);
  const getExistPart = await Part.findById(id);
  getExistPart.partName = partName || getExistPart.partName;
  getExistPart.active =
    active == false ? false : active == true ? true : getExistPart.active;
  getExistPart.category = category || getExistPart.category;
  getExistPart.subcategory = subcategory || getExistPart.subcategory;
  getExistPart.description = description || getExistPart.description;
  getExistPart.amount = amount || getExistPart.amount;

  getExistPart.inventoryStock = stock || getExistPart.inventoryStock;
  getExistPart.image = imagesArray || getExistPart.image;
  await getExistPart.save();
  return res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getExistPart,
  });
});

const UpdatePartsByID = asyncHandler(async (req, res) => {
  const { id, active } = req.body;
  const getExistPart = await Part.findById(id);
  getExistPart.active =
    active == false ? false : active == true ? true : getExistPart.active;
  await getExistPart.save();
  return res.status(200).json({
    errorCode: 200,
    errorMessage: 'success',
    list: getExistPart,
  });
});

// @desc    Delete a Part
// @route   DELETE /product/part
// @access  Private/Admin
const deletePart = asyncHandler(async (req, res) => {
  const { id } = req.body;
  const product = await Part.findByIdAndUpdate(id, { isDeleted: true });

  if (product) {
    res.json({ message: 'Part removed' });
  } else {
    res.status(404);
    throw new Error('Part not found');
  }
});

// @desc fetch single Part
// @route GET machine/part/:id
// @access Private
const getPartById = asyncHandler(async (req, res) => {
  const part = await Part.findById(req.params.id)
    .populate('machineName')
    .populate('Category')
    .populate('equipment')
    .populate('SubCategory');
  if (part) {
    res.send(part);
  } else {
    res.status(404);
    throw new Error('Part not Found');
  }
});

// @desc update single part inventory
// @route PUT machine/updateInventory/:id
// @access Private
const updateInventory = asyncHandler(async (req, res) => {
  const { minCartQuantity, maxCartQuantity, inventoryStock } = req.body;
  const part = await Part.findById(req.params.id)
    .populate('machineName')
    .populate('category');
  if (part) {
    part.minCartQuantity = minCartQuantity || part.minCartQuantity;
    part.maxCartQuantity = maxCartQuantity || part.maxCartQuantity;
    part.inventoryStock = inventoryStock || part.inventoryStock;
    const updatedPart = await part.save();
    res.send(updatedPart);
  } else {
    res.status(404);
    throw new Error('Part not Found');
  }
});

// @desc fetch all subcategory
// @route GET machine/subCategory
// @access Private
const getSubCategory = asyncHandler(async (req, res) => {
  const keyword = req.query.keyword
    ? {
        name: {
          $regex: req.query.keyword,
          $options: 'i',
        },
      }
    : {};
  const subCategory = await SubCategory.find({ ...keyword }).populate(
    'category'
  );
  res.send(subCategory);
});

// @desc    Add a subCategory
// @route   post /product/subcategory
// @access  Private/Admin
const addSubCategory = asyncHandler(async (req, res) => {
  const { category, name } = req.body;
  const subCategory = await SubCategory.create(req.body);

  if (subCategory) {
    res.status(201).json({
      _id: subCategory._id,
      name: subCategory.name,
    });
  } else {
    res.status(400);
    throw new Error('Invalid Sub Category Data');
  }
});

const editSubCategory = asyncHandler(async (req, res) => {
  const { name, id, category, active } = req.body;
  const subCategory = await SubCategory.findByIdAndUpdate(
    id,
    {
      ...req.body,
      // active: active == 'false' ? false : active == 'true' ? true : active,
    },
    {
      new: true,
    }
  );
  if (subCategory) {
    res.status(201).json({
      message: 'Updated!!!',
    });
  } else {
    res.status(400);
    throw new Error('Invalid Sub Category Data');
  }
});

// @desc    Delete a SubCategory
// @route   DELETE /machine/subcategory
// @access  Private/Admin
const deleteSubCategory = asyncHandler(async (req, res) => {
  const { id } = req.params;
  const product = await SubCategory.findByIdAndUpdate(id, { isDeleted: true });

  if (product) {
    res.json({ message: 'SubCategory removed' });
  } else {
    res.status(404);
    throw new Error('SubCategory not found');
  }
});

const getMachinAndPartByarrayId = asyncHandler(async (req, res) => {
  const machines = await MachineName.find({
    _id: { $in: req.body.list },
  }).populate('model');
  // .populate("equipment");
  if (machines) {
    res.json(machines);
  } else {
    res.status(404);
  }
});

export {
  getMachines,
  updateMachineStatus,
  deleteMachine,
  deleteEquipment,
  addMachine,
  getModelByArrayId,
  addMachineName,
  updateMachineName,
  getMachineById,
  getCategory,
  UpdatePartsByID,
  addCategory,
  statusUpdate,
  updatestatusMachineName,
  deleteCategory,
  editCategory,
  getParts,
  getPartById,
  addPart,
  updatePartStatus,
  deletePart,
  updateInventory,
  getSubCategory,
  addSubCategory,
  deleteSubCategory,
  editSubCategory,
  getMachinAndPartByarrayId,
  getEquipments,
};
