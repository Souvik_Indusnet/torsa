import Dashboard from '../pages/Dashboard/Dashboard';
import BusinessAdminDashboard from '../pages/BusinessAdmin/Dashboard';
import Orderlist from '../pages/Dashboard/Orderlist';
// import MyAccounut from '../pages/profile/MyAccounut';
import ReportManagement from '../pages/Report/ReportManagement';
import Register from '../pages/Register/Register';
// import Customerindex from '../pages/view/Customer';
import Adminindex from '../pages/view/Admin';

// import CmsViewpage from '../pages/Dashboard/CmsViewpage';

// import MasterSetting from '../pages/Dashboard/MasterSetting';
import Order from '../pages/order';
import MyOrder from '../pages/myorders/index';
import ViewMyOrders from '../pages/myorders/View';
// import ProfileSideAdmin from '../pages/sideAdmin/profile';
import ViewSection from '../pages/order/view/viewSection';
import Notification from '../pages/sideAdmin/notification/index';
import CustomeManagment from '../pages/sideAdmin/customer';
import Datails from '../pages/sideAdmin/customer/details';
import OrderDetailsBycustomer from '../pages/sideAdmin/customer/order';
import AdminNotificaton from '../pages/Notification';
import Paymentable from '../pages/payment/table';
import Enquerytable from '../pages/enquiery/table';
import Enquery_Form from '../pages/enquiery/form';
import Notfound from '../pages/notfound/Notfound';
import EquipmentTable from '../pages/master_setting/Equipment/table';
import EquipmentForm from '../pages/master_setting/Equipment/form';
import EquipmentModalTable from '../pages/master_setting/equipmentmodal/table';
import EquipmentformModal from '../pages/master_setting/equipmentmodal/form';
import CustomerTable from '../pages/customer/Table';
import AddUserForm from '../pages/customer/Form';
import BusinessAdminTable from '../pages/BusinessAdmin/Table';
import AddBusinessAdminForm from '../pages/BusinessAdmin/Form';
import Inventorytable from '../pages/Inventory/Table';
import Inventoryform from '../pages/Inventory/form';
import ProductTable from '../pages/Product/Table';
import ProductsForm from '../pages/Product/Form';
import CmsTable from '../pages/CMS/Table';
import CMSForm from '../pages/CMS/Form';
import UserViewPage from '../pages/customer/View';
import UserViewPageForBA from '../pages/customer/ViewBA';
import ProductView from '../pages/Product/View';
import ViewOrders from '../pages/order/View';
import MyAccount from '../pages/BusinessAdmin/Profile';
import CreateForm from '../pages/NotificationCreate/CreateForm';
import NotificationTable from '../pages/NotificationCreate/table';
export const routes = [
  // {
  //   path: '/admin-signin',
  //   component: Login,
  // },
  {
    path: '/signup',
    component: Register,
  },
  // {
  //   path: '/profile',
  //   component: MyAccounut,
  // },
  {
    path: '/dashboard',
    component: Dashboard,
    ////Tag: 'Dashboard',
  },
  {
    path: '/businessadmin_dashboard',
    component: BusinessAdminDashboard,
    ////Tag: 'Dashboard',
  },
  {
    path: '/orderlist',
    component: Orderlist,
  },
  {
    path: '/report-management',
    component: ReportManagement,
  },
  {
    path: '/vieworder',
    component: ViewOrders,
  },
  {
    path: '/user-management',
    component: CustomerTable,
    ////Tag: 'User Management',
  },
  {
    path: '/adduser',
    component: AddUserForm,
    ////Tag: 'Add User ',
  },
  {
    path: '/updateuser',
    component: AddUserForm,
    ////Tag: 'Update  User ',
  },
  {
    path: '/viewuser',
    component: UserViewPage,
    ////Tag: 'View User',
  },
  {
    path: '/view',
    component: UserViewPageForBA,
    ////Tag: 'View User',
  },
  // {
  //   path: '/customer/view/:id',
  //   component: Customerindex,
  // },
  // {
  //   path: '/customerlist/New-Customer',
  // component: CoCustomer,
  // },

  {
    path: '/Admin/view/:id',
    component: Adminindex,
  },
  {
    path: '/productmanagement',
    component: ProductTable,
    ////Tag: 'Product Management',
  },
  {
    path: '/addproduct',
    component: ProductsForm,
    ////Tag: 'Add Product',
  },
  {
    path: '/updateproduct',
    component: ProductsForm,
    ////Tag: 'Update Product',
  },
  {
    path: '/viewproduct',
    component: ProductView,
    ////Tag: 'View Product',
  },
  {
    path: '/business-admin-management',
    component: BusinessAdminTable,
    ////Tag: 'Business Admin Management',
  },
  {
    path: '/addbusinessadmin',
    component: AddBusinessAdminForm,
    ////Tag: 'Add Business Admin',
  },
  {
    path: '/updatebusinessadmin',
    component: AddBusinessAdminForm,
    ////Tag: 'Update Business Admin',
  },
  {
    path: '/cms',
    component: CmsTable,
    ////Tag: ' CMS',
  },
  {
    path: '/addcms',
    component: CMSForm,
    ////Tag: 'Add CMS',
  },
  {
    path: '/updatecms',
    component: CMSForm,
    ////Tag: 'Update CMS',
  },

  {
    path: '/inventory-management',
    component: Inventorytable,
    ////Tag: 'Inventory Management',
  },
  {
    path: '/updateinventory',
    component: Inventoryform,
    ////Tag: 'Update Inventory',
  },

  {
    path: '/equipment',
    component: EquipmentTable,
    ////Tag: 'Equipment',
  },
  {
    path: '/addequipment',
    component: EquipmentForm,
    ////Tag: 'Add Equipment',
  },
  {
    path: '/updateequipment',
    component: EquipmentForm,
    ////Tag: 'update Equipment',
  },
  {
    path: '/equipmentmodel',
    component: EquipmentModalTable,
    ////Tag: 'Equipment Modal',
  },
  {
    path: '/addequipmentmodel',
    component: EquipmentformModal,
    ////Tag: 'Add Equipment Modal',
  },
  {
    path: '/updateequipmentmodel',
    component: EquipmentformModal,
    ////Tag: 'Update Equipment Modal',
  },
  // {
  //   path: '/manage-machine',
  //   component: MasterSetting,
  //   ////Tag: 'Machine',
  // },

  {
    path: '/order-management',
    component: Order,
    ////Tag: 'Order Management',
  },
  {
    path: '/myorder',
    component: MyOrder,
    ////Tag: 'Order Management',
  },
  {
    path: '/viewmyorder',
    component: ViewMyOrders,
  },
  {
    path: '/order-view-management/:id',
    component: ViewSection,
    ////Tag: 'View Order',
  },
  {
    path: '/admin/notification',
    component: AdminNotificaton,
  },

  // business admin routs
  // {
  //   path: '/my-account',
  //   component: ProfileSideAdmin,
  // },
  {
    path: '/notification',
    component: Notification,
  },
  {
    path: '/customer-view',
    component: CustomeManagment,
  },
  {
    path: '/customer/details/order',
    component: OrderDetailsBycustomer,
    ////Tag: 'Order Management',
  },
  {
    path: '/404',
    component: Notfound,
  },
  {
    path: '/customer/details/:id',
    component: Datails,
  },
  {
    path: '/paymentmanagement',
    component: Paymentable,
    ////Tag: 'Payment Management',
  },
  {
    path: '/enquiry',
    component: Enquerytable,
    ////Tag: 'Enquery Management',
  },
  {
    path: '/Payment-enquiry',
    component: Enquery_Form,
  },
  {
    path: '/enquiryform',
    component: Enquery_Form,
    ////Tag: 'Share Enquery Management',
  },

  {
    path: '/my-account',
    component: MyAccount,
  },
  {
    path: '/notification-create',
    component: CreateForm,
  },
  {
    path: '/notification-management',
    component: NotificationTable,
  },
];
