export const Navbar = [
  {
    path: '/dashboard',
    title: 'Dashboard',
    active: 'dashboard',
    className: 'fa-regular fa-panel-ews',
  },
  {
    path: '/user-management',
    title: 'User Management',
    active: 'customer',
    className: 'fa-regular fa-users-gear',
  },
  {
    path: '/report-management',
    title: 'Report Management',
    active: 'report',
    className: 'fa-regular fa-file-chart-column',
  },
  {
    path: '/updateuser',
    active: 'customer',
  },
  {
    path: '/viewuser',
    active: 'customer',
  },
  {
    path: '/adduser',
    active: 'customer',
  },
  {
    path: '/business-admin-management',
    title: 'Business Admin Management',
    active: 'businessadmin',
    className: 'fa-regular fa-user-shield',
  },
  {
    path: '/addbusinessadmin',
    active: 'businessadmin',
  },
  {
    path: '/updatebusinessadmin',
    active: 'businessadmin',
  },

  {
    path: '/productmanagement',
    title: 'Product Management',
    active: 'machineandparts',
    className: 'fa-regular fa-box-open-full',
  },
  {
    path: '/addproduct',
    active: 'machineandparts',
  },
  {
    path: '/updateproduct',
    active: 'machineandparts',
  },
  {
    path: '/viewproduct',
    active: 'machineandparts',
  },
  {
    path: '/enquiry',
    title: 'Enquiry',
    active: 'enquiry',
    className: 'fa-regular fa-cloud-question',
  },
  {
    path: '/enquiryform',
    active: 'enquiry',
  },
  {
    path: '/paymentmanagement',
    title: 'Payment Management',
    active: 'paymentmanagement',
    className: 'fa-brands fa-cc-visa',
  },
  {
    path: '/Payment-enquiry',
    active: 'paymentmanagement',
  },
  {
    path: '/order-management',
    title: 'Order Management',
    active: 'order-management',
    className: 'fa-regular fa-cart-shopping',
  },
  {
    path: '/vieworder',
    active: 'order-management',
  },
  {
    path: '/inventory-management',
    title: 'Inventory Management',
    active: 'inventory-management',
    className: 'fa-regular fa-shelves',
  },
  {
    path: '/notification-management',
    title: 'Notification Management',
    active: 'notification-management',
    className: 'fa-regular fa-bells',
  },
  {
    path: '/notification-create',
    // title: 'Create notification',
    active: 'notification-management',
    // className: 'flaticon-381-settings-1',
  },
  {
    path: '/updateinventory',
    active: 'inventory-management',
  },
  {
    path: '/cms',
    title: 'CMS',
    active: 'cms',
    className: 'fa-regular fa-folder-gear',
  },
  {
    path: '/addcms',
    active: 'cms',
  },
  {
    path: '/updatecms',
    active: 'cms',
  },
  {
    // path: '/dashboard',
    submenu: true,
    title: 'Master Setting',
    active: 'master',
    className: 'fa-regular fa-gear',
    child: [
      {
        path: '/equipment',
        title: 'Equipment',
        active: 'master',
        className: 'fa-regular fa-screwdriver-wrench ibt-submenu',
      },
      {
        path: '/equipmentmodel',
        title: 'Equipment Model',
        active: 'master',
        className: 'fa-regular fa-screwdriver-wrench ibt-submenu',
      },
    ],
  },
];

export const BA_NavBar = [
  {
    path: '/businessadmin_dashboard',
    title: 'Dashboard',
    active: 'dashboard',
    className: 'fa-regular fa-panel-ews',
  },
  {
    path: '/user-management',
    title: 'User Management',
    active: 'customer',
    className: 'fa-regular fa-users-gear',
  },

  {
    path: '/my-account',
    title: 'My Account ',
    active: 'my-account',
    className: 'fa-regular fa-user',
  },
  {
    path: '/myorder',
    title: 'My Order ',
    active: 'my-order',
    className: 'fa-regular fa-cart-shopping',
  },
];
