import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { routes } from './constant/routes';
import Notfound from './pages/notfound/Notfound';

const RouterConfig = () => {
  return (
    <BrowserRouter>
      <Switch>
        {routes.map((i) => {
          return (
            <Route key={i.path} path={i.path} exact component={i.component} />
          );
        })}
        <Redirect from="/" to="/admin-signin" exact />
        <Route component={Notfound} />
      </Switch>
    </BrowserRouter>
  );
};

export default RouterConfig;
