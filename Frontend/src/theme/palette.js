// import { createMuiTheme } from "@material-ui/core/styles";
import { createTheme } from '@material-ui/core/styles';
export default createTheme({
  palette: {
    primary: {
      main: '#F08123',
    },
    secondary: {
      main: '#00c853',
    },
    error: {
      main: '#e57373',
    },
    warning: {
      main: '##ffb74d',
    },
    success: {
      main: '#4caf50',
    },
  },

  typography: {
    fontFamily: 'monospace , sans-serif',
  },
});
