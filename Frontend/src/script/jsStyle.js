const cssFile = [
  {
    files: 'vendor/jqvmap/css/jqvmap.min.css',
  },
  {
    files: 'vendor/chartist/css/chartist.min.css',
  },
  {
    files: 'vendor/bootstrap-select/dist/css/bootstrap-select.min.css',
  },
  {
    files:
      'vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
  },
  {
    files: 'css/style.css',
  },
];

const scriptJS = [
  // { s1: 'vendor/global/global.min.js' },
  // { s1: 'vendor/bootstrap-select/dist/js/bootstrap-select.min.js' },
  { s1: 'js/custom.min.js' },
  { s1: 'js/deznav-init.js' },
  // { s1: 'js/dashboard/analytics.js' },
];

export { scriptJS, cssFile };
