import {
  GET_ALL_MACHINES_REQ,
  GET_ALL_MACHINES_SUCCESS,
  GET_ALL_MACHINE_FAIL,
  USER_LOGOUT,
  USER_SIGNIN_FAIL,
  USER_SIGNIN_REQ,
  USER_SIGNIN_SUCCESS,
} from '../constant/productConstant';

export const userSigninReducers = (state = {}, action) => {
  switch (action.type) {
    case USER_SIGNIN_REQ:
      return { loading: true };
    case USER_SIGNIN_SUCCESS:
      return { loading: false, userInfo: action.payload };
    case 'UpdateState':
      console.log(state);
    case USER_SIGNIN_FAIL:
      return { loading: false, payload: action.payload };
    case USER_LOGOUT:
      return {};
    default:
      return state;
  }
};

//get all machines
export const getAllMachinesReducers = (state = {}, action) => {
  switch (action.type) {
    case GET_ALL_MACHINES_REQ:
      return { loading: true };
    case GET_ALL_MACHINES_SUCCESS:
      return { loading: false, Allmachine: action.payload };
    case GET_ALL_MACHINE_FAIL:
      return { loading: false, getMAchineError: action.payload };

    default:
      return state;
  }
};

export const getstatusreducer = (state = {}, action) => {
  switch (action.type) {
    case 'STATUS_GET_UPDATE':
      return { profileStatus: action.payload };

    default:
      return state;
  }
};

export const UserNotification = (
  state = { loading: false, error: null, list: 0 },
  action
) => {
  switch (action.type) {
    case 'BA_NOTIFICATION_COUNT_LIST_PENDING':
      return { loading: true };
    case 'BA_NOTIFICATION_COUNT_LIST_ERROR':
      return { loading: false, error: action.error };
    case 'BA_NOTIFICATION_COUNT_LIST_SUCCESS':
      return { loading: false, error: null, list: action?.payload?.data?.list };
    case 'BA_NOTIFICATION_LIST_MARK_READ':
      return { loading: false, error: null, list: state.list - 1 };
    case 'BA_NOTIFICATION_LIST_MARK__ALL_READ':
      return { loading: false, error: null, list: 0 };
    default:
      return state;
  }
};

export const UserNotificationList = (
  state = {
    loading: false,
    error: false,
    list: [],
    totalPage: 0,
    page: 0,
  },
  action
) => {
  switch (action.type) {
    case 'BA_NOTIFICATION_LIST_PENDING':
      return { ...state, loading: true };
    case 'BA_NOTIFICATION_LIST_ERROR':
      return { loading: false, error: action.error };
    case 'BA_NOTIFICATION_LIST_SUCCESS':
      return {
        ...state,
        loading: false,
        error: null,
        list: action?.payload?.data?.list,
        totalPage: action?.payload?.data?.totalPage,
        page: action?.payload?.data?.page,
      };

    case 'BA_NOTIFICATION_LIST_MARK_READ':
      state.list[
        state.list.findIndex((el) => el._id == action.payload.id)
      ].generateCount = 0;

      return {
        ...state,
        loading: false,
        error: null,
        list: state.list,
      };
    case 'BA_NOTIFICATION_LIST_MARK__ALL_READ':
      // state.list?.map((el) => {
      //   return { ...el, generateCount: 1 };
      // });
      // state.list[
      //   state.list.findIndex((el) => el.generateCount == 1)
      state.list.forEach((element) => {
        element.generateCount = 0;
      });
      // ].generateCount = 0;
      return {
        ...state,
        loading: false,
        error: null,
        list: state.list,
      };

    default:
      return state;
  }
};
