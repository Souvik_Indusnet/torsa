import {
  GET_ALL_CATEGORY_REQ,
  GET_ALL_CATEGORY_SUCCESS,
  GET_ALL_CATEGORY_FAIL,
  GET_ALL_PART_REQ,
  GET_ALL_PART_SUCCESS,
  GET_ALL_PART_FAIL,
  setProductsuccess,
  setProductFail,
  GET_ORDER_NOT_SUCCESS,
  GET_ORDER_NOT_FAIL,
  GET_ORDER_NOTIFICATION,
} from "../constant/productConstant";

export const CategoryReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_ALL_CATEGORY_REQ:
      return { loading: false };
    case GET_ALL_CATEGORY_SUCCESS:
      return { loading: false, category: action.payload };
    case GET_ALL_CATEGORY_FAIL:
      return { loading: false, payload: action.payload };
    default:
      return state;
  }
};

export const partReducers = (state = {}, action) => {
  switch (action.type) {
    case GET_ALL_PART_REQ:
      return { loading: false };
    case GET_ALL_PART_SUCCESS:
      return { loading: false, parts: action.payload };
    case GET_ALL_PART_FAIL:
      return { loading: false, payload: action.payload };

    default:
      return state;
  }
};

export const editpartReducers = (state = {}, action) => {
  switch (action.type) {
    case setProductsuccess:
      return { machines: action.payload };
    case setProductFail:
      return { error: action.payload };
    default:
      return state;
  }
};

export const getOrderNotificationReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_ORDER_NOTIFICATION:
      return { loading: true };
    case GET_ORDER_NOT_SUCCESS:
      return { loading: false, notification: action.payload };
    case GET_ORDER_NOT_FAIL:
      return { loading: false, msg: "Nothing Else" };
    default:
      return state;
  }
};
