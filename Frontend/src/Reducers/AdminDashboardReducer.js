import {
GET_ADMIN_DASHBOARD_LOADING,
GET_ADMIN_DASHBOARD_SUCCESS,
GET_ADMIN_DASHBOARD_FAIL
      } from "../constant/productConstant";
      
      const initalState = {
        loading: false,
        error: null,
        tableList: null,
        stateChange: false,
        success: null,
      };
      
    const reducer = (state = initalState, action) => {
        const { type, payload } = action;
        switch (type) {
          case GET_ADMIN_DASHBOARD_LOADING:
            return {
              ...state,
              loading: true,
              error: null,
            };
          case GET_ADMIN_DASHBOARD_FAIL:
            return {
              ...state,
              loading: false,
              error: payload,
            };
          case GET_ADMIN_DASHBOARD_SUCCESS: //get all
            return {
              ...state,
              loading: false,
              // stateChange: !state.stateChange,
              tableList: payload,
              error: null,
            };
          case "defaultCaseBusinessAdminDashboard":
            return {
              loading: false,
              error: null,
              tableList: null,
              stateChange: false,
              success: null,
            };
          default:
            return state;
        }
      };
      
      export default reducer;