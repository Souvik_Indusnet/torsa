import {
  ADMIN_GET_FAIL,
  ADMIN_GET_REQ,
  ADMIN_GET_SUCCESS,
} from "../constant/userConstant";

export const getadmin = (state = {}, action) => {
  switch (action.type) {
    case ADMIN_GET_REQ:
      return { loading: true };
    case ADMIN_GET_SUCCESS:
      return { loading: false, adminInfo: action.payload };
    case ADMIN_GET_FAIL:
      return { loading: false, error: action.err };
    default:
      return state;
  }
};
