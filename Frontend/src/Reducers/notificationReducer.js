import {
    NOTIFICATION_LOADING,
    NOTIFICATION_ERROR,
    NOTIFICATION_SUCCESS,     
} from "../constant/userConstant";
      
      const initalState = {
        loading: false,
        error: null,
        tableList: null,
        stateChange: false,
        success: null,
      };
      
const reducer = (state = initalState, action) => {
    const { type, payload } = action;
        switch (type) {
          case NOTIFICATION_LOADING:
            return {
              ...state,
              loading: true,
              error: null,
            };
          case NOTIFICATION_ERROR:
            return {
              ...state,
              loading: false,
              error: payload,
            };
          case NOTIFICATION_SUCCESS: //get all
            return {
              ...state,
              loading: false,
              // stateChange: !state.stateChange,
              tableList: payload,
              error: null,
            };
        case "defaultCaseAdmin":
            return {
              loading: false,
              error: null,
              tableList: null,
              stateChange: false,
              success: null,
            };
        default:
           return state;
        }
      };
export default reducer;