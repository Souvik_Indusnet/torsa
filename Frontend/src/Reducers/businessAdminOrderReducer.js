import {
//    ADMIN_SUBAdmin_CREATED,
      BUSINESS_ADMIN_LOADING,
      BUSINESS_ADMIN_ERROR,
      BUSINESS_ADMIN_SUCCESS,
    // ADMIN_SUBAdmin_UPDATED,
    // ADMIN_SUBAdmin_STATUS_SUCCESS,
  } from "../constant/userConstant";
  
  const initalState = {
    loading: false,
    error: null,
    tableList: null,
    stateChange: false,
    success: null,
  };
  
const reducer = (state = initalState, action) => {
    const { type, payload } = action;
    switch (type) {
      case BUSINESS_ADMIN_LOADING:
        return {
          ...state,
          loading: true,
          error: null,
        };
      case BUSINESS_ADMIN_ERROR:
        return {
          ...state,
          loading: false,
          error: payload,
        };
      case BUSINESS_ADMIN_SUCCESS: //get all
        return {
          ...state,
          loading: false,
          // stateChange: !state.stateChange,
          tableList: payload,
          error: null,
        };
    //   case ADMIN_SUBAdmin_CREATED: //created
    //     return {
    //       ...state,
    //       loading: false,
    //       stateChange: !state.stateChange,
    //       error: null,
    //       success: true,
    //     };
    //   case ADMIN_SUBAdmin_UPDATED:
    //     return {
    //       ...state,
    //       loading: false,
    //       stateChange: !state.stateChange,
    //       error: null,
    //       success: true,
    //     };
      case "defaultCaseAdmin":
        return {
          loading: false,
          error: null,
          tableList: null,
          stateChange: false,
          success: null,
        };
      default:
        return state;
    }
  };
  
  export default reducer;