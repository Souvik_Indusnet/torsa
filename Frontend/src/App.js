import { Suspense, useEffect } from 'react';
import { ThemeProvider } from '@material-ui/styles';
import RouterConfig from './router';
import theme from './theme/palette';
import { SnackbarProvider } from 'notistack';
import SimpleBackdrop from './Components/backdrop/Backdrop';
import { cssFile, scriptJS } from './script/jsStyle';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './pages/login/Login';
import TheLayout from './container/TheLayout';
import Notfound from './pages/notfound/Notfound';
import NetworkDetector from './Components/Network/network.js'
function App() {
  useEffect(() => {
    cssFile.forEach((element) => {
      const mycss = document.createElement('link');
      mycss.rel = 'stylesheet';
      mycss.href = element.files;
      mycss.type = 'text/css';
      mycss.async = true;
      mycss.onload = () => {
        console.log('mycss loaded');
      };
      document.head.appendChild(mycss);
    });

    scriptJS?.forEach((element) => {
      const script = document.createElement('script');
      script.src = element.s1;
      script.async = true;
      script.onload = () => {
        console.log('script loaded');
      };
      document.body.appendChild(script);
    });
  }, []);

  return (
    <BrowserRouter>
      <Suspense fallback={<SimpleBackdrop open="true" />}>
        <Switch>
          <ThemeProvider theme={theme}>
            <SnackbarProvider>
              <Route
                exact
                path="/admin-signin"
                name="login"
                render={(props) => <Login {...props} />}
              />
              <Route
                exact
                path="/404"
                name="Not found"
                render={(props) => <Notfound {...props} />}
              />
              <Route
                path="/"
                name="home "
                render={(props) => <TheLayout {...props} />}
              />
            </SnackbarProvider>
          </ThemeProvider>
        </Switch>
      </Suspense>
    </BrowserRouter>
  );
}

export default NetworkDetector(App);

// import { Suspense, useEffect } from 'react';
// import { ThemeProvider } from '@material-ui/styles';
// import RouterConfig from './router';
// import theme from './theme/palette';
// import { SnackbarProvider } from 'notistack';
// import SimpleBackdrop from './Components/backdrop/Backdrop';
// import { cssDynamo } from './script/jsStyle';

// function App() {
//   useEffect(() => {
//     cssDynamo?.forEach((element) => {
//       const mycss = document.createElement('link');
//       mycss.rel = 'stylesheet';
//       mycss.href = element.s1;
//       mycss.type = 'text/css';
//       mycss.async = true;
//       mycss.onload = () => {
//         console.log('mycss loaded');
//       };
//       document.head.appendChild(mycss);
//     });
//   }, []);

//   return (
//     <ThemeProvider theme={theme}>
//       <SnackbarProvider>
//         <Suspense fallback={<SimpleBackdrop open="true" />}>
//           <RouterConfig />
//         </Suspense>
//       </SnackbarProvider>
//     </ThemeProvider>
//   );
// }

// export default App;
