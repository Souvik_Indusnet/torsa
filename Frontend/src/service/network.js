import axios from "axios";

const resolver = async (promise, request, body, token) => {
  const resolved = {
    data: null,
    error: null,
  };
  try {
    resolved.data = await axios({
      method: request,
      url: promise,
      headers: {
        // 'Content-Type': 'multipart/form-data',
        Authorization: "Bearer " + token,
      },
      data: body,
    });
  } catch (e) {
    resolved.error = e;
  }
  return resolved;
};

export { resolver };
