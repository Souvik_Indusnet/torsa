let IPAddress = 'https://torsa.herokuapp.com/';
// let IPAddress = 'http://localhost:5000/';
// let IPAddress = 'http://localhost:4000/';

export const apiService = {
  IPAddress: IPAddress,
  login: IPAddress + 'admin/signin',

  //users
  getAlluser: IPAddress + 'admin/getAllUser',
  userpagination: IPAddress + 'admin/getallusers',
  userStatus: IPAddress + 'admin/updateUserStatus',
  userimage: IPAddress + 'uploads',

  TempIMageUpload: IPAddress + 'admin/imageupload',
  imagepathPro: IPAddress + 'uploads/product/',
  dashboard: IPAddress + 'admin/dashboard',
  badashboard: IPAddress + 'admin/businessadmindashboard',
  getAllMAchines: IPAddress + 'product/machine',
  getAllEquipments: IPAddress + 'product/machineName',
  getAllCategory: IPAddress + 'product/category',
  editCategory: IPAddress + 'product/edit-category',
  getAllMachinesInfo: IPAddress + 'product/parts',
  addBusinessAccount: IPAddress + 'admin/addBuisnessAdmin',
  addcustomer: IPAddress + 'admin/addUser',
  getAdminUser: IPAddress + 'admin/getAllAdmin',
  deleteUser: IPAddress + 'admin/deleteUser/',
  deleteAdmin: IPAddress + 'admin/deleteAdmin/',
  updateBlock: IPAddress + 'admin/updateUserStatus/',
  updateAdminBlock: IPAddress + 'admin/updateAdmin/',
  updateAdminAccount: IPAddress + 'admin/updateAdmin/',
  updateuser: IPAddress + 'admin/updateUser/',
  getUserById: IPAddress + 'admin/getUser/',
  getUserDetailsById: IPAddress + 'admin/userDetailsById/',
  getMachinById: IPAddress + 'product/machineById/',
  createMachine: IPAddress + 'product/machine',
  deleteEquipment: IPAddress + 'product/machineName/',
  getParts: IPAddress + 'product/parts',
  createCategory: IPAddress + 'product/category',
  getManagerById: IPAddress + 'admin/getAdmin/',

  createPart: IPAddress + 'product/parts',
  partDetailsByID: IPAddress + 'admin/partDetailsByid',

  deletePart: IPAddress + 'product/parts',
  blockParts: IPAddress + 'product/parts',
  getallSubcategory: IPAddress + 'product/subcategory',

  //CMS
  getAllPages: IPAddress + 'cms/cmspages',
  updateStatusCMS: IPAddress + 'cms/update-status',
  createcms: IPAddress + 'cms/create-page',
  updateCMS: IPAddress + 'cms/edit-page',

  //Inventory routes
  getUpdateInventory: IPAddress + 'product/partById/',
  deletecategory: IPAddress + 'product/category',
  deletSubcategory: IPAddress + 'product/subcategory',
  deleteMachine: IPAddress + 'product/machine',
  machineModel: IPAddress + 'product/machineModel',

  getmachineByarray: IPAddress + 'product/getmachinesInfo',

  //business admin routes
  getAdminByEachId: IPAddress + 'admin/userAdmin/',
  updateAdminByEachId: IPAddress + 'admin/updateUserAdmin',
  getUserByAdminID: IPAddress + 'admin/getUserByadminId',
  getAdminProfileInfo: IPAddress + 'admin/getAdminProfile',
  businessAdminGraph: IPAddress + 'admin/getbusinessAdminDashboard',
  businessAdminPagination: IPAddress + 'admin/getallbusinessadmin',
  updateStatus: IPAddress + 'admin/updateAdminStatus',

  //upload end points
  uploadpartImage: IPAddress + 'upload/',
  uploadAdminImage: IPAddress + 'upload/admin',

  //order endpoint
  getAllorders: IPAddress + 'admin/orderTable',
  getOrderUser: IPAddress + 'order/orderUser',
  getGraph: IPAddress + 'admin/dashboardgraph',
  OrderDelStatus: IPAddress + 'admin/order_del_status',
  orderDetailsByID: IPAddress + 'admin/orderDetailsByid',
  getAllordersByBusinessAdmin: IPAddress + 'admin/orderTable/orderlist',
  //image show end poin,
  productImage: IPAddress + 'uploads/product',

  //master setting equipment modal
  eqquipmentModal: IPAddress + 'product/subcategory',

  getnotification: IPAddress + 'admin/getNotification',
  getOrderNotification: IPAddress + 'user/getOrderNotification',

  //admin notification
  adminNotification: IPAddress + 'admin/allnotification',

  //enquery
  enquery: IPAddress + 'admin/enquiry',

  // payment
  payment: IPAddress + 'admin/payment',

  //paginate
  //*************  master setting    */
  paginateEquipment: IPAddress + 'admin/equipment',
  paginateEquipmentmodal: IPAddress + 'admin/equipmentmodal',

  //csv apis
  userCsv: IPAddress + 'admin/customer-csv',
  productcsv: IPAddress + 'admin/product-csv',
  BACsv: IPAddress + 'admin/ba-csv',
  enquirycsv: IPAddress + 'admin/enquiry-csv',
  paymentCsv: IPAddress + 'admin/payment-csv',
  ordercsv: IPAddress + 'admin/order-csv',
  businessordercsv: IPAddress + 'admin/businsess-order-csv',
  cmsCsv: IPAddress + 'admin/cms-csv',
  equipmentcsv: IPAddress + 'admin/equipment-csv',
  equipment_modalcsv: IPAddress + 'admin/equpment_modal-csv',

  //report
  report: IPAddress + 'admin/Reports',
  reportGraph: IPAddress + 'admin/ReportGraphs',

  //notification create
  createNotification: IPAddress + 'admin/create-notification',

  //notification count
  Notification_count_BA: IPAddress + 'admin/unread-count-notification',
  Notification_BA: IPAddress + 'admin/notification',
  notification_admin: IPAddress + 'admin/created-notification',
};
