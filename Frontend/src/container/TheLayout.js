import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import SimpleBackdrop from '../Components/backdrop/Backdrop';
import Header from '../Components/Dashboard/Header';
import Navbarheader from '../Components/Dashboard/Navbarheader';
import Sidebar from '../Components/Dashboard/Sidebar';
import AuthWrapped from '../wrapped/AuthWrapped';
import TheContains from './TheContains';
import ScriptTag from 'react-script-tag';
import { scriptJS } from '../script/jsStyle';

const TheLayout = () => {
  const { userInfo, loading } = useSelector((state) => state.userSigninInfo);
  const [tag, setTag] = useState('');
  const [menu, showMenu] = React.useState(true);
  const handlecb = (e) => {
    showMenu(e);
  };
  return (
    <AuthWrapped condition={userInfo} loading={loading} path="/admin-signin">
      {navigator.onLine ? (
        <div id="main-wrapper" className={menu ? 'show ' : 'show menu-toggle'}>
          {scriptJS?.map((i, index) => (
            <ScriptTag
              key={index}
              isHydrating={true}
              type="text/javascript"
              src={i.s1}
            />
          ))}
          <Navbarheader cb={handlecb} />
          <Header
            title={
              userInfo && userInfo.isSystemAdmin
                ? ' System Admin '
                : 'Business Admin '
            }
            pageTitle={tag?.Tag}
          />{' '}
          <Sidebar userInfo={userInfo} />
          <SimpleBackdrop open={loading} />
          <TheContains callBack={(e) => setTag(e)} />
        </div>
      ) : (
        <div
          // class="modal fade show"
          id="exampleModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          // style="padding-right: 21px; display: block;"
          aria-modal="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Check Your Internet connection
                </h5>
              </div>
            </div>
          </div>
        </div>
      )}
    </AuthWrapped>
  );
};

export default TheLayout;
