import { Fade, TextareaAutosize } from '@material-ui/core';
import React, { useEffect } from 'react';
import { Redirect, Route, Switch, useHistory } from 'react-router-dom';
import { routes } from '../constant/routes';
import Notfound from '../pages/notfound/Notfound';

const TheContains = ({ callBack }) => {
  const history = useHistory();
  useEffect(() => {
    const findTage = routes?.find((i) => i.path == history?.location.pathname);
    callBack(findTage);
  }, [history?.location.pathname]);
  return (
    <div className="content-body">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12  ">
          <div className={history?.location.pathname=='/dashboard'? null: history?.location.pathname ==="/report-management" ?
          null: history?.location.pathname ==="/my-account" ?
          null: history?.location.pathname ==="/businessadmin_dashboard" ?
          null : "card"}>
              <div className={history?.location.pathname=='/dashboard'?null:"card-body"}>
                <Switch>
                  {routes.map((i) => {
                    return (
                      <Route
                        key={i.path}
                        path={i.path}
                        exact={TextareaAutosize}
                        render={(props) => (
                          <Fade>
                            <i.component {...props} />
                          </Fade>
                        )}
                        // component={i.component}
                      />
                    );
                  })}
                  <Redirect from="/" to="/dashboard" />
                  <Route component={Notfound} />
                </Switch>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TheContains;
