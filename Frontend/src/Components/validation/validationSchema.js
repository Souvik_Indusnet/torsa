import * as yup from 'yup';

const signInschema = yup.object({
  email: yup
    .string()
    .trim()
    .email('Please enter valid Email Address')
    .required('Email is Required'),
  password: yup.string().required('Please Enter your password'),
  // .matches(
  //   /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
  //   'Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character'
  // ),
});

//inventory  validation

const InventorySchema = yup.object({
  // qty: yup.number().required('Qty is Required '),
  qty: yup
    .number()
    .integer('The field should have digits only')
    .required('Qty is Required ')
    .test('Is positive?', ' Qty must be greater than 0!', (value) => value > 0),
});

const productSchema = yup.object({
  name: yup
    .string()
    .trim()
    .matches(/(?!=[-])(?=.*[A-Za-z])/, 'Is not in correct format')
    .required('Name is Required')
    .max(60, 'max 60'),
  description: yup
    .string()
    .trim()
    .matches(
      /(?!=.*[@$-`!';:!%*#?&])(?=.*[A-Za-z])/,
      'Is not in correct format'
    )

    .required('Description is Required'),
  amount: yup.number().required('Amount is Required'),
  stock: yup.number().required('Stock is Required'),
});

const CmsPage = yup.object({
  pagename: yup.string().required(' Page name is Required'),
  // content: yup.string().required(' Description is Required'),
});

const customerSchema = yup.object({
  name: yup
    .string()
    .trim()
    .matches(/^([a-zA-Z]+\s)*[a-zA-Z]+$/, 'Is not in correct format')
    .required('Name is Required')
    .max(30, 'max 30'),
  email: yup
    .string()
    .trim()
    .email('Please enter valid Email Address')
    .required('Email is Required'),
  phone: yup
    .string()
    .required('Phone number is Required *')
    .matches(
      /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g,
      'Not a Valid Number*'
    ),
  businesadmin: yup.string().required('Business Admin is Required'),
});

const businessAdminSchema = yup.object({
  name: yup
    .string()
    .trim()
    .matches(/^([a-zA-Z]+\s)*[a-zA-Z]+$/, 'Is not in correct format')
    .required('Name is Required')
    .max(30, 'max 30'),
  email: yup
    .string()
    .trim()
    .email('Please enter valid Email Address')
    .required('Email is Required'),
  phone: yup
    .string()
    .required('Phone number is Required *')
    .matches(
      /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g,
      'Not a Valid Number*'
    ),
});

//master setting
// 1) Equipment modal
const EquipmentModalSchema = yup.object({
  name: yup
    .string()
    .trim()
    .matches(
      /(?!=.*[@$-`!';:!%*#?&])(?=.*[A-Za-z])/,
      'Is not in correct format'
    )
    .required('Name is Required')
    .max(30, 'max 30'),
  equipment: yup.string().required('Equipment is Required'),
});

// .string()
// .trim()
// .matches(
//   /(?!=.*[@$-`!';:!%*#?&])(?=.*[A-Za-z])/,
//   'Is not in correct format'
// )

//master setting
// 2) Equipment
const EquipmentSchema = yup.object({
  name: yup
    .string()
    // .trim()
    // .matches(/^([a-zA-Z]+\s)*[a-zA-Z]+$/, 'Is not in correct format')
    .required('Name is Required')
    .max(30, 'max 30'),
});

export {
  signInschema,
  customerSchema,
  EquipmentSchema,
  EquipmentModalSchema,
  businessAdminSchema,
  InventorySchema,
  productSchema,
  CmsPage,
};
