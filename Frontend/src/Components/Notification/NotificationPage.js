import * as React from 'react';
import { styled } from '@mui/material/styles';
import Divider from '@mui/material/Divider';
import Chip from '@mui/material/Chip';
import { useDispatch, useSelector } from 'react-redux';
import {
  BA_NOtification,
  MarkRead,
  MarkReadAll,
} from '../../Actions/notification';
import { Link } from 'react-router-dom';
import PaginationControlled from '../Table/AdvancedPagination';
import SimpleBackdrop from '../backdrop/Backdrop';
import DateFormate from '../Utils/DateFormate';

const Root = styled('div')(({ theme }) => ({
  width: '100%',
  ...theme.typography.body2,
  '& > :not(style) + :not(style)': {
    marginTop: theme.spacing(2),
  },
}));

const NotificationPage = ({ notificationOrder }) => {
  const dispatch = useDispatch();

  const { list, loading } = useSelector((state) => state.BA_NOTIFICATION_LIST);
  const { list: Totalcount } = useSelector(
    (state) => state.BA_NOTIFICATION_COUNT
  );
  React.useEffect(() => {
    dispatch(BA_NOtification({ page: 1 }));
  }, [dispatch]);

  return (
    <Root>
      <SimpleBackdrop open={loading} />

      <div style={{ textAlign: 'right' }}>
        <button
          onClick={() => dispatch(MarkReadAll())}
          className="ibt-not-mark-read-btn no-border"
          // disabled={Totalcount == 0}
        >
          Mark all read
        </button>
      </div>

      {list?.map((el) => (
        <div
          className={
            !el?.generateCount ? 'ibt-not-read-row' : 'ibt-not-unread-row'
          }
        >
          <div className="ibt-not-mark-read" style={{ textAlign: 'right' }}>
            <a
              onClick={() => Totalcount != 0 && dispatch(MarkRead(el._id))}
              // href="#"
              title="Mark read"
              className={'flaticon-381-view'}
            ></a>
          </div>
          <span
            className={
              el?.generateCount ? 'ringing-bell flaticon-381-ring' : ''
            }
          ></span>
          <div className="ml-25 mr-20">
            {el?.data?.msg}&nbsp; &nbsp;
            {el?.data?.url && (
              <Link
                className="ibt-not-link-btn"
                target="_blank"
                to={el?.data?.url}
              >
                Link
              </Link>
            )}
          </div>
          <div className="ibt-not-datetime">
            <DateFormate formatter={el?.createdAt} />
          </div>
          {/* <Divider></Divider> */}
        </div>
      ))}
      {/* <PaginationControlled
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      /> */}
      {/* <Divider textAlign="right">RIGHT</Divider>
      {content}
      <Divider></Divider>
      <br />
      <Divider textAlign="right">RIGHT</Divider>
      {content}
      <Divider></Divider>
      <Divider textAlign="right">RIGHT</Divider>
      {content}
      <Divider></Divider> */}
    </Root>
  );
};

export default NotificationPage;
