// import React, { useEffect, useState } from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import {
//   PieChart,
//   Pie,
//   Sector,
//   Cell,
//   ResponsiveContainer,
//   Tooltip,
// } from 'recharts';
// import { getlistReport } from '../../Actions/userAction';
// const SalesPieChart = ({}) => {
//   //    var demoUrl = 'https://codesandbox.io/s/pie-chart-with-customized-label-dlhhj';
//   const dispatch = useDispatch();
//   const [data, setData] = useState([]);

//   const COLORS = ['#50C878'];

//   const RADIAN = Math.PI / 180;
//   const renderCustomizedLabel = ({
//     name,
//     cx,
//     cy,
//     midAngle,
//     innerRadius,
//     outerRadius,
//     percent,
//     index,
//   }) => {
//     const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
//     const x = cx + radius * Math.cos(-midAngle * RADIAN);
//     const y = cy + radius * Math.sin(-midAngle * RADIAN);

//     return (
//       <text
//         x={x}
//         y={y}
//         fill="white"
//         textAnchor={x > cx ? 'start' : 'end'}
//         dominantBaseline="central"
//       >
//         {`${name}(${(percent * 100).toFixed(0)}%)`}
//       </text>
//     );
//   };
//   useEffect(() => {
//     dispatch(getlistReport()).then((data) => {
//       console.log(data);
//       setData(data);
//     });
//   }, [getlistReport]);
//   return (
//     <PieChart width={1200} height={600}>
//       <Pie
//         data={data}
//         cx="50%"
//         cy="50%"
//         labelLine={false}
//         label={renderCustomizedLabel}
//         outerRadius={300}
//         fill="#8884d8"
//         dataKey="value"
//         isAnimationActive={false}
//         innerRadius={60}
//         paddingAngle={5}
//       >
//         {data.map((entry, index) => (
//           <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
//         ))}
//       </Pie>
//       <Tooltip />
//     </PieChart>
//   );
// };
// export default SalesPieChart;

// import React, { useCallback, useEffect, useState } from 'react';
// import { PieChart, Pie, Cell, Tooltip } from 'recharts';
// import { useDispatch, useSelector } from 'react-redux';
// import { getlistReport } from '../../Actions/userAction';

// const COLORS = ['#50C878', '#00C49F', '#50C878', '#FF8042'];
// // const COLORS = ['#50C878'];
// const RADIAN = Math.PI / 180;
// const renderCustomizedLabel = ({
//   cx,
//   cy,
//   midAngle,
//   innerRadius,
//   outerRadius,
//   percent,
//   index,
// }) => {
//   const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
//   const x = cx + radius * Math.cos(-midAngle * RADIAN);
//   const y = cy + radius * Math.sin(-midAngle * RADIAN);

//   return (
//     <text
//       x={x}
//       y={y}
//       fill="white"
//       textAnchor={x > cx ? 'start' : 'end'}
//       dominantBaseline="central"
//     >
//       {`${name}(${(percent * 100).toFixed(0)}%)`}
//     </text>
//   );
// };
// export default function SalesPieChart() {
//   const dispatch = useDispatch();
//   const [data, setData] = useState([]);

//   useEffect(() => {
//     dispatch(getlistReport()).then((data) => {
//       console.log(data);
//       setData(data);
//     });
//   }, [getlistReport]);

//   return (
//     <PieChart width={1200} height={600}>
//       <Pie
//         data={data}
//         cx={'50%'}
//         cy={'50%'}
//         labelLine={false}
//         label={renderCustomizedLabel}
//         outerRadius={300}
//         fill="#8884d8"
//         dataKey="value"
//       >
//         {data.map((entry, index) => (
//           <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
//         ))}
//       </Pie>
//       <Tooltip />
//     </PieChart>
//   );
// }

import React, { useEffect, PureComponent, useState } from 'react';
import { PieChart, Pie, Sector, ResponsiveContainer } from 'recharts';
import { useDispatch, useSelector } from 'react-redux';
import { getlistReport } from '../../Actions/userAction';

// const data = [
//   { name: 'Group A', value: 400 },
//   { name: 'Group B', value: 300 },
//   { name: 'Group C', value: 300 },
//   { name: 'Group D', value: 200 },
// ];

const renderActiveShape = (props) => {
  const RADIAN = Math.PI / 180;
  const {
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    fill,
    payload,
    percent,
    value,
  } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? 'start' : 'end';

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
        {payload.name}
      </text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path
        d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
        stroke={fill}
        fill="none"
      />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 12}
        y={ey}
        textAnchor={textAnchor}
        fill="#333"
      >{`Sales ${value}`}</text>
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 12}
        y={ey}
        dy={18}
        textAnchor={textAnchor}
        fill="#999"
      >
        {`(Sales Rate ${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  );
};

export default function SalesPieChart() {
  const [state, setState] = useState({ activeIndex: 0 });
  const onPieEnter = (_, index) => {
    setState({
      activeIndex: index,
    });
  };

  const dispatch = useDispatch();
  const [data, setData] = useState([]);

  useEffect(() => {
    dispatch(getlistReport()).then((data) => {
      console.log(data);
      setData(data);
    });
  }, [getlistReport]);

  return (
    // <ResponsiveContainer width="100%" height="100%">
    <PieChart width={1200} height={600}>
      <Pie
        activeIndex={state.activeIndex}
        activeShape={renderActiveShape}
        data={data}
        cx="50%"
        cy="50%"
        innerRadius={150}
        outerRadius={180}
        fill="#50C878"
        dataKey="value"
        onMouseEnter={onPieEnter}
      />
    </PieChart>
    // </ResponsiveContainer>
  );
}
