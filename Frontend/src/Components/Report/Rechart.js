import React, { useState,useEffect } from 'react';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { useDispatch, useSelector } from 'react-redux';
import { getlistReportGraph } from '../../Actions/userAction';
const Example =()=> {
  const dispatch = useDispatch();    
  const [data,setData] = useState([]);
  const [year,setYear] = useState(new Date().getFullYear());
  useEffect(() => {
    dispatch(getlistReportGraph(year)).then((data) => {
      setData(data);
      console.log(data);
    });
  }, []);
  useEffect(() => {
    dispatch(getlistReportGraph(year)).then((data) => {
      setData(data);
      console.log(data);
    });
  }, [year]);
    return (
      <> 
         <h4>Year
           <select
                className=" form-select-lg mb-3 mr-2"
                aria-label=".form-select-lg example"
                onChange={(e) => setYear(e.target.value)}
              >
                <option selected value="2022">
                  2022
                </option>
                <option value="2021">2021</option>
                <option value="2020">2020</option>
         </select>
         </h4>    
         
        {data.length!=0?
        <BarChart width={1200} height={600} data={data}>
          <Bar dataKey="count" fill="#8884d8" />
          <XAxis dataKey="name" interval={'preserveStartEnd'}/>
          <YAxis />
        </BarChart>:''}
      </>
    );
}

  
export default Example;
  