import { Button } from '@material-ui/core';
import React from 'react';
import { useHistory } from 'react-router';

const CancelButton = ({ back_path, ButtonTitle }) => {
  const history = useHistory();
  return (
    <Button
      onClick={() => history.push(back_path)}
      className="btn tp-btn-light btn-primary btn-cancel width-100"
    >
      {ButtonTitle}
    </Button>
  );
};

export default CancelButton;
