import React from 'react';

const SubmitButton = ({ loading, ButtonName }) => {
  return (
    <button type="submit" className="btn tp-btn-light btn-primary width-100">
      {loading ? 'Loading...' : ButtonName}
    </button>
  );
};

export default SubmitButton;
