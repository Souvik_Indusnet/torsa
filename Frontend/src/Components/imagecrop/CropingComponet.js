import { useEffect, useState } from 'react';
import ReactCrop from 'react-image-crop';
import imageCompression from 'browser-image-compression';
import 'react-image-crop/dist/ReactCrop.css';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const CropingComponet = ({
  children,
  preview,
  baseIMG,
  CropCancle,
  result,
  setResult,
}) => {
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState(null);
  const [crop, setCrop] = useState({ aspect: 16 / 9 });
  // const [result, setResult] = useState(true);

  function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
  }

  // useEffect(() => {
  //   setResult(true);
  // }, [setResult]);

  const getCroppedImg = async () => {
    try {
      const canvas = document.createElement('canvas');
      const scaleX = image.naturalWidth / image.width;
      const scaleY = image.naturalHeight / image.height;
      canvas.width = crop.width;
      canvas.height = crop.height;
      const ctx = canvas.getContext('2d');
      ctx.drawImage(
        image,
        crop.x * scaleX,
        crop.y * scaleY,
        crop.width * scaleX,
        crop.height * scaleY,
        0,
        0,
        crop.width,
        crop.height
      );

      const base64Image = canvas.toDataURL('image/jpeg', 1);

      var file = dataURLtoFile(base64Image, 'cropped-image.jpeg');
      //   console.log(file);
      compresseing(file);
    } catch (e) {
      console.log(e);
      console.log('crop the image');
    }
  };

  const compresseing = async (image) => {
    // let file = URL.createObjectURL(image)
    setLoading(true);
    const options = {
      maxSizeMB: 2,
      maxWidthOrHeight: 800,
      useWebWorker: true,
    };

    try {
      const output = await imageCompression(image, options);

      //   setShow(!show);
      var file = new File([output], 'image.jpeg');
      console.log(file);
      baseIMG(file);
      setLoading(false);
      setResult(false);
    } catch (error) {
      console.log(error);
    }
  };

  // const [open, setOpen] = useState(true);
  // const handleOpen = () => setOpen(true);
  const handleClose = () => setResult(false);

  return (
    <div>
      {preview != null && result ? (
        <>
          {/* <Button onClick={handleOpen}>Open modal</Button> */}
          <Modal
            open={result}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box sx={style}>
              <ReactCrop
                style={{ maxWidth: '50%' }}
                src={preview}
                onImageLoaded={setImage}
                crop={crop}
                onChange={setCrop}
              />
              <div>
                <Button
                  // variant="contained"
                  onClick={() => {
                    CropCancle();
                    setResult(false);
                  }}
                  color="secondary"
                >
                  Close
                </Button>
                <Button
                  variant="contained"
                  onClick={getCroppedImg}
                  color="primary"
                >
                  {loading ? 'Loading...' : 'Crop'}
                </Button>
              </div>
            </Box>
          </Modal>
        </>
      ) : (
        // <div
        //   alignment="center"
        //   className="show d-block position-static"
        //   visible
        // >
        //   <div>
        //     <ReactCrop
        //       style={{ maxWidth: '50%' }}
        //       src={preview}
        //       onImageLoaded={setImage}
        //       crop={crop}
        //       onChange={setCrop}
        //     />
        //   </div>

        //   <div>
        //     <button
        //       onClick={() => {
        //         CropCancle();
        //         setResult(false);
        //       }}
        //       color="secondary"
        //     >
        //       Close
        //     </button>
        //     <button onClick={getCroppedImg} color="primary">
        //       {loading ? 'Loading...' : 'Crop'}
        //     </button>
        //   </div>
        // </div>

        <> {children}</>
      )}
    </div>
  );
};

export default CropingComponet;
