import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../../Actions/userAction';
import Chip from '@material-ui/core/Chip';
import {
  getBANotiicationCOunt,
  getOrderNotification,
} from '../../Actions/notification';
import NotificationsIcon from '@mui/icons-material/Notifications';
import AccountCircleIcon from '@mui/icons-material/Person';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import { Badge } from '@material-ui/core';
const Header = ({ title, pageTitle }) => {
  const dispatch = useDispatch();
  // const { loading, notification: newnote } = useSelector(
  //   (state) => state.OrderNote
  // );
  const { userInfo, payload } = useSelector((state) => state.userSigninInfo);
  const { list } = useSelector((state) => state.BA_NOTIFICATION_COUNT);
  // const [notification, setNotification] = useState({
  //   details: [],
  //   ask: [],
  // });
  // console.log(notification);

  // useEffect(() => {
  //   if (userInfo) {
  //     // dispatch(getOrderNotification());
  //     let setnow = setInterval(() => {
  //       dispatch(getOrderNotification());
  //     }, 6000);
  //     if (newnote) {
  //       setNotification({
  //         ...notification,
  //         details: newnote.order,
  //         ask: newnote.askForPrice,
  //       });
  //     }
  //     // if (!userInfo) {
  //     clearInterval(setnow);
  //     // }
  //   }
  // }, [notification.details]);

  //BA notification

  useEffect(() => {
    if (userInfo && !userInfo.isSystemAdmin) {
      dispatch(getBANotiicationCOunt());
    }
  }, [dispatch, !userInfo.isSystemAdmin]);

  return (
    <div className="header">
      <div className="header-content">
        <nav className="navbar navbar-expand">
          <div className="collapse navbar-collapse justify-content-between">
            <div className="header-left">
              <div
                className="dashboard_bar"
                // style={{
                //   fontVariant: 'stylistic',
                //   fontSize: '20px',
                //   color: 'black',
                // }}
              >
                {pageTitle}
              </div>
            </div>
            <ul className="navbar-nav header-right position-fixed">
              {userInfo && !userInfo.isSystemAdmin && (
                <li className="nav-item dropdown notification_dropdown">
                  <Link
                    to="/admin/notification"
                    className="nav-link primary"
                    onClick={() => {
                      console.log('check');
                    }}
                  >
                    <Badge badgeContent={list || 0} color="primary">
                      <NotificationsIcon />
                    </Badge>
                  </Link>
                </li>
              )}

              <li className="nav-item dropdown header-profile">
                <a
                  className="nav-link"
                  // href="#"
                  role="button"
                  data-toggle="dropdown"
                >
                  <AccountCircleIcon />
                </a>
                <div className="dropdown-menu dropdown-menu-right">
                  <span
                    style={{ cursor: 'pointer' }}
                    onClick={() => dispatch(logout())}
                    className="dropdown-item ai-icon"
                  >
                    <ExitToAppIcon />
                    <span className="ml-2">Sign-out </span>
                  </span>
                </div>
              </li>

              <li className="dropdown schedule-event-inner">
                <span
                  // href="javascript:void(0);"
                  className="btn btn-outline-primary btn-rounded event-btn"
                  style={{ pointerEvents: 'none' }}
                >
                  <svg
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="scale5 mr-0 mb-0 mr-sm-2 mb-sm-1"
                  >
                    <path
                      d="M19 4H5C3.89543 4 3 4.89543 3 6V20C3 21.1046 3.89543 22 5 22H19C20.1046 22 21 21.1046 21 20V6C21 4.89543 20.1046 4 19 4Z"
                      stroke="white"
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    ></path>
                    <path
                      d="M16 2V6"
                      stroke="white"
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    ></path>
                    <path
                      d="M8 2V6"
                      stroke="white"
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    ></path>
                    <path
                      d="M3 10H21"
                      stroke="white"
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    ></path>
                  </svg>
                  <Chip label={title} disabled />
                  {/* <i className="fa fa-caret-right scale3 ml-2 d-none d-sm-inline-block"></i> */}
                </span>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  );
};

export default Header;
