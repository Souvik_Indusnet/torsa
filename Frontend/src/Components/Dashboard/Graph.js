import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  ComposedChart,
  Line,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from 'recharts';
import { getOrder } from '../../Actions/orderAction';
import { GraphTable } from '../../Actions/sideAdminAction';

export default function Graph() {
  const [orderid, setOrderid] = useState([]);
  const [order, setOrder] = useState('');
  const { loading, userInfo } = useSelector((state) => state.userSigninInfo);

  const dispatch = useDispatch();

  useEffect(() => {
    if (userInfo && userInfo.isSystemAdmin) {
      dispatch(getOrder()).then((data) => {
        setOrder(data);
      });
    } else {
      dispatch(GraphTable(userInfo._id)).then((data) => {
        setOrder(data);
      });
    }

    // if (order) {
    //   let uniqueChars = [...new Set(orderid)];

    //   const mydata = order.filter(
    //     (i, index) => i.product._id === uniqueChars[index]
    //   );
    //   console.log(mydata);
    // }
  }, [getOrder]);

  return (
    <>
      <ComposedChart
        width={1000}
        height={500}
        data={order}
        margin={{
          top: 20,
          right: 20,
          bottom: 20,
          left: 20,
        }}
      >
        <CartesianGrid stroke="#f5f5f5" />
        <XAxis dataKey="partName" scale="band" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="orderQty" barSize={40} fill="#413ea0" />
        {/* <Line type="monotone" dataKey="orderQty" stroke="#ff7300" /> */}
      </ComposedChart>
    </>
  );
}
