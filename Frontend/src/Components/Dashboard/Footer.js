import React from 'react';

const Footer = () => {
  return (
    <div className="footer">
      <div className="copyright">
        <p>Coyright @ 2021 Torsa</p>
      </div>
    </div>
  );
};

export default Footer;
