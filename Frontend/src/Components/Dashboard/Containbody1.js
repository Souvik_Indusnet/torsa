import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getlistBADashboard } from '../../Actions/userAction';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { DropdownButton, Dropdown } from 'react-bootstrap';
import PeopleAltSharpIcon from '@mui/icons-material/PeopleAltSharp';
import PeopleOutlineSharpIcon from '@mui/icons-material/PeopleOutlineSharp';
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';
const Containbody1 = () => {
  const dispatch = useDispatch();
  const { userInfo } = useSelector((state) => state.userSigninInfo);
  const { loading, error, tableList, stateChange, success, year } = useSelector(
    (state) => state.Business_Admin_Dashboard
  );
  const years = ['2022', '2021', '2020'];
  useEffect(() => {
    // console.log('233333')
    // console.log(year);
    getAlllistBADashboard(year);
  }, []);
  const getAlllistBADashboard = (year) => {
    dispatch(getlistBADashboard(year));
  };
  return (
    <div>
      {!userInfo.isSystemAdmin && !loading && (
        <div className="col-lg-12">
          {!loading && (
            <div>
              <div className="card">
              <div className='row'>
              {tableList?.signInList &&
                tableList?.signInList.map((p, i) => {
                  return (
                    <div className="col-xl-3 col-xxl-6 col-lg-6 col-sm-6">
                      <div className="widget-stat ">
                        <div className=" p-4">
                          <div className="media ai-icon">
                            <span className="mr-3 bgl-primary text-primary">
                              <PeopleAltSharpIcon />
                            </span>
                            <div className="media-body">
                              <p className="mb-1">
                                User Sign-In (
                                {i == 0
                                  ? 'Today'
                                  : i == 1
                                  ? 'Last Week'
                                  : i == 2
                                  ? 'Last Month'
                                  : 'Last Year'}
                                )
                              </p>
                              {p && <h4 className="mb-0">{p}</h4>}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              <div className="col-xl-3 col-xxl-6 col-lg-6 col-sm-6">
                <div className="widget-stat ">
                  <div className=" p-4">
                    <div className="media ai-icon">
                      <span className="mr-3 bgl-secondary text-secondary">
                        <PeopleOutlineSharpIcon />
                      </span>
                      <div className="media-body">
                        <p className="mb-1">Customer's</p>
                        {tableList?.user && (
                          <h4 className="mb-0">{tableList?.user}</h4>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-xxl-6 col-lg-6 col-sm-6">
                <div className="widget-stat ">
                  <div className=" p-4">
                    <div className="media ai-icon">
                      <span className="mr-3 bgl-success text-success">
                        <svg
                          id="icon-database-widget"
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewbox="0 0 24 24"
                          fill="none"
                          stroke="currentColor"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          className="feather feather-database"
                        >
                          <ellipse cx="12" cy="5" rx="9" ry="3"></ellipse>
                          <path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path>
                          <path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path>
                        </svg>
                      </span>
                      <div className="media-body">
                        <p className="mb-1">Total Equipments</p>
                        {tableList?.machines && (
                          <h4 className="mb-0">{tableList?.machines}</h4>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-xl-3 col-xxl-6 col-lg-6 col-sm-6">
                <div className="widget-stat ">
                  <div className=" p-4">
                    <div className="media ai-icon">
                      <span className="mr-3 bgl-secondary text-secondary">
                        <svg
                          id="icon-database-widget"
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewbox="0 0 24 24"
                          fill="none"
                          stroke="currentColor"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          className="feather feather-database"
                        >
                          <ellipse cx="12" cy="5" rx="9" ry="3"></ellipse>
                          <path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path>
                          <path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path>
                        </svg>
                      </span>
                      <div className="media-body">
                        <p className="mb-1">Total Equipment Part's</p>
                        {tableList?.machineName && (
                          <h4 className="mb-0">{tableList?.machineName}</h4>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
              </div>
              <div className="card">
                <div className="card-header-temp">
                  <Fragment className="list">
                    <h4 className='card-title' style={{ width: '50%' }}>Sales Overview</h4>
                    <Select
                      sx={{ minWidth: 120, maxWidth: 150 }}
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={year}
                      label="Age"
                      onChange={(e) => getAlllistBADashboard(e.target.value)}
                    >
                      {years.map((p) => {
                        return <MenuItem value={p}>{p}</MenuItem>;
                      })}
                    </Select>
                  </Fragment>
                </div>
                <div className="card-body overflow-scroll-hidey">
                  {tableList?.totalSalesCount.length != 0 ? (
                    <div>
                      <BarChart
                        width={800}
                        height={400}
                        data={tableList?.totalSalesCount}
                      >
                        <Bar dataKey="count" fill="#8884d8" />
                        <XAxis dataKey="name" interval={'preserveStartEnd'} />
                        <YAxis />
                      </BarChart>
                    </div>
                  ) : (
                    ''
                  )}
                </div>
              </div>

              {/*            <h3>Year :
              <select
                className=" form-select-lg mb-3 mr-2"
                aria-label=".form-select-lg example"
                onChange={(e) =>getAlllistBADashboard(e.target.value)}
                value={year}
              >
                <option value="2022">
                  2022
                </option>
                <option value="2021">2021</option>
                <option value="2020">2020</option>
              </select>
              </h3>*/}
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default Containbody1;
