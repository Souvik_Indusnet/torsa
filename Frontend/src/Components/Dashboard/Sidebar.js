import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Navbar, BA_NavBar } from '../../constant/Navbar';

const Sidebar = ({ userInfo }) => {
  const history = useHistory();
  const currentLocation = history?.location?.pathname;

  const activeNav =
    userInfo?.isSystemAdmin && userInfo?.active
      ? Navbar?.find((el, index) => el.path == currentLocation)
      : BA_NavBar?.find((el, index) => el.path == currentLocation);
  const [drop, setDrop] = useState(false);
  // const [navState, SetnavState] = useState({});

  const [CurrentArray, setcurrentArray] = useState([]);
  useEffect(() => {
    setcurrentArray(
      userInfo?.isSystemAdmin && userInfo?.active ? Navbar : BA_NavBar
    );
    // if (
    //   currentLocation == '/equipment' ||
    //   currentLocation == '/equipmentmodel'
    // ) {
    //   setDrop(true);
    // } else {
    //   setDrop(false);
    // }
  }, [
    currentLocation,
    history?.location?.pathname,
    userInfo?.isSystemAdmin,
    userInfo?.active,
  ]);
  // console.log(drop);
  document.body.scrollTop = document.documentElement.scrollTop = 0;

  useEffect(() => {
    if (
      currentLocation == '/equipment' ||
      currentLocation == '/equipmentmodel' ||
      activeNav?.active == 'master'
    ) {
      setDrop(true);
    }
    setDrop(false);
  }, [setDrop, activeNav?.active, currentLocation]);

  return (
    <div className="deznav">
      <div className="deznav-scroll deznav-scroll  ps ps--active-y ps--scrolling-y">
        <ul className="metismenu" id="menu">
          {CurrentArray?.map((element, index) => {
            // console.log(
            //   element?.active == activeNav?.active,
            //   element?.active,
            //   '==',
            //   activeNav?.active
            // );
            return (
              <>
                <li
                  title={element.title}
                  key={element.title}
                  className={
                    element?.active === activeNav?.active &&
                    !drop &&
                    'mm-active'
                  }
                >
                  {element?.title && (
                    <Link
                      to={element?.path}
                      onClick={element?.submenu ? () => setDrop(!drop) : ''}
                      className={
                        element?.submenu ? 'has-arrow ai-icon ' : 'ai-icon'
                      }
                      // aria-expanded="false"
                      aria-expanded={drop ? 'true' : 'false'}
                    >
                      <i className={element.className}></i>
                      <span className="nav-text"> {element.title}</span>
                    </Link>
                  )}

                  {element.submenu && (
                    <ul
                      // aria-expanded="false"
                      aria-expanded={drop ? 'true' : 'false'}
                      style={{ height: '16px' }}
                      className={!drop ? 'mm-collapse' : 'mm-collapsed mm-show'}
                    >
                      {element?.child?.map((el, inx) => {
                        return (
                          <li title={el.title} key={el.path}>
                            <Link to={el.path}>
                              <i className={el.className}></i>
                              <span className="nav-text show-nav-textibt"> {el.title}</span>
                            </Link>
                          </li>
                        );
                      })}
                    </ul>
                  )}
                </li>
                <li></li>
              </>
            );
          })}
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
