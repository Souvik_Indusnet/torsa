import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const Navbarheader = ({ cb }) => {
  const [hamber, setHamber] = useState(false);
  const handleback = (e) => {
    setHamber(!hamber);
    cb(hamber);
  };
  return (
    <div className="nav-header">
      <Link to="/dashboard" className="brand-logo">
        {/* <img className="logo-abbr" src="/images/logo.png" alt="" /> */}
        {/* <img className="logo-compact" src="/images/logo-text.png" alt="" /> */}
        {/* <img className="brand-title" src="/images/logo-text.png" alt="" /> */}
        <div style={{ backgroundBlendMode: 'screen' }}>
          <img
            // className="brand-title"
            style={{
              boxSizing: 'border-box',
              overflow: 'hidden',
              width: '100%',
              height: '100%',
              backgroundBlendMode: 'screen',
              padding: '0px 10px'
            }}
            src="/logo/white_logo.png"
            alt="torsa_logo"
          />
        </div>
      </Link>

      <div className="nav-control">
        <div
          onClick={handleback}
          className={!hamber ? 'hamburger' : 'hamburger is-active'}
        >
          <span className="line"></span>
          <span className="line"></span>
          <span className="line"></span>
        </div>
      </div>
    </div>
  );
};

export default Navbarheader;
