import React from 'react';
// import "./header.css";
const CustomerlistHeader = ({ title }) => {
  return (
    <div className="header__desh">
      <h3>{title}</h3>
    </div>
  );
};

export default CustomerlistHeader;
