import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getlistDashboard } from '../../Actions/userAction';
import './style.css';
import Graph from './Graph';
import StorageIcon from '@mui/icons-material/Storage';
import PeopleAltSharpIcon from '@mui/icons-material/PeopleAltSharp';
import PeopleOutlineSharpIcon from '@mui/icons-material/PeopleOutlineSharp';
import SimpleBackdrop from '../backdrop/Backdrop';
import DateFormate from '../Utils/DateFormate';
import TableStatusButton from '../Table/TableStatusButton';
const Containbody = () => {
  const dispatch = useDispatch();
  const { userInfo } = useSelector((state) => state.userSigninInfo);
  const [state, setState] = useState('');
  const { loading, error, tableList, stateChange, success } = useSelector(
    (state) => state.Admin_Dashboard
  );
  useEffect(() => {
    dispatch(getlistDashboard()).then((data) => {
      setState(data);
    });
    //    console.log(tableList)
  }, [getlistDashboard]);
  useEffect(() => {
    console.log(tableList);
  }, [loading]);

  const tableCreate = ({ tableList, product }) => {
    return (
      <table className="table table-sm table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>{product ? 'Equipment' : 'Email'}</th>
            {product && <th>Amount</th>}
            <th>Created at</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {tableList?.map((el, index) => (
            <tr>
              <td>{index + 1}.</td>
              <td>{el?.name || el?.partName}</td>
              <td>{product ? el?.category?.name : el?.email}</td>
              {product && <td>{el?.amount}</td>}
              <td>
                <DateFormate formatter={el.createdAt} />
              </td>
              <td>
                <TableStatusButton
                  status={el?.active}
                  handleBack={(e) => console.log('intiallly')}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  };

  return (
    <>
      <SimpleBackdrop open={loading} />
      {!loading && userInfo.isSystemAdmin && (
        <div className="col-lg-12">
          <div className="row">
          <div className="col-xl-3 col-xxl-6 col-lg-6 col-sm-6">
              <div className="widget-stat card media">
                <div className=" p-4">
                  <div className="media ai-icon">
                    <span className="mr-3 bgl-secondary text-secondary">
                      <PeopleOutlineSharpIcon />
                    </span>
                    <div className="media-body table-responsive">
                      <p className="mb-1 fs-25">Customer</p>
                      {tableList?.user && (
                        <h4 className="mb-0">
                          {
                            tableList.user
                            // state.user.filter((i) => i.isDeleted === false)
                            //   .length
                          }
                        </h4>
                        // <span className="badge badge-secondary">-3.5%</span>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-3 col-xxl-6 col-lg-6 col-sm-6">
              <div className="widget-stat card media">
                <div className=" p-4">
                  <div className="media ai-icon">
                    <span className="mr-3 bgl-primary text-primary">
                      <svg
                        id="icon-database-widget"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewbox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        className="feather feather-database"
                      >
                        <ellipse cx="12" cy="5" rx="9" ry="3"></ellipse>
                        <path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path>
                        <path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path>
                      </svg>
                    </span>
                    <div className="media-body table-responsive">
                      <p className="mb-1 fs-25">Admin</p>
                      {tableList?.admin && (
                        <h4 className="mb-0">
                          {
                            tableList?.admin
                            /* {
                            state.admin.filter((i) => i.isDeleted === false)
                              .length
                          } */
                          }
                        </h4>
                        // <span className="badge badge-success">-3.5%</span>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-xxl-6 col-lg-6 col-sm-6">
              <div className="widget-stat card media">
                <div className=" p-4">
                  <div className="media ai-icon">
                    <span className="mr-3 bgl-success text-success">
                      <svg
                        id="icon-database-widget"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewbox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        className="feather feather-database"
                      >
                        <ellipse cx="12" cy="5" rx="9" ry="3"></ellipse>
                        <path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path>
                        <path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path>
                      </svg>
                    </span>
                    <div className="media-body table-responsive">
                      <p className="mb-1 fs-25">Total Equipments</p>
                      {tableList?.machines && (
                        <h4 className="mb-0">
                          {
                            tableList?.machines
                            // state.machineName.filter(
                            //   (i) => i.isDeleted === false
                            // ).length
                          }
                        </h4>
                        // <span className="badge badge-success">-3.5%</span>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-3 col-xxl-6 col-lg-6 col-sm-6">
              <div className="widget-stat card media">
                <div className=" p-4">
                  <div className="media ai-icon">
                    <span className="mr-3 bgl-secondary text-secondary">
                      <svg
                        id="icon-database-widget"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewbox="0 0 24 24"
                        fill="none"
                        stroke="#f08123"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        className="feather feather-database"
                      >
                        <ellipse cx="12" cy="5" rx="9" ry="3"></ellipse>
                        <path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path>
                        <path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path>
                      </svg>
                    </span>
                    <div className="media-body table-responsive">
                      <p className="mb-1 fs-25">Total Equipment Part</p>
                      {tableList?.machineName && (
                        <h4 className="mb-0">
                          {
                            tableList?.machineName
                            // state.part.filter((i) => i.isDeleted === 'false')
                            //   .length
                          }
                        </h4>
                        // <span className="badge badge-success">-3.5%</span>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-12 col-xxl-12 col-lg-12 col-sm-12">
              <div className="widget-stat card">
                <div className=" p-4">
                  <div className="media ai-icon">
                    <div className="media-body table-responsive">
                      <p className="mb-1 fs-25">
                        <b>Last 5 added user</b>
                      </p>
                      <br />
                      {tableCreate({ tableList: tableList?.lastUser || [] })}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-12 col-xxl-12 col-lg-12 col-sm-12">
              <div className="widget-stat card">
                <div className=" p-4">
                  <div className="media ai-icon">
                    {/* <span className="mr-3 bgl-primary text-primary">
                      <PeopleAltSharpIcon />
                    </span> */}
                    <div className="media-body table-responsive">
                      <p className="mb-1 fs-25">
                        <b>Last 5 added business admin</b>
                      </p>
                      <br />
                      {tableCreate({ tableList: tableList?.lastBA || [] })}
                      {/* <ol>
                        {tableList?.lastBA?.map((el) => (
                          <li>{el?.name}</li>
                        ))}
                      </ol> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xl-12 col-xxl-12 col-lg-12 col-sm-12">
              <div className="widget-stat card">
                <div className=" p-4">
                  <div className="media ai-icon">
                    {/* <span className="mr-3 bgl-primary text-primary">
                      <PeopleAltSharpIcon />
                    </span> */}
                    <div className="media-body table-responsive">
                      <p className="mb-1 fs-25">
                        <b>Last 5 added product</b>
                      </p>
                      <br />
                      {tableCreate({
                        tableList: tableList?.lastProduct || [],
                        product: true,
                      })}
                      {/* <ol>
                        {tableList?.lastProduct?.map((el) => (
                          <li>{el?.partName}</li>
                        ))}
                      </ol> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* {tableList?.signInList &&
              tableList?.signInList.map((p, i) => {
                return (
                  <div className="col-xl-3 col-xxl-6 col-lg-6 col-sm-6">
                    <div className="widget-stat ">
                      <div className=" p-4">
                        <div className="media ai-icon">
                          <span className="mr-3 bgl-primary text-primary">
                            <PeopleAltSharpIcon />
                          </span>
                          <div className="media-body">
                            <p className="mb-1">
                              User and Business Admin Sign-In (
                              {i == 0
                                ? 'Today'
                                : i == 1
                                ? 'Last Week'
                                : i == 2
                                ? 'Last Month'
                                : 'Last Year'}
                              )
                            </p>
                            <br />
                            {p && (
                              <h4 className="mb-0">
                                {
                                  p
                                  
                                }
                              </h4>
                              // <span className="badge badge-success">-3.5%</span>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })} */}

          </div>
        </div>
      )}

      {/* <Graph /> */}
    </>

    //   </div>
    // </div>
  );
};

export default Containbody;
