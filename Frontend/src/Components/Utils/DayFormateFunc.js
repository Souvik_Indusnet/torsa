import dayjs from 'dayjs';

const formateDatereturn = (formatter) => {
  return dayjs(formatter).format('MMM DD , YYYY	');
};

export { formateDatereturn };
