// import React from "react";
// import PropTypes from "prop-types";
// import { makeStyles } from "@material-ui/core/styles";
// import AppBar from "@material-ui/core/AppBar";
// import Tabs from "@material-ui/core/Tabs";
// import Tab from "@material-ui/core/Tab";
// import Typography from "@material-ui/core/Typography";
// import Box from "@material-ui/core/Box";
// import { useDispatch } from "react-redux";
// import DetailsAdmin from "./Details";

// function TabPanel(props) {
//   const { children, value, index, ...other } = props;

//   return (
//     <div
//       role="tabpanel"
//       hidden={value !== index}
//       id={`simple-tabpanel-${index}`}
//       aria-labelledby={`simple-tab-${index}`}
//       {...other}
//     >
//       {value === index && (
//         <Box p={3}>
//           <Typography>{children}</Typography>
//         </Box>
//       )}
//     </div>
//   );
// }

// TabPanel.propTypes = {
//   children: PropTypes.node,
//   index: PropTypes.any.isRequired,
//   value: PropTypes.any.isRequired,
// };

// function a11yProps(index) {
//   return {
//     id: `simple-tab-${index}`,
//     "aria-controls": `simple-tabpanel-${index}`,
//   };
// }

// const useStyles = makeStyles((theme) => ({
//   root: {
//     flexGrow: 1,
//     backgroundColor: theme.palette.background.paper,
//   },
// }));

// export default function AdminPanel(props) {
//   const { data } = props;
//   const classes = useStyles();
//   const [value, setValue] = React.useState(3);

//   const dispatch = useDispatch();

//   // console.log(props)
//   const handleChange = (event, newValue) => {
//     setValue(newValue);
//   };

//   return (
//     <div className={classes.root}>
//       <AppBar position="static">
//         <Tabs
//           value={value}
//           onChange={handleChange}
//           aria-label="simple tabs example"
//         >
//           {/* <Tab label="Edit" {...a11yProps(0)} />
//           <Tab label="Back" {...a11yProps(1)} />
//           <Tab label="Delete" {...a11yProps(2)} /> */}
//           <Tab label="User Details" {...a11yProps(3)} />
//         </Tabs>
//       </AppBar>
//       {/* <TabPanel value={value} index={0}>
//         Edit
//       </TabPanel>
//       <TabPanel value={value} index={1}>
//         Back
//       </TabPanel>
//       <TabPanel value={value} index={2}>
//         Delete
//       </TabPanel> */}
//       <TabPanel key={data._id} value={value} index={3}>
//         <DetailsAdmin key={data._id} id={data.id} userData={data} />
//       </TabPanel>
//     </div>
//   );
// }
