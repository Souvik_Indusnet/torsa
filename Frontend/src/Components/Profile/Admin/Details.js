import React, { useEffect } from 'react';
import moment from 'moment';
import { Typography } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { useDispatch } from 'react-redux';
import { deleteAdmin } from '../../../Actions/userAction';
import { useHistory } from 'react-router-dom';
import SimpleBackdrop from '../../backdrop/Backdrop';
import NewCustomer from '../../user/business/Newadmin';

const DetailsAdmin = (props) => {
  console.log(props);
  const history = useHistory();
  const [loading, setLoading] = React.useState(false);
  const dispatch = useDispatch();

  const handleDelete = () => {
    setLoading(true);
    dispatch(deleteAdmin(props.userData._id))
      .then((data) => {
        setLoading(false);
        history.push('/business-admin-management');
      })
      .catch((e) => {
        setLoading(false);
      });
  };
  const [open, setOpen] = React.useState(false);
  const handleEdit = () => {
    setOpen(true);
  };
  const handleBack = (e) => {
    setOpen(e.open);
  };
  return (
    <div>
      {!props ? (
        <div>Loading...</div>
      ) : loading ? (
        <SimpleBackdrop open={loading} />
      ) : open ? (
        <NewCustomer
          id={props.userData}
          callBack={(e) => handleBack(e)}
          newC={false}
        />
      ) : (
        <div style={{ margin: '10px', padding: '20px' }}>
          <div className="float-right">
            <button
              onClick={() => history.push('/business-admin-management')}
              className="btn btn-primary  btn-xs sharp mr-2"
            >
              <i className="fa">
                {' '}
                <ArrowBackIcon />
              </i>
            </button>
            <button
              onClick={handleDelete}
              className="btn btn-primary shadow btn-xs sharp mr-1"
            >
              {' '}
              <i className="fa fa-trash"></i>
            </button>
            <button
              onClick={handleEdit}
              className="btn btn-primary shadow btn-xs sharp mr-1"
            >
              {' '}
              <i className="fa fa-pencil"></i>
            </button>
          </div>
          <div
            style={{
              padding: '20px',
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <lable>Name:</lable>
            <Typography>
              {props.userData ? props.userData.name : 'loading...'}
            </Typography>
          </div>
          <div
            style={{
              padding: '20px',
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <lable>Email:</lable>
            <Typography>
              {props.userData ? props.userData.email : 'loading'}
            </Typography>
          </div>
          <div
            style={{
              padding: '20px',
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <lable>ContactNo: </lable>
            <Typography>
              {' '}
              {props.userData ? props.userData.contactNo : 'loading'}
            </Typography>
          </div>

          <div
            style={{
              padding: '20px',
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <lable>AddOn:</lable>
            <Typography>
              {' '}
              {props.userData
                ? moment(props.userData.updatedAt).format('ll')
                : 'loading'}
            </Typography>
          </div>
        </div>
      )}
    </div>
  );
};

export default DetailsAdmin;
