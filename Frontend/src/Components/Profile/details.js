// import React, { useEffect } from 'react';
// import { useDispatch } from 'react-redux';
// import {
//   deleteUser,
//   getManagerById,
//   getuserById,
// } from '../../Actions/userAction';
// import moment from 'moment';
// import { Typography } from '@material-ui/core';
// import { getMachineById } from '../../Actions/productAction';
// import { useHistory } from 'react-router-dom';
// import ArrowBackIcon from '@material-ui/icons/ArrowBack';
// import NewCustomer from '../user/customer/NewCustomer';
// import SimpleBackdrop from '../backdrop/Backdrop';

// const Details = ({ userData }) => {
//   // console.log(props);
//   const history = useHistory();
//   const [loading, setLoading] = React.useState(false);
//   const dispatch = useDispatch();
//   // const [manager, setManager] = React.useState("");
//   // const [machine, setmachine] = React.useState("");
//   const [main, setMain] = React.useState({
//     _id: '',
//     name: '',
//     email: '',
//     contactNo: '',
//     buisnessManager: '',
//     machines: '',
//   });
//   // console.log(props);
//   useEffect(() => {
//     // dispatch(getManagerById(props.userData.buisnessManager)).then((data) => {
//     //   // console.log(data);
//     //   setManager(data);
//     //   setMain({ ...main, buisnessManager: data });
//     // });

//     // dispatch(
//     //   getMachineById(props.userData.machines && props.userData.machines[0])
//     // ).then((data) => {
//     //   setMain({ ...main, machines: data });
//     //   setmachine(data);
//     // });
//     setMain({
//       ...main,
//       _id: userData._id,
//       name: userData.name,
//       email: userData.email,
//       contactNo: userData.contactNo,
//       buisnessManager: userData.buisnessManager,
//       machines: userData.machines,
//     });
//   }, []);
//   // console.log(main);
//   const handleDelete = () => {
//     setLoading(true);
//     dispatch(deleteUser(userData._id))
//       .then((data) => {
//         setLoading(false);
//         history.push('/customer-management');
//       })
//       .catch((e) => {
//         setLoading(false);
//       });
//   };
//   const [open, setOpen] = React.useState(false);
//   const handleEdit = () => {
//     setOpen(true);
//   };
//   const handleBack = (e) => {
//     setOpen(e.open);
//   };
//   return (
//     <div>
//       {!userData ? (
//         <div>Loading...</div>
//       ) : open ? (
//         <NewCustomer
//           callBack={(e) => handleBack(e)}
//           id={userData && userData}
//           newC={false}
//         />
//       ) : (
//         <div style={{ margin: '10px', padding: '20px' }}>
//           <SimpleBackdrop open={loading} />
//           <div className="float-right">
//             <button
//               onClick={() => history.push('/customer-management')}
//               className="btn btn-primary shadow btn-xs sharp mr-1"
//             >
//               <ArrowBackIcon />
//             </button>
//             {/* <button
//               onClick={handleDelete}
//               className="btn btn-primary shadow btn-xs sharp mr-1"
//             >
//               {" "}
//               <i className="fa fa-trash"></i>
//             </button> */}
//             {/* <button
//               onClick={handleEdit}
//               className="btn btn-primary shadow btn-xs sharp mr-1"
//             >
//               {" "}
//               <i className="fa fa-pencil"></i>
//             </button>*/}
//           </div>
//           <div
//             style={{
//               padding: '20px',
//               display: 'flex',
//               flexDirection: 'row',
//               alignItems: 'center',
//             }}
//           >
//             <lable>Name:</lable>
//             <Typography>{userData ? userData.name : 'loading...'}</Typography>
//           </div>
//           <div
//             style={{
//               padding: '20px',
//               display: 'flex',
//               flexDirection: 'row',
//               alignItems: 'center',
//             }}
//           >
//             <lable>Email:</lable>
//             <Typography>{userData ? userData.email : 'loading'}</Typography>
//           </div>
//           <div
//             style={{
//               padding: '20px',
//               display: 'flex',
//               flexDirection: 'row',
//               alignItems: 'center',
//             }}
//           >
//             <lable>ContactNo: </lable>
//             <Typography>
//               {' '}
//               {userData ? userData.contactNo : 'loading'}
//             </Typography>
//           </div>

//           <div
//             style={{
//               padding: '20px',
//               display: 'flex',
//               flexDirection: 'row',
//               alignItems: 'center',
//             }}
//           >
//             <lable>Added-On:</lable>
//             <Typography>
//               {' '}
//               {userData ? moment(userData.updatedAt).format('ll') : 'loading'}
//             </Typography>
//           </div>
//           <div
//             style={{
//               padding: '20px',
//               display: 'flex',
//               flexDirection: 'row',
//               alignItems: 'center',
//             }}
//           >
//             <lable>Machine:</lable>

//             {main.machines
//               ? main.machines.map((i) => (
//                   <Typography style={{ marginLeft: '10px' }} key={i._id}>
//                     {'  '} {i.machineName} {'  '}
//                   </Typography>
//                 ))
//               : 'loading'}
//           </div>
//           <div
//             style={{
//               padding: '20px',
//               display: 'flex',
//               flexDirection: 'row',
//               alignItems: 'center',
//             }}
//           >
//             <lable>Manager:</lable>
//             <Typography>
//               {' '}
//               {main.buisnessManager ? main.buisnessManager.name : 'loading'}
//             </Typography>
//           </div>
//         </div>
//       )}
//     </div>
//   );
// };

// export default Details;
