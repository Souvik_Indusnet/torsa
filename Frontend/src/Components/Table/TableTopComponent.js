import React, { useEffect, useState } from 'react';
import SimpleBackdrop from '../backdrop/Backdrop';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { useHistory } from 'react-router';
import DoubleArrowIcon from '@mui/icons-material/DoubleArrow';
import ArrowLeftIcon from '@mui/icons-material/ArrowLeft';
import './backbutton.css';
import { CSVLink, CSVDownload } from 'react-csv';

const TableHeaderComponent = ({
  children,
  loading,
  card_title,
  add_tittle,
  addpath,
  arrowIcon,
  backButtonPath,
  editButtonPath,
  search,
  SearchKeyBack,
  searchButton,
  exportButton,
  headers,
  csvData,
  csvfilename,
}) => {
  const history = useHistory();
  const [searchValue, setSearchValue] = useState('');
  useEffect(()=>{
    console.log(search)
    if(search!=null&&search!='null'){
      setSearchValue(search);
    }
  },[search])
  useEffect(() => {
    SearchKeyBack(searchValue);
  }, [searchValue]);

  const handleSubmit = (e) => {
    e.preventDefault();
    SearchKeyBack(searchValue);
  };

  return (
    <div className="col-12">
      <div>
        <SimpleBackdrop open={loading} />
        <div className="card-header row">
          <h4 className="card-title py-2 col-lg-4">{card_title}</h4>
          {searchButton && (
            <ul className="navbar-nav header-right col-lg-4 border-none">
              <li className="">
                <div className="search-input-div">
                  <div className="dropdown-menu p-0 m-0 show">
                    <form onSubmit={handleSubmit}>
                      <input
                        className="form-control"
                        type="search"
                        placeholder="Search ..."
                        aria-label="Search"
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                      />
                      <input style={{ display: 'none' }} type="submit" />
                    </form>
                  </div>
                </div>
              </li>
            </ul>
          )}
          {addpath && (
            <div
              className="add-btn col-lg-1 text-center"
              onClick={() => history.push(addpath)}
            >
              <AddCircleOutlineIcon color="primary"> </AddCircleOutlineIcon>
              {add_tittle}
            </div>
          )}
          {backButtonPath && (
            <button
              className="mybackButton"
            >
              {/* <svg
                height="16"
                width="16"
                xmlns="http://www.w3.org/2000/svg"
                version="1.1"
                viewBox="0 0 1024 1024"
              >
                <path d="M874.690416 495.52477c0 11.2973-9.168824 20.466124-20.466124 20.466124l-604.773963 0 188.083679 188.083679c7.992021 7.992021 7.992021 20.947078 0 28.939099-4.001127 3.990894-9.240455 5.996574-14.46955 5.996574-5.239328 0-10.478655-1.995447-14.479783-5.996574l-223.00912-223.00912c-3.837398-3.837398-5.996574-9.046027-5.996574-14.46955 0-5.433756 2.159176-10.632151 5.996574-14.46955l223.019353-223.029586c7.992021-7.992021 20.957311-7.992021 28.949332 0 7.992021 8.002254 7.992021 20.957311 0 28.949332l-188.073446 188.073446 604.753497 0C865.521592 475.058646 874.690416 484.217237 874.690416 495.52477z"></path>
              </svg> */}
              <span>Back</span>
            </button>
          )}
          {editButtonPath && (
            <button
              className="mybackButton"
              onClick={() => history.push(editButtonPath)}
            >
              {/* <svg
                height="16"
                width="16"
                xmlns="http://www.w3.org/2000/svg"
                version="1.1"
                viewBox="0 0 1024 1024"
              >
                <path d="M874.690416 495.52477c0 11.2973-9.168824 20.466124-20.466124 20.466124l-604.773963 0 188.083679 188.083679c7.992021 7.992021 7.992021 20.947078 0 28.939099-4.001127 3.990894-9.240455 5.996574-14.46955 5.996574-5.239328 0-10.478655-1.995447-14.479783-5.996574l-223.00912-223.00912c-3.837398-3.837398-5.996574-9.046027-5.996574-14.46955 0-5.433756 2.159176-10.632151 5.996574-14.46955l223.019353-223.029586c7.992021-7.992021 20.957311-7.992021 28.949332 0 7.992021 8.002254 7.992021 20.957311 0 28.949332l-188.073446 188.073446 604.753497 0C865.521592 475.058646 874.690416 484.217237 874.690416 495.52477z"></path>
              </svg> */}
              <span>Edit</span>
            </button>
          )}
          {exportButton && (
            <div class="export-btn col-lg-1 text-center">
              <CSVLink
                data={csvData}
                headers={headers}
                target="_blank"
                filename={csvfilename}
              >
                <svg
                  className="MuiSvgIcon-root MuiSvgIcon-colorPrimary"
                  viewBox="0 0 24 24"
                  width="24"
                  height="24"
                  stroke="currentColor"
                  strokeWidth="2"
                  fill="none"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="css-i6dzq1"
                >
                  <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                  <polyline points="7 10 12 15 17 10"></polyline>
                  <line x1="12" y1="15" x2="12" y2="3"></line>
                </svg>
                Export
              </CSVLink>
            </div>
          )}
        </div>
      </div>
      {children}
    </div>
  );
};

export default TableHeaderComponent;
