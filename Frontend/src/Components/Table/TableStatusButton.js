import React from 'react';

const TableStatusButton = ({ status, handleBack, Order, Orderpaid }) => {
  // console.log(Order);
  // console.log(Orderpaid)
  const statusCheck = (status) => {
    switch (`${status}`) {
      case 'true':
      case true:
      case 'approved':
      case 'Paid':
        return 'badge light badge-success';
      case 'Due':
      default:
        return 'badge light badge-danger';
    }
  };

  return (
    <p
      onClick={(e) => handleBack(!status)}
      style={{ cursor: 'pointer' }}
      className={statusCheck(status || Order || Orderpaid)}
    >
      {Orderpaid
        ? Orderpaid
        : Order == true
        ? 'Delivered'
        : Order == false
        ? 'Undelivered'
        : status == 'approved'
        ? 'Approved'
        : status
        ? 'Active'
        : 'InActive'}
    </p>
  );
};

export default TableStatusButton;
