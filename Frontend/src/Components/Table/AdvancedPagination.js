import * as React from 'react';
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

export default function PaginationControlled({ pageInfo, pageChange }) {
  const [page, setPage] = React.useState(1);
  const handleChange = (event, value) => {
    setPage(value);
    pageChange(value);
  };

  return (
    <Stack spacing={2}>
      <Typography>Page: {page}</Typography>
      <Pagination
        count={pageInfo?.current?.totalPage || pageInfo?.meta?.pages}
        page={page}
        onChange={handleChange}
      />
    </Stack>
  );
}
