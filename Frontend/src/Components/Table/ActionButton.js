import React from 'react';
import { useHistory } from 'react-router';
import SvgButton from '../Analytics/SvgButton';

const ActionButton = ({ stateData, selectPath, viewPath, nonEditable }) => {
  const history = useHistory();
  return (
    <div className="dropdown text-sans-serif">
      <SvgButton />
      <div
        className="dropdown-menu dropdown-menu-right border py-0"
        aria-labelledby="order-dropdown-0"
      >
        <div className="py-2">
          {selectPath && (
            <button
              onClick={() =>
                history.push({
                  pathname: selectPath,
                  state: { ...stateData, nonEditable: nonEditable },
                })
              }
              className=" dropdown-item "
            >
              Edit
            </button>
          )}
          {viewPath && (
            <button
              onClick={() =>
                history.push({
                  pathname: viewPath,
                  state: stateData,
                })
              }
              className=" dropdown-item "
            >
              View
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default ActionButton;
