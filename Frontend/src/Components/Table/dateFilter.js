// import { button } from "@coreui/react";

const Report = ({ dateFormat, setDateFormat, product_data, filterData }) => {
  // const { start, end } = dateFormat;
  var startDate = new Date(dateFormat && dateFormat.start);
  var endDate = new Date(dateFormat && dateFormat.end);
  const allData = product_data && product_data;
  const beforefilter = product_data ? product_data : [];
  var resultProductData =
    beforefilter.length > 0 &&
    beforefilter.filter((a) => {
      var date = new Date(a.createdAt);
      return date >= startDate && date <= endDate;
    });

  const handleSubmit = (e) => {
    e.preventDefault();

    filterData({ list: resultProductData });
  };
  const handleReset = () => {
    setDateFormat({ start: '', end: '' });

    filterData({ list: product_data });
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        By Date: {'       '}
        <input
          value={dateFormat.start}
          format="yy-mm-dd"
          onChange={(e) =>
            setDateFormat({ ...dateFormat, start: e.target.value })
          }
          style={{ marginLeft: '1rem', marginRight: '1rem' }}
          type="date"
        />{' '}
        to{' '}
        <input
          style={{ marginLeft: '1rem', marginRight: '1rem' }}
          value={dateFormat.end}
          format="yy-mm-dd"
          disabled={dateFormat.start === ''}
          min={dateFormat.start}
          onChange={(e) =>
            setDateFormat({ ...dateFormat, end: e.target.value })
          }
          type="date"
        />
        <button
          type="submit"
          disabled={dateFormat.start === '' || dateFormat.end === ''}
          color="primary"
        >
          Submit
        </button>
        <button
          onClick={() => handleReset()}
          className="ml-2"
          type="button"
          color="secondary"
        >
          Reset
        </button>
      </form>
    </>
  );
};

export default Report;
