const Pagination = ({ pageInfo, pageChange }) => {
  let len = pageInfo?.current?.totalPage;

  const pageActive = () => {
    let newarr = [];
    for (let i = 0; i < len; i++) {
      newarr.push({ title: i });
    }
    return newarr.map((el, ind) => {
      let currentpage = el.title + 1;

      return (
        <li
          className={
            currentpage == pageInfo.current?.page
              ? 'page-item active'
              : 'page-item'
          }
        >
          <span onClick={() => pageChange(currentpage)} className="page-link">
            {currentpage}
          </span>
        </li>
      );
    });
  };
  return (
    <nav aria-label="Page navigation example">
      <ul className="pagination justify-content-end">
        <li
          className={pageInfo?.previous ? 'page-item ' : 'page-item disabled'}
        >
          <span
            className="page-link"
            aria-disabled={pageInfo?.previous ? 'false' : 'true'}
            onClick={() => pageChange(pageInfo?.previous?.page)}
          >
            Previous
          </span>
        </li>
        {pageActive()}

        <li className={pageInfo?.next ? 'page-item ' : 'page-item disabled'}>
          <span
            className="page-link"
            aria-disabled={pageInfo?.next ? 'false' : 'true'}
            onClick={() => pageChange(pageInfo?.next?.page)}
          >
            Next
          </span>
        </li>
      </ul>
    </nav>
  );
};

export default Pagination;
