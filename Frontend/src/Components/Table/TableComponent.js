import React, { useEffect } from 'react';
// import SweetAlert from 'react-bootstrap-sweetalert';
// import 'datatables.net-dt/js/dataTables.dataTables';
// import 'datatables.net-dt/css/jquery.dataTables.min.css';
// var $ = require('jquery');

const TableComponent = ({ children, TableHeading }) => {
  // useEffect(() => {
  //   setTimeout(() => {
  //     $('#dataTable2').DataTable({
  //       bDestroy: true,
  //       paging: false,
  //     });
  //   }, 3000);
  // }, []);

  return (
    <table
      id="dataTable2"
      className="table table-sm table-hover "
      style={{ minWidth: '845px' }}
    >
      <thead className="thead-primary">
        <tr>
          {TableHeading?.map((el) => {
            return <th key={el.title}>{el.key || el.title}</th>;
          })}
        </tr>
      </thead>
      <tbody className="page-data">{children}</tbody>
    </table>
  );
};

export default TableComponent;
