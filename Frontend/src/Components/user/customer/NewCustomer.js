// import { useState, useEffect } from 'react';
// import FormElement from '../../form';
// import useMarkedFeilds from '../../formTouch';
// import { Multiselect } from 'multiselect-react-dropdown';
// import { AdminList, UpdateuserByid } from '../../../Actions/userAction';
// import { useDispatch, useSelector } from 'react-redux';
// import {
//   AddUserByAdmin,
//   getAllEquipment,
//   machineModelArray,
// } from '../../../Actions/productAction';
// import { useSnackbar } from 'notistack';
// import Button from '@material-ui/core/Button';
// import { useHistory } from 'react-router-dom';
// import { Form, Formik } from 'formik';
// import { customerSchema } from '../../validation/validationSchema';
// import InputBox from '../../form/input';

// const NewCustomer = ({ id, callBack, newC, isadmin }) => {
//   const { enqueueSnackbar } = useSnackbar();
//   const history = useHistory();
//   const dispatch = useDispatch();
//   const [setAllFeildsTouched] = useMarkedFeilds();
//   const [admin, setAdmin] = useState('');
//   const [loading, setLoading] = useState(false);
//   const { Allmachine } = useSelector((state) => state.AllMAchineList);
//   const [equipment, setEquipment] = useState([]);
//   const [state, setState] = useState({
//     _id: '',
//     // name: '',
//     // email: '',
//     // contact: '',
//     password: '',
//     isBusinessAdmin: '',
//     Addmachine: '',
//     equipment: [],
//   });
//   useEffect(() => {
//     dispatch(getAllEquipment())
//       .then((data) => {
//         setEquipment(data);
//       })
//       .catch((er) => {
//         console.log(er);
//       });
//     dispatch(AdminList()).then((data) => {
//       const businesadmin = data.filter((i) => i.isSystemAdmin === false);
//       setAdmin(businesadmin);
//     });

//     if (id && !newC) {
//       setState({
//         _id: id._id,
//         // name: id.name,
//         // email: id.email,
//         // contact: id.contactNo,
//         isBusinessAdmin: id.buisnessManager._id,
//         Addmachine: id.machines.map((i) => i._id),
//         equipment: id.equipment.map((i) => i._id),
//       });
//     }
//   }, [AdminList, getAllEquipment]);
//   const [arraymodel, setArraymodel] = useState([]);
//   useEffect(() => {
//     dispatch(machineModelArray(state.equipment)).then((data) => {
//       console.log(data);
//       setArraymodel(data);
//     });
//   }, [state.equipment, machineModelArray]);

//   const handleChange = (e) => {
//     setState({ ...state, [e.target.name]: e.target.value });
//   };
//   const onChange = (event) => {
//     setState({
//       ...state,
//       name: event.target.value.replace(/[^\w\s]/gi, ''),
//     });
//   };
//   // console.log(id.buisnessManager._id);
//   const handleClose = () => {
//     setState({
//       _id: '',
//       name: '',
//       email: '',
//       contact: '',
//       password: '',
//       isBusinessAdmin: '',
//       Addmachine: '',
//       equipment: [],
//     });
//     callBack({ open: false, newC: false });
//   };
//   // console.log(state.equipment);

//   const handleSubmit = (e) => {
//     // e.preventDefault();
//     console.log(state.Addmachine === '');
//     if (state.isBusinessAdmin === undefined || state.isBusinessAdmin === '') {
//       enqueueSnackbar('Please Select Business Manager', {
//         variant: 'error',
//         anchorOrigin: {
//           vertical: 'bottom',
//           horizontal: 'right',
//         },
//       });
//     } else if (state.equipment === undefined || state.equipment.length === 0) {
//       enqueueSnackbar(
//         !newC ? 'Please Select Equipment ' : 'Please Updates Equipment  ',
//         {
//           variant: 'error',
//           anchorOrigin: {
//             vertical: 'bottom',
//             horizontal: 'right',
//           },
//         }
//       );
//     } else if (
//       state.Addmachine === undefined ||
//       state.Addmachine === '' ||
//       state.Addmachine.length === 0
//     ) {
//       enqueueSnackbar(
//         !newC
//           ? 'Please Select Equipment Model'
//           : 'Please Updates Equipment Model ',
//         {
//           variant: 'error',
//           anchorOrigin: {
//             vertical: 'bottom',
//             horizontal: 'right',
//           },
//         }
//       );
//     } else {
//       setLoading(true);

//       const validdata = {
//         _id: state._id,
//         name: e.name,
//         email: e.email,
//         contact: e.phone,
//         isBusinessAdmin: state.isBusinessAdmin,
//         Addmachine: state.Addmachine,
//         equipment: state.equipment,
//       };
//       // console.log(validdata);
//       dispatch(newC ? AddUserByAdmin(validdata) : UpdateuserByid(validdata))
//         .then((data) => {
//           if (data._id) {
//             setState({
//               _id: '',
//               name: '',
//               email: '',
//               contact: '',
//               password: '',
//               isBusinessAdmin: '',
//               Addmachine: '',
//               equipment: [],
//             });
//             setLoading(false);
//             callBack({ open: false, newC: false });
//             enqueueSnackbar(newC ? 'Account Created!' : 'Account Updated !', {
//               variant: 'success',
//               anchorOrigin: {
//                 vertical: 'bottom',
//                 horizontal: 'right',
//               },
//             });
//           }
//         })
//         .catch((err) => {
//           setLoading(false);
//           console.log(err.message);
//         });
//     }
//   };
//   // console.log(state);

//   return (
//     <div className="col-xl-12 col-xxl-12">
//       <div className="card">
//         <div className="card-header">
//           <h4 className="card-title">{newC ? 'Add' : 'Update'} Customer</h4>
//         </div>
//         <div className="card-body">
//           <div className="basic-form" _id="1">
//             <Formik
//               initialValues={{
//                 name: id?.name || '',
//                 email: id?.email || '',
//                 phone: id?.contactNo || '',
//               }}
//               validationSchema={customerSchema}
//               onSubmit={handleSubmit}
//             >
//               {(formik) => {
//                 return (
//                   <Form>
//                     {/* <div className="F-row"> */}
//                     <div className="form-group col-md-12">
//                       <InputBox
//                         type="text"
//                         id="nf-name"
//                         placeholder="Enter Name..."
//                         title="Name"
//                         name="name"
//                       />
//                     </div>
//                     <div className="form-group col-md-12">
//                       <InputBox
//                         title="Email"
//                         type="email"
//                         name="email"
//                         placeholder="Enter Email..."
//                       />
//                     </div>
//                     {/* <div className="form-group col-md-12">
//                         <InputBox
//                           name="password"
//                           title="Password"
//                           type="password"
//                           placeholder="Enter Password..."
//                         />
//                       </div> */}
//                     <div className="form-group col-md-12">
//                       <InputBox
//                         title="Contact"
//                         type="text"
//                         name="phone"
//                         placeholder="Enter Contact..."
//                       />
//                     </div>
//                     <div className="form-group col-md-12">
//                       <label>Business Admin</label>
//                       <Multiselect
//                         options={
//                           admin
//                             ? admin
//                                 .filter((i) => i.active === true)
//                                 .filter((i) => i.isDeleted === false)
//                             : []
//                         }
//                         onSelect={(e) =>
//                           setState({
//                             ...state,
//                             isBusinessAdmin: e.map((i) => i._id).toString(),
//                           })
//                         }
//                         selectedValues={!newC ? [id.buisnessManager] : ''}
//                         displayValue="name"
//                         required="true"
//                         closeIcon="close"
//                         singleSelect="true"
//                         required="true"
//                       />
//                     </div>
//                     <div className="form-group col-md-12">
//                       <label>Add Equipment</label>
//                       <Multiselect
//                         options={
//                           equipment
//                             ? equipment
//                                 .filter((i) => i.active === true)
//                                 .filter((i) => i.isDeleted === false)
//                             : ''
//                         }
//                         selectedValues={!newC ? id.equipment : ''}
//                         onSelect={(e) =>
//                           setState({
//                             ...state,
//                             equipment: e.map((i) => i._id),
//                           })
//                         }
//                         onRemove={(e) =>
//                           setState({
//                             ...state,
//                             equipment: e.map((i) => i._id),
//                           })
//                         }
//                         displayValue="equipment"
//                         closeIcon="close"
//                         // selectionLimit="1"
//                         required={true}
//                         // style="{color:red}"
//                         emptyRecordMsg="Sorry! no Equipment found"
//                       />
//                     </div>

//                     <div className="form-group col-md-12">
//                       <label>Add Equipment Model</label>
//                       <Multiselect
//                         options={
//                           arraymodel
//                             ? arraymodel
//                                 .filter((i) => i.active === true)
//                                 .filter((i) => i.isDeleted === false)
//                             : // .filter(
//                               //   (i) =>
//                               //     i.equipment &&
//                               //     i.equipment._id === "6036381839a65b34540c5b89"
//                               // )
//                               ''
//                         }
//                         selectedValues={!newC ? id.machines : ''}
//                         onSelect={(e) =>
//                           setState({
//                             ...state,
//                             Addmachine: e.map((i) => i._id),
//                           })
//                         }
//                         onRemove={(e) =>
//                           setState({
//                             ...state,
//                             Addmachine: e.map((i) => i._id),
//                           })
//                         }
//                         displayValue="machineName"
//                         closeIcon="close"
//                         required={true}
//                         style={{ background: 'red' }}
//                         emptyRecordMsg="Sorry! no Model found"
//                       />
//                     </div>
//                     <div className="form-group col-md-12"></div>
//                     <Button
//                       onClick={() => handleClose()}
//                       className="btn tp-btn-light btn-primary"
//                     >
//                       Cancel
//                     </Button>

//                     <button
//                       type="submit"
//                       onClick={setAllFeildsTouched}
//                       className="btn tp-btn-light btn-primary"
//                     >
//                       {loading ? 'Loading...' : newC ? 'Create' : 'Update'}
//                     </button>
//                     {/* </div> */}
//                   </Form>
//                 );
//               }}
//             </Formik>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default NewCustomer;
