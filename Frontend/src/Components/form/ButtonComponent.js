import { Button } from '@material-ui/core';

const ButtonComponent = () => {
  return <Button variant="outlined">Primary</Button>;
};

export default ButtonComponent;
