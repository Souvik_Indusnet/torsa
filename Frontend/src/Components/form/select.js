import React from 'react';
import { ErrorMessage, useField } from 'formik';

const SelectType = ({
  title,
  id,
  placeholder,
  callBack,
  values,
  selectedValue,
  disabled,
  ...props
}) => {
  const [field, meta] = useField(props);
  // console.log('15........................................')
  return (
    <>
      <label htmlFor={title}>{title}</label>
      <select
        disabled={disabled}
        id={title}
        className={`form-control shadow-none ${
          meta.touched && meta.error && 'is-invalid'
        }`}
        defaultValue={!selectedValue && 'DEFAULT'}
        value={selectedValue?._id}
        onChange={(e) => {
          console.log(e);
          return callBack({ category: e.target.value });
        }}
        {...field}
        {...props}
      >
        <option value="DEFAULT" disabled>
          {placeholder}
        </option>

        {values?.map((i) => {
          return (
            <option key={i._id} value={i._id}>
              {i.name || i.city || i.machineName || i.equipment || i.pageName}
            </option>
          );
        })}
      </select>
      <ErrorMessage
        style={{
          color: 'red',
        }}
        component="div"
        name={field.name}
        className="error"
      />
    </>
  );
};

export default SelectType;
