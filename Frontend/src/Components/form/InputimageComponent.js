// import { CFormGroup, CLabel } from '@coreui/react';
// import { render } from 'enzyme/build';
import { useRef, useState, useEffect } from 'react';
import CropingComponet from '../imagecrop/CropingComponet';
import './imageButton.css';
export default function InputImage({
  id,
  title,
  buttonTXT,
  imageBack,
  imgsrc,
  imagesize,
  style,
  cropActive,
}) {
  const [image, setImage] = useState('');
  const [preview, setPreview] = useState('');
  const [result, setResult] = useState(false);
  console.log('imagesize', imagesize != 5);
  const fileInputRef = useRef('');
  useEffect(() => {
    if (image) {
      const reader = new FileReader();

      reader.onloadend = () => {
        setPreview(reader.result);
      };
      reader.readAsDataURL(image);
      // imageBack({ file: image });
    } else if (imgsrc) {
      setPreview(imgsrc);
    } else {
      setPreview(null);
    }
  }, [image, imgsrc]);

  useEffect(() => {
    // alert(result);
    cropActive(preview != null);
  }, [preview != null]);

  // console.log('result', result);

  return (
    <CropingComponet
      preview={preview}
      baseIMG={(e) => {
        setPreview(e);
        setImage(e);
        imageBack({ file: e });
      }}
      CropCancle={() => {
        // setPreview(null);
        console.log('crop cancel');
      }}
      result={result}
      setResult={setResult}
    >
      <div>
        {title}

        <br />
        {preview ? (
          <div className="container">
            <img
              src={preview}
              className="img-thumbnail"
              style={{ objectFit: 'cover' }}
              onClick={() => {
                setImage(null);
              }}
            />

            <button
              onClick={(e) => {
                e.preventDefault();
                setImage(null);
                setPreview(null);
              }}
              className="noselect btnxyz myimageButton"
            >
              <span className="text">Remove</span>
              <span className="icon">
                <svg
                  viewBox="0 0 24 24"
                  height="24"
                  width="24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"></path>
                </svg>
              </span>
            </button>
          </div>
        ) : (
          <button
            onClick={(event) => {
              event.preventDefault();
              fileInputRef.current.click();
              setResult(true);
            }}
          >
            {imagesize != 5 && buttonTXT}
          </button>
        )}

        <input
          type="file"
          style={{ display: 'none' }}
          ref={fileInputRef}
          accept="image/*"
          onChange={(event) => {
            const file = event.target.files[0];
            if (file && file.type.substr(0, 5) === 'image') {
              setImage(file);
            } else {
              setImage(null);
            }
          }}
        />
      </div>
    </CropingComponet>
  );
}
