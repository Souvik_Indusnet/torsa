import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import { ErrorMessage, useField } from 'formik';

const animatedComponents = makeAnimated();

const Multiselect = ({
  Options,
  defaultOption,
  placeholder,
  callback,
  title,
  multi,
}) => {
  // console.log('options', Options);
  // console.log('defaultOptions', defaultOption);

  return (
    <>
      <label htmlFor={title}>{title}</label>
      <Select
        id={title}
        placeholder={placeholder}
        isMulti={!multi}
        options={Options}
        getOptionValue={(option) => option.value}
        components={animatedComponents}
        defaultValue={defaultOption}
        onChange={(option) => {
          callback({
            value: multi ? option.value : option.map((o) => o.value),
          });
        }}
      />
    </>
  );
};

export default Multiselect;
