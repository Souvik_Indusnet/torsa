import React from 'react';
import { ErrorMessage, useField } from 'formik';

const TextArea = ({ title, ...props }) => {
  const [field, meta] = useField(props);

  return (
    <>
      <label htmlFor={title}>{title}</label>
      <textarea
        className={`form-control shadow-none ${
          meta.touched && meta.error && 'is-invalid'
        }`}
        {...field}
        {...props}
      />
      <ErrorMessage
        style={{
          color: 'red',
        }}
        component="div"
        name={field.name}
        className="error"
      />
    </>
  );
};

export default TextArea;
