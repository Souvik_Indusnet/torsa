import React, { useState } from 'react';
import { ErrorMessage, useField } from 'formik';

const setStyle = (condition) => {
  switch (condition) {
    case 'true':
      return { background: 'lightgray', cursor: 'not-allowed' };
    default:
      return {
        background: '',
        cursor: 'pointer',
      };
  }
};

const InputBox = ({ title, type, readonly, ...props }) => {
  const [field, meta] = useField(props);

  const [showPassword, setShowpassword] = useState(false);

  return (
    <div>
      <label htmlFor={title}>{title}</label>
      <div className={field.name == 'password' && 'input-group mb-3'}>
        <input
          type={showPassword ? 'text' : type}
          id={title}
          readOnly={readonly}
          style={setStyle(readonly)}
          className={`form-control shadow-none ${
            meta.touched && meta.error && 'is-invalid'
          }`}
          {...field}
          {...props}
        />
        {field.name == 'password' && (
          <div className="input-group-append">
            <span className="input-group-text ">
              <span
                onClick={() => setShowpassword(!showPassword)}
                className={
                  !showPassword
                    ? 'fa fa-fw fa-eye field-icon '
                    : 'fa fa-fw fa-eye-slash field-icon'
                }
              ></span>
            </span>
          </div>
        )}
      </div>

      <ErrorMessage
        style={{
          color: 'red',
        }}
        component="div"
        name={field.name}
        className="error"
      />
    </div>
  );
};

export default InputBox;
