import React from "react";
import useMarkedFeilds from "../formTouch";
import "../formTouch/style.css";
const FormElement = ({
  lable,
  type,
  name,
  onchange,
  value,
  reqText,
  isRequire,
  pattern,
  placeholder,
  maxlength,
  minLength,
  htmlfor,
}) => {
  const [bindFeild, setAllFeildsTouched] = useMarkedFeilds();
  // function firstMethod(e) {
  //   const re = /[0-9:]+/g;
  //   if (re.test(e.key)) {
  //     console.log("Number is not required");
  //   }
  // }
  return (
    <div>
      {/* {!placeholder && (
        <lable htmlFor={htmlfor} className=" col-form-label">
          {lable} *
        </lable>
      )} */}

      <lable htmlFor={htmlfor} className=" col-form-label">
        {lable} *
      </lable>

      <input
        placeholder={placeholder}
        className="add"
        type={type}
        name={name}
        {...bindFeild(`${name}`)}
        style={{ width: "100%", height: "40px" }}
        onChange={onchange}
        value={value}
        required={!isRequire}
        pattern={pattern}
        maxlength={maxlength}
        minLength={minLength}

        // onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode &gt;= 48 && event.charCode &lt;= 57"
        // onKeyPress={(e) => firstMethod(e)}
      />
      <p
        style={{ color: "#a9b0ab", fontWeight: "bold", fontSize: "12px" }}
        className="form-reqs mb-4"
      >
        {reqText}
      </p>
    </div>
  );
};

export default FormElement;
