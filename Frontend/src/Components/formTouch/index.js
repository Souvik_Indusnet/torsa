import { useState } from "react";
export default function useMarkedFeilds() {
  const [touchedFeilds, setTouchedFeilds] = useState({ all: false });
  const setFieldsTouched = (event) => {
    setTouchedFeilds((touchedFeilds) => ({
      ...touchedFeilds,
      [event.target.name]: true,
    }));
  };

  const setAllFeildsTouched = (event) => {
    setTouchedFeilds({ all: true });
  };

  const bindFeild = (filename) => ({
    "data-touched": touchedFeilds.all || touchedFeilds[filename],
    onBlur: setFieldsTouched,
  });

  return [bindFeild, setAllFeildsTouched];
}
