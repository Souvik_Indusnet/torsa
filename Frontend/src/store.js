import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import Cookies from 'js-cookie';
import {
  getAllMachinesReducers,
  getstatusreducer,
  UserNotification,
  UserNotificationList,
  userSigninReducers,
} from './Reducers/userReducer';
import BusinessAdminOrderReducer from './Reducers/businessAdminOrderReducer';
import BusinessAdminDashboardReducer from './Reducers/businessAdminDashboardReducer';
import Admin_DashboardReducer from './Reducers/AdminDashboardReducer';
import NotificationReducer from './Reducers/notificationReducer';

import {
  CategoryReducer,
  editpartReducers,
  getOrderNotificationReducer,
  partReducers,
} from './Reducers/productReducer';
import { getadmin } from './Reducers/businessReduers';
// import { getOrderNotification } from "./Actions/notification";

const userInfo = Cookies.getJSON('UserInfo') || null;
const profile = Cookies.getJSON('Bprofile') || false;
// console.log(Cookies.getJSON("Bprofile"));
const notification = Cookies.getJSON('notification') || [];
// console.log("jhgfffffffffffff", notification);
const intialState = {
  userSigninInfo: { userInfo },
  category: [],
  parts: [],
  openStatus: { profileStatus: profile },
  OrderNote: notification,
  Business_Admin_Dashboard: {
    loading: false,
    error: null,
    tableList: null,
    stateChange: false,
    success: null,
    year: new Date().getFullYear(),
  },
  Admin_Dashboard: '',
};

const reducer = combineReducers({
  userSigninInfo: userSigninReducers,
  AllMAchineList: getAllMachinesReducers,
  category: CategoryReducer,
  parts: partReducers,
  readyEditTOParts: editpartReducers,

  //business admin redux data
  openStatus: getstatusreducer,
  adminInfo: getadmin,
  OrderNote: getOrderNotificationReducer,
  BusinessAdminOrder: BusinessAdminOrderReducer,
  Business_Admin_Dashboard: BusinessAdminDashboardReducer,
  NotificationReducer: NotificationReducer,
  Admin_Dashboard: Admin_DashboardReducer,
  BA_NOTIFICATION_COUNT: UserNotification,
  BA_NOTIFICATION_LIST: UserNotificationList,
});

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducer,
  intialState,
  composeEnhancer(applyMiddleware(thunk))
);
export default store;
