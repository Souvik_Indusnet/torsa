import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import ActionButton from '../../Components/Table/ActionButton';
import TableComponent from '../../Components/Table/TableComponent';
import TableStatusButton from '../../Components/Table/TableStatusButton';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
// import moment from 'moment';
import { getPayment } from '../../Actions/payment';
import DateFormate from '../../Components/Utils/DateFormate';
import { formateDatereturn } from '../../Components/Utils/DayFormateFunc';
import { PaymentCsv } from '../../Actions/CSVAction';
import PaginationControlled from '../../Components/Table/AdvancedPagination';

const Enquerytable = () => {
  const dispatch = useDispatch();
  const [getData, setGetData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [csvdata, setCsvdata] = useState([]);

  const [currentPage, setCurrentPage] = useState(1);
  const getAllData = async ({ pageNumber, Keyword }) => {
    setLoading(true);
    const Equipments = await dispatch(getPayment({ pageNumber, Keyword }));
    console.log(Equipments);
    setGetData(Equipments?.list);
    setLoading(false);
  };
  useEffect(() => {
    dispatch(PaymentCsv()).then((data) => {
      setCsvdata(data?.data?.data?.list);
    });
    getAllData({ pageNumber: 1 });
    document.title = 'Payment Management | Torsa';
  }, []);

  const Heading = [
    { title: 'Sr. No' },
    { title: ' User Name', key: '' },
    { title: 'Order No.', key: '' },
    { title: 'Type Of Payment', key: '' },
    { title: 'Mode Of Payment', key: '' },
    { title: 'Order Amount', key: '' },
    { title: 'Payment Status', key: '' },
    { title: 'Last Update', key: '' },
    { title: 'Part' },
    { title: 'Approved', key: '' },
    { title: 'Action', key: '' },
  ];

  const csvheader = [
    { label: 'Sr', key: 'sr' },
    {
      label: 'User Name ',
      key: 'name',
    },
    {
      label: 'Order No. ',
      key: 'orderno',
    },
    {
      label: 'Type Of Payment',
      key: 'top',
    },
    {
      label: 'Mode Of Payment',
      key: 'mop',
    },
    {
      label: 'Order Amount',
      key: 'order_amount',
    },
    {
      label: 'Payment Status',
      key: 'pay_status',
    },
    {
      label: 'Last Update ',
      key: 'updatedAt',
    },
    {
      label: 'Part',
      key: 'part',
    },
    {
      label: 'Approved',
      key: 'Approved',
    },
    {
      // Partial Percentage, Select Date.
      label: 'Partial Percentage',
      key: 'Partial_Amount_Paid_in_percentage',
    },
    {
      label: 'Select Date',
      key: 'Partial_Date',
    },
  ];
  console.log('cs', csvdata);
  const csv_Data = csvdata?.map((i, index) => {
    return {
      sr: index + 1,
      orderno: i._id,
      Partial_Amount_Paid_in_percentage: i.Partial_Amount_Paid_in_percentage,
      pay_status: i.Payment_Status,
      Partial_Date: i.Partial_Date,
      order_amount: parseInt(i?.part?.amount) * parseInt(i?.qty),
      mop: i.Mode_Of_Payment,
      top: i.Type_Of_Payment,
      name: i?.user?.name,
      part: i?.part?.partName,
      updatedAt: formateDatereturn(i.updatedAt),
      Approved: i.isApproved,
    };
  });

  // console.log('csv data', csvdata);
  return (
    <TableHeaderComponent
      searchButton={true}
      loading={loading}
      card_title="Payment Management"
      exportButton="true"
      csvData={csv_Data}
      headers={csvheader}
      SearchKeyBack={(e) => {
        getAllData({ pageNumber: 1, Keyword: e });
      }}
      csvfilename="Payment-csv.csv"
    >
      <div className="card-body">
        <div className="table-responsive">
          <TableComponent TableHeading={Heading}>
            {getData?.results?.map((el, index) => {
              return (
                <tr key={el._id}>
                  <td>
                    {' '}
                    {(getData?.current?.page + 1 || 1 + 1) *
                      getData?.current?.limit +
                      (index + 1) -
                      20}
                  </td>
                  <td>{el?.user?.name}</td>
                  <td>{el._id}</td>
                  <td>{el?.Type_Of_Payment}</td>
                  <th>{el?.Mode_Of_Payment}</th>
                  <th>{parseInt(el?.part?.amount) * parseInt(el?.qty)}</th>
                  <td>{el?.Payment_Status}</td>
                  <td>
                    {/* {moment(el?.updatedAt).format('ll')} */}
                    <DateFormate formatter={el?.updatedAt} />
                  </td>
                  <td>
                    {el?.part?.partName}
                    {el?.part?.inventoryStock < 0 ? (
                      <img
                        style={{ width: '40px', height: '40px' }}
                        src="images/out-of-stock.png"
                      />
                    ) : (
                      <span className="item">
                        &nbsp;
                        <i className="fa fa-check-circle text-success"></i>
                      </span>
                    )}
                  </td>
                  <td>
                    <TableStatusButton
                      status={el.isApproved}
                      handleBack={(e) => console.log(e)}
                    />
                  </td>
                  <td className="py-2 ">
                    {el?.for_Enquery && el?.Type_Of_Payment !== 'Full' ? (
                      <ActionButton
                        stateData={el}
                        selectPath="/Payment-enquiry"
                      />
                    ) : null}
                  </td>
                </tr>
              );
            })}
          </TableComponent>
        </div>
      </div>
      <PaginationControlled
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      />
      {/* <Pagination
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      /> */}
    </TableHeaderComponent>
  );
};

export default Enquerytable;
