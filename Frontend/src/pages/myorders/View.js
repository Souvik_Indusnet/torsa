import React, { useEffect, useState } from 'react';
import { orderDetailsById } from '../../Actions/orderAction';
import { useDispatch } from 'react-redux';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
// import moment from 'moment';
import DateFormate from '../../Components/Utils/DateFormate';
const ViewMyOrders = (props) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [userState, setUserState] = useState('');
  console.log(userState);
  useEffect(() => {
    setLoading(true);
    document.title = 'View Order |Torsa';
    dispatch(orderDetailsById(props?.location?.state?._id)).then((data) => {
      setUserState(data?.data?.data?.list);
      setLoading(false);
    });
  }, [props?.location?.state?._id, dispatch]);
  return (
    <TableHeaderComponent
      loading={loading}
      arrowIcon={true}
      // card_title={`${userState?._id} Details`}
      backButtonPath="/myorder"
      SearchKeyBack={(e) => {
        console.log(e);
      }}
    >
      <div className="card-body">
        <div
          style={{
            display: 'flex',
            alignContent: 'center',
            justifyContent: 'space-around',
          }}
        >
          <div>
            <h5>Order ID</h5>
            <h5>Order Date</h5>
            <h5>Order Status</h5>
            <h5>Order Qty</h5>
            <h5>Customer Name</h5>
            <h5>Customer Number</h5>
            <h5>Customer Address</h5>
            <h5>Product</h5>
            <h5>Equipment</h5>
            <h5>Equipment Model</h5>
            <h5>Order Amount</h5>
            <h5>Order Status</h5>
          </div>
          <div>
            <h5>-</h5>
            <h5>-</h5>
            <h5>-</h5>
            <h5>-</h5>
            <h5>-</h5>
            <h5>-</h5>
            <h5>-</h5>
            <h5>-</h5>
            <h5>-</h5>
            <h5>-</h5>
            <h5>-</h5>
          </div>
          <div>
            <h5>{userState?._id}</h5>
            <h5>
              <DateFormate formatter={userState?.createdAt} />
              {/* {moment(userState?.createdAt).format('ll')} */}
            </h5>
            <h5>{userState?.Payment_Status}</h5> <h5>{userState?.qty}</h5>
            <h5>{userState?.user?.name}</h5>
            <h5>{userState?.user?.contactNo}</h5>
            <h5>
              null
              {/* {userState?.user?.address} */}
            </h5>
            <h5>{userState?.part?.partName}</h5>
            <h5>
              {userState?.equipment?.map((i) => {
                return i.name;
              })}
            </h5>
            <h5>
              {userState?.equipmentModal?.map((i) => {
                return i.name;
              })}
            </h5>
            <h5>{userState?.part?.amount}</h5>
            <h5>{userState?.Payment_Status}</h5>
          </div>
        </div>
      </div>
    </TableHeaderComponent>
  );
};

export default ViewMyOrders;