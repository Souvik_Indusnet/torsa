import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BusinsessOrdercsv } from '../../Actions/CSVAction';
import {
  getAllOrderBySubAdmin,
  orderStatusChange,
} from '../../Actions/orderAction';
import ActionButton from '../../Components/Table/ActionButton';
import PaginationControlled from '../../Components/Table/AdvancedPagination';
import TableComponent from '../../Components/Table/TableComponent';
import TableStatusButton from '../../Components/Table/TableStatusButton';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
import DateFormate from '../../Components/Utils/DateFormate';
import { formateDatereturn } from '../../Components/Utils/DayFormateFunc';

const Order = () => {
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState(1);
  const [searchKeyword, setSearchKeyword] = useState('');
  const [csvdata, setCsvdata] = useState([]);
  const { loading, error, tableList, stateChange, success } = useSelector(
    (state) => state.BusinessAdminOrder
  );
  useEffect(() => {
    dispatch(getAllOrderBySubAdmin(currentPage, searchKeyword));
  }, [currentPage, stateChange, searchKeyword]);
  // console.log(getData);
  useEffect(() => {
    dispatch(BusinsessOrdercsv()).then((data) => {
      setCsvdata(data?.data?.data?.list);
    });
  }, [loading]);

  useEffect(() => {
    dispatch(BusinsessOrdercsv()).then((data) => {
      setCsvdata(data?.data?.data?.list);
    });
    setCurrentPage(1);
    document.title = 'Business Order Managment | Torsa';
  }, []);

  const Heading = [
    { title: 'Sr. No' },
    { title: 'Order No' },
    { title: 'Customer Name', key: '' },
    { title: 'Equipment', key: '' },
    { title: 'Equipment Model', key: '' },
    { title: 'Order Qty', key: '' },
    // { title: 'Product', key: '' },
    { title: 'Order Amount Paid', key: '' },
    { title: 'Mode of Payment', key: '' },
    { title: 'Remaining Amount', key: '' },
    // { title: 'Order Amount', key: '' },
    { title: 'Order Status', key: '' },
    { title: ' Order Date', key: '' },
    { title: 'Delivery Status', key: '' },
    { title: 'Action', key: '' },
  ];

  const csvheader = [
    { label: 'Sr', key: 'sr' },
    {
      label: 'Order No. ',
      key: 'orderno',
    },
    {
      label: 'Customer Name ',
      key: 'name',
    },

    {
      label: 'Order Qty. ',
      key: 'orderqty',
    },
    {
      label: 'Order Amount Paid in Percent',
      key: 'Partial_Amount_Paid_in_percentage',
    },
    {
      label: 'Mode of Payment',
      key: 'Mode_Of_Payment',
    },
    {
      label: 'Remaining Amount',
      key: 'Partial_Remaining_Amount',
    },
    {
      label: 'Order Status',
      key: 'Order_Status',
    },
    {
      label: 'Order Date	 ',
      key: 'updatedAt',
    },

    {
      label: 'Delivery Status',
      key: 'ds',
    },
  ];
  const csv_Data = csvdata?.map((i, index) => {
    return {
      sr: index + 1,
      Delivered: i.Delivered,
      Mode_Of_Payment: i.Mode_Of_Payment,
      Partial_Amount_Paid: i.Partial_Amount_Paid,
      Partial_Amount_Paid_in_percentage:
        i.Partial_Amount_Paid_in_percentage + '%',
      Partial_Date: formateDatereturn(i.Partial_Date),
      Partial_Due: i.Partial_Due,
      Partial_Remaining_Amount: i.Partial_Remaining_Amount,
      Partial_Remaining_Amount_in_percentage:
        i.Partial_Remaining_Amount_in_percentage,
      Type_Of_Payment: i.Type_Of_Payment,
      updatedAt: formateDatereturn(i.createdAt),
      for_Enquery: i.for_Enquery ? 'Yes' : 'No',
      isApproved: i.isApproved,
      //part: "601111e2643d0400152fae93"
      orderqty: i.qty,
      orderno: i._id,
      Order_Status: i.Payment_Status,
      //order_amount: parseInt(i?.part?.amount) * parseInt(i?.qty),
      ds: i.Delivered ? 'Delivered' : 'Undelivered',
      name: i?.user?.name,
      part: i?.part?.partName,
    };
  });

  return (
    <TableHeaderComponent
      loading={loading}
      card_title="Order Management"
      searchButton={true}
      SearchKeyBack={(e) => {
        setCurrentPage(1);
        setSearchKeyword(e);
      }}
      exportButton="true"
      csvData={csv_Data}
      headers={csvheader}
      csvfilename="order-csv.csv"
    >
      <div className="card-body">
        <div className="table-responsive">
          <TableComponent TableHeading={Heading}>
            {tableList &&
              tableList
                //?.filter((i) => i.for_Enquery == false)
                ?.map((el, index) => {
                  return (
                    <tr key={el._id}>
                      <td>
                        {' '}
                        {(getData?.current?.page + 1 || 1 + 1) *
                          getData?.current?.limit +
                          (index + 1) -
                          20}
                      </td>
                      <td>{el?._id}</td>
                      <td>{el?.user?.name}</td>
                      <td>{el?.category?.name}</td>
                      <td>{el?.subcategory?.name}</td>
                      <td>{el?.qty}</td>
                      {/* <td>{el?.part?.partName}</td> */}
                      <td>{el?.Partial_Amount_Paid_in_percentage}%</td>
                      <td>{el?.Mode_Of_Payment}</td>
                      <td>{el?.Partial_Remaining_Amount}</td>
                      {/* <td>{el?.part?.amount * el?.qty}</td> */}
                      <td>
                        <TableStatusButton
                          Orderpaid={el.Payment_Status}
                          handleBack={(e) => console.log('payment status')}
                        />
                      </td>
                      <td>
                        <DateFormate formatter={el.createdAt} />
                      </td>
                      <td>
                        <TableStatusButton
                          Order={el.Delivered ? true : false}
                          handleBack={(e) =>
                            dispatch(
                              orderStatusChange(el._id, !el.Delivered)
                            ).then((data) => {
                              getAllData({ pageNumber: currentPage });
                            })
                          }
                        />
                      </td>
                      <td className="py-2 ">
                        {' '}
                        <ActionButton
                          stateData={el}
                          // selectPath="/updateuser"
                          viewPath="/viewmyorder"
                        />
                      </td>
                    </tr>
                  );
                })}
          </TableComponent>
        </div>
      </div>
      <PaginationControlled
        pageInfo={tableList}
        pageChange={(e) => {
          setCurrentPage(e);
        }}
      />
    </TableHeaderComponent>
  );
};

export default Order;
