import { Form, Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { GetEquipmentAll } from '../../Actions/categoryAction';
import {
  machineModelArray,
  updateInventory,
} from '../../Actions/productAction';
import { AddnewUser, AdminList } from '../../Actions/userAction';
import CancelButton from '../../Components/Button/Cancel';
import SubmitButton from '../../Components/Button/Submit';
import InputBox from '../../Components/form/input';
import Multiselect from '../../Components/form/multiselect';
import SelectType from '../../Components/form/select';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
import { InventorySchema } from '../../Components/validation/validationSchema';

const Inventoryform = (props) => {
  const history = useHistory();
  const [equipment, setEquipment] = useState('');
  const [B_admins, setB_admins] = useState([]);
  const [state, setState] = useState({
    equipment: [],
    equipmentModal: [],
  });
  const [modal_list, setModal_list] = useState([]);
  useEffect(() => {
    document.title = 'Update Inventory |Torsa';
  }, []);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    setLoading(true);
    const validData = {
      id: props?.location?.state?._id || '',
      allowQty: e.qty,
    };

    dispatch(updateInventory(validData)).then((data) => {
      setLoading(false);

      history.push('/inventory-management');
    });
  };

  const options =
    equipment &&
    equipment.map((el) => {
      return { value: el._id, label: el.name };
    });

  const modalList = modal_list.map((el) => {
    return {
      values: el._id,
      label: el.name,
    };
  });
  // console.log(props);
  // const defaultValue = props?.location?.state?.console.log(props);
  return (
    <TableHeaderComponent
      loading={loading}
      card_title={'Update Inventory'}
      SearchKeyBack={(e) => {
        console.log(e);
      }}
    >
      <div className="card-body">
        <div className="table-responsive">
          <div className="basic-form">
            <Formik
              initialValues={{
                Equipment: props?.location?.state?.category?.name || '',
                partName: props?.location?.state?.partName || '',
                qty: props?.location?.state?.inventoryStock || '',
              }}
              validationSchema={InventorySchema}
              onSubmit={handleSubmit}
            >
              {(formik) => {
                return (
                  <Form>
                    <div className="form-group ">
                      <div className="form-group col-12">
                        <InputBox
                          type="text"
                          id="nf-name"
                          //   placeholder="Enter  Name..."
                          title="Equipment Name"
                          name="Equipment"
                          readonly="true"
                        />
                      </div>
                      <div className="form-group col-12">
                        <InputBox
                          type="text"
                          id="nf-name"
                          //   placeholder="Enter  Email..."
                          title="  Part Name"
                          name="partName"
                          readonly="true"
                        />
                      </div>
                      <div className="form-group col-md-12">
                        <InputBox
                          type="number"
                          id="nf-name"
                          placeholder="Enter allowed quantity ..."
                          title=" Allowed quantity *"
                          name="qty"
                        />
                      </div>
                    </div>

                    <CancelButton
                      back_path="/inventory-management"
                      ButtonTitle="Cancel"
                    />
                    <SubmitButton
                      ButtonName={props?.location?.state ? 'Update' : 'Add'}
                      loading={loading}
                    />
                  </Form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    </TableHeaderComponent>
  );
};

export default Inventoryform;
