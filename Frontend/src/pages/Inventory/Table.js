import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import ActionButton from '../../Components/Table/ActionButton';
import Pagination from '../../Components/Table/Pagination';
import TableComponent from '../../Components/Table/TableComponent';
import TableStatusButton from '../../Components/Table/TableStatusButton';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
// import moment from 'moment';
import {
  blockInventory,
  getAllMachinesInfo,
} from '../../Actions/productAction';
import PaginationControlled from '../../Components/Table/AdvancedPagination';
import DateFormate from '../../Components/Utils/DateFormate';
import { Productcsv } from '../../Actions/CSVAction';
import { formateDatereturn } from '../../Components/Utils/DayFormateFunc';

const Inventorytable = () => {
  const dispatch = useDispatch();
  const [getData, setGetData] = useState([]);
  const [csvdata, setCsvdata] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const getAllData = async ({ pageNumber, Keyword }) => {
    setLoading(true);
    const Enventory = await dispatch(
      getAllMachinesInfo({ pageNumber, Keyword })
    );
    setGetData(Enventory?.data?.data?.list);
    setLoading(false);
  };
  useEffect(() => {
    dispatch(Productcsv()).then((data) => {
      setCsvdata(data?.data?.data?.list);
    });
    getAllData({ pageNumber: 1 });
    document.title = 'Inventory Management | Torsa';
  }, []);

  //   console.log(getData);
  const Heading = [
    { title: 'Sr. No' },
    { title: ' Parts Name', key: '' },
    { title: 'Equipment', key: '' },
    { title: 'Equipment Model', key: '' },
    {
      title: 'No. of items present',
      key: '',
    },
    { title: 'Created On', key: '' },
    // { title: 'Status', key: '' },
    { title: 'Action', key: '' },
  ];

  const csvheader = [
    { label: 'Sr', key: 'sr' },
    {
      label: 'Equipment ',
      key: 'equipment',
    },
    {
      label: 'Equipment Modal ',
      key: 'equipmentmodal',
    },
    {
      label: 'No. of items present',
      key: 'presentitems',
    },

    {
      label: 'Created on',
      key: 'createdAt',
    },
    // {
    //   label: 'Status',
    //   key: 'active',
    // },
  ];
  const csv_Data = csvdata?.map((i, index) => {
    return {
      sr: index + 1,
      equipment: i?.equipment?.name,
      equipmentmodal: i?.equipmentmodal?.name,
      presentitems: i?.inventoryStock,
      createdAt: formateDatereturn(i.createdAt),
      active: i.active == false ? 'false' : i.active,
    };
  });

  return (
    <TableHeaderComponent
      loading={loading}
      card_title="Inventory Management"
      searchButton={true}
      SearchKeyBack={(e) => {
        getAllData({ pageNumber: 1, Keyword: e });
      }}
      exportButton="true"
      csvData={csv_Data}
      headers={csvheader}
      csvfilename="Inventory-csv.csv"
    >
      <div className="card-body">
        <div className="table-responsive">
          <TableComponent TableHeading={Heading}>
            {getData?.results?.map((el, index) => {
              return (
                <tr key={el._id}>
                  <td>
                    {' '}
                    {(getData?.current?.page + 1 || 1 + 1) *
                      getData?.current?.limit +
                      (index + 1) -
                      20}
                  </td>
                  <td>{el.partName}</td>
                  <td>{el?.category?.name}</td>
                  <td>{el?.subcategory?.name}</td>
                  <td>{el?.inventoryStock}</td>
                  <td>
                    <DateFormate formatter={el?.createdAt} />

                    {/* {moment(el.createdAt).format('ll')} */}
                  </td>
                  {/* <td>
                    <TableStatusButton
                      status={el.active}
                      handleBack={(e) =>
                        dispatch(blockInventory(el._id, e)).then((data) => {
                          getAllData({ pageNumber: currentPage });
                        })
                      }
                    />
                  </td> */}
                  <td className="py-2 ">
                    {' '}
                    <ActionButton
                      stateData={el}
                      selectPath="/updateinventory"
                    />
                  </td>
                </tr>
              );
            })}
          </TableComponent>
        </div>
      </div>
      <PaginationControlled
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      />
      {/* <Pagination
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      /> */}
    </TableHeaderComponent>
  );
};

export default Inventorytable;
