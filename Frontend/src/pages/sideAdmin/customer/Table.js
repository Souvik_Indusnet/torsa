// import React, { useEffect } from 'react';
// // import Heading from '../../../Components/tableHeading/userTable';
// import moment from 'moment';
// // import "../../inventory/mytable.css";
// import 'datatables.net-dt/js/dataTables.dataTables';
// import 'datatables.net-dt/css/jquery.dataTables.min.css';
// import SimpleBackdrop from '../../../Components/backdrop/Backdrop';
// import TableAction from './tableAction';
// var $ = require('jquery');
// // import "datatables.net-bs4";
// import 'datatables.net-responsive';
// // import "datatables.net-buttons-bs4";
// import 'datatables.net-buttons/js/buttons.colVis';
// import 'datatables.net-buttons/js/buttons.html5';
// import 'datatables.net-buttons/js/buttons.flash';
// import 'datatables.net-buttons/js/buttons.print';
// const JSZip = require('jszip');
// window.JSZip = JSZip;
// const TableData = ({ loading, userInfo }) => {
//   useEffect(() => {
//     if (userInfo) {
//       setTimeout(() => {
//         $('#dataTable3').DataTable({
//           dom: 'Bfrtip',
//           buttons: ['csv', 'excel', 'print'],
//         });
//       }, 2000);
//     }
//   }, []);
//   return (
//     <div className="card">
//       <SimpleBackdrop open={loading || !userInfo} />
//       <div className="card-body">
//         <div className="table-responsive">
//           <table className="table table-striped" id="dataTable3">
//             <thead>{/* <Heading /> */}</thead>
//             <tbody>
//               {userInfo &&
//                 userInfo
//                   .filter((i) => i.isDeleted === false)
//                   .map((i) => (
//                     <tr key={i._id}>
//                       <td>{i.name}</td>
//                       <td>{i.email}</td>
//                       <td>{i.contactNo}</td>
//                       <td>
//                         <span
//                           className={
//                             i.active
//                               ? 'badge light badge-success'
//                               : 'badge light badge-danger'
//                           }
//                         >
//                           {i.active ? 'Active' : 'Inactive'}
//                         </span>
//                       </td>
//                       <td>{moment(i.createdAt).format('ll')}</td>
//                       <td>
//                         <TableAction row={i} />
//                       </td>
//                     </tr>
//                   ))}
//             </tbody>
//           </table>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default TableData;
