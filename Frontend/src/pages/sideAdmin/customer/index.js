// import { memo, useEffect, useState } from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import { Link } from 'react-router-dom';
// import { getUserByThisAdmin } from '../../../Actions/sideAdminAction';
// import Header from '../../../Components/Dashboard/Header';
// import Navbarheader from '../../../Components/Dashboard/Navbarheader';
// import Sidebar from '../../../Components/Dashboard/Sidebar';
// import AuthWrapped from '../../../wrapped/AuthWrapped';
// import TableData from './Table';

// const CustomeManagment = memo(() => {
//   const dispatch = useDispatch();
//   const [menu, showMenu] = useState(true);
//   const { loading, userInfo } = useSelector((state) => state.userSigninInfo);

//   const [pageLoading, setPageLoading] = useState(false);
//   const [user, setUser] = useState([]);

//   useEffect(() => {
//     setPageLoading(true);
//     document.title = 'Customer | Torsa';
//     dispatch(getUserByThisAdmin(userInfo._id))
//       .then((data) => {
//         setPageLoading(false);
//         setUser(data);
//       })
//       .catch((error) => {
//         setPageLoading(false);
//         console.log(error.message);
//       });
//   }, []);

//   const handlecb = (e) => {
//     showMenu(e);
//   };

//   return (
//     <AuthWrapped condition={userInfo} path="/admin-signin" loading={loading}>
//       <div id="main-wrapper" className={menu ? 'show' : 'show menu-toggle'}>
//         <Navbarheader cb={handlecb} />
//         {/* <Chatbox /> */}
//         <Header
//           title={
//             userInfo && userInfo.isSystemAdmin
//               ? ' System Admin '
//               : 'Business Admin '
//           }
//           pageTitle="View Customer"
//         />
//         <Sidebar />
//         <div className="content-body">
//           <div className="container-fluid">
//             <div className="page-titles">
//               <ol className="breadcrumb">
//                 <li className="breadcrumb-item">
//                   <Link to="/dashboard">Dashboard</Link>
//                 </li>
//                 <li className="breadcrumb-item active">
//                   <Link>View Customer</Link>
//                 </li>
//               </ol>
//             </div>
//           </div>
//           {/* <div className="row"> */}
//           {/* <div className="container"> */}
//           <div className="col-xl-12 col-xxl-12">
//             <TableData loading={pageLoading} userInfo={user} />
//           </div>
//           {/* </div> */}
//           {/* </div> */}
//           {/* </div> */}
//         </div>
//       </div>
//     </AuthWrapped>
//   );
// });

// export default CustomeManagment;
