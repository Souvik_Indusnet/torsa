// import React, { memo, useEffect } from 'react';
// import { useSelector } from 'react-redux';
// import { Link, useHistory } from 'react-router-dom';
// import SimpleBackdrop from '../../../../Components/backdrop/Backdrop';
// import Header from '../../../../Components/Dashboard/Header';
// import Navbarheader from '../../../../Components/Dashboard/Navbarheader';
// import Sidebar from '../../../../Components/Dashboard/Sidebar';
// import Order from '../../../../Components/machine/orderPage';
// import AuthWrapped from '../../../../wrapped/AuthWrapped';

// const OrderDetailsBycustomer = memo((props) => {
//   // console.log(props.location.state.Datails);
//   const [menu, showMenu] = React.useState(true);
//   const { userInfo, loading } = useSelector((state) => state.userSigninInfo);
//   // console.log(userInfo);
//   useEffect(() => {
//     document.title = 'Order Managment | Torsa';
//   }, []);

//   const handlecb = (e) => {
//     showMenu(e);
//   };
//   return (
//     <AuthWrapped condition={userInfo} loading={loading} path="/admin-signin">
//       <div id="main-wrapper" className={menu ? 'show ' : 'show menu-toggle'}>
//         <Navbarheader cb={handlecb} />
//         <Header
//           title={
//             userInfo && userInfo.isSystemAdmin
//               ? ' System Admin '
//               : 'Business Admin '
//           }
//           pageTitle="Order Management"
//         />{' '}
//         <Sidebar />
//         <SimpleBackdrop open={loading} />
//         <div className="content-body">
//           <div className="container-fluid">
//             <div className="page-titles">
//               <ol className="breadcrumb">
//                 <li className="breadcrumb-item">
//                   <Link to="/dashboard">Dashboard</Link>
//                 </li>
//                 <li className="breadcrumb-item">
//                   <Link to="/customer-view">Customer</Link>
//                 </li>
//                 <li className="breadcrumb-item">
//                   <Link
//                     to={`/customer/details/${
//                       props && props.location.state.Datails.row._id
//                     }`}
//                   >
//                     Machine Details
//                   </Link>
//                 </li>
//                 <li className="breadcrumb-item active">
//                   <a href="javascript:void(0)">Order Details</a>
//                 </li>
//               </ol>
//             </div>
//           </div>
//           <Order
//             machineName={props && props.location.state.machineName}
//             Datails={props && props.location.state.Datails}
//           />
//         </div>
//       </div>
//       {/* <Footer /> */}
//     </AuthWrapped>
//   );
// });

// export default OrderDetailsBycustomer;
