// import React, { useEffect, memo } from 'react';

// import { useSelector } from 'react-redux';
// import { Link } from 'react-router-dom';
// import Header from '../../../Components/Dashboard/Header';
// import Navbarheader from '../../../Components/Dashboard/Navbarheader';
// import Sidebar from '../../../Components/Dashboard/Sidebar';
// import AuthWrapped from '../../../wrapped/AuthWrapped';
// import ProfileBody from './body';

// const ProfileSideAdmin = memo(() => {
//   const [menu, showMenu] = React.useState(true);
//   const { loading, userInfo } = useSelector((state) => state.userSigninInfo);

//   useEffect(() => {
//     document.title = 'Profile | Torsa';
//   }, []);

//   const handlecb = (e) => {
//     showMenu(e);
//   };
//   return (
//     <AuthWrapped condition={userInfo} path="/admin-signin" loading={loading}>
//       <div id="main-wrapper" className={menu ? 'show' : 'show menu-toggle'}>
//         <Navbarheader cb={handlecb} />
//         {/* <Chatbox /> */}
//         <Header
//           title={
//             userInfo && userInfo.isSystemAdmin
//               ? ' System Admin '
//               : 'Business Admin '
//           }
//           pageTitle="Profile - Torsa Machinery"
//         />
//         <Sidebar />
//         <div className="content-body">
//           <div className="container-fluid">
//             <div className="page-titles">
//               <ol className="breadcrumb">
//                 <li className="breadcrumb-item">
//                   <Link to="/dashboard">Dashboard</Link>
//                 </li>
//                 <li className="breadcrumb-item active">
//                   <Link to="/customerlist">My Profile</Link>
//                 </li>
//               </ol>
//             </div>
//             <ProfileBody />
//           </div>
//         </div>
//       </div>
//     </AuthWrapped>
//   );
// });

// export default ProfileSideAdmin;
