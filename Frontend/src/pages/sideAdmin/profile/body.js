// import { Button } from "@material-ui/core";
// import React, { useEffect, useState } from "react";
// import { useDispatch, useSelector } from "react-redux";
// import { getAdminById, openStatus } from "../../../Actions/userAction";
// import UpdateForm from "./update";
// import "./style.css";
// import { imageUpload } from "../../../Actions/imageupload";
// import { apiService } from "../../../service/apiservice";
// import SvgButton from "../../../Components/Analytics/SvgButton";
// import SettingBody from "./SettingBody";
// import { AdminProfile } from "../../../Actions/sideAdminAction";

// const ProfileBody = () => {
//   const dispatch = useDispatch();
//   const { userInfo } = useSelector((state) => state.userSigninInfo);
//   const { profileStatus } = useSelector((state) => state.openStatus);
//   const [state, setState] = useState({
//     user: [],
//     order: [],
//   });
//   const [loading, setLoading] = useState(false);
//   // console.log(userInfo);
//   const handleupdate = () => {
//     dispatch(openStatus({ status: true }));
//     dispatch(getAdminById(userInfo._id));
//   };

//   const handleChange = (e) => {
//     e.preventDefault();
//     // console.log(e.target.files[0]);
//     const formData = new FormData();
//     formData.append("admin", e.target.files[0]);
//     dispatch(imageUpload(formData));
//   };

//   useEffect(() => {
//     dispatch(AdminProfile(userInfo && userInfo._id)).then((data) => {
//       setState({ ...state, user: data.userList, order: data.OrderList });
//     });
//   }, []);

//   return (
//     <>
//       <div className="row">
//         <div className="col-lg-12">
//           <div className="profile card card-body px-3 pt-3 pb-0">
//             <div className="profile-head">
//               <div className="photo-content">
//                 <div className="cover-photo"></div>
//               </div>
//               <div className="profile-info">
//                 <div className="profile-photo">
//                   {/* <img
//                     src={`${apiService.productImage}/${userInfo.image}`}
//                     className="img-fluid rounded-circle"
//                     alt="image"
//                   /> */}
//                   <div className="img-fluid rounded-circle">
//                     <img
//                       className="img-fluid rounded-circle"
//                       src={`${apiService.productImage}/${userInfo.image}`}
//                       alt="profile pic"
//                       alt="polaroid"
//                     />
//                     <lable
//                       htmlFor="file"
//                       className="label"
//                       style={{ cursor: "pointer" }}
//                     >
//                       <i className="fa fa-camera"></i>
//                     </lable>
//                   </div>
//                 </div>
//                 <input
//                   type="file"
//                   id="file"
//                   name="admin"
//                   onChange={(e) => handleChange(e)}
//                 />
//                 <div className="profile-details">
//                   <div className="profile-name px-3 pt-2">
//                     <h4 className="text-primary mb-0">{}</h4>
//                   </div>
//                   <div className="profile-email px-2 pt-2">
//                     <h4 className="text-muted mb-0">
//                       {userInfo && userInfo.email}
//                     </h4>
//                     <p>
//                       {userInfo && userInfo.isSystemAdmin
//                         ? "System Admin"
//                         : "Business Admin"}
//                     </p>
//                   </div>
//                   <div className="dropdown ml-auto">
//                     <ul
//                       className="dropdown-menu dropdown-menu-right"
//                       x-placement="bottom-end"
//                       style={{
//                         position: "absolute",
//                         willChange: "transform",
//                         top: "0px",
//                         left: "0px",
//                         transform: "translate3d(-169px, 30px, 0px)",
//                       }}
//                     >
//                       <li className="dropdown-item">
//                         <i className="fa fa-user-circle text-primary mr-2"></i>{" "}
//                         View profile
//                       </li>
//                       <li className="dropdown-item">
//                         <i className="fa fa-users text-primary mr-2"></i> Add to
//                         close friends
//                       </li>
//                       <li className="dropdown-item">
//                         <i className="fa fa-plus text-primary mr-2"></i> Add to
//                         group
//                       </li>
//                       <li className="dropdown-item">
//                         <i className="fa fa-ban text-primary mr-2"></i> Block
//                       </li>
//                     </ul>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//       <div className="row">
//         <div className="col-xl-4">
//           <div className="row">
//             <div className="card">
//               <div className="card-body">
//                 <div className="profile-statistics">
//                   <div className="text-center">
//                     <div className="row">
//                       <div className="col">
//                         <h3 className="m-b-0">
//                           {state.user
//                             .filter((i) => i.isDeleted === false)
//                             .filter((i) => i.active === true) &&
//                             state.user.length}
//                         </h3>
//                         <span>Customer's</span>
//                       </div>
//                       <div className="col">
//                         <h3 className="m-b-0"></h3>
//                         <span></span>
//                       </div>
//                       <div className="col">
//                         <h3 className="m-b-0">
//                           {state.order && state.order.length}
//                         </h3>
//                         <span>Order's</span>
//                       </div>
//                     </div>
//                     <div className="mt-4"></div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//         <SettingBody />
//       </div>
//     </>
//   );
// };

// export default ProfileBody;
