// import { useSnackbar } from 'notistack';
// import React, { useEffect, useState } from 'react';
// import { useDispatch, useSelector } from 'react-redux';
// import { updateForm } from '../../../Actions/userAction';
// import FormElement from '../../../Components/form';
// import useMarkedFeilds from '../../../Components/formTouch';

// const SettingBody = () => {
//   const [setAllFeildsTouched] = useMarkedFeilds();
//   const { userInfo } = useSelector((state) => state.userSigninInfo);
//   console.log(userInfo);
//   const [state, setState] = useState({
//     _id: '',
//     name: '',
//     email: '',
//     contact: '',
//   });
//   const [loading, setLoading] = useState(false);
//   const { enqueueSnackbar } = useSnackbar();
//   const dispatch = useDispatch();

//   useEffect(() => {
//     if (userInfo) {
//       setState({
//         ...state,
//         _id: userInfo._id,
//         name: userInfo.name,
//         email: userInfo.email,
//         contact: userInfo.contactNo,
//       });
//     }
//   }, [userInfo]);

//   const handleChange = (e) => {
//     setState({ ...state, [e.target.name]: e.target.value });
//   };

//   const handleSubmit = (e) => {
//     e.preventDefault();
//     setLoading(true);
//     // console.log(state);
//     dispatch(updateForm(state))
//       .then((data) => {
//         enqueueSnackbar('Account Updated !', {
//           variant: 'success',
//           anchorOrigin: {
//             vertical: 'bottom',
//             horizontal: 'right',
//           },
//         });
//         setLoading(false);
//       })
//       .catch((err) => {
//         setLoading(false);
//       });
//   };

//   return (
//     <>
//       <div className="col-xl-8">
//         <div className="card">
//           <div className="card-body">
//             <div className="profile-tab">
//               <div className="custom-tab-1">
//                 <ul className="nav nav-tabs">
//                   <li className="nav-item">
//                     <a href="#about-me" data-toggle="tab" className="nav-link">
//                       About Me
//                     </a>
//                   </li>
//                   <li className="nav-item">
//                     <a
//                       href="#profile-settings"
//                       data-toggle="tab"
//                       className="nav-link"
//                     >
//                       Setting
//                     </a>
//                   </li>
//                 </ul>
//                 <div className="tab-content">
//                   <div id="about-me" className="tab-pane fade">
//                     <div className="profile-about-me">
//                       <div className="pt-4 border-bottom-1 pb-3">
//                         <h4 className="text-primary"></h4>
//                       </div>
//                     </div>

//                     <div className="profile-personal-info">
//                       <h4 className="text-primary mb-4">
//                         Personal Information
//                       </h4>
//                       <div className="row mb-4 mb-sm-4">
//                         <div className="col-sm-3">
//                           <h5 className="f-w-500">
//                             Name{' '}
//                             <span className="pull-right d-none d-sm-block">
//                               :
//                             </span>
//                           </h5>
//                         </div>
//                         <div className="col-sm-9">
//                           <span> {userInfo && userInfo.name}</span>
//                         </div>
//                       </div>
//                       <div className="row mb-4 mb-sm-4">
//                         <div className="col-sm-3">
//                           <h5 className="f-w-500">
//                             Email{' '}
//                             <span className="pull-right d-none d-sm-block">
//                               :
//                             </span>
//                           </h5>
//                         </div>
//                         <div className="col-sm-9">
//                           <span>{userInfo && userInfo.email}</span>
//                         </div>
//                       </div>
//                       <div className="row mb-4 mb-sm-4">
//                         <div className="col-sm-3">
//                           <h5 className="f-w-500">
//                             Availability{' '}
//                             <span className="pull-right d-none d-sm-block">
//                               :
//                             </span>
//                           </h5>
//                         </div>
//                         <div className="col-sm-9">
//                           <span>
//                             {userInfo && userInfo.active ? 'True' : 'False'}
//                           </span>
//                         </div>
//                       </div>
//                       <div className="row mb-4 mb-sm-4">
//                         <div className="col-sm-3">
//                           <h5 className="f-w-500">
//                             Contact Number{' '}
//                             <span className="pull-right d-none d-sm-block">
//                               :
//                             </span>
//                           </h5>
//                         </div>
//                         <div className="col-sm-9">
//                           <span>{userInfo && userInfo.contactNo}</span>
//                         </div>
//                       </div>
//                     </div>
//                   </div>
//                   <div id="profile-settings" className="tab-pane fade">
//                     <div className="profile-about-me">
//                       <div className="pt-4 border-bottom-1 pb-3">
//                         <h4 className="text-primary"></h4>
//                       </div>
//                     </div>
//                     <div className="pt-3">
//                       <div className="settings-form">
//                         <h4 className="text-primary">Account Setting</h4>
//                         <form onSubmit={handleSubmit}>
//                           <div className="form-row">
//                             <div className="form-group col-md-6">
//                               <div className="form-group">
//                                 <FormElement
//                                   lable="Update your Name"
//                                   placeholder="Name"
//                                   type="text"
//                                   name="name"
//                                   onchange={handleChange}
//                                   value={state.name}
//                                   reqText="Enter Your Profile Name"
//                                 />
//                               </div>
//                               <div className="form-group">
//                                 <FormElement
//                                   lable="Update email address"
//                                   placeholder="Email"
//                                   type="email"
//                                   name="email"
//                                   onchange={handleChange}
//                                   value={state.email}
//                                   reqText="Enter Profile Email"
//                                 />
//                               </div>
//                               <div className="form-group">
//                                 <FormElement
//                                   lable="Update contact Number"
//                                   placeholder="Contact"
//                                   type="number"
//                                   name="contact"
//                                   onchange={handleChange}
//                                   value={state.contact}
//                                   reqText="Enter Profile Contact Nu"
//                                 />
//                               </div>
//                             </div>
//                           </div>
//                           <button
//                             className="btn btn-primary"
//                             type="submit"
//                             onClick={setAllFeildsTouched}
//                             color={loading ? 'secondary' : 'primary'}
//                           >
//                             {loading ? 'Loading...' : 'Update'}
//                           </button>
//                         </form>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </>
//   );
// };

// export default SettingBody;
