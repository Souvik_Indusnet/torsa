// import { Button } from "@material-ui/core";
// import { useSnackbar } from "notistack";
// import { useEffect, useState } from "react";
// import { useDispatch, useSelector } from "react-redux";
// import {
//   getAdminById,
//   openStatus,
//   updateForm,
// } from "../../../Actions/userAction";
// import FormElement from "../../../Components/form";
// import useMarkedFeilds from "../../../Components/formTouch";

// const UpdateForm = ({ id }) => {
//   const { enqueueSnackbar } = useSnackbar();

//   const dispatch = useDispatch();
//   const [setAllFeildsTouched] = useMarkedFeilds();
//   const { adminInfo, error } = useSelector((state) => state.adminInfo);
//   console.log(adminInfo, error);
//   const handlecancle = ({ id }) => {
//     setState({
//       name: "",
//       email: "",
//       contact: "",
//     });
//     dispatch(openStatus({ status: false }));
//   };

//   useEffect(() => {
//     dispatch(getAdminById(id));
//     if (adminInfo) {
//       setState({
//         _id: adminInfo._id,
//         name: adminInfo.name,
//         email: adminInfo.email,
//         contact: adminInfo.contactNo,
//       });
//     } else {
//       // dispatch(openStatus({ status: false }));
//     }
//   }, [adminInfo, getAdminById]);

//   const [state, setState] = useState({
//     _id: "",
//     name: "",
//     email: "",
//     contact: "",
//   });
//   const [loading, setLoading] = useState(false);
//   const handleSubmit = (e) => {
//     e.preventDefault();
//     setLoading(true);
//     // console.log(state);
//     dispatch(updateForm(state))
//       .then((data) => {
//         enqueueSnackbar("Account Updated !", {
//           variant: "success",
//           anchorOrigin: {
//             vertical: "bottom",
//             horizontal: "right",
//           },
//         });
//         setLoading(false);
//         dispatch(openStatus({ status: false }));
//       })
//       .catch((err) => {
//         setLoading(false);
//       });
//   };
//   const handleChange = (e) => {
//     setState({ ...state, [e.target.name]: e.target.value });
//   };
//   return (
//     <div className="row">
//       <div className="col-xl-12 col-lg-12">
//         <div className="card">
//           <div className="card-header">
//             <h4 className="card-title">Update Profile</h4>
//           </div>
//           <div className="card-body">
//             <div className="basic-form">
//               <form onSubmit={handleSubmit}>
//                 <div className="form-group">
//                   <FormElement
//                     placeholder="Name"
//                     type="text"
//                     name="name"
//                     onchange={handleChange}
//                     value={state.name}
//                     reqText="Enter Your Profile Name"
//                   />
//                 </div>
//                 <div className="form-group">
//                   <FormElement
//                     placeholder="Email"
//                     type="email"
//                     name="email"
//                     onchange={handleChange}
//                     value={state.email}
//                     reqText="Enter Profile Email"
//                   />
//                 </div>
//                 <div className="form-group">
//                   <FormElement
//                     placeholder="Contact"
//                     type="number"
//                     name="contact"
//                     onchange={handleChange}
//                     value={state.contact}
//                     reqText="Enter Profile Contact Nu"
//                   />
//                   <div className="card-footer" style={{ marginTop: "10px" }}>
//                     <Button
//                       type="submit"
//                       onClick={setAllFeildsTouched}
//                       color={loading ? "secondary" : "primary"}
//                     >
//                       {loading ? "Loading..." : "Update"}
//                     </Button>
//                     <Button onClick={handlecancle} color="primary">
//                       Cancle
//                     </Button>
//                   </div>
//                 </div>
//               </form>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default UpdateForm;
