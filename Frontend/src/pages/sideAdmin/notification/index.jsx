// import { memo, useEffect, useState } from "react";
// import { useSelector } from "react-redux";
// import { Link } from "react-router-dom";
// import Header from "../../../Components/Dashboard/Header";
// import Navbarheader from "../../../Components/Dashboard/Navbarheader";
// import Sidebar from "../../../Components/Dashboard/Sidebar";
// import AuthWrapped from "../../../wrapped/AuthWrapped";
// import TableBody from "./table";

// const Notification = memo(() => {
//   const [menu, showMenu] = useState(true);
//   const { loading, userInfo } = useSelector((state) => state.userSigninInfo);

//   useEffect(() => {
//     document.title = "Notification | Torsa";
//   }, []);

//   const handlecb = (e) => {
//     showMenu(e);
//   };

//   return (
//     <AuthWrapped condition={userInfo} path="/admin-signin" loading={loading}>
//       <div id="main-wrapper" className={menu ? "show" : "show menu-toggle"}>
//         <Navbarheader cb={handlecb} />
//         <Header
//           title={
//             userInfo && userInfo.isSystemAdmin
//               ? " System Admin "
//               : "Business Admin "
//           }
//           pageTitle="Notification Management"
//         />
//         <Sidebar />
//         <div className="content-body">
//           <div className="container-fluid">
//             <div className="page-titles">
//               <ol className="breadcrumb">
//                 <li className="breadcrumb-item">
//                   <Link to="/dashboard">Dashboard</Link>
//                 </li>
//                 <li className="breadcrumb-item active">
//                   <Link to="/customerlist">Notification Management</Link>
//                 </li>
//               </ol>
//             </div>
//             <div className="row">
//               <div className="container">
//                 <div className="col-xl-12 col-xxl-12">
//                   <TableBody />
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </AuthWrapped>
//   );
// });
// export default Notification;
