// import React, { useEffect } from 'react';
// import AuthWrapped from '../../wrapped/AuthWrapped';
// import { useDispatch, useSelector } from 'react-redux';
// import Navbarheader from '../../Components/Dashboard/Navbarheader';
// import Sidebar from '../../Components/Dashboard/Sidebar';
// import Header from '../../Components/Dashboard/Header';
// import { Paper } from '@material-ui/core';
// import { makeStyles } from '@material-ui/core/styles';
// import { Container, Typography } from '@material-ui/core';
// import { getManagerById } from '../../Actions/userAction';
// import AdminPanel from '../../Components/Profile/Admin/Pannel';
// import { apiService } from '../../service/apiservice';
// const useStyles = makeStyles((theme) => ({
//   root: {
//     ...theme.typography.button,
//     backgroundColor: theme.palette.background.paper,
//     padding: theme.spacing(1),
//   },
// }));

// const Adminindex = (props) => {
//   const dispatch = useDispatch();
//   const classes = useStyles();
//   const { userInfo, loading } = useSelector((state) => state.userSigninInfo);
//   const [menu, showMenu] = React.useState(true);
//   const [state, setState] = React.useState([]);
//   const handlecb = (e) => {
//     showMenu(e);
//   };
//   // console.log(state);
//   useEffect(() => {
//     dispatch(getManagerById(props.match.params.id)).then((data) =>
//       setState(data)
//     );
//   }, [getManagerById]);

//   return (
//     <AuthWrapped condition={userInfo} loading={loading} path="/admin-signin">
//       <div id="main-wrapper" className={menu ? 'show' : 'show menu-toggle'}>
//         <Navbarheader cb={handlecb} />
//         <Header
//           title={
//             userInfo && userInfo.isSystemAdmin
//               ? ' System Admin Machinery'
//               : 'Business Admin Machinery'
//           }
//         />
//         <Sidebar />
//         {/* <Eventlist /> */}
//         <div className="content-body">
//           <div className="container-fluid">
//             <img
//               src={`${apiService.productImage}/${state.image}`}
//               alt="logo"
//               style={{
//                 borderRadius: '50%',
//                 marginBottom: '-80px',
//                 marginLeft: '10px',
//               }}
//             />
//             <Paper>
//               <div className={classes.root}>
//                 <div className="head__customer">
//                   <Typography style={{ marginLeft: '190px' }}>
//                     {!state ? 'Loading...' : state.name}- Torsa Machinery Admin
//                   </Typography>
//                   <div className="header_page" style={{ padding: '40px' }} />

//                   <AdminPanel key={props.match.params.id} data={state} />
//                 </div>
//               </div>
//             </Paper>
//           </div>
//         </div>
//       </div>
//     </AuthWrapped>
//   );
// };

// export default Adminindex;
