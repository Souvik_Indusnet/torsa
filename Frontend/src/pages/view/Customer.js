// import React, { useEffect } from 'react';
// import AuthWrapped from '../../wrapped/AuthWrapped';
// import { useDispatch, useSelector } from 'react-redux';
// import Login from '../login/Login';
// import Navbarheader from '../../Components/Dashboard/Navbarheader';
// import Sidebar from '../../Components/Dashboard/Sidebar';
// import Header from '../../Components/Dashboard/Header';
// import { Paper } from '@material-ui/core';
// import { makeStyles } from '@material-ui/core/styles';
// // import Eventlist from '../../Components/Dashboard/Eventlist';
// import { Container, Typography } from '@material-ui/core';
// import Panel from '../../Components/Profile/Panel';
// import Footer from '../../Components/Dashboard/Footer';
// import { getuserById } from '../../Actions/userAction';
// // import { Panel } from "../../Components/Dashboard/Profile/Pannel";
// const useStyles = makeStyles((theme) => ({
//   root: {
//     ...theme.typography.button,
//     backgroundColor: theme.palette.background.paper,
//     padding: theme.spacing(1),
//   },
// }));

// const Customerindex = (props) => {
//   const classes = useStyles();
//   const { userInfo, loading } = useSelector((state) => state.userSigninInfo);
//   // console.log(props);
//   const [menu, showMenu] = React.useState(true);
//   const handlecb = (e) => {
//     showMenu(e);
//   };

//   const dispatch = useDispatch();
//   const [state, setState] = React.useState([]);
//   useEffect(() => {
//     dispatch(getuserById(props.match.params.id)).then((data) => {
//       setState(data);
//     });
//   }, [getuserById]);

//   return (
//     <AuthWrapped condition={userInfo} loading={loading} path="/admin-signin">
//       <div id="main-wrapper" className={menu ? 'show' : 'show menu-toggle'}>
//         <Navbarheader cb={handlecb} />
//         <Header
//           title={
//             userInfo && userInfo.isSystemAdmin
//               ? ' System Admin '
//               : 'Business Admin '
//           }
//           pageTitle="Customer Management"
//         />
//         <Sidebar />
//         {/* <Eventlist /> */}
//         <div className="content-body">
//           <div className="container-fluid">
//             {/* <img
//               src="/images/avatar/1.jpg"
//               alt="logo"
//               style={{
//                 borderRadius: '50%',
//                 marginBottom: '-80px',
//                 marginLeft: '10px',
//               }}
//             /> */}
//             <Paper>
//               <div className={classes.root}>
//                 <div className="head__customer">
//                   {/* <Typography style={{ marginLeft: '170px' }}>
//                     {state ? state.name : 'Loading..'} - Torsa Machinery
//                   </Typography> */}
//                   {/* <div
//                     className="header_page"
//                     style={{ padding: '40px' }}
//                   ></div> */}
//                   <Panel id={props.match.params.id} />
//                 </div>
//               </div>
//             </Paper>
//           </div>
//         </div>
//         {/* <Footer /> */}
//       </div>
//     </AuthWrapped>
//   );
// };

// export default Customerindex;
