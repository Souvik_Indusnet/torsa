import { memo, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getOrderNotification } from '../../Actions/notification';
import Header from '../../Components/Dashboard/Header';
import Navbarheader from '../../Components/Dashboard/Navbarheader';
import Sidebar from '../../Components/Dashboard/Sidebar';
import AuthWrapped from '../../wrapped/AuthWrapped';
import { Link } from 'react-router-dom';
import SimpleBackdrop from '../../Components/backdrop/Backdrop';
import NotificationPage from '../../Components/Notification/NotificationPage';

const AdminNotificaton = memo(() => {
  const dispatch = useDispatch();
  const [menu, showMenu] = useState(true);
  const { loading, userInfo } = useSelector((state) => state.userSigninInfo);
  const { loading: loadingNotification, notification: newnote } = useSelector(
    (state) => state.OrderNote
  );
  const [pageload, setLoading] = useState(false);
  // console.log(newnote);
  useEffect(() => {
    setLoading(true);
    document.title = 'Dashboard | Torsa';

    // dispatch(getOrderNotification());
    setLoading(false);
  }, [getOrderNotification]);

  const handlecb = (e) => {
    showMenu(e);
  };
  //   console.log(newnote);
  return <NotificationPage />;
});

export default AdminNotificaton;
