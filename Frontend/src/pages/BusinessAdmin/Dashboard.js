import React, { useEffect, memo } from 'react';

import Containbody1 from '../../Components/Dashboard/Containbody1';

const BusinessAdminDashboard = memo(() => {
  useEffect(() => {
    document.title = 'Dashboard | Torsa';
  }, []);

  return <Containbody1 />;
});

export default BusinessAdminDashboard;