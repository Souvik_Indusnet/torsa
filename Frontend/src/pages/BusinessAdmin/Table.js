import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  blockStatusAdmin,
  blockStatusAdminB,
  GetAllBusinessAdmin,
} from '../../Actions/userAction';
import ActionButton from '../../Components/Table/ActionButton';
import Pagination from '../../Components/Table/Pagination';
import TableComponent from '../../Components/Table/TableComponent';
import TableStatusButton from '../../Components/Table/TableStatusButton';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
import PaginationControlled from '../../Components/Table/AdvancedPagination';
import DateFormate from '../../Components/Utils/DateFormate';
import { BACsv } from '../../Actions/CSVAction';
import { formateDatereturn } from '../../Components/Utils/DayFormateFunc';
import { useHistory } from 'react-router-dom';

const BusinessAdminTable = () => {
  const history = useHistory();
  const urlParams = new URLSearchParams(window.location.search).get('search');
  const dispatch = useDispatch();
  const [getData, setGetData] = useState([]);
  const [csvdata, setCsvdata] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const getAllData = async ({ pageNumber, Keyword }) => {
    setLoading(true);
    const Equipments = await dispatch(
      GetAllBusinessAdmin({ pageNumber, Keyword })
    );
    // console.log(pageNumber, Keyword, Equipments);
    setGetData(Equipments?.data?.data?.list);
    setLoading(false);
  };

  useEffect(() => {
    dispatch(BACsv()).then((data) => {
      setCsvdata(data?.data?.data?.list);
    });
  }, [loading]);

  useEffect(() => {
    history.replace({
      pathname: window.location.pathname,
      search: `?search=${urlParams}`,
    });
    if (urlParams != '' && urlParams != 'null' && urlParams != null) {
      getAllData({ pageNumber: 1, Keyword: urlParams });
    } else {
      getAllData({ pageNumber: 1 });
    }
    document.title = 'Business Admin Management | Torsa';
  }, []);

  const Heading = [
    { title: 'Sr. No' },
    { title: ' Name', key: '' },
    { title: 'Email', key: '' },
    { title: 'Create On', key: '' },
    { title: 'Status', key: '' },
    { title: 'Action', key: '' },
  ];

  const csvheader = [
    { label: 'Sr', key: 'sr' },
    { label: 'Name', key: 'name' },
    {
      label: 'Email',
      key: 'email',
    },
    {
      label: 'Created on',
      key: 'createdAt',
    },
    {
      label: 'Status',
      key: 'active',
    },
  ];
  const csv_Data = csvdata?.map((i, index) => {
    return {
      sr: index + 1,
      name: i?.name,
      email: i?.email,
      createdAt: formateDatereturn(i.createdAt),
      active: i.active == false ? 'false' : i.active,
    };
  });
  // console.log(getData);
  return (
    <TableHeaderComponent
      loading={loading}
      card_title="Business Admin Management"
      add_tittle="Add"
      addpath="/addbusinessadmin"
      searchButton={true}
      search={urlParams}
      SearchKeyBack={(e) => {
        history.replace({
          pathname: window.location.pathname,
          search: `?search=${e}`,
        });
        getAllData({ pageNumber: 1, Keyword: e });
      }}
      exportButton="true"
      csvData={csv_Data}
      headers={csvheader}
      csvfilename="Business-admin-csv.csv"
    >
      <div className="card-body">
        <div className="table-responsive">
          <TableComponent TableHeading={Heading}>
            {getData?.results
              ?.filter((i) => i.isSystemAdmin == false)
              .filter((i) => i.isDeleted == false)
              ?.map((el, index) => {
                return (
                  <tr key={index}>
                    <td>
                      {(getData?.current?.page + 1 || 1 + 1) *
                        getData?.current?.limit +
                        (index + 1) -
                        20}
                      {/* {index + 1} */}
                    </td>
                    <td>{el?.name}</td>
                    <td>{el?.email}</td>

                    <td>
                      <DateFormate formatter={el.createdAt} />
                    </td>
                    <td>
                      <TableStatusButton
                        status={el.active}
                        handleBack={(e) =>
                          dispatch(blockStatusAdminB(el._id, e)).then(
                            (data) => {
                              getAllData({ pageNumber: currentPage });
                            }
                          )
                        }
                      />
                    </td>
                    <td className="py-2 ">
                      {' '}
                      <ActionButton
                        stateData={el}
                        selectPath="/updatebusinessadmin"
                        // nonEditable={false}
                      />
                    </td>
                  </tr>
                );
              })}
          </TableComponent>
        </div>
      </div>
      <PaginationControlled
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      />
      {/* <Pagination
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      /> */}
    </TableHeaderComponent>
  );
};

export default BusinessAdminTable;
