import {useEffect} from "react";
import { useSelector } from 'react-redux';
import SimpleBackdrop from '../../Components/backdrop/Backdrop';
import ActionButton from '../../Components/Table/ActionButton';
import { apiService } from '../../service/apiservice';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';

const MyAccount = () => {
  const { userInfo, loading } = useSelector((state) => state.userSigninInfo);
  // console.log(userInfo);
  useEffect(()=>{
   document.title="My Account | Torsa"
  },[])
  return (
    <div className="row">
      <div className="col-lg-12">
        <SimpleBackdrop open={loading} />

        <div className="profile   px-3 pt-3 pb-0">
          <div className="profile-head">
            {/* <div className="photo-content">
              <div className="cover-photo"></div>
            </div> */}
            <div className="profile-info">
            <b>{loading ? 'Loading...' : `${userInfo?.name} Details`}</b>
              <div className="dropdown ml-auto">
                  <ActionButton
                    stateData={userInfo}
                    selectPath="/updatebusinessadmin"
                    nonEditable={true}
                  />
                </div>


            </div>
              <div className="profile-details pb-4">
                <div className="profile-info-row-bg profile-name px-3 pt-2 pb-2">
                  <h4 className="text-primary mb-0">
                  <svg
                  viewBox="0 0 24 24"
                  width="24"
                  height="24"
                  stroke="currentColor"
                  stroke-width="2"
                  fill="none"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  class="css-i6dzq1 svg-profile"
                >
                  <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                  <circle cx="12" cy="7" r="4"></circle>
                </svg>{' '}
                 {userInfo?.name}</h4>
                  {/* <p>Business Admin</p> */}
                </div>
                <div className="profile-email px-3 pt-2 pb-2">
                  <h4 className="text-muted mb-0">
                  <svg
                  viewBox="0 0 24 24"
                  width="24"
                  height="24"
                  stroke="currentColor"
                  stroke-width="2"
                  fill="none"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  class="css-i6dzq1 svg-profile"
                >
                  <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                  <polyline points="22,6 12,13 2,6"></polyline>
                </svg>{' '}
                {userInfo?.email}</h4>
                  {/* <p>Email</p> */}
                </div>
                <div className="profile-info-row-bg profile-contact px-3 pt-2 pb-2">
                  <h4 className="text-muted mb-0">
                  <svg
                  viewBox="0 0 24 24"
                  width="24"
                  height="24"
                  stroke="currentColor"
                  stroke-width="2"
                  fill="none"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  class="css-i6dzq1 svg-profile"
                >
                  <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                </svg>{' '}
                {userInfo?.contactNo}</h4>
                  {/* <p>Contact</p> */}
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyAccount;
