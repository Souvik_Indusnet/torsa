import { Form, Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { useSnackbar } from 'notistack';

import {
  addBusinessAdmins,
  updateBusinessAdmins,
  updateBusinessAdminsAndState,
} from '../../Actions/userAction';
import CancelButton from '../../Components/Button/Cancel';
import SubmitButton from '../../Components/Button/Submit';
import InputBox from '../../Components/form/input';
import InputImage from '../../Components/form/InputimageComponent';

import TableHeaderComponent from '../../Components/Table/TableTopComponent';
import { businessAdminSchema } from '../../Components/validation/validationSchema';
import { apiService } from '../../service/apiservice';

const AddBusinessAdminForm = (props) => {
  // console.log(props);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const history = useHistory();
  const { userInfo } = useSelector((state) => state.userSigninInfo);
  const [state, setState] = useState({
    image: '',
  });
  useEffect(() => {
    document.title = props?.location?.state
      ? 'Update Business Admin |Torsa'
      : 'Add Business Admin | Torsa';
  }, []);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    setLoading(true);
    const formData = new FormData();
    formData.append(
      '_id',
      props?.location?.state?._id?._id || props?.location?.state?._id
    );
    formData.append('name', e.name);
    formData.append('email', e.email.toLowerCase());
    formData.append('contactNo', e.phone);
    formData.append('image', state.image);

    // const validData = {
    //   _id: props?.location?.state?._id?._id || props?.location?.state?._id,
    //   name: e.name,
    //   email: e.email,
    //   contact: e.phone,
    // };
    // console.log(validData);
    dispatch(
      !userInfo?.isSystemAdmin
        ? updateBusinessAdminsAndState(
            formData,
            props?.location?.state?._id?._id || props?.location?.state?._id
          )
        : props?.location?.state
        ? updateBusinessAdmins(
            formData,
            props?.location?.state?._id?._id || props?.location?.state?._id
          )
        : addBusinessAdmins(formData)
    ).then((data) => {
      setLoading(false);
      if (data?.data) {
        history.push(
          !userInfo?.isSystemAdmin
            ? '/my-account'
            : '/business-admin-management'
        );
      } else {
        enqueueSnackbar(`${data?.error?.response?.data?.errorMessage}`, {
          variant: 'warning',
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'right',
          },
        });
        // console.log(data?.error?.response?.data?.errorMessage);
      }
    });
  };

  return (
    <TableHeaderComponent
      loading={loading}
      card_title={
        props?.location?.state ? 'Update Business Admin' : 'Add Business Admin'
      }
      SearchKeyBack={(e) => {
        console.log(e);
      }}
    >
      <div className="mt-20">
        <div className="basic-form">
          <Formik
            initialValues={{
              name: props?.location?.state?.name || '',
              email: props?.location?.state?.email || '',
              phone: props?.location?.state?.contactNo || '',
            }}
            validationSchema={businessAdminSchema}
            onSubmit={handleSubmit}
          >
            {(formik) => {
              return (
                <Form>
                  <div className="row">
                    <div className="form-group col-md-6">
                      <InputBox
                        type="text"
                        id="nf-name"
                        placeholder="Enter Name..."
                        title="  Name"
                        name="name"
                        readonly={props?.location?.state ? 'true' : false}
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <InputBox
                        type="email"
                        id="nf-name"
                        placeholder="Enter  Email..."
                        title="  Email"
                        name="email"
                        readonly={props?.location?.state ? 'true' : false}
                      />
                    </div>
                    <div className="form-group col-md-12">
                      <InputBox
                        type="text"
                        id="nf-name"
                        placeholder="Enter Contact Number..."
                        title=" Contact"
                        name="phone"
                      />
                    </div>
                    {/* {!userInfo?.isSystemAdmin && (
                      <div className="form-group col-md-12">
                        <InputImage
                          title="Image"
                          buttonTXT="To upload the image of a Product"
                          name="image"
                          imgsrc={
                            props.location?.state
                              ? apiService.IPAddress +
                                'uploads/' +
                                props.location?.state.image
                              : null
                          }
                          imageBack={(e) =>
                            setState({ ...state, image: e.file })
                          }
                        />
                      </div>
                    )} */}

                    <div className="col-md-8">&nbsp;</div>
                    <div className="col-md-2">
                      <CancelButton
                        back_path={
                          !userInfo?.isSystemAdmin
                            ? '/my-account'
                            : '/business-admin-management'
                        }
                        ButtonTitle="Cancel"
                      />
                    </div>
                    <div className="col-md-2">
                      <SubmitButton
                        ButtonName={props?.location?.state ? 'Update' : 'Add'}
                        loading={loading}
                      />
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </TableHeaderComponent>
  );
};

export default AddBusinessAdminForm;
