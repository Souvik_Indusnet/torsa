import React, { useState } from 'react';
import { forgotPassword } from '../../Actions/userAction';
const Forgate = () => {
  const [loading, setLoading] = useState(false);
  const [state, setState] = useState({
    email: '',
  });
  const handleform = (e) => {
    setState({ ...state, [e.target.name]: e.target.value });
  };
  const handleformSubmit = (e) => {
    e.preventDefault();
    if (state.email.length > 0) {
      setLoading(true);
      const data = forgotPassword(state.email);
      data.then((data) => {
        // console.log(data);
      });
    }
  };
  return (
    <div className="h-100 middlejs" component="body">
      <div className="authincation h-100">
        <div className="container h-100">
          <div className="row justify-content-center h-100 align-items-center">
            <div className="col-md-6">
              <div className="authincation-content">
                <div className="row no-gutters">
                  <div className="col-xl-12">
                    <div className="auth-form">
                      <h4 className="text-center mb-4">Forgot Password</h4>
                      <form onSubmit={handleformSubmit}>
                        <div className="form-group">
                          <label htmlFor="email">
                            <strong>Email</strong>
                          </label>
                          <input
                            type="email"
                            name="email"
                            className="form-control"
                            required
                            value={state.email}
                            onChange={handleform}
                          />
                        </div>
                        <div
                          className=" alert-success m-2  p-2 alert-danger"
                          role="alert"
                        >
                          This is a success alert—check it out!
                        </div>
                        <div className="text-center">
                          <button
                            type="submit"
                            value="submit"
                            className="btn btn-primary btn-block"
                          >
                            SUBMIT
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Forgate;
