import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { userSignin } from '../../Actions/userAction';

import Recaptcha from 'react-recaptcha';
import { Form, Formik } from 'formik';
import { signInschema } from '../../Components/validation/validationSchema';
import InputBox from '../../Components/form/input';

import { useSnackbar } from 'notistack';
const Login = (props) => {
  console.log('12');
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [captcha, setCaptch] = React.useState({
    loadCaptcha: false,
    captchaDone: false,
  });
  const dispatch = useDispatch();

  const { loading, userInfo, payload } = useSelector(
    (state) => state.userSigninInfo
  );
  useEffect(() => {
    document.title = 'Sign in | Torsa';
    if (payload) {
      // console.log(payload);
      enqueueSnackbar(`${payload}`, {
        variant: 'error',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'right',
        },
      });
    }
    if (!navigator.onLine) {
      // console.log(payload);
      enqueueSnackbar(`Please check your internet conncetion`, {
        variant: 'error',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'right',
        },
      });
    }
    if (userInfo) {
      enqueueSnackbar(`Login Success Redirect to dashboard`, {
        variant: 'success',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'right',
        },
      });
    }
  }, [payload, navigator.onLine, userInfo]);

  const signformHandle = (e) => {
    if (!navigator.onLine) {
      enqueueSnackbar('Please check your internet conncetion', {
        variant: 'error',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'right',
        },
      });
    }
    if (captcha.captchaDone) {
      dispatch(
        userSignin({ username: e.email.toLowerCase(), password: e.password })
      );
    }
  };

  if (userInfo) {
    setTimeout(() => {
      if (userInfo.isSystemAdmin) {
        props.history.push('/dashboard');
      } else {
        props.history.push('/businessadmin_dashboard');
      }
    }, 1000);
  }
  const callback = (e) => {
    setCaptch({ ...captcha, loadCaptcha: true });
  };
  var verifyCallback = function (response) {
    if (response) {
      setCaptch({ ...captcha, captchaDone: true });
    }
  };

  return (
    <div className="h-100  middlejs2 ">
      <div className="authincation h-100">
        <div className="container h-100">
          <div className="row justify-content-center h-100 align-items-center">
            <div className="col-md-6">
              <h4 className="text-center ml-6 mb-4">
                {/* Torsa */}
                <div className="text-center  mb-4">
                  <img
                    style={{
                      width: '40%',
                      height: '100%',
                      boxSizing: 'border-box',
                      overflow: 'hidden',
                    }}
                    src="/logo/Torsa_Logo_Final2.png"
                    alt="logo"
                  />
                </div>{' '}
              </h4>
              <div className="authincation-content">
                <div className="row no-gutters">
                  <div className="col-xl-12">
                    <div className="auth-form">
                      <Formik
                        initialValues={{
                          email: '',
                          password: '',
                        }}
                        validationSchema={signInschema}
                        onSubmit={signformHandle}
                      >
                        {(formik) => {
                          return (
                            <Form>
                              <div className="form-group">
                                <label className="color-white">Email *</label>
                                <InputBox
                                  type="email"
                                  id="nf-name"
                                  placeholder="Email"
                                  name="email"
                                  cursor="none"
                                />
                              </div>
                              <div className=" form-group mb-3">
                                <label className="color-white">
                                  Password *
                                </label>

                                <InputBox
                                  type="password"
                                  id="nf-name"
                                  placeholder="Password"
                                  name="password"
                                />
                              </div>

                              {/* <div className="input-group mb-3">
                                <InputBox
                                  type="password"
                                  id="nf-name"
                                  placeholder="Password"
                                  name="password"
                                />
                              </div> */}

                              {!userInfo && (
                                <div className="form-group mb-4 ml-3">
                                  <Recaptcha
                                    style={{
                                      border: 'none',
                                      width: '100%',
                                      height: '100%',
                                      boxSizing: 'border-box',
                                      overflow: 'hidden',
                                    }}
                                    sitekey="6LeXHTQaAAAAAAFbPWdQNakKAez-ZRiNix5pZv7z"
                                    render="explicit"
                                    onloadCallback={callback}
                                    verifyCallback={verifyCallback}
                                  />
                                </div>
                              )}

                              <div className="text-center">
                                <button
                                  className="btn btn-primary"
                                  type="submit"
                                  disabled={loading || captcha.loadCaptcha}
                                  style={{
                                    cursor:
                                      loading || captcha.loadCaptcha
                                        ? 'not-allowed'
                                        : 'pointer',
                                  }}
                                >
                                  {' '}
                                  {loading && (
                                    <span
                                      className="spinner-border spinner-border-sm"
                                      role="status2"
                                      aria-hidden="true"
                                    ></span>
                                  )}
                                  {'      '}
                                  {loading ? 'Loading...' : 'Sign in'}
                                </button>
                              </div>
                            </Form>
                          );
                        }}
                      </Formik>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
