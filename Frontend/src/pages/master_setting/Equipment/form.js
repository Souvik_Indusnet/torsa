import { Form, Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import {
  CreateEquipment,
  UpdateEquipment,
} from '../../../Actions/categoryAction';
import CancelButton from '../../../Components/Button/Cancel';
import SubmitButton from '../../../Components/Button/Submit';
import InputBox from '../../../Components/form/input';
import TableHeaderComponent from '../../../Components/Table/TableTopComponent';
import { EquipmentSchema } from '../../../Components/validation/validationSchema';
import { useSnackbar } from 'notistack';
const EquipmentForm = (props) => {
  const history = useHistory();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  useEffect(() => {
    document.title = props?.location?.state
      ? 'Update Equipment |Torsa'
      : 'Add Equipment | Torsa';
  }, []);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const [image, setImage] = useState('');

  const handleSubmit = (values, actions) => {
    const formData = new FormData();
    formData.append('category', values.name);
    formData.append('images', image);
    formData.append('id', props?.location?.state?._id?._id);
    setLoading(true);
    setLoading(true);
    dispatch(
      props?.location?.state
        ? UpdateEquipment(formData)
        : CreateEquipment(formData)
    ).then((data) => {
      console.log(data);
      setLoading(false);
      if (
        data?.error?.response?.data?.errorCode == 500 ||
        data?.error?.response?.status == 500
      ) {
        enqueueSnackbar('Equipment Already Exists', {
          variant: 'error',
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'right',
          },
        });
        // alert(JSON.stringify(data.error.response.data.errorMessage));
        actions.setSubmitting(false);
      } else {
        history.push('/equipment');
      }
    });
  };
  console.log('props?.location?.state?._id', props?.location?.state?._id?._id);
  // useEffect(() => {
  //   if (props?.location?.state?.image) {
  //     setImage(props?.location?.state?.image);
  //   }
  // }, [props?.location?.state?.image]);
  // console.log('props?.location?.state', image);
  return (
    <TableHeaderComponent
      loading={loading}
      card_title={props?.location?.state ? 'Update Equipment' : 'Add Equipment'}
      SearchKeyBack={(e) => {
        console.log(e);
      }}
    >
      <div className="mt-20">
        <div className="basic-form">
          <Formik
            initialValues={{
              name: props?.location?.state?.name || '',
              // image: props?.location?.state?.image || '',
            }}
            validationSchema={EquipmentSchema}
            onSubmit={(values, actions) => handleSubmit(values, actions)}
          >
            {(formik) => {
              return (
                <Form>
                  <div className="row">
                    <div className="form-group col-md-12">
                      <InputBox
                        type="text"
                        id="nf-name"
                        placeholder="Enter Name..."
                        title="Name"
                        name="name"
                      />
                      <div
                        style={{ marginTop: '20px' }}
                        className="form-group col-md-12"
                      >
                        <input
                          type="file"
                          id="imageUpload"
                          name="image"
                          accept=".png, .jpg, .jpeg"
                          // accept="image/*"
                          onChange={(event) => {
                            const file = event.target.files[0];
                            if (file && file.type.substr(0, 5) === 'image') {
                              setImage(file);
                            } else {
                              setImage(null);
                            }
                          }}
                          val
                        />
                      </div>
                      {/* <InputBox
                        type="file"
                        id="nf-name"
                        
                        // placeholder="select image"
                        title="image"
                        name="image"
                      /> */}
                    </div>
                    <div className="col-md-8">&nbsp;</div>
                    <div className="col-md-2">
                      <CancelButton
                        back_path="/equipment"
                        ButtonTitle="Cancel"
                      />
                    </div>
                    <div className="col-md-2">
                      <SubmitButton
                        ButtonName={props?.location?.state ? 'Update' : 'Add'}
                        loading={loading}
                      />
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </TableHeaderComponent>
  );
};

export default EquipmentForm;
