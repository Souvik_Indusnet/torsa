import { Form, Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import {
  CreateEquipmentmodal,
  GetEquipmentAll,
  updateequipmentmodel,
} from '../../../Actions/categoryAction';
import CancelButton from '../../../Components/Button/Cancel';
import SubmitButton from '../../../Components/Button/Submit';
import InputBox from '../../../Components/form/input';
import SelectType from '../../../Components/form/select';
import TableHeaderComponent from '../../../Components/Table/TableTopComponent';
import { EquipmentModalSchema } from '../../../Components/validation/validationSchema';

const EquipmentformModal = (props) => {
  const history = useHistory();
  const [equipment, setEquipment] = useState('');

  useEffect(() => {
    dispatch(GetEquipmentAll()).then((data) => {
      setEquipment(data?.data?.data?.filter((i) => i.active == true));
    });
    document.title = props?.location?.state
      ? 'Update Equipment Model |Torsa'
      : 'Add Equipment Model | Torsa';
  }, []);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    // console.log(e);
    setLoading(true);

    dispatch(
      props?.location?.state
        ? updateequipmentmodel(props?.location?.state._id, e.name, e.equipment)
        : CreateEquipmentmodal(e)
    ).then((data) => {
      setLoading(false);

      history.push('/equipmentmodel');
    });
  };

  return (
    <TableHeaderComponent
      loading={loading}
      card_title={
        props?.location?.state
          ? 'Update Equipment Model'
          : 'Add Equipment Model'
      }
      SearchKeyBack={(e) => {
        console.log(e);
      }}
    >
      <div className="mt-20">
        <div className="basic-form">
          <Formik
            initialValues={{
              name: props?.location?.state?.name || '',
              equipment: props?.location?.state?.category?._id || '',
            }}
            validationSchema={EquipmentModalSchema}
            onSubmit={handleSubmit}
          >
            {(formik) => {
              return (
                <Form>
                  <div className="row">
                    <div className="form-group col-md-6">
                      <SelectType
                        name="equipment"
                        title="Select Equipment"
                        placeholder="Equipment..."
                        values={equipment ? equipment : []}
                        callBack={(e) => console.log(e?.category)}
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <InputBox
                        type="text"
                        id="nf-name"
                        placeholder="Enter Model Name..."
                        title=" Model Name"
                        name="name"
                      />
                    </div>
                    <div className="col-md-8">&nbsp;</div>
                    <div className="col-md-2">
                      <CancelButton
                        back_path="/equipmentmodel"
                        ButtonTitle="Cancel"
                      />
                    </div>
                    <div className="col-md-2">
                      <SubmitButton
                        ButtonName={props?.location?.state ? 'Update' : 'Add'}
                        loading={loading}
                      />
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </TableHeaderComponent>
  );
};

export default EquipmentformModal;
