import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  GetEquipmentmodal,
  UpdateStatusmodal,
} from '../../../Actions/categoryAction';
import ActionButton from '../../../Components/Table/ActionButton';
import PaginationControlled from '../../../Components/Table/AdvancedPagination';
import Pagination from '../../../Components/Table/Pagination';
import TableComponent from '../../../Components/Table/TableComponent';
import TableStatusButton from '../../../Components/Table/TableStatusButton';
import TableHeaderComponent from '../../../Components/Table/TableTopComponent';
import DateFormate from '../../../Components/Utils/DateFormate';
const EquipmentModalTable = () => {
  const dispatch = useDispatch();
  const [getData, setGetData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);

  const getAllData = async ({ pageNumber }) => {
    setLoading(true);
    const Equipments = await dispatch(GetEquipmentmodal({ pageNumber }));
    setGetData(Equipments?.data?.data?.list);
    setLoading(false);
  };
  useEffect(() => {
    getAllData({ pageNumber: 1 });
    document.title = 'Equipment Model| Torsa';
  }, []);

  const Heading = [
    { title: 'Sr. No' },
    { title: 'Equipment Model Name', key: '' },
    { title: 'Equipment  Name', key: '' },
    { title: 'Create On', key: '' },
    { title: 'Status', key: '' },
    { title: 'Action', key: '' },
  ];
  // console.log(getData);
  return (
    <TableHeaderComponent
      loading={loading}
      card_title="Equipment Model"
      add_tittle="Add"
      addpath="/addequipmentmodel"
      // exportButton="true"
      SearchKeyBack={(e) => {
        console.log(e);
      }}
    >
      <div className="card-body">
        <div className="table-responsive">
          <TableComponent TableHeading={Heading}>
            {getData?.results?.map((el, index) => {
              return (
                <tr key={el._id}>
                  <td>
                    {' '}
                    {(getData?.current?.page + 1 || 1 + 1) *
                      getData?.current?.limit +
                      (index + 1) -
                      20}
                  </td>
                  <td>{el.name}</td>
                  <td>{el?.category?.name}</td>
                  <td>
                    <DateFormate formatter={el?.createdAt} />

                    {/* {el.createdAt} */}
                  </td>
                  <td>
                    <TableStatusButton
                      status={el.active}
                      handleBack={(e) =>
                        dispatch(UpdateStatusmodal(el._id, e)).then((data) => {
                          getAllData({ pageNumber: currentPage });
                        })
                      }
                    />
                  </td>
                  <td className="py-2 ">
                    {' '}
                    <ActionButton
                      stateData={el}
                      selectPath="/updateequipmentmodel"
                    />
                  </td>
                </tr>
              );
            })}
          </TableComponent>
        </div>
      </div>
      {/* <Pagination
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      /> */}
      <PaginationControlled
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      />
    </TableHeaderComponent>
  );
};

export default EquipmentModalTable;
