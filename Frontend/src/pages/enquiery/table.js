import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
// import { blockStatusAdmin, GetALlUsers } from '../../Actions/userAction';
// import ActionButton from '../../Components/Table/ActionButton';
import SelectType from '../../Components/form/select';
import { useHistory } from 'react-router-dom';
import Pagination from '../../Components/Table/Pagination';
import TableComponent from '../../Components/Table/TableComponent';
// import TableStatusButton from '../../Components/Table/TableStatusButton';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
// import moment from 'moment';
import InputBox from '../../Components/form/input';
import { getEnquery, updateEnquery } from '../../Actions/enquery';
import SvgButton from '../../Components/Analytics/SvgButton';
import { Form, Formik } from 'formik';

import * as yup from 'yup';
import PaginationControlled from '../../Components/Table/AdvancedPagination';
import DateFormate from '../../Components/Utils/DateFormate';
import { formateDatereturn } from '../../Components/Utils/DayFormateFunc';
import { Enquirycsv } from '../../Actions/CSVAction';
import { useSnackbar } from 'notistack';
const validation = yup.object({
  res_category: yup.string().required('Required *'),
});

const Enquerytable = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [getData, setGetData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [csvdata, setCsvdata] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const getAllData = async ({ pageNumber }) => {
    setLoading(true);
    console.log(pageNumber);
    const Equipments = await dispatch(getEnquery({ pageNumber }));

    setGetData(Equipments?.data?.data?.list);
    // console.log(Equipments?.data?.data?.list)
    setLoading(false);
  };
  useEffect(() => {
    dispatch(Enquirycsv()).then((data) => {
      setCsvdata(data?.data?.data?.list);
    });
    getAllData({ pageNumber: 1 });
    document.title = 'Enquiry  | Torsa';
  }, []);

  const [status, setStatus] = useState({
    isApproved: '',
    id: '',
  });

  const handleStatus = (en_data) => {
    setStatus({ ...status, id: en_data._id });
  };
  const Heading = [
    { title: 'Sr. No' },
    { title: ' User Name', key: '' },
    { title: 'Last Update', key: '' },
    { title: 'Part', key: '' },
    { title: 'Status', key: '' },
    { title: 'Action', key: '' },
  ];

  const handleSubmit = (e) => {
    const validData = {
      isApproved: e.res_category,
      id: status.id,
    };
    if (e.res_category == 'rejected') {
      validData.reason = e.reason;
    }
    // e.preventDefault();
    // console.log(getData)
    // if(getData.getAll.filter(p=>{return p._id==validData.id})[0].Partial_Date!=null||validData.isApproved=='rejected'){
    dispatch(updateEnquery(validData)).then((data) => {
      getAllData({ pageNumber: 1 });
      setStatus({ id: '', isApproved: '' });
    });
    // }else{
    //   enqueueSnackbar(`Please update the Enquiry Details first`, {
    //     variant: "warning",
    //     anchorOrigin: {
    //       vertical: "bottom",
    //       horizontal: "right",
    //     },
    //   });
    // }

    document.getElementById('clickMe').click();
  };

  const handleEdit = (data) => {
    console.log('91', data);
    history.push({
      pathname: '/enquiryform',
      state: data,
    });
  };

  const Options = [
    { name: 'Pending', _id: 'pending', value: 'pending' },
    { name: 'Approved', _id: 'approved', value: 'approved' },
    { name: 'Reject', _id: 'rejected', value: 'rejected' },
  ];

  const csvheader = [
    { label: 'Sr', key: 'sr' },
    {
      label: 'User Name ',
      key: 'name',
    },
    {
      label: 'Last Update ',
      key: 'updatedAt',
    },
    {
      label: 'Part  ',
      key: 'part',
    },

    {
      label: 'Status',
      key: 'active',
    },
  ];
  const csv_Data = csvdata?.map((i, index) => {
    return {
      sr: index + 1,
      name: i?.user?.name,
      part: i?.part?.partName,
      updatedAt: formateDatereturn(i.updatedAt),
      active: i.isApproved,
    };
  });

  return (
    <>
      <TableHeaderComponent
        loading={loading}
        card_title="Enquiry Management"
        // add_tittle="Add  User"
        // addpath="/adduser"
        // exportButton="false"
        csvData={csv_Data}
        headers={csvheader}
        SearchKeyBack={(e) => {
          console.log(e);
        }}
        csvfilename="Enquiry-csv.csv"
      >
        <div className="card-body">
          <div className="table-responsive">
            <TableComponent TableHeading={Heading}>
              {getData?.getAll?.map((i, index) => {
                return (
                  <tr key={i._id}>
                    <td>
                      {' '}
                      {(getData?.current?.page + 1 || 1 + 1) *
                        getData?.current?.limit +
                        (index + 1) -
                        20}
                    </td>
                    <td>{i?.user?.name}</td>
                    <td>
                      {/* {moment(i?.updatedAt).format('lll')} */}
                      <DateFormate formatter={i.updatedAt} />
                    </td>
                    <td>
                      {i?.part?.partName}
                      {i?.part?.inventoryStock < 0 ? (
                        <img
                          style={{ width: '40px', height: '40px' }}
                          src="images/out-of-stock.png"
                        />
                      ) : (
                        <span className="item">
                          &nbsp;&nbsp;
                          <i className="fa fa-check-circle text-success"></i>
                        </span>
                      )}
                    </td>
                    <th>
                      {i.isApproved != 'approved' ? (
                        <span
                          onClick={() => handleStatus(i)}
                          className="badge light badge-warning btn btn-primary"
                          type="button"
                          data-toggle="modal"
                          data-target="#exampleModal"
                          style={
                            i.isApproved == 'rejected'
                              ? { background: '#ff2625', color: 'white' }
                              : null
                          }
                        >
                          {i.isApproved}
                        </span>
                      ) : (
                        <span
                          onClick={() => handleStatus(i)}
                          className="badge light badge-warning btn btn-primary"
                          type="button"
                        >
                          {i.isApproved}
                        </span>
                      )}
                    </th>
                    {i.isApproved == 'approved' ? (
                      <td className="py2">
                        <div className="dropdown text-sans-serif">
                          <SvgButton />
                          <div
                            className="dropdown-menu dropdown-menu-right border py-0"
                            aria-labelledby="order-dropdown-0"
                          >
                            <div className="py-2">
                              <button
                                onClick={() => handleEdit(i)}
                                className=" dropdown-item "
                              >
                                Share Enquiry to User{' '}
                              </button>
                            </div>
                          </div>
                        </div>
                      </td>
                    ) : null}
                  </tr>
                );
              })}
            </TableComponent>
          </div>
        </div>
        <PaginationControlled
          pageInfo={getData}
          pageChange={(e) => {
            setCurrentPage(e);
            getAllData({ pageNumber: e });
          }}
        />
      </TableHeaderComponent>

      <div
        className="modal fade"
        id="exampleModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Change Status
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <Formik
              initialValues={{
                res_category: '',
                reason: '',
              }}
              validationSchema={validation}
              onSubmit={handleSubmit}
            >
              {(formik) => {
                return (
                  <Form>
                    <div className="modal-body">
                      <SelectType
                        name="res_category"
                        values={Options}
                        title="Select Enquiry Status"
                        placeholder="options..."
                        // selectedValue={props?.location?.state?.res_category}
                        callBack={(e) =>
                          setStatus({ ...status, isApproved: e.category })
                        }
                      />
                      {formik.values.res_category == 'rejected' ? (
                        <InputBox
                          type="text"
                          name="reason"
                          title="Reason"
                          placeholder="enter the reason"
                          // readonly="true"
                        />
                      ) : null}
                    </div>
                    <div className="modal-footer">
                      <button
                        type="button"
                        className="btn btn-secondary"
                        data-dismiss="modal"
                        id="clickMe"
                      >
                        Close
                      </button>
                      <button
                        // disabled={status.isApproved == ''}
                        // style={{
                        //   cursor:
                        //     status.isApproved == '' ? 'not-allowed' : 'pointer',
                        // }}
                        type="submit"
                        className="btn btn-primary"
                        // data-dismiss="modal"
                      >
                        Save changes
                      </button>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    </>
  );
};

export default Enquerytable;
