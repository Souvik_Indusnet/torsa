import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import InputBox from '../../Components/form/input';
import { makeStyles } from '@material-ui/core/styles';

import * as React from 'react';
import DatePicker from '@mui/lab/DatePicker';
import { Form, Formik } from 'formik';
import * as yup from 'yup';

import SelectType from '../../Components/form/select';
import { updateEnquery } from '../../Actions/enquery';
import CancelButton from '../../Components/Button/Cancel';
// import DateTimePicker from 'react-datetime-picker';
const validation = yup.object({
  mop: yup.string().required('Mode of payment is required'),
  top: yup.string().required('Type of payment is required'),
  select_date: yup.date().required('Date is Required'),
  partial_percetage: yup
    .number()

    .when('top', {
      is: 'Partial',
      then: yup
        .number()
        .required('Please enter an Percentage')
        .integer('Please enter a valid Percentage without decimal values')
        .typeError('Please enter a valid Percentage')
        .positive('Please enter a valid Percentage')
        // .required('Please enter an Percentage')
        .min(1, 'Percentage must be greater than or equal to 1')
        .max(99, 'Percentage must be less than or equal to 99'),
    }),
});

const paymentmode = [
  { name: 'Cash', value: 'Cash', _id: 'Cash' },
  { name: 'Online', value: 'Online', _id: 'Online' },
];
const paymenttype = [
  { name: 'Partial', value: 'Partial', _id: 'Partial' },
  { name: 'Full', value: 'Full', _id: 'Full' },
];
const Enquery_Form = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const useStyles = makeStyles((theme) => ({
      picker: {
        outerWidth: "100px",
      },
  }));
  const styles=useStyles();
  var partial_date=props?.location?.state?.Partial_Date?props?.location?.state?.Partial_Date:null;
  const [value, setValue] = useState(()=>{
  if(partial_date!=null){
   var temp=partial_date.split('-');
   console.log(partial_date);
   return new Date(partial_date)
  //  .setFullYear(temp[0]).setMonth(temp[1]).setDate(temp[2]);
  }else{
   return null
  }
  });
// console.log(props?.location?.Partial_Date)
  const handleChange = (newValue) => {
    setValue(newValue);
  };

  const [state, setState] = useState({
    mop: '',
    top: '',
  });

  useEffect(() => {
    console.log('53',props?.location?.state?.Partial_Remaining_Amount,value);
    document.title =
      props?.location?.pathname == '/Payment-enquiry'
        ? 'Update Payment Enquiry | Torsa'
        : 'Update Enquiry | Torsa';
  }, []);
  // console.log(props);

  Date.prototype.yyyymmdd = function () {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [
      this.getFullYear(),
      (mm > 9 ? '' : '0') + mm,
      (dd > 9 ? '' : '0') + dd,
    ].join('-');
  };
  var date = new Date();
  const [loading, setLoading] = useState(false);
  const submitHandler = (e) => {
    var month=new Date(value).getMonth()+1
    var date=new Date(value).getFullYear().toString()+'-'+month.toString()+'-'+new Date(value).getDate().toString();
    setLoading(true);
    
    const stateData = {
      id: props?.location?.state._id,
      Mode_Of_Payment: e.mop,
      Type_Of_Payment: e.top,
      Partial_Remaining_Amount:e.Amount,
      Partial_Amount_Paid_in_percentage:
        e.top != 'Full' ? e.partial_percetage : '0',
      Partial_Remaining_Amount_in_percentage: 100 - parseInt(e.partial_percetage), 
      Partial_Date: e.select_date,
      enquiry_update:true,
    };
    stateData.Partial_Date=date;
    // console.log(stateData);
    dispatch(updateEnquery(stateData)).then((data) => {
        setLoading(false);
        history.push(
            props?.location?.pathname == '/Payment-enquiry'
          ? '/paymentmanagement'
          : '/enquiry'
      );
    });
  };
  // console.log(props);
  return (
    <>
      <div className="col-xl-12 col-xxl-12">
        <div className="card">
          <div className="card-header">
            <h4 className="card-title">
              {props?.location?.pathname == '/Payment-enquiry'
                ? 'Update Payment Enquiry'
                : 'Update Enquiry'}{' '}
            </h4>
          </div>
          <div className="card-body">
            <div className="basic-form">
              {/* <div className="form-row"> */}
              <Formik
                initialValues={{
                  User_name: props?.location?.state?.user?.name || '',
                  part_Name: props?.location?.state?.part?.partName || '',
                  Amount:props?.location?.state?.Partial_Remaining_Amount || '',
                  select_date: props?.location?.state?.Partial_Date || '',
                  partial_percetage:
                    props?.location?.state?.Partial_Amount_Paid_in_percentage ||
                    '',
                  top: props?.location?.state?.Type_Of_Payment || '',
                  mop: props?.location?.state?.Mode_Of_Payment || '',
                }}
                validationSchema={validation}
                onSubmit={submitHandler}
              >
                {(formik) => {
                  //  console.log(formik);
                  return (
                    <Form>
                      <div className="form-row">
                        <div className="form-group col-6">
                          <InputBox
                            type="text"
                            name="User_name"
                            title="User Name"
                            placeholder="enter the name"
                            readonly="true"
                          />
                        </div>
                        <div className="form-group col-6">
                          <InputBox
                            type="text"
                            name="part_Name"
                            title="Part Name"
                            placeholder="enter the name"
                            readonly="true"
                          />
                        </div>
                      </div>
                      <div className="form-row">
                        <div className="form-group col-6">
                          <InputBox
                            type="text"
                            name="Amount"
                            title="Price"
                            placeholder="enter the price"
                            // readonly="true"
                          />
                        </div>
                        <div className="form-group col-6">
                          <SelectType
                            name="top"
                            title="Type Of Payment"
                            placeholder="options..."
                            values={paymenttype}
                            selectedValue={
                              props?.location?.state?.Type_Of_Payment
                            }
                            callBack={(e) => {
                              // console.log(e, e.category);
                              setState({ ...state, top: e.category });
                            }}
                          />
                        </div>
                      </div>
                      <div className="form-row">
                        {formik.values.top != 'Full' && (
                          <div className="form-group col-12">
                            <InputBox
                              // readonly={formik.values.top == 'Full'}
                              type="text"
                              name="partial_percetage"
                              title="Partial Percetage"
                              placeholder="Partial Percetage"
                            />
                          </div>
                        )}

                        <div className="form-group col-12">
                          {/* <InputBox
                            type="date"
                            name="select_date"
                            title="Select Date"
                            placeholder="Select Date"
                            min={
                              props?.location?.state?.valid_till
                                ? `${props?.location?.state?.valid_till}`
                                : `${date.yyyymmdd()}`
                            }
                          /> */}
                          <LocalizationProvider className={styles.picker} dateAdapter={AdapterDateFns}>
                            <DatePicker
                              name="select_date"
                              // label="Select Date"
                              value={value}
                              onChange={(newValue) => {
                                formik.setFieldValue("select_date", newValue);
                                setValue(newValue);
                              }}
                              renderInput={(params) => <TextField {...params} />}
                            />
                          </LocalizationProvider>

                          {/* <LocalizationProvider dateAdapter={AdapterDateFns}>
                          <DesktopDatePicker
                            // width={}
                            label="Select Date"
                            inputFormat="MM/dd/yyyy"
                            name="select_date"
                            value={value}
                            onChange={handleChange}
                            renderInput={(params) => <TextField {...params} />}
                          />
                          </LocalizationProvider> */}
                           {/* <MobileDatePicker
                            label="Select Date"
                            inputFormat="MM/dd/yyyy"
                            value={value}
                            onChange={handleChange}
                            renderInput={(params) => <TextField {...params} />}
                          /> */}
                          {/* <DateTimePicker
          label="Date&Time picker"
          value={value}
          onChange={handleChange}
          renderInput={(params) => <TextField {...params} />}
        /> */}
                        </div>
                        <div className="col-md-2">
                          <CancelButton
                            back_path={
                              props?.location?.pathname == '/enquiryform'
                                ? '/enquiry'
                                : '/paymentmanagement'
                            }
                            ButtonTitle="Cancel"
                          />
                        </div>
                        <div className="col-md-2">
                          <button
                            // disabled={!formik?.isValid}
                            type="submit"
                            className="btn btn-primary"
                          >
                            {loading
                              ? 'Loading...'
                              : props?.location?.pathname == '/Payment-enquiry'
                              ? 'Save'
                              : 'Send to User'}
                          </button>
                        </div>
                      </div>{' '}
                    </Form>
                  );
                }}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Enquery_Form;
