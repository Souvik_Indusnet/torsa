import React from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { userRegister } from "../../Actions/userAction";
const Register = () => {
  const dispatch = useDispatch();
  const [state, setState] = React.useState({
    username: "",
    email: "",
    password: "",
  });
  const userformHandler = (e) => {
    e.preventDefault();
    dispatch(userRegister(state));
  };

  const formHandler = (e) => {
    setState({ ...state, [e.target.name]: e.target.value });
  };
  return (
    <div className="h-100">
      <div className="authincation h-100">
        <div className="container h-100">
          <div className="row justify-content-center h-100 align-items-center">
            <div className="col-md-6">
              <div className="authincation-content">
                <div className="row no-gutters">
                  <div className="col-xl-12">
                    <div className="auth-form">
                      <h4 className="text-center mb-4">Sign up your account</h4>
                      <form noValidate onSubmit={userformHandler}>
                        <div className="form-group">
                          <label className="mb-1">
                            <strong>Username</strong>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="username"
                            name="username"
                            value={state.username}
                            onChange={formHandler}
                            required="true"
                          />
                        </div>
                        <div className="form-group">
                          <label className="mb-1">
                            <strong>Email</strong>
                          </label>
                          <input
                            type="email"
                            className="form-control"
                            placeholder="hello@example.com"
                            name="email"
                            value={state.email}
                            onChange={formHandler}
                            required="true"
                          />
                        </div>
                        <div className="form-group">
                          <label className="mb-1">
                            <strong>Password</strong>
                          </label>
                          <input
                            type="password"
                            className="form-control"
                            value={state.password}
                            name="password"
                            onChange={formHandler}
                            required="true"
                          />
                        </div>
                        <div className="text-center mt-4">
                          <button
                            type="submit"
                            className="btn btn-primary btn-block"
                          >
                            Sign me up
                          </button>
                        </div>
                      </form>
                      <div className="new-account mt-3">
                        <p>
                          Already have an account?{" "}
                          <Link className="text-primary" to="/login">
                            Sign in
                          </Link>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
