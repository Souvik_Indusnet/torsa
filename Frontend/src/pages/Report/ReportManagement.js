import React, { useEffect, memo } from 'react';

import SalesPieChart from '../../Components/Report/Report';
import Example from'../../Components/Report/Rechart';
const ReportManagement = memo(() => {
  useEffect(() => {
    document.title = 'Report Management | Torsa';
  }, []);

  return <div>
    <div className='card'>
      <div className='card-body'>
        <div className='p-3'>
          <h4 class="card-title">Sales per Business Admin</h4>
          <SalesPieChart />
        </div>
      </div>
    </div>
  {/* <br/> */}
    <div className='card'>
      <div className='card-body'>
        <div className='p-3 overflow-scroll-hidey'>
          <h4 class="card-title">Sales Overview</h4>
          <Example />
      </div>
    </div>
  </div>
</div>  
});

export default ReportManagement;