import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { blockStatusAdmin, GetALlUsers } from '../../Actions/userAction';
import ActionButton from '../../Components/Table/ActionButton';
import TableComponent from '../../Components/Table/TableComponent';
import TableStatusButton from '../../Components/Table/TableStatusButton';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
import PaginationControlled from '../../Components/Table/AdvancedPagination';
import { useSelector } from 'react-redux';
import DateFormate from '../../Components/Utils/DateFormate';
import { UserCSV } from '../../Actions/CSVAction';
import { formateDatereturn } from '../../Components/Utils/DayFormateFunc';
import { useHistory } from 'react-router-dom';

const CustomerTable = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [getData, setGetData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [csvdata, setCsvdata] = useState([]);
  const urlParams = new URLSearchParams(window.location.search).get('search');
  const getAllData = async ({ pageNumber, Keyword, modify }) => {
    if (modify) {
      console.log(Keyword, modify);
      localStorage.setItem('search', Keyword);
    }
    setLoading(true);
    const Equipments = await dispatch(
      GetALlUsers({ pageNumber, Keyword: Keyword != undefined ? Keyword : '' })
    );
    setGetData(Equipments?.data?.data?.list);
    setLoading(false);
  };

  useEffect(() => {
    dispatch(UserCSV()).then((data) => {
      console.log(data?.data?.data?.list);
      setCsvdata(data?.data?.data?.list);
    });
  }, [loading]);

  useEffect(() => {
    // console.log(urlParams);
    // console.log(localStorage.getItem('search'));
    history.replace({
      pathname: window.location.pathname,
      search: `?search=${urlParams}`,
    });
    if (urlParams != '') {
      getAllData({ pageNumber: 1, Keyword: urlParams, modify: false });
    } else {
      getAllData({ pageNumber: 1, modify: false });
    }
    // setTimeout(()=>{
    // console.log(urlParams);
    //   getAllData({ pageNumber: 1});
    // },[8000])
    document.title = 'User Management | Torsa';
  }, [urlParams]);
  const { userInfo } = useSelector((state) => state.userSigninInfo);
  // console.log(userInfo)
  const csvheader = [
    { label: 'Sr', key: 'sr' },
    { label: 'Name', key: 'name' },
    {
      label: 'Email',
      key: 'email',
    },
    {
      label: 'Contact number',
      key: 'contact',
    },
    {
      label: 'Business Admin',
      key: 'ba',
    },
    {
      label: 'Equipment',
      key: 'eq',
    },
    {
      label: 'Equipment modal',
      key: 'modal',
    },
    {
      label: 'Created on',
      key: 'createdAt',
    },
    {
      label: 'Status',
      key: 'active',
    },
  ];

  const csv_Data = csvdata?.map((i, index) => {
    return {
      sr: index + 1,
      email: i?.email,
      contact: i?.contactNo,
      name: i?.name,
      eq: i?.equipments?.equipment?.map((p) => {
        return `${p?.name} `;
      }),
      modal: i?.EquipmentModal?.equipmentModal?.map((j) => {
        return `${j?.name}  `;
      }),
      ba: i?.buisnessManager?.name,
      createdAt: formateDatereturn(i.createdAt),
      active: i.active == false ? 'false' : i.active,
    };
  });
  const pageAccessedByReload =
    (window.performance.navigation &&
      window.performance.navigation.type === 1) ||
    window.performance
      .getEntriesByType('navigation')
      .map((nav) => nav.type)
      .includes('reload');

  // console.log(getData);
  const Heading = [
    { title: 'Sr. No' },
    { title: ' Name', key: '' },
    { title: 'Email', key: '' },
    { title: 'Business Admin', key: '' },
    { title: 'Create On', key: '' },
    { title: 'Status', key: '' },
    { title: 'Action', key: '' },
  ];
  // console.log('getData', getData);
  return (
    <TableHeaderComponent
      loading={loading}
      card_title="User Management"
      add_tittle="Add"
      addpath={userInfo.isSystemAdmin ? '/adduser' : false}
      searchButton={true}
      search={urlParams}
      SearchKeyBack={(e) => {
        var modify;
        if (pageAccessedByReload) {
          modify = false;
        } else {
          modify = true;
        }
        console.log('.............', pageAccessedByReload, modify);
        history.replace({
          pathname: window.location.pathname,
          search: `?search=${e}`,
        });
        getAllData({ pageNumber: 1, Keyword: e, modify: modify });
      }}
      exportButton="true"
      csvData={csv_Data}
      headers={csvheader}
      csvfilename="user-csv.csv"
    >
      <div className="card-body">
        <div className="table-responsive">
          <TableComponent TableHeading={Heading}>
            {getData?.results?.map((el, index) => {
              return (
                <tr key={el._id}>
                  <td>
                    {(getData?.current?.page + 1 || 1 + 1) *
                      getData?.current?.limit +
                      (index + 1) -
                      20}
                  </td>
                  <td>{el?.name}</td>
                  <td>{el?.email}</td>
                  <td>{el?.buisnessManager?.name}</td>
                  <td>
                    <DateFormate formatter={el.createdAt} />
                  </td>
                  <td>
                    <TableStatusButton
                      status={el.active}
                      handleBack={(e) =>
                        dispatch(blockStatusAdmin(el._id, e)).then((data) => {
                          getAllData({
                            pageNumber: currentPage,
                            modify: false,
                          });
                        })
                      }
                    />
                  </td>
                  <td className="py-2 ">
                    {' '}
                    <ActionButton
                      stateData={el}
                      selectPath={
                        userInfo.isSystemAdmin ? '/updateuser' : false
                      }
                      viewPath={userInfo.isSystemAdmin ? '/viewuser' : '/view'}
                    />
                  </td>
                </tr>
              );
            })}
          </TableComponent>
        </div>
      </div>
      <PaginationControlled
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e, modify: false });
        }}
      />
    </TableHeaderComponent>
  );
};

export default CustomerTable;
