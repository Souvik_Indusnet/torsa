import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getUserDetailsByID } from '../../Actions/userAction';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
import { apiService } from '../../service/apiservice';

const UserViewPage = (props) => {
  const [loading, setLoading] = useState(false);

  const [userState, setUserState] = useState('');
  // console.log(userState);

  const dispatch = useDispatch();
  useEffect(() => {
    document.title = 'View User |Torsa';
    setLoading(true);
    dispatch(getUserDetailsByID(props?.location?.state?._id)).then((data) => {
      setUserState(data?.data?.data?.list);
      setLoading(false);
    });
  }, [dispatch, props?.location?.state?._id]);

  return (
    <TableHeaderComponent
      loading={loading}
      arrowIcon={true}
      card_title={loading ? 'Loading...' : `${userState?.name} Details`}
      backButtonPath="/user-management"
      SearchKeyBack={(e) => {
        console.log(e);
      }}
    >
      <div className="row">
        {/* <div className="card-body col-xl-4">
          <img
            src={`${apiService.userimage}/${props?.location?.state?.image}`}
            alt="user_image"
          />
        </div> */}
        <div className="card-body">
          <div
            // style={{
            //   display: 'flex',
            //   alignContent: 'center',
            //   justifyContent: 'space-around',
            // }}
          >
            <div>
              <h5 className='view-user-prof-row-color'>
                <svg
                  viewBox="0 0 24 24"
                  width="24"
                  height="24"
                  stroke="currentColor"
                  stroke-width="2"
                  fill="none"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  class="css-i6dzq1 svg-profile"
                >
                  <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                  <circle cx="12" cy="7" r="4"></circle>
                </svg>{' '}
                {userState?.name}
              </h5>
              <h5 className='view-user-prof-row'>
                <svg
                  viewBox="0 0 24 24"
                  width="24"
                  height="24"
                  stroke="currentColor"
                  stroke-width="2"
                  fill="none"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  class="css-i6dzq1 svg-profile"
                >
                  <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                  <polyline points="22,6 12,13 2,6"></polyline>
                </svg>{' '}
                {userState?.email}
              </h5>
              <h5 className='view-user-prof-row-color'>
                <svg
                  viewBox="0 0 24 24"
                  width="24"
                  height="24"
                  stroke="currentColor"
                  stroke-width="2"
                  fill="none"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  class="css-i6dzq1 svg-profile"
                >
                  <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                </svg>{' '}
                {userState?.phone}
              </h5>
              <h5 className='view-user-prof-row'>
                <svg
                  viewBox="0 0 24 24"
                  width="24"
                  height="24"
                  stroke="currentColor"
                  stroke-width="2"
                  fill="none"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  class="css-i6dzq1 svg-profile"
                >
                  <rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect>
                  <path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path>
                </svg>{' '}
                {userState?.businessadmin?.name}
              </h5>
              <h5 className='view-user-prof-row-color'>
              <i class="fa-regular fa-screwdriver-wrench svg-profile"></i>
              Equipments:
                {userState?.equipments?.equipment?.map((i) => {
                  {
                    return <span className="parts-name">{i.name}</span>;
                  }
                })}{' '}
                
              </h5>
              <h5 className='view-user-prof-row'>
              <i class="fa-regular fa-screwdriver-wrench svg-profile"></i>
              Models:
                {userState?.EquipmentModal?.equipmentModal?.map((i) => {
                  {
                    return <span className="parts-name">{i.name}</span>;
                  }
                })}{' '}
              </h5>
            </div>
          </div>
        </div>
      </div>
    </TableHeaderComponent>
  );
};

export default UserViewPage;
