import { Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { GetEquipmentAll } from "../../Actions/categoryAction";
import { machineModelArray } from "../../Actions/productAction";
import {
  AddnewUser,
  AdminList,
  UpdateuserByid,
} from "../../Actions/userAction";
import CancelButton from "../../Components/Button/Cancel";
import SubmitButton from "../../Components/Button/Submit";
import InputBox from "../../Components/form/input";
import Multiselect from "../../Components/form/multiselect";
import SelectType from "../../Components/form/select";
import TableHeaderComponent from "../../Components/Table/TableTopComponent";
import { customerSchema } from "../../Components/validation/validationSchema";
import { useSnackbar } from 'notistack';

const AddUserForm = (props) => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const defaultSet = props?.location?.state;
  const dispatch = useDispatch();
  // console.log('props', props);
  const history = useHistory();
  const [equipment, setEquipment] = useState("");
  const [B_admins, setB_admins] = useState([]);
  const [loading, setLoading] = useState(false);

  const [state, setState] = useState({
    equipment: [],
    equipmentModal: [],
  });

  const [modal_list, setModal_list] = useState([]);
  // useEffect(() => {
  //   // console.log(modalList,modalListDefault,defaultOptionsEquipment)
  //   // console.log(state, equipment, modal_list,modalList,modalListDefault);
  //   // for (var i = 0; i < modalList.length; i++) {
  //   //   for (var j = 0; j < state.equipmentModal.length; j++) {
  //   //     // console.log(state.equipmentModal[j],modal_list[i]._id,state.equipmentModal[j] == modal_list[i]._id);
  //   //     if (state.equipmentModal[j] == modal_list[i]._id) {
  //   //       var index = state.equipment.indexOf(modal_list[i].category);
  //   //       // console.log('42',index);
  //   //       if (index == -1) {
  //   //         // console.log(modal_list[i]);
  //   //       var temp=state;
  //   //       temp.equipmentModal.splice(j, 1)
  //   //       // console.log(j,temp);
  //   //         setState((prevState) => {
  //   //           return {
  //   //             ...prevState,
  //   //             [prevState.equipmentModal]: temp.equipmentModal,
  //   //           };
  //   //         });
  //   //       }
  //   //     }
  //   //   }
  //   // }
  //   // for(var i=0;i<modalList.length;i++){
  //   //   var index =state.equipment.indexOf(p=>p==modalList.value);
  //   //   console.log(index);
  //   //   if(index==-1){
  //   //       modalList.splice(i,1);
  //   //   }
  //   // }
  //   console.log(state,modalList,modal_list);
  // }, [state, equipment, modal_list]);

  useEffect(() => {
    setLoading(true);
    // setModal_list([]);
    // setState({ ...state, equipmentModal: [] });
    dispatch(machineModelArray(state.equipment)).then((data) => {
      setModal_list(data);
      setLoading(false);
    });
  }, [state?.equipment?.length]);

  useEffect(() => {
    setLoading(true);
    dispatch(AdminList()).then((data) => {
      setB_admins(data);
    });

    dispatch(GetEquipmentAll()).then((data) => {
      setEquipment(data?.data?.data?.filter((i) => i.active == true));
      setLoading(false);
    });
    document.title = props?.location?.state
      ? "Update User |Torsa"
      : "Add User | Torsa";
  }, [props?.location?.state?._id]);
const handleSubmit = (e) => {
  if(state.equipment.length==0&&state.equipmentModal.length==0){
    enqueueSnackbar(`Please select at least One Equipment and One Equipment Model`, {
      variant: 'error',
      anchorOrigin: {
        vertical: 'bottom',
        horizontal: 'right',
      }
    });
  setTimeout(()=>{
    closeSnackbar();
  },[3000])
  }else if(state.equipment.length==0){
      enqueueSnackbar(`Please select at least One Equipment`, {
        variant: 'error',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'right',
        }
      });
  setTimeout(()=>{
    closeSnackbar();
  },[3000])
  }else if(state.equipmentModal.length==0){
      enqueueSnackbar(`Please select at least One Equipment Model`, {
        variant: 'error',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'right',
        }
      });
  setTimeout(()=>{
    closeSnackbar();
  },[3000])
  }else{
      setLoading(true);
      const validData = {
        _id: props?.location?.state?._id || "",
        name: e.name,
        email: e.email.toLowerCase(),
        contactNo: e.phone,
        buisnessManager: e.businesadmin,
        equipment:
          state.equipment.length > 0
            ? state.equipment
            : defaultSet?.equipments?.equipment?.map((i) => i._id),
        equipmentModal:
          state.equipmentModal.length > 0
            ? state.equipmentModal
            : defaultSet?.EquipmentModal?.equipmentModal?.map((i) => i._id),
      };
      console.log(validData);
  
      dispatch(
        props?.location?.state ? UpdateuserByid(validData) : AddnewUser(validData)
      ).then((data) => {
        setLoading(false);
  
        history.push("/user-management");
      });
  }    
};

  const options =
    equipment &&
    equipment
      .filter((i) => i.active == true)
      .map((el) => {
        return { value: el._id, label: el.name };
      });

  const defaultOptionsEquipment =
    props?.location?.state?.equipments?.equipment?.map((i) => {
      return { label: i.name, value: i._id };
    });

  var modalList =
    modal_list &&
    modal_list
      .filter((i) => i.active == true)
      .map((el) => {
        return {
          value: el._id,
          label: el.name,
        };
      });

  var modalListDefault =
    props?.location?.state?.EquipmentModal?.equipmentModal?.map((el) => {
      return { value: el._id, label: el.name };
    });

  return (
    <TableHeaderComponent
      loading={loading}
      card_title={props?.location?.state ? "Update User" : "Add User"}
      SearchKeyBack={(e) => {
        console.log(e);
      }}
    >
      <div className="mt-20">
        <div className="basic-form" _id="22">
          <Formik
            initialValues={{
              name: props?.location?.state?.name || "",
              email: props?.location?.state?.email || "",
              phone: props?.location?.state?.contactNo || "",
              businesadmin: props?.location?.state?.buisnessManager?._id || "",
            }}
            validationSchema={customerSchema}
            onSubmit={handleSubmit}
          >
            {(formik) => {
              console.log(formik);
              return (
                <Form>
                  <div className="row">
                    <div className="form-group col-md-6">
                      <InputBox
                        type="text"
                        id="nf-name"
                        placeholder="Enter  Name..."
                        title="  Name"
                        name="name"
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <InputBox
                        type="email"
                        id="nf-name"
                        placeholder="Enter  Email..."
                        title="  Email"
                        name="email"
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <InputBox
                        type="text"
                        id="nf-name"
                        placeholder="Enter Contact ..."
                        title=" Contact"
                        name="phone"
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <SelectType
                        name="businesadmin"
                        title=" Business Admin"
                        placeholder="Select Business Admin..."
                        values={
                          B_admins
                            ? B_admins.filter((i) => i.active == true)
                                .filter((i) => i.isDeleted == false)
                                .filter((i) => i.isSystemAdmin == false)
                            : []
                        }
                        callBack={(e) => console.log(e?.category)}
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <Multiselect
                        title="Equipment"
                        placeholder="Select Equipment "
                        defaultOption={defaultOptionsEquipment}
                        Options={options}
                        callback={(e) =>
                          setState({ ...state, equipment: e.value })
                        }
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <Multiselect
                        title=" Equipment Modal"
                        placeholder="Select Equipment Modal"
                        Options={modalList}
                        defaultOption={modalListDefault}
                        callback={(e) =>
                          setState({ ...state, equipmentModal: e.value })
                        }
                      />
                    </div>
                    <div className="col-md-8">&nbsp;</div>

                    <div className="col-md-2">
                      <CancelButton
                        back_path="/user-management"
                        ButtonTitle="Cancel"
                      />
                    </div>
                    <div className="col-md-2">
                      <SubmitButton
                        ButtonName={props?.location?.state ? "Update" : "Add"}
                        loading={loading}
                      />
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </TableHeaderComponent>
  );
};

export default AddUserForm;
