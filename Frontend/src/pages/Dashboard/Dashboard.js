import React, { useEffect, memo } from 'react';

import Containbody from '../../Components/Dashboard/Containbody';

const Dashboard = memo(() => {
  useEffect(() => {
    // console.log('7')
    document.title = 'Dashboard | Torsa';
  }, []);

  return <Containbody />;
});

export default Dashboard;
