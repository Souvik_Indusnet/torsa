// import { useEffect, useState } from 'react';
// import { useDispatch } from 'react-redux';
// import { getAllOrder } from '../../Actions/orderAction';
// import { useHistory } from 'react-router-dom';
// import moment from 'moment';
// import 'datatables.net-dt/js/dataTables.dataTables';
// import 'datatables.net-dt/css/jquery.dataTables.min.css';
// import SimpleBackdrop from '../../Components/backdrop/Backdrop';
// import Head from '../../Components/order/head';
// import Delevered from '../../Components/order/delevered';
// var $ = require('jquery');
// import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';

// const Card = () => {
//   const history = useHistory();
//   const dispatch = useDispatch();
//   const [loading, setLoading] = useState(false);
//   const [order, setOrder] = useState([]);

//   // console.log(order);
//   const loaduser = () => {
//     setLoading(true);
//     dispatch(getAllOrder())
//       .then((data) => {
//         setOrder(data);
//         {
//           /* </div> */
//         }
//         setLoading(false);
//       })
//       .catch((err) => {
//         setLoading(false);
//       });
//   };

//   useEffect(() => {
//     setLoading(true);
//     loaduser();

//     if (order) {
//       setTimeout(() => {
//         $('#datatable').DataTable({
//           // dom: "Bfrtip",
//           // buttons: ["csv", "excel", "pdf", "print"],
//         });
//         setLoading(false);
//       }, 2000);
//     }
//   }, [getAllOrder]);
//   // console.log(order);
//   const handleView = (item) => {
//     history.push({
//       pathname: `/order-view-management/${item._id}`,
//       state: { id: item._id },
//     });
//   };

//   return (
//     <>
//       {loading && <SimpleBackdrop open="true" />}
//       <div className="col-12  ">
//         {order && (
//           <table
//             id="datatable"
//             className="table table-sm table-hover "
//             style={{ minWidth: '845px' }}
//           >
//             {order && (
//               <thead className="thead-primary">
//                 <Head />
//               </thead>
//             )}
//             <tbody id="orders ">
//               {order &&
//                 order.map((item, i) => (
//                   <tr key={item._id} className="btn-reveal-trigger">
//                     <td className="py-2">
//                       xxxx-xxxxx-
//                       {item.orderNumber && item.orderNumber.slice(12)}
//                     </td>
//                     <td className="py-2">
//                       {moment(item.createdAt).format('LT')}
//                     </td>
//                     <td className="py-2">
//                       {moment(item.createdAt).format('MMM Do YY')}
//                     </td>
//                     <td>{item.user && item.user.name}</td>
//                     <td className="py-2">
//                       {item.shippingAddress && item.shippingAddress.Address},
//                       {item.shippingAddress && item.shippingAddress.City}
//                     </td>

//                     <td className="py-2">
//                       {item.orderItems &&
//                         item.orderItems.map((i, index) => (
//                           <p>
//                             {index + 1}) {i.product && i.product.partName}
//                           </p>
//                         ))}
//                     </td>

//                     <td className="py2">
//                       {item.orderItems.reduce(
//                         (a, c) =>
//                           a + c && c.product && c.product.amount * c.qty,
//                         0
//                       )}
//                     </td>
//                     <td className="py-2 text-right">
//                       <span
//                         className={
//                           item.isPaid
//                             ? 'badge badge-success'
//                             : 'badge badge-warning'
//                         }
//                       >
//                         {item.isPaid ? 'Paid' : 'Unpaid'}
//                         {item.isPaid && (
//                           <span className="ml-1 fa fa-check"></span>
//                         )}
//                       </span>
//                     </td>
//                     <td className="py-2 text-right">
//                       <Delevered item={item} loadData={() => loaduser()} />
//                     </td>
//                     <td
//                       className="py-2 text-right"
//                       style={{
//                         cursor: 'pointer',
//                       }}
//                       onClick={() => handleView(item)}
//                     >
//                       {' '}
//                       <VisibilityOutlinedIcon />
//                     </td>
//                   </tr>
//                 ))}
//             </tbody>
//           </table>
//         )}
//       </div>
//     </>
//   );
// };

// export default Card;
