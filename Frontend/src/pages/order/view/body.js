// import React, { useEffect, useState } from 'react';
// import { useLocation } from 'react-router-dom';
// import { useDispatch } from 'react-redux';
// import SimpleBackdrop from '../../../Components/backdrop/Backdrop';
// import { getOrderById } from '../../../Actions/orderAction';
// import { Link } from 'react-router-dom';
// import moment from 'moment';
// import { apiService } from '../../../service/apiservice';

// const ViewBody = () => {
//   const history = useLocation();
//   const dispatch = useDispatch();
//   const [isData, setIsdata] = useState([]);
//   const [loading, setLoading] = useState(false);

//   useEffect(() => {
//     setLoading(true);

//     dispatch(getOrderById(history.state.id))
//       .then((data) => {
//         setIsdata(data);
//         setLoading(false);
//         // console.log(data);
//       })
//       .catch((e) => {
//         setLoading(false);
//       });
//     // if (isData) {
//     //   isData.orderItems &&
//     //     isData.orderItems.product.reduce((a, c) => console.log(a.c));
//     // }
//   }, []);

//   return (
//     <div className="col-xl-12 col-xxl-12 ">
//       <SimpleBackdrop open={loading} />
//       <div className="card">
//         <div className="card-header">
//           <div>
//             <p>Order Details</p>
//           </div>
//           <Link to="/order-management" color="primary" className="previous">
//             &laquo; Back
//           </Link>
//         </div>
//         <div className="card-body">
//           <div className="row">
//             <div className="col-lg-12">
//               <div className="card mt-3">
//                 <div className="card-header">
//                   {' '}
//                   OrderNo : {isData.orderNumber}{' '}
//                   <strong>{moment(isData.createdDate).format('lll')}</strong>{' '}
//                   <span className="float-right">
//                     <strong>Status:</strong> Pending
//                   </span>
//                 </div>
//                 <div className="card-body">
//                   <div className="row">
//                     <div className="col-xl-3 col-sm-6 mb-4">
//                       <h6>Customer Info:</h6>
//                       <div>
//                         {' '}
//                         <strong>{isData.user && isData.user.name}</strong>{' '}
//                       </div>
//                       <div>
//                         {isData.user &&
//                           isData.user.address &&
//                           isData.user.address.Address}
//                         {'  '}
//                         {isData.user &&
//                           isData.user.address &&
//                           isData.user.address.City}
//                       </div>
//                       <div>
//                         {isData.user &&
//                           isData.user.address &&
//                           isData.user.address.District}{' '}
//                         {isData.user &&
//                           isData.user.address &&
//                           isData.user.address.Locality}{' '}
//                         {isData.user &&
//                           isData.user.address &&
//                           isData.user.address.Town}{' '}
//                         {isData.user &&
//                           isData.user.address &&
//                           isData.user.address.State}
//                       </div>
//                       <div>{isData.user && isData.user.email}</div>
//                       <div>
//                         Phone: +91{isData.user && isData.user.contactNo}
//                       </div>
//                     </div>
//                     <div className="col-xl-3 col-sm-6 mb-4">
//                       <h6>Part Info:</h6>
//                       <div>
//                         {' '}
//                         {/* <strong> */}
//                         {isData.orderItems &&
//                           isData.orderItems.map((i) => (
//                             <strong>
//                               {i.product.partName}
//                               {'  '}
//                             </strong>
//                           ))}
//                         {/* </strong>{" "} */}
//                       </div>
//                       {/* <div>Attn: Daniel Marek</div> */}
//                       {/* <div>43-190 Mikolow, Poland</div>
//                       <div>Email: marek@daniel.com</div>
//                       <div>Phone: +48 123 456 789</div> */}
//                     </div>
//                     <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-4 d-flex justify-content-lg-end justify-content-md-center justify-content-xs-start">
//                       <div className="row align-items-center">
//                         <div className="col-sm-9">
//                           <div className="brand-logo mb-3">
//                             <img
//                               className="logo-abbr mr-2"
//                               width="50"
//                               // src={`${apiService.productImage}/${
//                               //   isData.orderItems &&
//                               //   isData.orderItems.product.map((i) => i.image)
//                               // }`}
//                               alt=""
//                             />
//                           </div>
//                         </div>
//                       </div>
//                     </div>
//                   </div>
//                   <div className="table-responsive">
//                     <table className="table table-striped">
//                       <thead>
//                         <tr>
//                           <th className="center">#</th>
//                           <th>Part</th>
//                           <th></th>
//                           <th className="right">Product Price</th>
//                           <th className="center">Qty</th>
//                           <th className="right">Total Price</th>
//                         </tr>
//                       </thead>
//                       <tbody>
//                         {isData.orderItems &&
//                           isData.orderItems.map((i, index) => (
//                             <tr>
//                               <td className="center">{index + 1}</td>
//                               <td className="left strong">
//                                 {i.product.partName}
//                               </td>
//                               <td className="left"></td>
//                               <td className="right">{i.product.amount} Rs</td>
//                               <td className="center">{i.qty}</td>
//                               <td className="right">
//                                 {i.product.amount * i.qty}
//                               </td>
//                             </tr>
//                           ))}
//                       </tbody>
//                     </table>
//                   </div>
//                   <div className="row">
//                     <div className="col-lg-4 col-sm-5"> </div>
//                     <div className="col-lg-4 col-sm-5 ml-auto">
//                       <table className="table table-clear mb-0">
//                         <tbody>
//                           <tr>
//                             <td className="left">
//                               <strong>Total</strong>
//                             </td>
//                             <td className="right">
//                               {/* {isData.orderItems &&
//                                 isData.orderItems.reduce((a, c) => (
//                                   <strong>
//                                     {a.product.amount * a.qty +
//                                       c.product.amount * c.qty}
//                                   </strong>
//                                 ))} */}
//                               <strong>
//                                 {isData.orderItems &&
//                                   isData.orderItems.reduce(
//                                     (a, c) => a + c.product.amount * c.qty,
//                                     0
//                                   )}
//                               </strong>
//                             </td>
//                           </tr>
//                         </tbody>
//                       </table>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default ViewBody;
