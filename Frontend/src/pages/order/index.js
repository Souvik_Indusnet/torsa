// import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Ordercsv } from '../../Actions/CSVAction';
import { getAllOrder, orderStatusChange } from '../../Actions/orderAction';
import ActionButton from '../../Components/Table/ActionButton';
import PaginationControlled from '../../Components/Table/AdvancedPagination';
import TableComponent from '../../Components/Table/TableComponent';
import TableStatusButton from '../../Components/Table/TableStatusButton';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
import DateFormate from '../../Components/Utils/DateFormate';
import { formateDatereturn } from '../../Components/Utils/DayFormateFunc';
import { Form, Formik } from 'formik';
import SelectType from '../../Components/form/select';
import * as yup from 'yup';
const Order = () => {
  const dispatch = useDispatch();
  const [getData, setGetData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [csvdata, setCsvdata] = useState([]);
  var data = '';

  const validation = yup.object({
    res_category: yup.string().required('Required *'),
  });
  const handleSubmit = (e) => {
    // console.log(e)
    // const validData = {
    //   orderStatus: e.res_category,
    //   _id: status.id,
    // };
    dispatch(orderStatusChange(status.id, e.res_category)).then((data) => {
      getAllData({ pageNumber: currentPage });
    });
    // if(e.res_category=='rejected'){
    //    validData.reason=e.reason;
    // }
    // // e.preventDefault();
    // // console.log(getData)
    // if(getData.getAll.filter(p=>{return p._id==validData.id})[0].Partial_Date!=null||validData.isApproved=='rejected'){
    //   dispatch(updateEnquery(validData)).then((data) => {
    //     getAllData({ pageNumber: 1 });
    //     setStatus({ id: '', isApproved: '' });
    //   });
    // }else{
    //   enqueueSnackbar(`Please update the Enquiry Details first`, {
    //     variant: "warning",
    //     anchorOrigin: {
    //       vertical: "bottom",
    //       horizontal: "right",
    //     },
    //   });
    // }

    document.getElementById('clickMe').click();
  };
  const [status, setStatus] = useState({
    isApproved: '',
    id: '',
    index: '',
  });
  const handleStatus = (en_data, index) => {
    setStatus({ ...status, id: en_data._id, index: index });
  };
  const getAllData = async ({ pageNumber, Keyword }) => {
    setLoading(true);
    const Equipments = await dispatch(getAllOrder({ pageNumber, Keyword }));
    setGetData(Equipments?.data?.data?.list);
    setLoading(false);
  };
  const Options = [
    { name: 'Placed', _id: 'Placed', value: 'Placed' },
    // { name: 'Ready_to_ship', _id: 'Ready_to_ship', value: 'Ready_to_ship' },
    { name: 'Shipped', _id: 'Shipped', value: 'Shipped' },
    { name: 'Delivered', _id: 'Delivered', value: 'Delivered' },
    // { name: 'Cancelled', _id: 'Cancelled', value: 'Cancelled' },
  ];
  // console.log(getData);

  useEffect(() => {
    dispatch(Ordercsv()).then((data) => {
      setCsvdata(data?.data?.data?.list);
    });
  }, [loading]);

  useEffect(() => {
    // dispatch(Ordercsv()).then((data) => {
    //   setCsvdata(data?.data?.data?.list);
    // });
    getAllData({ pageNumber: 1 });
    document.title = 'Order Managment | Torsa';
  }, []);

  const Heading = [
    { title: 'Sr. No' },
    { title: 'Order No' },
    { title: 'Customer Name', key: '' },
    { title: 'Order Qty', key: '' },
    { title: 'Product', key: '' },
    { title: 'Order Amount', key: '' },
    { title: 'Order Status', key: '' },
    { title: ' Order Date', key: '' },
    { title: 'Delivery Status', key: '' },
    { title: 'Action', key: '' },
  ];

  const csvheader = [
    { label: 'Sr', key: 'sr' },
    {
      label: 'Order No. ',
      key: 'orderno',
    },
    {
      label: 'User Name ',
      key: 'name',
    },

    {
      label: 'Order Qty. ',
      key: 'orderqty',
    },
    {
      label: 'Part',
      key: 'part',
    },
    {
      label: 'Order Amount',
      key: 'order_amount',
    },
    {
      label: 'Order Status',
      key: 'Order_Status',
    },
    {
      label: 'Order Date	 ',
      key: 'updatedAt',
    },

    {
      label: 'Delivery Status',
      key: 'ds',
    },
  ];
  const csv_Data = csvdata?.map((i, index) => {
    return {
      sr: index + 1,
      orderqty: i.qty,
      orderno: i._id,
      Order_Status: i.Payment_Status,
      order_amount: parseInt(i?.part?.amount) * parseInt(i?.qty),
      ds: i.Delivered ? true : 'false',
      name: i?.user?.name,
      part: i?.part?.partName,
      updatedAt: formateDatereturn(i.createdAt),
    };
  });

  // console.log('csv data', csvdata);
  return (
    <>
      <TableHeaderComponent
        loading={loading}
        card_title="Order Management"
        searchButton={true}
        SearchKeyBack={(e) => {
          getAllData({ pageNumber: 1, Keyword: e });
        }}
        exportButton="true"
        csvData={csv_Data}
        headers={csvheader}
        csvfilename="order-csv.csv"
      >
        <div className="card-body">
          <div className="table-responsive">
            <TableComponent TableHeading={Heading}>
              {getData?.results
                ?.filter((i) => i.for_Enquery == false)
                .map((el, index) => {
                  return (
                    <tr key={el._id}>
                      <td>
                        {' '}
                        {(getData?.current?.page + 1 || 1 + 1) *
                          getData?.current?.limit +
                          (index + 1) -
                          20}
                      </td>
                      <td>{el?._id}</td>
                      <td>{el?.user?.name}</td>
                      <td>{el?.qty}</td>
                      <td>{el?.part?.partName}</td>
                      <td>{el?.part?.amount * el?.qty}</td>
                      <td>
                        <TableStatusButton
                          Orderpaid={el.Payment_Status}
                          handleBack={(e) => console.log('payment status')}
                        />
                      </td>
                      <td>
                        <DateFormate formatter={el.createdAt} />

                        {/* {moment(el.createdAt).format('ll')} */}
                      </td>
                      {/* <td>
                      <TableStatusButton
                        Order={el.Delivered ? true : false}
                        handleBack={(e) =>
                          dispatch(
                            orderStatusChange(el._id, !el.Delivered)
                          ).then((data) => {
                            getAllData({ pageNumber: currentPage });
                          })
                        }
                      />
                    </td> */}
                      <td>
                        <span
                          onClick={() => handleStatus(el, index)}
                          className="badge light badge-warning btn btn-primary"
                          type="button"
                          data-toggle="modal"
                          data-target={
                            el.orderStatus !== 'Delivered' && '#exampleModal'
                          }
                          // style={i.isApproved=='rejected'?{background:'#ff2625',color: 'white'}:null}
                        >
                          {el.orderStatus}
                        </span>
                      </td>
                      <td className="py-2 ">
                        {' '}
                        <ActionButton
                          stateData={el}
                          // selectPath="/updateuser"
                          viewPath="/vieworder"
                        />
                      </td>
                    </tr>
                  );
                })}
            </TableComponent>
          </div>
        </div>
        <PaginationControlled
          pageInfo={getData}
          pageChange={(e) => {
            setCurrentPage(e);
            getAllData({ pageNumber: e });
          }}
        />
      </TableHeaderComponent>
      <div
        className="modal fade"
        id="exampleModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Delivery Status
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <Formik
              // enableReinitialize={true}
              initialValues={{
                res_category: '',
                reason: '',
              }}
              validationSchema={validation}
              onSubmit={handleSubmit}
            >
              {(formik) => {
                // if(status.id!=''){
                //   formik.setFieldValue('res_category', getData.results[status.index].orderStatus);
                // }
                console.log(formik);
                return (
                  <Form>
                    <div className="modal-body">
                      <SelectType
                        name="res_category"
                        values={Options}
                        title="Select Enquiry Status"
                        placeholder="options..."
                        // selectedValue={status.index!=''?getData.results[status.index].orderStatus:''}
                        callBack={(e) => {
                          // console.log('e',e);
                          setStatus({ ...status, isApproved: e.category });
                        }}
                      />
                      {formik.values.res_category == 'Cancelled' ? (
                        <InputBox
                          type="text"
                          name="reason"
                          title="Reason"
                          placeholder="enter the reason"
                          // readonly="true"
                        />
                      ) : null}
                    </div>
                    <div className="modal-footer">
                      <button
                        type="button"
                        className="btn btn-secondary"
                        data-dismiss="modal"
                        id="clickMe"
                      >
                        Close
                      </button>
                      <button
                        // disabled={status.isApproved == ''}
                        // style={{
                        //   cursor:
                        //     status.isApproved == '' ? 'not-allowed' : 'pointer',
                        // }}
                        type="submit"
                        className="btn btn-primary"
                        // data-dismiss="modal"
                      >
                        Save changes
                      </button>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    </>
  );
};

export default Order;
