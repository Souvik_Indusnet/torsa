import { Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { GetEquipmentAll } from "../../Actions/categoryAction";
import ImageUploading from "react-images-uploading";

import {
  AddProduct,
  // createParts,
  machineModelArray,
  UpateParts,
  imageUpload,
} from "../../Actions/productAction";
// import { AdminList } from '../../Actions/userAction';
import CancelButton from "../../Components/Button/Cancel";
import SubmitButton from "../../Components/Button/Submit";
import InputBox from "../../Components/form/input";
// import InputImage from '../../Components/form/InputimageComponent';
import Multiselect from "../../Components/form/multiselect";
// import SelectType from '../../Components/form/select';
import TextArea from "../../Components/form/textArea";
import TableHeaderComponent from "../../Components/Table/TableTopComponent";
import { productSchema } from "../../Components/validation/validationSchema";
import { apiService } from "../../service/apiservice";
import { useSnackbar } from "notistack";
import axios from "axios";

// console.log('...............................29');
const ProductsForm = (props) => {
  const [images, setImages] = useState([]);
  // const [imagesUrls, setImagesUrls] = React.useState([]);
  const maxNumber = 5;
  const onChange = (imageList, addUpdateIndex) => {
    console.log(imageList);
    setImages(imageList);
    // alert('35')
  };

  const [state, setState] = useState({
    equipment: "",
    equipmentModal: "",
    image: [],
  });

  const history = useHistory();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [equipment, setEquipment] = useState("");

  const [modal_list, setModal_list] = useState([]);

  useEffect(() => {
    // absc?.forEach((i) => {
    //   //     setDataSources((oldArray) => [...oldArray, i]);
    //   //   });
    console.log(".....................", props?.location?.state?.image);
    if (props?.location?.state?.image.length != 0) {
      props?.location?.state?.image?.forEach((i) => {
        // setImages([...images, { data_url: `${apiService.imagepathPro}/${i}` }]);
        setImages((old) => [...old, { data_url: i }]);
        console.log(i);
      });
      console.log(images);
      // setImages();
    }
  }, [props?.location?.state?.image]);

  useEffect(() => {
    dispatch(machineModelArray(state.equipment)).then((data) => {
      console.log(data)
      setModal_list(data);
    });
    if (props?.location?.state?.category?._id) {
      setState({
        ...state,
        equipment: props?.location?.state?.category?._id,
        equipmentModal: props?.location?.state?.subcategory?._id,
      });
    }
    dispatch(GetEquipmentAll()).then((data) => {
      setEquipment(data?.data?.data?.filter((i) => i.active == true));
    });
    document.title = props?.location?.state
      ? "Update Product |Torsa"
      : "Add Product | Torsa";
  }, [props?.location?.state?.category?._id]);

  useEffect(() => {
    dispatch(machineModelArray(state.equipment)).then((data) => {
      console.log(data)
      setModal_list(data);
    });
  }, [state.equipment]);

  const [loading, setLoading] = useState(false);
  const [cropStatus, setCropStatus] = useState(false);

  const dispatch = useDispatch();
  const handleSubmit = async (e) => {
    if (props?.location.pathname == "/addproduct" && images.length == 0) {
      enqueueSnackbar(`please select image`, {
        variant: "warning",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
      });
      return 0;
    } else if (
      props?.location.pathname == "/updateproduct" &&
      images.length == 0
    ) {
      enqueueSnackbar(`please select image`, {
        variant: "warning",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
      });
      return 0;
    }
    setLoading(true);
    const formData = new FormData();
    formData.append("id", props?.location?.state?._id || null);
    formData.append("partName", e.name);
    formData.append("category", state.equipment);
    formData.append("subcategory", state.equipmentModal);
    formData.append("description", e.description);
    formData.append("amount", e.amount);
    formData.append("stock", e.stock);
    images
      ?.filter((i) => i.file != undefined)
      ?.map((i) => {
        formData.append("images", i.file);
      });
    images
      ?.filter((i) => i.file == undefined)
      .map((i) => {
        formData.append("alreadyUploaded", i.data_url);
      });

    // formData.append("images", images);
    console.log("186", images);
    // axios.post('http://localhost:4000/upload_aws/',formData)
    // formData.append(
    //   "imagearray",
    //   JSON.stringify(
    //     images
    //       ?.filter((i) => i.file == undefined)
    //       ?.map((i) => i.data_url.replace(`${apiService.imagepathPro}/`, ""))
    //   )
    // );
    // var temp=[];
    // for (var i = 0; i < images.length; i++) {
    //   if(images[i].file!=undefined){
    //     const url= await S3Upload(images[i].file)
    //     var t="https://bridge-media-2021.s3.ap-south-1.amazonaws.com/" + String(url);
    //     temp.push(t);
    //   }else{
    //     temp.push(images[i].data_url)
    //   }
    // // setImagesUrls((prevState)=>[...prevState,t]);
    // }
    // // formData.append(
    // //   'image',
    // //   JSON.stringify(
    // //     dataSources?.map((i) =>
    // //       i.dataURL.replace(`${apiService.imagepathPro}/`, '')
    // //     )
    // //   )
    // // );
    // formData.append(
    //   "imageUrlarray",
    //   temp
    // );
    dispatch(
      props?.location?.state ? UpateParts(formData) : AddProduct(formData)
    ).then((data) => {
      setLoading(false);
      history.push("/productmanagement");
    });
  };
  const options =
    equipment &&
    equipment.map((el) => {
      return { value: el._id, label: el.name };
    });
  const defaul_Eq = {
    value: props?.location?.state?.category?._id,
    label: props?.location?.state?.category?.name,
  };
  const default_Eq_modal = {
    value: props?.location?.state?.subcategory?._id,
    label: props?.location?.state?.subcategory?.name,
  };
  const modalList = modal_list.map((el) => {
    return {
      value: el._id,
      label: el.name,
    };
  });

  // const [fileList, setFileList] = useState([]);

  return (
    <TableHeaderComponent
      loading={loading}
      card_title={props?.location?.state ? "Update Product" : "Add Product"}
      SearchKeyBack={(e) => {
        console.log(e);
      }}
    >
      <div className="mt-20">
        <div className="basic-form">
          <Formik
            initialValues={{
              name: props?.location?.state?.partName || "",
              description: props?.location?.state?.description || "",
              amount: props?.location?.state?.amount || "",
              stock: props?.location?.state?.inventoryStock || "",
            }}
            validationSchema={productSchema}
            onSubmit={handleSubmit}
          >
            {(formik) => {
              return (
                <Form>
                  <div className="row">
                    <div className="form-group col-md-6">
                      <Multiselect
                        multi={true}
                        title=" Equipment"
                        placeholder="Select Equipment "
                        Options={options}
                        defaultOption={defaul_Eq}
                        callback={(e) =>
                          setState({ ...state, equipment: e.value })
                        }
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <Multiselect
                        multi={true}
                        title=" Equipment Modal"
                        placeholder="Select Equipment Modal"
                        Options={modalList}
                        defaultOption={default_Eq_modal}
                        callback={(e) =>
                          setState({ ...state, equipmentModal: e.value })
                        }
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <InputBox
                        type="text"
                        id="nf-name"
                        placeholder="Enter Product Name..."
                        title=" Product Name"
                        name="name"
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <TextArea
                        // name="description"
                        placeholder="Enter Description ..."
                        title="  Description "
                        name="description"
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <InputBox
                        type="number"
                        id="nf-name"
                        placeholder="Enter Amount ..."
                        title=" Amount  "
                        name="amount"
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <InputBox
                        type="number"
                        id="nf-name"
                        placeholder="Enter Stock ..."
                        title=" Stock"
                        name="stock"
                      />
                    </div>

                    <div className="form-group col-md-12">
                      <ImageUploading
                        multiple
                        value={images}
                        onChange={onChange}
                        maxNumber={maxNumber}
                        dataURLKey="data_url"
                      >
                        {({
                          imageList,
                          onImageUpload,
                          onImageRemoveAll,
                          onImageUpdate,
                          onImageRemove,
                          isDragging,
                          dragProps,
                          errors
                        }) => (
                          // write your building UI
                          <div className="upload__image-wrapper">
                            <span
                              style={isDragging ? { color: "red" } : null}
                              onClick={onImageUpload}
                              {...dragProps}
                            >
                              Click or Drop here
                            </span>
                            &nbsp; &nbsp; &nbsp;
                            <span onClick={onImageRemoveAll}>
                              Remove all images
                            </span>
                            <div className="image-flex">
                              {imageList.map((image, index) => (
                                <div key={index} className="image-item">
                                  <img
                                    src={image.data_url}
                                    alt=""
                                    width="100"
                                  />
                                  <div className="image-item__btn-wrapper">
                                    <span onClick={() => onImageUpdate(index)}>
                                      Update
                                    </span>{" "}
                                    &nbsp;
                                    <span onClick={() => onImageRemove(index)}>
                                      Remove
                                    </span>
                                  </div>
                                </div>
                              ))}
                            </div>
                          </div>
                        )}
                      </ImageUploading>
                    </div>

                    <div className="col-md-8">&nbsp;</div>
                    <div className="col-md-2">
                      <CancelButton
                        back_path="/productmanagement"
                        ButtonTitle="Cancel"
                      />
                    </div>
                    <div className="col-md-2">
                      <SubmitButton
                        ButtonName={props?.location?.state ? "Update" : "Add"}
                        loading={loading}
                      />
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </TableHeaderComponent>
  );
};
export default ProductsForm;
