import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import ActionButton from '../../Components/Table/ActionButton';
import Pagination from '../../Components/Table/Pagination';
import TableComponent from '../../Components/Table/TableComponent';
import TableStatusButton from '../../Components/Table/TableStatusButton';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
import {
  blockInventory,
  getAllMachinesInfo,
} from '../../Actions/productAction';
import PaginationControlled from '../../Components/Table/AdvancedPagination';
import DateFormate from '../../Components/Utils/DateFormate';
import { Productcsv } from '../../Actions/CSVAction';
import { formateDatereturn } from '../../Components/Utils/DayFormateFunc';
import { apiService } from '../../service/apiservice';
import { useHistory } from 'react-router-dom';
const urlParams = new URLSearchParams(window.location.search).get('search');
const ProductTable = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [getData, setGetData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [csvdata, setCsvdata] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const getAllData = async ({ pageNumber, Keyword }) => {
    setLoading(true);
    console.log(pageNumber, Keyword);
    if (Keyword != 'null') {
      const Enventory = await dispatch(
        getAllMachinesInfo({ pageNumber, Keyword })
      );
      setGetData(Enventory?.data?.data?.list);
      dispatch(Productcsv()).then((data) => {
        setCsvdata(data?.data?.data?.list);
      });
    }
    setLoading(false);
  };

  // useEffect(() => {

  // }, [loading]);

  useEffect(() => {
    history.replace({
      pathname: window.location.pathname,
      search: `?search=${urlParams}`,
    });
    if (urlParams != '') {
      getAllData({ pageNumber: 1, Keyword: urlParams });
    } else {
      getAllData({ pageNumber: 1 });
    }
    document.title = 'Product Management | Torsa';
  }, []);

  // console.log('csv data', csvdata);
  const Heading = [
    { title: 'Sr. No' },
    { title: 'Equipment', key: '' },
    { title: 'Product Name', key: '' },
    { title: 'Amount', key: '' },
    { title: 'Created-On', key: '' },
    { title: 'Status', key: '' },
    { title: 'Action', key: '' },
  ];

  const csvheader = [
    { label: 'Sr', key: 'sr' },
    {
      label: 'Equipment ',
      key: 'equipment',
    },
    {
      label: 'Equipment modal',
      key: 'modal',
    },
    {
      label: 'Product name ',
      key: 'product',
    },
    {
      label: 'Amount  ',
      key: 'amount',
    },
    {
      label: 'Stock',
      key: 'stocks',
    },
    {
      label: 'Image path',
      key: 'image',
    },
    {
      label: 'Created on',
      key: 'createdAt',
    },
    {
      label: 'Status',
      key: 'active',
    },
    {
      label: 'Description',
      key: 'desc',
    },
  ];
  const csv_Data = csvdata?.map((i, index) => {
    // console.log(i);
    return {
      sr: index + 1,
      desc: i?.description,
      image:
        i.image.length == 0
          ? ''
          : i.image.map((j) => {
              return `${apiService.imagepathPro}${j}`;
            }),
      stocks: i?.inventoryStock,
      equipment: i?.equipment?.name,
      modal: i?.equipmentmodal?.name,
      product: i?.partName,
      amount: i?.amount,
      createdAt: formateDatereturn(i.createdAt),
      active: i.active == false ? 'false' : i.active,
    };
  });
  // console.log('usrInfor', getData);

  return (
    <TableHeaderComponent
      loading={loading}
      card_title="Product Management"
      add_tittle="Add"
      addpath="/addproduct"
      search={urlParams}
      SearchKeyBack={(e) => {
        history.replace({
          pathname: window.location.pathname,
          search: `?search=${e}`,
        });
        getAllData({ pageNumber: 1, Keyword: e });
      }}
      searchButton={true}
      exportButton="true"
      csvData={csv_Data}
      headers={csvheader}
      csvfilename="Product-csv.csv"
    >
      <div className="card-body">
        <div className="table-responsive">
          <TableComponent TableHeading={Heading}>
            {getData?.results?.map((el, index) => {
              return (
                <tr key={el._id}>
                  <td>
                    {' '}
                    {(getData?.current?.page + 1 || 1 + 1) *
                      getData?.current?.limit +
                      (index + 1) -
                      20}
                  </td>
                  <td>{el?.category?.name}</td>
                  <td>{el.partName}</td>
                  <td>{el?.amount}</td>
                  <td>
                    <DateFormate formatter={el.createdAt} />
                  </td>

                  <td>
                    <TableStatusButton
                      status={el.active}
                      handleBack={(e) => {
                        setLoading(true);
                        dispatch(blockInventory(el._id, e)).then((data) => {
                          getAllData({ pageNumber: currentPage });
                        });
                      }}
                    />
                  </td>
                  <td className="py-2 ">
                    {' '}
                    <ActionButton
                      stateData={el}
                      selectPath="/updateproduct"
                      viewPath={'/viewproduct'}
                    />
                  </td>
                </tr>
              );
            })}
          </TableComponent>
        </div>
      </div>
      <PaginationControlled
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      />
      {/* <Pagination
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      /> */}
    </TableHeaderComponent>
  );
};

export default ProductTable;
