import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getProductDetailsById } from '../../Actions/productAction';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
import { apiService } from '../../service/apiservice';

const ProductView = (props) => {
  const [loading, setLoading] = useState(false);
  const [stateData, setdata] = useState('');
  console.log(stateData);
  const dispatch = useDispatch();

  useEffect(() => {
    setLoading(true);
    console.log('15',props?.location?.state);
    dispatch(getProductDetailsById(props?.location?.state?._id)).then(
      (data) => {
        setdata(data?.data?.data?.list[0]);
        setLoading(false);
      }
    );
    document.title = 'View Product | Torsa';
  }, [props?.location?.state?._id]);

  const [active, setActive] = useState(0);

  return (
    <TableHeaderComponent
      loading={loading}
      arrowIcon={true}
      backButtonPath="/productmanagement"
      SearchKeyBack={(e) => {
        console.log(e);
      }}
    >
      <div className="card-body">
        <div
          style={{
            display: 'flex',
            alignContent: 'center',
            justifyContent: 'space-around',
          }}
        >
          <div className="row">
            <div className="col-lg-3">
              <div className="tab-content">
                {props?.location?.state?.image?.map((i, index) => (
                  <div
                    key={i}
                    role="tabpanel"
                    className={
                      index == active
                        ? 'tab-pane fade show active'
                        : 'tab-pane fade show '
                    }
                    id={`${i}`}
                  >
                    <img
                      className="img-fluid"
                      src={`${i}`}
                      alt="product image"
                    />
                  </div>
                ))}
              </div>
              <div className="tab-slide-content new-arrival-product mb-4 mb-xl-0">
                <ul className="nav slide-item-list mt-3" role="tablist">
                  {props?.location?.state?.image?.map((i, index) => (
                    <li
                      key={i}
                      role="presentation"
                      className="show"
                      onClick={() => setActive(index)}
                    >
                      <a href={`#${i}`} role="tab" data-toggle="tab">
                        <img
                          className="img-fluid"
                          src={`${i}`}
                          alt=""
                          width="50"
                        />
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
            <div className="col-lg-8">
              <div className="product-detail-content">
                <div className="new-arrival-content pr">
                  <h4>{stateData?.partName}</h4>
                  <div className="d-table mb-2">
                    <p className="price float-left d-block">
                      ${stateData?.amount}
                    </p>
                  </div>
                  <p>Category: {stateData?.category?.name}</p>
                  <p>Sub Category : {stateData?.subcategory?.name}</p>
                  <p>
                    Availability:{' '}
                    <span className="item">
                      {stateData?.inventoryStock} In stock{' '}
                      <i className="fa fa-shopping-basket"></i>
                    </span>
                  </p>{' '}
                  <p className="text-content">
                    Description : {stateData?.description}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </TableHeaderComponent>
  );
};

export default ProductView;
