import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import ActionButton from '../../Components/Table/ActionButton';
import TableComponent from '../../Components/Table/TableComponent';
import TableStatusButton from '../../Components/Table/TableStatusButton';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
import {
  blockInventory,
  getAllMachinesInfo,
} from '../../Actions/productAction';
import PaginationControlled from '../../Components/Table/AdvancedPagination';
import DateFormate from '../../Components/Utils/DateFormate';

import { useHistory } from 'react-router-dom';
import { getAllCreateNotification } from '../../Actions/notification';
const urlParams = new URLSearchParams(window.location.search).get('search');

const NotificationTable = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [getData, setGetData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const getAllData = async ({ pageNumber, Keyword }) => {
    setLoading(true);

    if (Keyword != 'null') {
      const Enventory = await dispatch(
        getAllCreateNotification({ pageNumber, Keyword })
      );
      setGetData(Enventory?.data?.list?.[0]);
      console.log(Enventory?.data?.list?.[0]);
    }
    setLoading(false);
  };

  useEffect(() => {
    history.replace({
      pathname: window.location.pathname,
      search: `?search=${urlParams}`,
    });
    if (urlParams != '') {
      getAllData({ pageNumber: 1, Keyword: urlParams });
    } else {
      getAllData({ pageNumber: 1 });
    }
    document.title = 'Notification Management | Torsa';
  }, []);

  // console.log('csv data', csvdata);
  const Heading = [
    { title: 'Sr. No' },
    { title: 'Category', key: '' },
    { title: 'Text message', key: '' },

    { title: 'Created-On', key: '' },
  ];

  return (
    <TableHeaderComponent
      loading={loading}
      card_title="Notification Management"
      add_tittle="Add"
      addpath="/notification-create"
      search={urlParams}
      SearchKeyBack={(e) => {
        history.replace({
          pathname: window.location.pathname,
          search: `?search=${e}`,
        });
        getAllData({ pageNumber: 1, Keyword: e });
      }}
    >
      <div className="card-body">
        <div className="table-responsive">
          <TableComponent TableHeading={Heading}>
            {getData?.data?.map((el, index) => {
              return (
                <tr key={el._id}>
                  <td>
                    {' '}
                    {(getData?.meta?.page + 1 || 1 + 1) * getData?.meta?.limit +
                      (index + 1) -
                      20}
                  </td>
                  <td>
                    {el?.type == 'New  Notification for customer'
                      ? 'User'
                      : 'Business Admin'}
                  </td>
                  <td>{el._id}</td>

                  <td>
                    <DateFormate formatter={el.createdAt} />
                  </td>

                  {/* <td className="py-2 ">
                    {' '}
                    <ActionButton
                      stateData={el}
                      selectPath="/updateproduct"
                      viewPath={'/viewproduct'}
                    />
                  </td> */}
                </tr>
              );
            })}
          </TableComponent>
        </div>
      </div>
      <PaginationControlled
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      />
    </TableHeaderComponent>
  );
};

export default NotificationTable;
