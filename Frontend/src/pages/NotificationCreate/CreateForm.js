import { Form, Formik } from 'formik';
import InputBox from '../../Components/form/input';
import SelectType from '../../Components/form/select';

import * as yup from 'yup';
import SubmitButton from '../../Components/Button/Submit';
import { useDispatch } from 'react-redux';
import { createNotification } from '../../Actions/notification';
import { useEffect, useState } from 'react';
import { useSnackbar } from 'notistack';

const validation = yup.object({
  category: yup.string().required('Category  is required'),
  notification_text: yup
    .string()
    .trim()
    .required('Notification text  is required'),
  notification_url: yup.string().trim(),
});

const CreateForm = (props) => {
  const dispatch = useDispatch();

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [state, setState] = useState(false);

  const submitHandler = (e) => {
    setState(true);
    dispatch(createNotification(e))
      .then((data) => {
        setState(false);
        enqueueSnackbar(`Notification create success`, {
          variant: 'success',
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'right',
          },
        });
        setTimeout(() => {
          props.history.push('/notification-management');
        }, 1000);
      })
      .catch((e) => {
        console.log('error', e);
        enqueueSnackbar(e?.message || 'Something went wrong', {
          variant: 'error',
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'right',
          },
        });
      });
    // console.log(e);
  };

  return (
    <div className="col-xl-12 col-xxl-12">
      <div>
        <div className="card-header">
          <h4 className="card-title">Create Notification</h4>
        </div>
        <div className="card-body">
          <div className="basic-form">
            <Formik
              initialValues={{}}
              validationSchema={validation}
              onSubmit={submitHandler}
            >
              {(formik) => {
                return (
                  <Form>
                    <div className="form-row">
                      <div className="form-group col-12">
                        <SelectType
                          name="category"
                          title="Category *"
                          placeholder="Category"
                          callBack={(e) => console.log(e)}
                          values={[
                            { name: 'Business Admin', _id: 'business_admin' },
                            { name: 'User', _id: 'user' },
                          ]}
                        />
                      </div>
                      <div className="form-group col-12">
                        <InputBox
                          name="notification_text"
                          title="Notification text *"
                          placeholder="Notification text"
                        />
                      </div>
                      <div className="form-group col-12">
                        <InputBox
                          name="notification_url"
                          title="Notification url (optional)"
                          placeholder="Notification url"
                        />
                      </div>

                      <div className="col-md-8">&nbsp;</div>
                    </div>
                    <div style={{ float: 'right' }} className="col-md-2">
                      <SubmitButton ButtonName="Send" loading={state} />
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CreateForm;
