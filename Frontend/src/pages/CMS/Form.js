import { Editor } from '@tinymce/tinymce-react';
import { Form, Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import {
  createCMS,
  fetchAllPageName,
  UpdateCMS,
} from '../../Actions/CmsActions';
import CancelButton from '../../Components/Button/Cancel';
import SubmitButton from '../../Components/Button/Submit';
import InputBox from '../../Components/form/input';
import { useSnackbar } from 'notistack';

import SelectType from '../../Components/form/select';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
import { CmsPage } from '../../Components/validation/validationSchema';

const CMSForm = (props) => {
  const history = useHistory();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [errorMessage, setErrorMessage] = useState('');
  const [pagename, setPageName] = useState([]);
  const [content, setContent] = useState('');
  const handleChange = (e) => {
    setContent(e);
  };
  useEffect(() => {
    dispatch(fetchAllPageName()).then((el) => {
      setPageName(el?.data?.data?.resultData);
    });
    if (props?.location?.state) {
      setContent(props?.location?.state?.content);
    }
    document.title = props?.location?.state
      ? 'Update CMS |Torsa'
      : 'Add CMS | Torsa';
  }, []);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  //   console.log(props);
  const handleSubmit = (e) => {
    setLoading(true);

    const validData = {
      editID: props?.location?.state?._id || '',
      content,
      forPageName: e.pagename,
    };

    dispatch(
      props?.location?.state ? UpdateCMS(validData) : createCMS(validData)
    ).then((data) => {
      if (data?.error) {
        setLoading(false);
        enqueueSnackbar(`Page already exists`, {
          variant: 'error',
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'right',
          },
        });
      } else {
        setLoading(false);
        history.push('/cms');
      }
    });
  };

  // const defaultValue = props?.location?.state?.console.log(props);
  return (
    <TableHeaderComponent
      loading={loading}
      card_title={props?.location?.state ? 'Update CMS' : 'Add CMS'}
      SearchKeyBack={(e) => {
        console.log(e);
      }}
    >
      <div className="mt-20">
        <div className="basic-form">
          <Formik
            initialValues={{
              pagename: props?.location?.state?.forPageName || '',
            }}
            validationSchema={CmsPage}
            onSubmit={handleSubmit}
          >
            {(formik) => {
              return (
                <Form>
                  <div className="row">
                    <div className="form-group col-md-12">
                      {/* <SelectType
                        disabled={props?.location?.state ? true : false}
                        name="pagename"
                        title=" Page"
                        placeholder="Select Page..."
                        values={pagename ? pagename : []}
                        callBack={(e) => console.log(e?.category)}
                      /> */}
                      <InputBox
                        type="text"
                        id="nf-name"
                        placeholder="Enter  Name..."
                        title="Title"
                        name="pagename"
                      />
                    </div>
                    <div className="form-group col-md-12">
                      <Editor
                        value={content}
                        apiKey="l9s0qbrgvih2hpscfbcbt3k71fr9gidz4dil2vjwsmyrgab7"
                        init={{
                          height: 200,
                          menubar: 'file insert help',
                          plugins: 'lists link image paste help',
                          toolbar:
                            'undo redo | formatselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image link |  help',
                          branding: false,
                        }}
                        onEditorChange={handleChange}
                      />
                    </div>
                    <div className="col-md-8">&nbsp;</div>

                    <div className="col-md-2">
                      <CancelButton back_path="/cms" ButtonTitle="Cancel" />
                    </div>
                    <div className="col-md-2">
                      <SubmitButton
                        ButtonName={props?.location?.state ? 'Update' : 'Add'}
                        loading={loading}
                      />
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </TableHeaderComponent>
  );
};

export default CMSForm;
