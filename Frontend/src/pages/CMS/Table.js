import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import ActionButton from '../../Components/Table/ActionButton';
import Pagination from '../../Components/Table/Pagination';
import TableComponent from '../../Components/Table/TableComponent';
import TableStatusButton from '../../Components/Table/TableStatusButton';
import TableHeaderComponent from '../../Components/Table/TableTopComponent';
// import moment from 'moment';
import { GetAllCMSpages, GetAllCMSpagesStatus } from '../../Actions/CmsActions';
import PaginationControlled from '../../Components/Table/AdvancedPagination';
import DateFormate from '../../Components/Utils/DateFormate';

const CmsTable = () => {
  const dispatch = useDispatch();
  const [getData, setGetData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const getAllData = async ({ pageNumber }) => {
    setLoading(true);
    const CMS = await dispatch(GetAllCMSpages({ pageNumber }));
    setGetData(CMS?.data?.data?.list);
    setLoading(false);
  };
  useEffect(() => {
    getAllData({ pageNumber: 1 });
    document.title = 'CMS  | Torsa';
  }, []);

  // console.log(getData);
  const Heading = [
    { title: 'Sr. No' },
    { title: 'Page Name', key: '' },
    { title: 'Create On', key: '' },
    { title: 'Status', key: '' },
    { title: 'Action', key: '' },
  ];
  // console.log(getData);

  const [state, setState] = useState('');

  return (
    <TableHeaderComponent
      loading={loading}
      card_title="CMS "
      // add_tittle="Add"
      // addpath="/addcms"
      // // exportButton="true"
      SearchKeyBack={(e) => {
        console.log(e);
      }}
    >
      <div className="card-body">
        <div className="table-responsive">
          <TableComponent TableHeading={Heading}>
            {getData?.results?.map((el, index) => {
              return (
                <tr key={el._id}>
                  <td>{index + 1}</td>
                  <td>{el?.forPageName}</td>
                  <td>
                    <DateFormate formatter={el?.createdAt} />

                    {/* {moment(el.createdAt).format('ll')} */}
                  </td>
                  <td>
                    <TableStatusButton
                      status={el.status}
                      handleBack={(e) =>
                        dispatch(
                          GetAllCMSpagesStatus({ id: el._id, status: e })
                        ).then((data) => {
                          getAllData({ pageNumber: currentPage });
                        })
                      }
                    />
                  </td>
                  <td className="py-2 ">
                    {' '}
                    <ActionButton stateData={el} selectPath="/updatecms" />
                  </td>
                </tr>
              );
            })}
          </TableComponent>
        </div>
      </div>
      <PaginationControlled
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      />
      {/* <Pagination
        pageInfo={getData}
        pageChange={(e) => {
          setCurrentPage(e);
          getAllData({ pageNumber: e });
        }}
      /> */}
    </TableHeaderComponent>
  );
};

export default CmsTable;
