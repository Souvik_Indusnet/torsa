import Cookies from 'js-cookie';
import { apiService } from '../service/apiservice';
import axios from 'axios';
import {
  GET_ORDER_NOTIFICATION,
  GET_ORDER_NOT_FAIL,
  GET_ORDER_NOT_SUCCESS,
} from '../constant/productConstant';
import {
  NOTIFICATION_LOADING,
  NOTIFICATION_ERROR,
  NOTIFICATION_SUCCESS,
} from '../constant/userConstant';
import { resolver } from '../service/network';
export const getnote = () => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  // console.log(id);
  request = axios.get(apiService.getnotification, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      console.log(error.message);
      throw error;
    });
};

export const getOrderNotification = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization:
        'Bearer ' + userInfo === undefined
          ? ''
          : userInfo.token === undefined
          ? ''
          : userInfo.token,
    },
  };
  try {
    dispatch({ type: GET_ORDER_NOTIFICATION });
    let { data } = await axios.get(apiService.getOrderNotification, config);
    // console.log(request.data);
    dispatch({ type: GET_ORDER_NOT_SUCCESS, payload: data });
    Cookies.set('notification', JSON.stringify(data));
  } catch (error) {
    dispatch({ type: GET_ORDER_NOT_FAIL, payload: error.message });
  }
};

export const updateOrderNotification = (id, admin) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  // console.log(id);
  request = axios.post(apiService.getnotification, { id, admin }, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      console.log(error.message);
      throw error;
    });
};

export const getNotification = (id) => async (dispatch, getState) => {
  const percent1 = (e) => {
    // uploader({ percent: e.percent });
  };

  dispatch({ type: NOTIFICATION_LOADING });
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;
  const resolveData = await resolver(
    `${apiService.adminNotification}?id=${id}`,
    'get',
    {},
    Token
    //percent1
  );
  // console.log(resolveData)
  // console.log(resolveData?.error?.response?.status)
  // console.log(resolveData?.response?.data?.data?.errorMessage)
  if (resolveData?.data?.data?.errorcode == '1') {
    // console.log('109')
    dispatch({
      type: NOTIFICATION_SUCCESS,
      payload: resolveData?.data?.data?.list,
    });
  } else if (resolveData?.data?.data?.errorCode == '500') {
    dispatch({
      type: NOTIFICATION_ERROR,
      payload: resolveData?.error?.response?.data?.errorMessage,
    });
  } else {
    dispatch({
      type: NOTIFICATION_ERROR,
      payload: resolveData?.error?.response?.statusText,
    });
  }
  return resolveData?.data?.data;
};

export const createNotification = (body) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;
  const resolveData = await resolver(
    `${apiService.createNotification}`,
    'post',
    body,
    Token
    //percent1
  );
  return resolveData?.data;
};

export const getBANotiicationCOunt = () => async (dispatch, getState) => {
  dispatch({ type: 'BA_NOTIFICATION_COUNT_LIST_PENDING' });
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.Notification_count_BA}`,
    'get',
    {},
    Token
  );
  if (resolveData?.error) {
    dispatch({
      type: 'BA_NOTIFICATION_COUNT_LIST_ERROR',
      error: resolveData?.error,
    });
  } else {
    dispatch({
      type: 'BA_NOTIFICATION_COUNT_LIST_SUCCESS',
      payload: resolveData?.data,
    });
  }
};

//get allbusiness admin notification
export const BA_NOtification = (body) => async (dispatch, getState) => {
  dispatch({ type: 'BA_NOTIFICATION_LIST_PENDING' });
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.Notification_BA}?page=${body.page}`,
    'get',
    {},
    Token
  );
  if (resolveData?.error) {
    dispatch({
      type: 'BA_NOTIFICATION_LIST_ERROR',
      error: resolveData?.error,
    });
  } else {
    dispatch({
      type: 'BA_NOTIFICATION_LIST_SUCCESS',
      payload: resolveData?.data,
    });
  }
};

//mark read notification
export const MarkRead = (body) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.Notification_BA}`,
    'put',
    { id: body },
    Token
  );

  if (resolveData?.error) {
    dispatch({
      type: 'BA_NOTIFICATION_LIST_ERROR',
      error: resolveData?.error,
    });
  } else {
    dispatch({
      type: 'BA_NOTIFICATION_LIST_MARK_READ',
      payload: { id: body, data: resolveData?.data },
    });
  }
};

//mark read all notification
export const MarkReadAll = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.Notification_BA}`,
    'post',
    {},
    Token
  );

  if (resolveData?.error) {
    dispatch({
      type: 'BA_NOTIFICATION_LIST_ERROR',
      error: resolveData?.error,
    });
  } else {
    dispatch({
      type: 'BA_NOTIFICATION_LIST_MARK__ALL_READ',
      payload: {},
    });
  }
};

export const getAllCreateNotification =
  (body) => async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;

    const resolveData = await resolver(
      `${apiService.notification_admin}?page=${body?.pageNumber}`,
      'get',
      {},
      Token
    );
    return resolveData?.data;
  };
