import {
  GET_ALL_CATEGORY_FAIL,
  GET_ALL_CATEGORY_REQ,
  GET_ALL_CATEGORY_SUCCESS,
  GET_ALL_MACHINES_REQ,
  GET_ALL_MACHINES_SUCCESS,
  GET_ALL_MACHINE_FAIL,
  GET_ALL_PART_FAIL,
  GET_ALL_PART_REQ,
  GET_ALL_PART_SUCCESS,
  setProductFail,
  setProductsuccess,
} from '../constant/productConstant';
import axios from 'axios';
import { apiService } from '../service/apiservice';
import { resolver } from '../service/network';
// import e from "cors";

export const getAllMachines = () => async (dispatch, getState) => {
  try {
    dispatch({ type: GET_ALL_MACHINES_REQ });
    const {
      userSigninInfo: { userInfo },
    } = getState();

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + userInfo.token,
      },
    };
    const { data } = await axios.get(apiService.getAllMAchines, config);
    dispatch({ type: GET_ALL_MACHINES_SUCCESS, payload: data });
  } catch (error) {
    dispatch({ type: GET_ALL_MACHINE_FAIL, payload: error.message });
  }
};

//tem iimage upload
export const imageUpload = (imgdata) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  const { data } = await axios.post(
    apiService.TempIMageUpload,
    imgdata,
    config
  );
  return data;
};

export const createMachine =
  ({ machineName }) =>
  (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + userInfo.token,
      },
    };
    let request;
    request = axios.post(apiService.createMachine, { machineName }, config);

    return request
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  };

//get all category
export const getAllCatory = () => async (dispatch, getState) => {
  try {
    dispatch({ type: GET_ALL_CATEGORY_REQ });
    const {
      userSigninInfo: { userInfo },
    } = getState();

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + userInfo.token,
      },
    };
    const { data } = await axios.get(apiService.getAllCategory, config);
    dispatch({ type: GET_ALL_CATEGORY_SUCCESS, payload: data });
  } catch (error) {
    dispatch({ type: GET_ALL_CATEGORY_FAIL, payload: error.message });
  }
};

//all parts with pagination

export const getAllMachinesInfo =
  ({ pageNumber, Keyword }) =>
  async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;

    const resolveData = await resolver(
      `${apiService.getAllMachinesInfo}?page=${pageNumber}&keyword=${Keyword}`,
      'get',
      '',
      Token
    );
    console.log(resolveData);
    return resolveData;
  };

//update parts
export const UpateParts = (state) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.createPart}`,
    'patch',
    state,
    Token,
    state
  );
  return resolveData;
};

export const AddProduct = (state) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;
  console.log('146',state);
  const resolveData = await resolver(
    `${apiService.createPart}`,
    'post',
    state,
    Token,
    state
  );
  return resolveData;
};

//add business admin

export const addBusinessAdmin =
  ({ name, email, contact, password }) =>
  async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + userInfo.token,
      },
    };
    let request;
    request = axios.post(
      apiService.addBusinessAccount,
      { name, email, password, contactNo: contact },
      config
    );

    return request
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  };

//update business Admin
export const updateBusinessAdmin =
  ({ _id, name, email, contact, password }) =>
  (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + userInfo.token,
      },
    };
    let request;

    request = axios.put(
      apiService.updateAdminAccount + _id,
      { name, email, password, contactNo: contact },
      config
    );

    return request
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  };

//Add userBy System Admin
export const AddUserByAdmin = (data) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  request = axios.post(apiService.addcustomer, data, config);

  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

export const getMachineById = (id) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request = axios.get(apiService.getMachinById + id, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

//get all parts

export const getAllParts = () => async (dispatch, getState) => {
  try {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + userInfo.token,
      },
    };
    dispatch({ type: GET_ALL_PART_REQ });

    const { data } = await axios.get(apiService.getParts, config);
    dispatch({ type: GET_ALL_PART_SUCCESS, payload: data });
  } catch (error) {
    dispatch({ type: GET_ALL_PART_FAIL, payload: error.message });
  }
};

//create parts

export const createParts = (state) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      // 'Content-Type': 'multipart/form-data',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  request = axios.post(apiService.createPart, { state }, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      console.log(error);
      throw error;
    });
};

export const createNewCategory =
  ({ category }) =>
  (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + userInfo.token,
      },
    };
    let request;
    request = axios.post(
      apiService.createCategory,
      {
        name: category.toUpperCase(),
      },
      config
    );
    return request
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  };

//delete machines part
export const DeletePart = (id) => (dipatch, getState) => {
  console.log(id);
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  request = axios.patch(apiService.deletePart, { id: id }, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

//delete equipments
export const DeleteEquipment = (id) => (dipatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  request = axios.delete(apiService.deleteEquipment + id, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

//UPDATE pARTS
export const blockParts =
  (
    upId,
    {
      machineName,
      partName,
      image,
      category,
      subcategory,
      description,
      amount,
      stock,
      equipment,
    },
    status
  ) =>
  (dispatch, getState) => {
    // console.log(image.length);
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + userInfo.token,
      },
    };
    let request;
    request = axios.put(
      apiService.blockParts,
      {
        id: upId,
        machineName,
        partName: partName,
        image: image.length > 6 ? image.split(',') : image,
        category: category,
        subcategory,
        description,
        amount: amount,
        inventoryStock: stock,
        active: status,
        equipment,
      },
      config
    );
    return request
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  };

// bLOCK parts parts

export const blOCKSParts = (id, status) => (dispatch, getState) => {
  console.log(id, status);
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  request = axios.put(
    apiService.blockParts,
    {
      id: id,

      active: status,
    },
    config
  );
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};
//update tages
export const updateTages =
  ({ id, tag }) =>
  (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + userInfo.token,
      },
    };
    let request;
    request = axios.put(
      apiService.blockParts,
      {
        id: id,

        tag: tag,
      },
      config
    );
    return request
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  };

//get Inventory
export const getInventory = (state) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  request = axios.get(
    apiService.getUpdateInventory + state,

    config
  );
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

//update Inventory
export const updateInventory = (state) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;
  const resolveData = await resolver(
    `${apiService.getUpdateInventory}${state.id}`,
    'post',
    {
      inventoryStock: state.allowQty,
    },
    Token
  );

  return resolveData;
};

//block inventory
export const blockInventory =
  (adminid, status) => async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;
    const resolveData = await resolver(
      `${apiService.blockParts}`,
      'put',
      {
        id: adminid,
        active: status,
      },
      Token
    );

    return resolveData;
  };

export const editParts = (machine) => async (dispatch, getState) => {
  try {
    dispatch({ type: setProductsuccess, payload: machine });
  } catch (error) {
    dispatch({ type: setProductFail, payload: error.message });
  }
};
//get all equipments
export const getAllEquipment = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  request = axios.get(apiService.getAllEquipments, config);

  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

//create new machine
export const newEquipment = (name) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  request = axios.post(
    apiService.getAllEquipments,
    { machinename: name },
    config
  );

  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

//update machine
export const updateEquipment =
  ({ nameQ, _id, status, isDeleted }) =>
  async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + userInfo.token,
      },
    };
    let request;
    request = axios.put(
      apiService.getAllEquipments,
      { machinename: nameQ, _id, status, isDeleted },
      config
    );
    console.log(nameQ, _id, status);
    return request
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  };

//update status

export const updateEquipmentStatus =
  ({ id, newStatus }) =>
  async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + userInfo.token,
      },
    };
    let request;
    request = axios.patch(
      apiService.getAllEquipments,
      { id, newStatus },
      config
    );
    return request
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        throw error;
      });
  };

export const machineModelArray = (state) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  request = axios.post(apiService.machineModel, { ids: state }, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

export const getProductDetailsById = (id) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.partDetailsByID}?id=${id}`,
    'get',
    '',
    Token
  );
  return resolveData;
};
