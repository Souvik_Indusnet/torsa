import axios from 'axios';
import { apiService } from '../service/apiservice';
import { resolver } from '../service/network';
export const getSubCategory = () => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  request = axios.get(apiService.getallSubcategory, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

export const deleteCategoryByID = (id) => (dispatch, getState) => {
  // console.log(id);
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  request = axios.delete(apiService.deletecategory + '/' + id, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

export const deleteSubCategoryByID = (id) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  // console.log(id);
  request = axios.delete(apiService.deletSubcategory + '/' + id, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      // console.log(error.message);
      throw error;
    });
};

export const deleteMachine = (id) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request;
  // console.log(id);
  request = axios.delete(apiService.deleteMachine + '/' + id, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      // console.log(error.message);
      throw error;
    });
};

//master setting create equipment
export const CreateEquipment = (data) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.getAllCategory}`,
    'post',
    data,
    Token
  );
  return resolveData;
};

//master setting get All Equipment with pagination
export const GetEquipment =
  ({ pageNumber }) =>
  async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;
    const resolveData = await resolver(
      `${apiService.paginateEquipment}?page=${pageNumber}&keyword=${undefined}`,
      'get',

      Token
    );

    return resolveData;
  };

// master setting get All Equipment without pagination

export const GetEquipmentAll = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.getAllCategory}`,
    'get',

    Token
  );

  return resolveData;
};

//master setting update Status Equipment
export const UpdateStatus = (id, status) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.getAllCategory}`,
    'put',
    { active: status ? status : 'false', id: id },
    Token
  );
  return resolveData;
};

//master setting update equipment
export const UpdateEquipment = (body) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;
  // console.log({ category, editId });
  const resolveData = await resolver(
    `${apiService.editCategory}`,
    'put',
    body,
    Token
  );
  return resolveData;
};

//master setting create equipment modal
export const CreateEquipmentmodal = (data) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.eqquipmentModal}`,
    'post',
    { name: data.name, category: data.equipment },
    Token
  );
  return resolveData;
};

//master setting get All Equipment modal
export const GetEquipmentmodal =
  ({ pageNumber }) =>
  async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;

    const resolveData = await resolver(
      `${apiService.paginateEquipmentmodal}?page=${pageNumber}&keyword=${undefined}`,
      'get',

      Token
    );
    // console.log(resolveData);
    return resolveData;
  };

//master setting update Status Equipment modal
export const UpdateStatusmodal = (id, status) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.eqquipmentModal}`,
    'put',
    { active: status ? status : 'false', id: id },
    Token
  );
  return resolveData;
};

//master setting update equipment modal
export const updateequipmentmodel =
  (id, name, category) => async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;
    // console.log({ category, editId });
    const resolveData = await resolver(
      `${apiService.eqquipmentModal}`,
      'put',
      { id, name, category },
      Token
    );
    return resolveData;
  };
