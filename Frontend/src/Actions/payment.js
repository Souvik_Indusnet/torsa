import { apiService } from '../service/apiservice';
import { resolver } from '../service/network';

export const getPayment =
  ({ pageNumber, Keyword }) =>
  async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;

    const resolveData = await resolver(
      `${apiService.payment}?page=${pageNumber}&&keyword=${Keyword}`,
      'get',
      '',

      Token
    );

    return resolveData?.data?.data;
  };
