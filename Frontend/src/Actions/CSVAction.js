import { apiService } from '../service/apiservice';
import { resolver } from '../service/network';

export const UserCSV = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(`${apiService.userCsv}`, 'get',{}, Token);

  return resolveData;
};

export const Productcsv = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.productcsv}`,
    'get',
    '',
    Token
  );

  return resolveData;
};

export const BACsv = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(`${apiService.BACsv}`, 'get', '', Token);

  return resolveData;
};

export const Enquirycsv = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.enquirycsv}`,
    'get',
    '',
    Token
  );

  return resolveData;
};

export const PaymentCsv = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.paymentCsv}`,
    'get',
    '',
    Token
  );

  return resolveData;
};

export const Ordercsv = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.ordercsv}`,
    'get',
    '',
    Token
  );
//console.log(resolveData);
  return resolveData;
};
export const BusinsessOrdercsv = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.businessordercsv}`,
    'get',
    '',
    Token
  );
//console.log(resolveData);
  return resolveData;
};

export const CmsCsv = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(`${apiService.cmsCsv}`, 'get', '', Token);

  return resolveData;
};

export const Equipmentcsv = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.equipmentcsv}`,
    'get',
    '',
    Token
  );

  return resolveData;
};

export const Equipment_modalcsv = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.equipment_modalcsv}`,
    'get',
    '',
    Token
  );

  return resolveData;
};
