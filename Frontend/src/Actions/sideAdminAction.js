import axios from "axios";
import { apiService } from "../service/apiservice";

export const getUserByThisAdmin = (id) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userInfo.token,
    },
  };
  let request;
  // console.log(id);
  request = axios.post(apiService.getUserByAdminID, { id }, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      console.log(error.message);
      throw error;
    });
};

export const AdminProfile = (id) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userInfo.token,
    },
  };
  let request;
  // console.log(id);
  request = axios.post(apiService.getAdminProfileInfo, { id }, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      console.log(error.message);
      throw error;
    });
};

export const getMachineInfo = (array) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userInfo.token,
    },
  };
  let request;
  // console.log(id);
  request = axios.post(apiService.getmachineByarray, { list: array }, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      console.log(error.message);
      throw error;
    });
};

export const GraphTable = (id) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userInfo.token,
    },
  };
  let request;
  // console.log(id);
  request = axios.post(apiService.businessAdminGraph, { id }, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      console.log(error.message);
      throw error;
    });
};
