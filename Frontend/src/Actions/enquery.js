import { apiService } from '../service/apiservice';
import { resolver } from '../service/network';

export const getEnquery = ({pageNumber}) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const percent1 = (e) => {
    // uploader({ percent: e.percent });
  };
  const resolveData = await resolver(
    `${apiService.enquery}?page=${pageNumber}`,
    'get',
    '',
   
    Token
  );

  return resolveData;
};

export const updateEnquery = (data) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const percent1 = (e) => {
    // uploader({ percent: e.percent });
  };

  const resolveData = await resolver(
    `${apiService.enquery}?_id=${data.id}`,
    'put',
    data,

    Token
  );
  return resolveData?.data?.data;
};
