import { apiService } from '../service/apiservice';
import axios from 'axios';
import { resolver } from '../service/network';
import {
        BUSINESS_ADMIN_LOADING,
        BUSINESS_ADMIN_ERROR,
        BUSINESS_ADMIN_SUCCESS,
    } from "../constant/userConstant";
export const getAllOrder =
  ({ pageNumber, Keyword }) =>
  async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;

    const resolveData = await resolver(
      `${apiService.getAllorders}?page=${pageNumber}&keyword=${Keyword}`,
      'get',
      {},
      Token
    );

    return resolveData;
  };

export const getAllOrderBySubAdmin =
  ({ pageNumber, Keyword }) =>
  async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    dispatch({type:BUSINESS_ADMIN_LOADING})
    const Token = userInfo.token;
    console.log('Keyword',Keyword)
    const resolveData = await resolver(
      `${apiService.getAllordersByBusinessAdmin}?page=${pageNumber}&keyword=${Keyword||''}`,
      'get',
      {},
      Token
    );
    // console.log(resolveData)
    // console.log()
    if(resolveData?.data?.data?.errorCode=='200'){
      dispatch({
        type:BUSINESS_ADMIN_SUCCESS,
        payload:resolveData?.data?.data?.list
      })
    }else if(resolveData?.data?.data?.errorCode=='500'){
      dispatch({
        type: BUSINESS_ADMIN_ERROR,
        payload: resolveData?.error?.response?.data?.errorMessage,
      });
    }else{
      dispatch({
        type: BUSINESS_ADMIN_ERROR,
        payload: resolveData?.error?.response?.statusText,
      });
    }
//    return resolveData;
  };
export const orderStatusChange = (id, status) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.OrderDelStatus}/${id}`,
    'put',
    { active: status },
    Token
  );

  return resolveData;
};

export const orderDetailsById = (id) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.orderDetailsByID}?id=${id}`,
    'get',
    '',
    Token
  );

  return resolveData;
};

//graph data
export const getOrder = () => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request = axios.get(apiService.getGraph, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      console.log(error.message);
      throw error;
    });
};

export const UpdateDelStatus = (id, status) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request = axios.patch(apiService.getAllorders, { id, status }, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      console.log(error.message);
      throw error;
    });
};

//get order by id
export const getOrderById = (id) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request = axios.get(apiService.getAllorders + 'getbyid/' + id, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      console.log(error.message);
      throw error;
    });
};

export const getOrderUser = (id) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request = axios.post(apiService.getOrderUser, { id }, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      console.log(error.message);
      throw error;
    });
};
