import axios from "axios";
import Cookies from "js-cookie";
import { USER_SIGNIN_SUCCESS } from "../constant/productConstant";

import { apiService } from "../service/apiservice";
export const productimageupload = (image) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: "Bearer " + userInfo.token,
    },
  };
  // console.log(image);
  let request;
  request = axios.post(apiService.uploadpartImage, image);
  return request
    .then((result) => {
      console.log(result.data);
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

export const imageUpload = (image) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();

  const config = {
    headers: {
      "content-type": "multipart/form-data",

      Authorization: "Bearer " + userInfo.token,
    },
  };
  // console.log(image);

  const { data } = await axios.post(apiService.uploadAdminImage, image, config);
  console.log(data);
  dispatch({ type: USER_SIGNIN_SUCCESS, payload: data });
  Cookies.set("UserInfo", JSON.stringify(data));
};
