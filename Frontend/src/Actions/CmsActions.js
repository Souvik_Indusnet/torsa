import { apiService } from '../service/apiservice';
import { resolver } from '../service/network';

//all  cms with pagination
export const GetAllCMSpages =
  ({ pageNumber }) =>
  async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;

    const resolveData = await resolver(
      `${apiService.getAllPages}?page=${pageNumber}&keyword=${undefined}`,
      'get',
      '',
      Token
    );

    return resolveData;
  };

//update cms status
export const GetAllCMSpagesStatus = (data) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.updateStatusCMS}`,
    'put',
    data,
    Token
  );

  return resolveData;
};

//get all pages
export const fetchAllPageName = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `https://torsa.herokuapp.com/cms/all-page-name`,
    'get',
    '',
    Token
  );

  return resolveData;
};

//create cms
export const createCMS = (data) => async (dispatch, getState) => {
  ////////////////////
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.createcms}`,
    'post',
    data,
    Token
  );

  return resolveData;
};

//update cms
export const UpdateCMS = (data) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.updateCMS}`,
    'put',
    data,
    Token
  );

  return resolveData;
};

// export const updateOne = async (data) => {
//   console.log(data);
//   const url = `https://torsa.herokuapp.com/cms/edit-page`;
//   return await axios({
//     method: "put",
//     url,
//     data,
//   });
// };
