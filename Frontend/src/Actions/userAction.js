import Cookies from 'js-cookie';
import {
  USER_LOGOUT,
  USER_REGISTER_FAIL,
  USER_REGISTER_REQ,
  USER_REGISTER_SUCCESS,
  USER_SIGNIN_FAIL,
  USER_SIGNIN_REQ,
  USER_SIGNIN_SUCCESS,
  GET_BA_DASHBOARD_SUCCESS,
  GET_BA_DASHBOARD_LOADING,
  GET_BA_DASHBOARD_FAIL,
  GET_ADMIN_DASHBOARD_LOADING,
  GET_ADMIN_DASHBOARD_SUCCESS,
  GET_ADMIN_DASHBOARD_FAIL
} from '../constant/productConstant';
import axios from 'axios';
import { apiService } from '../service/apiservice';
import {
  ADMIN_GET_FAIL,
  ADMIN_GET_REQ,
  ADMIN_GET_SUCCESS,
} from '../constant/userConstant';
import { resolver } from '../service/network';

export const userRegister =
  ({ username, email, password }) =>
  async (dispatch) => {
    try {
      dispatch({ type: USER_REGISTER_REQ });
      const data = await axios.post('/users/signin', {
        name: username,
        email,
        password,
      });
      dispatch({ type: USER_REGISTER_SUCCESS, payload: data });
    } catch (error) {
      dispatch({ type: USER_REGISTER_FAIL, payload: error.message });
    }
  };

//signin actions
export const userSignin =
  ({ username, password }) =>
  async (dispatch) => {
    try {
      dispatch({ type: USER_SIGNIN_REQ });
      const config = {
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const { data } = await axios.post(
        apiService.login,
        {
          email: username,
          password,
        },
        config
      );
      // console.log(data);
      if (data.isSystemAdmin === 'undefined') {
        dispatch({ type: USER_SIGNIN_FAIL, payload: 'Invalid-Credentials' });
      }
      if (data.active === false) {
        dispatch({
          type: USER_SIGNIN_FAIL,
          payload: 'Sorry!  Your are Blocked',
        });
      }
      if (data.active === true) {
        dispatch({ type: USER_SIGNIN_SUCCESS, payload: data });
        Cookies.set('UserInfo', JSON.stringify(data));
      }
    } catch (error) {
      dispatch({
        type: USER_SIGNIN_FAIL,
        payload: error?.response?.data.errorMessage,
      });
    }
  };

export const logout = () => (dispatch) => {
  Cookies.remove('UserInfo');
  dispatch({ type: USER_LOGOUT });
};

// forgote password send action
export function forgotPassword(email) {
  let request = axios.post('');
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
}

//get all user

export const userListData = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request = axios.get(apiService.getAlluser, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

export const deleteUser = (id) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request = axios.delete(apiService.deleteUser + id, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

export const deleteAdmin = (id) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request = axios.delete(apiService.deleteAdmin + id, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

//block user

export const blockStatusAdmin = (id, status) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.userStatus}/${id}`,
    'put',
    { active: status },
    Token
  );

  return resolveData;
};

export const blockStatusAdminB = (id, status) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;
  console.log(`${apiService.updateStatus}/${id}`, id, status);
  const resolveData = await resolver(
    `${apiService.updateStatus}/${id._id}`,
    'put',
    { active: `${status}` },
    Token
  );

  return resolveData;
};
export const getlistReport = () => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request = axios.get(apiService.report, config);
  return request
    .then((result) => {
      return result.data.admin;
    })
    .catch((error) => {
      console.log(error.message);
      throw error;
    });
};
export const getlistReportGraph = (year) => (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request = axios.get(`${apiService.reportGraph}?year=${year||2021}`, config);
  return request
    .then((result) => {
      return result.data.admin;
    })
    .catch((error) => {
      console.log(error.message);
      throw error;
    });
};
//getAdminUser by Id

export const getuserById = (id) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;
  const resolveData = await resolver(
    `${apiService.getUserById}` + id,
    'get',
    ' ',
    Token
  );
  // console.log(resolveData);
  return resolveData;

  // const {
  //   userSigninInfo: { userInfo },
  // } = getState();
  // const config = {
  //   headers: {
  //     'Content-Type': 'application/json',
  //     Authorization: 'Bearer ' + userInfo.token,
  //   },
  // };
  // let request = axios.get(apiService.getUserById + id, config);
  // return request
  //   .then((result) => {
  //     return result.data;
  //   })
  //   .catch((error) => {
  //     throw error;
  //   });
};

//get Manager by id
export const getManagerById = (id) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  let request = axios.get(apiService.getManagerById + id, config);
  return request
    .then((result) => {
      return result.data;
    })
    .catch((error) => {
      throw error;
    });
};

//dashbaord action
export const getlistDashboard = () => async(dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  dispatch({type:GET_ADMIN_DASHBOARD_LOADING})
  console.log('....',userInfo.token);
  let request =await axios.get(apiService.dashboard, config);
  console.log('305',request);
  if (request?.data && request?.data?.errorCode == "200") {
    var data=request?.data;
    delete data.errorCode;
    delete data.errorMessage;
    dispatch({
      type: GET_ADMIN_DASHBOARD_SUCCESS,
      payload: request?.data,
    });
  } else if (request?.error?.response?.data?.errorCode == "500") {
    dispatch({
      type: GET_ADMIN_DASHBOARD_FAIL,
      payload: request?.error?.response?.data?.errorMessage,
    });
  } else {
    dispatch({
      type: GET_ADMIN_DASHBOARD_FAIL,
      payload: request?.error?.response?.statusText,
    });
  }  
  return request.data
    // .then((result) => {
    //   return result.data;
    // })
    // .catch((error) => {
    //   console.log(error.message);
    //   throw error;
    // });
};
export const getlistBADashboard = (year) =>async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  dispatch({type:GET_BA_DASHBOARD_LOADING})
  let request =await axios.get(`${apiService.badashboard}?year=${year}`, config);
  //console.log(request?.data)
  if (request?.data && request?.data?.errorCode == "200") {
    var data=request?.data;
    delete data.errorCode;
    delete data.errorMessage;
    dispatch({
      type: GET_BA_DASHBOARD_SUCCESS,
      payload: request,
    });
  } else if (request?.error?.response?.data?.errorCode == "500") {
    dispatch({
      type: GET_BA_DASHBOARD_FAIL,
      payload: request?.error?.response?.data?.errorMessage,
    });
  } else {
    dispatch({
      type: GET_BA_DASHBOARD_FAIL,
      payload: request?.error?.response?.statusText,
    });
  }
};

///////////////////////////////////////

//---------------BusinessAdmin Action----------------

/////////////////////////////////////

//status action
export const openStatus =
  ({ status }) =>
  (dispatch, getState) => {
    dispatch({ type: 'STATUS_GET_UPDATE', payload: status });
    Cookies.set('Bprofile', JSON.stringify(status ? 'true' : 'false'));
  };

//getAdmin by id

export const getAdminById = (id) => async (dispatch, getState) => {
  // console.log(id);
  dispatch({ type: ADMIN_GET_REQ });
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userInfo.token,
    },
  };
  try {
    const { data } = await axios.post(apiService.getAdminByEachId + id, config);
    dispatch({ type: ADMIN_GET_SUCCESS, payload: data });
  } catch (error) {
    dispatch({ type: ADMIN_GET_FAIL, err: error.message });
  }
};

//update admin by id
export const updateForm =
  ({ _id, name, password, email, contact }) =>
  (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + userInfo.token,
      },
    };
    let request = axios.put(
      apiService.updateAdminByEachId,
      { _id, name, email, contact, password },
      config
    );
    return request
      .then((result) => {
        return result.data;
      })
      .catch((error) => {
        console.log(error.message);
        throw error;
      });
  };

//all user with pagination
export const GetALlUsers =
  ({ pageNumber, Keyword }) =>
  async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;
    // console.log(pageNumber);
    // console.log(Token);
    const resolveData = await resolver(
      `${apiService.userpagination}?page=${pageNumber}&keyword=${Keyword}`,
      'get',
      '',
      Token
    );
console.log(resolveData);
    return resolveData;
  };

//all business admin with pagination
export const GetAllBusinessAdmin =
  ({ pageNumber, Keyword }) =>
  async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;

    const resolveData = await resolver(
      `${apiService.businessAdminPagination}?page=${pageNumber}&keyword=${Keyword}`,
      'get',
      '',
      Token
    );

    return resolveData;
  };

//get all Admins
export const AdminList = () => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;

  const resolveData = await resolver(
    `${apiService.getAdminUser}`,
    'get',
    '',
    Token
  );

  return resolveData?.data?.data;
};

// business admin add
export const addBusinessAdmins = (formdata) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;
  const resolveData = await resolver(
    `${apiService.addBusinessAccount}`,
    'post',
    formdata,
    Token
  );

  return resolveData;
};

export const updateBusinessAdminsAndState =
  (updateData, _id) => async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;
    const resolveData = await resolver(
      `${apiService.updateAdminAccount}${_id}`,
      'put',
      updateData,
      Token
    );
    // Cookies.remove('UserInfo');

    if (resolveData?.data) {
      dispatch({ type: USER_SIGNIN_SUCCESS, payload: resolveData?.data?.data });

      Cookies.set('UserInfo', JSON.stringify(resolveData?.data?.data));
    }
    return resolveData;
  };

//business admin update
export const updateBusinessAdmins =
  (data, _id) => async (dispatch, getState) => {
    const {
      userSigninInfo: { userInfo },
    } = getState();
    const Token = userInfo.token;
    const resolveData = await resolver(
      `${apiService.updateAdminAccount}${_id}`,
      'put',
      data,
      Token
    );

    return resolveData;
  };

//user management
export const AddnewUser = (data) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;
  const resolveData = await resolver(
    `${apiService.addcustomer}`,
    'post',
    data,
    Token
  );

  return resolveData;
};

//update user
export const UpdateuserByid = (data) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;
  const resolveData = await resolver(
    `${apiService.updateuser}${data._id}`,
    'put',
    data,
    Token
  );

  return resolveData;
};

//get UserDetails by Id

export const getUserDetailsByID = (id) => async (dispatch, getState) => {
  const {
    userSigninInfo: { userInfo },
  } = getState();
  const Token = userInfo.token;
  const resolveData = await resolver(
    `${apiService.getUserDetailsById}?id=${id}`,
    'get',
    '',
    Token
  );
console.log(resolveData)
  return resolveData;
};
