import React from "react";
import { Redirect } from "react-router-dom";
import Preloaders from "../Components/Dashboard/Preloaders";

const AuthWrapped = ({ children, condition, path, loading }) => {
  return loading ? (
    <Preloaders />
  ) : condition ? (
    <>{children}</>
  ) : (
    <Redirect to={path} />
  );
};

export default AuthWrapped;
